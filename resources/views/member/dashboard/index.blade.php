@extends('layouts.member-app')
<style>
    </style>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>
        .col-in {
            padding: 0 20px !important;

        }

        .fc-event{
            font-size: 10px !important;
        }

        .m-checkbox {
            display: block;
            position: relative;
            padding-left: 30px;
            margin-bottom: 10px;
            cursor: pointer;
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
        }

    </style>
@endpush

@section('content')



    <div class="row">
        @if(\App\ModuleSetting::checkModule('tickets'))
            @if($viewDashboardTickets == "true")
            <div class="col-md-4 col-sm-6">
                <div class="white-box">
                    <h3 class="box-title">Tickets (Total = {{$userTicketsCount}})</h3>
                    <div id="dashboardTicketsCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                </div>
            </div>
            @endif
        @endif
        @if(\App\ModuleSetting::checkModule('tasks'))
            @if($viewDashboardTasks == "true")
                <div class="col-md-4 col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Task (Total = {{$userTaskCount}})</h3>
                        <div id="dashboardTaskCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                    </div>
                </div>
            @endif
        @endif
        @if(\App\ModuleSetting::checkModule('Dashboard Guards'))
            @if($viewDashboardGuards == "true")
                <div class="col-md-4 col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Guards (Total = {{$guardsCount}})</h3>
                        <div id="dashboardGuardsCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                    </div>
                </div>
            @endif
        @endif

        @if(\App\ModuleSetting::checkModule('Dashboard Assignments'))
            @if($viewDashboardAssignments == "true")
                <div class="col-md-4 col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Assignment (Total = {{$userTaskCount}})</h3>
                        <div id="dashboardAssignmentCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                    </div>
                </div>
            @endif
        @endif
        @if(\App\ModuleSetting::checkModule('Dashboard Salaries'))
            @if($viewDashboardSalaries == "true")
                <div class="col-md-4 col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Salaries</h3>
                        <div id="dashboardSalaryCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                    </div>
                </div>
            @endif
        @endif
        @if(\App\ModuleSetting::checkModule('Dashboard Store'))
            @if($viewDashboardStore == "true")
                <div class="col-md-4 col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Store</h3>
                        <div id="dashboardStoreCanvas" style="height: 200px; max-width: 320px; margin: 0px auto;"></div>
                    </div>
                </div>
            @endif
        @endif
    </div>
    <div class="row">
        @if(\App\ModuleSetting::checkModule('projects'))
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="col-in row">
                    <h3 class="box-title">@lang('modules.dashboard.totalProjects')</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-layers text-info"></i></li>
                        <li class="text-right"><span class="counter">{{ $totalProjects }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        @if(\App\ModuleSetting::checkModule('timelogs'))
        <div class="col-md-3 col-sm-6">
            <div class="white-box" style="padding-bottom: 32px">
                <div class="col-in row">
                    <h3 class="box-title">@lang('modules.dashboard.totalHoursLogged')</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-clock text-warning"></i></li>
                        <li class="text-right">{{ $counts->totalHoursLogged }}</li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

        @if(\App\ModuleSetting::checkModule('tasks'))
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="col-in row">
                    <h3 class="box-title">@lang('modules.dashboard.totalPendingTasks')</h3>
                    <ul class="list-inline two-part">
                        <li><i class="ti-alert text-danger"></i></li>
                        <li class="text-right"><span class="counter">{{ $counts->totalPendingTasks }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="col-in row">
                    <h3 class="box-title">@lang('modules.dashboard.totalCompletedTasks')</h3>
                    <ul class="list-inline two-part">
                        <li><i class="ti-check-box text-success"></i></li>
                        <li class="text-right"><span class="counter">{{ $counts->totalCompletedTasks }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif

    </div>
    <!-- .row -->

    <div class="row">

        @if(\App\ModuleSetting::checkModule('attendance'))
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.menu.attendance')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="col-xs-6">
                            <h3>@lang('modules.attendance.clock_in')</h3>
                        </div>
                        <div class="col-xs-6">
                            <h3>@lang('modules.attendance.clock_in') IP</h3>
                        </div>
                        <div class="col-xs-6">
                            @if(is_null($todayAttendance))
                                {{ \Carbon\Carbon::now()->timezone($global->timezone)->format('h:i A') }}
                            @else
                                {{ $todayAttendance->clock_in_time->timezone($global->timezone)->format('h:i A') }}
                            @endif
                        </div>
                        <div class="col-xs-6">
                            {{ $todayAttendance->clock_in_ip or request()->ip() }}
                        </div>

                        @if(!is_null($todayAttendance) && !is_null($todayAttendance->clock_out_time))
                            <div class="col-xs-6 m-t-20">
                                <label for="">@lang('modules.attendance.clock_out')</label>
                                <br>{{ $todayAttendance->clock_out_time->timezone($global->timezone)->format('h:i A') }}
                            </div>
                            <div class="col-xs-6 m-t-20">
                                <label for="">@lang('modules.attendance.clock_out') IP</label>
                                <br>{{ $todayAttendance->clock_out_ip }}
                            </div>
                        @endif

                        <div class="col-xs-12 m-t-20">
                            <label for="">@lang('modules.attendance.working_from')</label>
                            @if(is_null($todayAttendance))
                                <input type="text" class="form-control" id="working_from" name="working_from">
                            @else
                                <br> {{ $todayAttendance->working_from }}
                            @endif
                        </div>

                        <div class="col-xs-6 m-t-20">
                            @if(is_null($todayAttendance))
                                <button class="btn btn-success btn-sm" id="clock-in">@lang('modules.attendance.clock_in')</button>
                            @endif
                            @if(!is_null($todayAttendance) && is_null($todayAttendance->clock_out_time))
                                <button class="btn btn-danger btn-sm" id="clock-out">@lang('modules.attendance.clock_out')</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(\App\ModuleSetting::checkModule('tasks'))
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.overdueTasks')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <ul class="list-task list-group" data-role="tasklist">
                            <li class="list-group-item" data-role="task">
                                <strong>@lang('app.title')</strong> <span
                                        class="pull-right"><strong>@lang('app.dueDate')</strong></span>
                            </li>
                            @forelse($pendingTasks as $key=>$task)
                                <li class="list-group-item" data-role="task">
                                    {{ ($key+1).'. '.ucfirst($task->heading) }}
                                    @if(!is_null($task->project_id))
                                        <a href="{{ route('member.projects.show', $task->project_id) }}" class="text-danger">{{ ucwords($task->project->project_name) }}</a>
                                    @endif
                                    <label class="label label-danger pull-right">{{ $task->due_date->format('d M') }}</label>
                                </li>
                            @empty
                                <li class="list-group-item" data-role="task">
                                    @lang('messages.noOpenTasks')
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

    <div class="row" >

        @if(\App\ModuleSetting::checkModule('projects'))
        <div class="col-md-6" id="project-timeline">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.projectActivityTimeline')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            @foreach($projectActivities as $activity)
                                <div class="sl-item">
                                    <div class="sl-left"><i class="fa fa-circle text-info"></i>
                                    </div>
                                    <div class="sl-right">
                                        <div><h6><a href="{{ route('member.projects.show', $activity->project_id) }}" class="text-danger">{{ ucwords($activity->project_name) }}:</a> {{ $activity->activity }}</h6> <span class="sl-date">{{ $activity->created_at->timezone($global->timezone)->diffForHumans() }}</span></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(\App\ModuleSetting::checkModule('employees'))
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('modules.dashboard.userActivityTimeline')</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="steamline">
                            @forelse($userActivities as $key=>$activity)
                                <div class="sl-item">
                                    <div class="sl-left">
                                        {!!  ($activity->user->image) ? '<img src="'.$activity->user->employeeImage.'"
                                                                    alt="user" class="img-circle">' : '<img src="'.$activity->user->employeeImage.'"
                                                                    alt="user" class="img-circle">' !!}
                                    </div>
                                    <div class="sl-right">
                                        <div class="m-l-40"><a href="{{ route('member.employees.show', $activity->user_id) }}" class="text-success">{{ ucwords($activity->user->name) }}</a> <span  class="sl-date">{{ $activity->created_at->timezone($global->timezone)->diffForHumans() }}</span>
                                            <p>{!! ucfirst($activity->activity) !!}</p>
                                        </div>
                                    </div>
                                </div>
                                @if(count($userActivities) > ($key+1))
                                    <hr>
                                @endif
                            @empty
                                <div>@lang('messages.noActivityByThisUser')</div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif



    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="color: red ;">Security Organizing System (SOS) Pakistan (Pvt.) Limited --- Digital declaration</h4>
                </div>
                <div class="modal-body">
                    <b>I declare that:</b>
                    <br>
                    <ol type="i">
                        <li>I will abide by the applicable laws, rules and regulations of SOS Pakistan (Pvt.) Limited and all relevant policies that may be found under policy section at http://sosweb/policies/index.html.</li>
                        <li>I will not disclose or make use of confidential information, during or after my employment at SOS Pakistan (Pvt.) Limited.</li>
                        <li>I will not engage in any external or private activities which will result in a conflict or potential conflict of interest with any of my duties in SOS Pakistan (Pvt.) Limited.</li>
                        <li>I will pay due respect to all my seniors, colleagues, and juniors without any gender  discrimination and abide by the workplace harassment policy.</li>
                        <li>I will perform my duties with honesty, dedication and utmost level of my knowledge and skills.</li>
                        <li>I am ready to undergo post-employment antecedents checks and give my consent to screening and checks periodically at the discretion of SOS Pakistan (Pvt.) Limited.</li>
                        <li>I have never been investigated in respect of any suspected criminal offences, arrested, reported for, or pleaded guilty to or been found guilty of any criminal offences.</li>
                        <li>The information that I have provided in relation to my application for employment is true and correct in every detail.  I understand that any incorrect statement in my application to my employment in SOS Pakistan (Pvt.) Limited, including (but not restricted to) my qualifications, experience, ability, physical or mental health or professional and personal integrity, may make me liable to disciplinary action which may include termination of employment.</li>
                        <li>I understand that unauthorized or improper use of IT resource may result in administrative disciplinary action and civil and criminal penalties.</li>
                        <li>I understand that unauthorized or improper use of IT resource may result in administrative disciplinary action and civil and criminal penalties.</li>
                    </ol>
                    <p style="color: red">By continuing to use this system you indicate your awareness of and consent to these terms and conditions of use.<br>
                        YOU WILL LOG OFF immediately if you do not agree to the conditions stated in this declaration.
                    </p>
                    {!! Form::open(array('route' => 'member.dashboard.store')) !!}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="checkbox checkbox-info">
                                    <input id="is_accepted" name="is_accepted" value="true" type="checkbox" >
                                    <label for="is_accepted"> I Agree {{--@lang('modules.client.generateRandomPassword')--}}</label>
                                </div>
                            </div>
                        <!--/span-->
                        <div class="col-md-9">
                            <div class="form-group">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.proceed')</button>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>


@endsection

@push('footer-script')

<script src="{{asset('canvasjs/canvasjs.min.js')}}"></script>
<script>
    /**
     *Ticket stats
     */
    var viewDashboardTickets= '{{$viewDashboardTickets}}';
    var openTicket          = '{{$openTicketsCount}}';
    var pendingTicket       = '{{$pendingTicketsCount}}';
    var resolvedTicket      = '{{$resolvedTicketsCount}}';
    var closedTicket        = '{{$closedTicketsCount}}';
    var userTicketsCount    = '{{$userTicketsCount}}';

    /**
     *Task stats
     */
    var viewDashboardTasks          = '{{$viewDashboardTasks}}';
    var userTaskCount               = '{{$userTaskCount}}';
    var completedTaskCount          = '{{$completedTaskCount}}';
    var incompleteTaskCount         = '{{$incompleteTaskCount}}';
    var pendingTaskCount            = '{{$pendingTaskCount}}';

    /**
     *Guard stats
     */
    var viewDashboardGuards        = '{{$viewDashboardGuards}}';
    var guardsCount                 = '{{$guardsCount}}';
    var newGuardsCount              = '{{$newGuardsCount}}';
    var terminatedGuardsCount       = '{{$terminatedGuardsCount}}';

    /**
     *Assignment stats
     */
    var viewDashboardAssignments    = '{{$viewDashboardAssignments}}';
    var userTotalProjectsCount      = parseInt('{{$userTotalProjectsCount}}');
    var completedProjectsCount      = parseInt('{{$completedProjectsCount}}');
    var inProcessProjectsCount      = parseInt('{{$inProcessProjectsCount}}');
    var overdueProjectsCount        = parseInt('{{$overdueProjectsCount}}');

    /**
     *Salaries stats
     */
    var viewDashboardSalaries       = '{{$viewDashboardSalaries}}';
    var salaryPayable               = parseInt('{{$salaryPayable}}');
    var salaryDeductions            = parseInt('{{$salaryDeductions}}');
    var salaryPending               = parseInt('{{$salaryPending}}');
    var salaryOnHold                = parseInt('{{$salaryOnHold}}');

    /**
     *Store stats
     */
    var viewDashboardStore          = '{{$viewDashboardStore}}';
    var storeTotalItems             = parseInt('{{$storeTotalItems}}');
    var storeNewIns                 = parseInt('{{$storeNewIns}}');
    var storeNewOuts                = parseInt('{{$storeNewOuts}}');
    var storeTotalInStock           = parseInt('{{$storeTotalInStock}}');
    var storeTotalDeployed          = parseInt('{{$storeTotalDeployed}}');


</script>
<script src="{{asset('canvasjs/DashboardCanvas.js')}}"></script>

<script>
    $(window).load(function(){
        var is_accepted = "<?php echo $is_accepted; ?>";
        if(is_accepted == 0){
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            })
        }
    });
    $('#clock-in').click(function () {
        var workingFrom = $('#working_from').val();

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            url: '{{route('member.attendances.store')}}',
            type: "POST",
            data: {
                working_from: workingFrom,
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    })

    @if(!is_null($todayAttendance))
    $('#clock-out').click(function () {

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            url: '{{route('member.attendances.update', $todayAttendance->id)}}',
            type: "PUT",
            data: {
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    })
    @endif

</script>
@endpush