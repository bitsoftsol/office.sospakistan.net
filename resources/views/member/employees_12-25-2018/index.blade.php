@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="white-box bg-inverse">
                <h3 class="box-title text-white">@lang('modules.dashboard.totalEmployees')</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-user text-white"></i></li>
                    <li class="text-right"><span id="totalWorkingDays" class="counter text-white">{{ $totalEmployees }}</span></li>
                </ul>
            </div>
        </div>
        <div class="white-box col-md-12">'
            <div class="row m-b-10">
                {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Center*{{--@lang('modules.employees.jobTitle')--}}</label>
                        <select name="center_id" id="center_id" class="form-control" required>
                            @if($centers->count() > 0)
                                <option value=""> Select Center </option>
                                @foreach($centers as $centersDataIn)
                                    <option value="{{$centersDataIn->odoo_id}}" name="odoo_center_id">{{$centersDataIn->name}}</option>
                                @endforeach
                            @else
                                <option value="0">No Center Data Import From ERP</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Division</label>
                        <select name="divisions" id="segment_id" class="form-control divisions">
                            @if(count($divisions) > 0)
                                <option value="">Select Division</option>
                                @foreach($divisions as $division)
                                    <option value="{{$division->id}}">{{$division->po_level_name}}</option>
                                @endforeach
                            @else
                                <option value="0">No Division Found</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Department</label>
                        <select name="departments" id="department_id" class="form-control departments">
                            @if($departments->count() > 0)
                                <option value=""> Select Department</option>
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            @else
                                <option value="0">Department Not Found</option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i>  Filter
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">

                    @if($user->can('add_employees'))
                        <div class="col-sm-1">

                        </div>
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-11 text-right hidden-xs">
                            <div class="form-group">
                                {{--<a href="{{ route('admin.employees.importOdoo') }}" class="btn btn-info btn-sm"><i class="ti-import" aria-hidden="true"></i> Import Employees</a>--}}
                                {{--<a href="{{ route('admin.employees.importCenters') }}" class="btn btn-info btn-sm"><i class="ti-import" aria-hidden="true"></i> Import Centers</a>--}}
                                {{--<a href="{{ route('admin.employees.importSegments') }}" class="btn btn-info btn-sm"><i class="ti-import" aria-hidden="true"></i> Import Segments</a>--}}
                                {{--<a href="{{ route('admin.employees.importSubSegments') }}" class="btn btn-info btn-sm"><i class="ti-import" aria-hidden="true"></i> Import Sub Seg</a>--}}
                                {{--<a href="{{ route('admin.employees.importDepartments') }}" class="btn btn-info btn-sm"><i class="ti-import" aria-hidden="true"></i> Import Depart</a>--}}
                                <a href="{{ route('admin.employees.export') }}" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.id')</th>
                            <th>@lang('app.name')</th>
                            <th>ERP(Odoo/CITMS)</th>
                            <th>ERP No</th>
                            <th>@lang('app.email')</th>
                            <th>@lang('app.status')</th>
<<<<<<< HEAD
                            <th>Profile Status</th>
=======
                            <th>Completed</th>
>>>>>>> 65f3abeac863cd9bd0236dde89095cf4ff21f308
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>
    $(function() {
        $(".divisions").change(function () {
            var divisionId = $(this).val();
            if(divisionId != ''){
                $.get( "{{route('member.positions.ajaxGetDepartmentByDivision')}}/"+divisionId, {  }, function( data ) {
                    if(data.error == 0){
                        var departmentsHtml     = data.data.departments;
                        $(".departments").html('');
                        $(".departments").html(departmentsHtml);
                    }else{
                        var departmentsHtml     = data.data.departments;
                        $(".departments").html('');
                        $(".departments").html('<option value="0">No Data Found</option>');
                    }
                }, "json");
            }
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $('#filter-results').click(function () {
            showTable();
        });

        var table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: '{!! route('member.employees.data') !!}',
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'erp', name: 'erp' },
                { data: 'employee_erp', name: 'employee_erp' },
                { data: 'email', name: 'email' },
                { data: 'status', name: 'status'},
                { data: 'completion_precent', name: 'completion_precent', width: '15%' },
                { data: 'action', name: 'action', width: '15%' }
            ]
        });

        function showTable() {

            var segmentID = $('#segment_id').val();
            if (!segmentID) {
                segmentID = 0;
            }

            var departmentID = $('#department_id').val();
            if (!departmentID) {
                departmentID = 0;
            }

            var centerID = $('#center_id').val();
            if (!centerID) {
                centerID = 0;
            }

            var url = '{!!  route('member.filter-employee.data', [':centerID', ':departmentID', ':segmentID']) !!}';

            url = url.replace(':centerID', centerID);
            url = url.replace(':departmentID', departmentID);
            url = url.replace(':segmentID', segmentID);

            table.dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: url,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'erp', name: 'erp' },
                    { data: 'employee_erp', name: 'employee_erp' },
                    { data: 'email', name: 'email' },
                    { data: 'status', name: 'status'},
                    { data: 'completion_precent', name: 'completion_precent', width: '15%' },
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
        }

        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('user-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('member.employees.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('body').on('click', '.assign_role', function(){
            var id = $(this).data('user-id');
            var role = $(this).val();
            var token = "{{ csrf_token() }}";

            $.easyAjax({
                url: '{{route('member.employees.assignRole')}}',
                type: "POST",
                data: {role: role, userId: id, _token : token},
                success: function (response) {
                    if(response.status == "success"){
                        $.unblockUI();
                        table._fnDraw();
                    }
                }
            })

        });
    });

</script>
@endpush