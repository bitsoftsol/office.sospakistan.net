@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.employees.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300i,400,400i,500,700,900">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .multi_step_form {
        /*background: #f6f9fb;*/
        display: block;
        overflow: hidden;
    }
    .multi_step_form #msform {
        text-align: center;
        position: relative;
        padding-top: 50px;
        min-height: 820px;
        /*max-width: 810px;*/
        margin: 0 auto;
        background: #ffffff;
        z-index: 1;
    }
    .multi_step_form #msform .tittle {
        text-align: center;
        padding-bottom: 55px;
    }
    .multi_step_form #msform .tittle h2 {
        font: 500 24px/35px "Roboto", sans-serif;
        color: #3f4553;
        padding-bottom: 5px;
    }
    .multi_step_form #msform .tittle p {
        font: 400 16px/28px "Roboto", sans-serif;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset {
        border: 0;
        padding: 20px 105px 0;
        position: relative;
        width: 100%;
        left: 0;
        right: 0;
    }
    .multi_step_form #msform fieldset:not(:first-of-type) {
        display: none;
    }
    .multi_step_form #msform fieldset h3 {
        font: 500 18px/35px "Roboto", sans-serif;
        color: #3f4553;
    }
    .multi_step_form #msform fieldset h6 {
        font: 400 15px/28px "Roboto", sans-serif;
        color: #5f6771;
        padding-bottom: 30px;
    }
    .multi_step_form #msform fieldset .intl-tel-input {
        display: block;
        background: transparent;
        border: 0;
        box-shadow: none;
        outline: none;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag {
        padding: 0 20px;
        background: transparent;
        border: 0;
        box-shadow: none;
        outline: none;
        width: 65px;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow {
        border: 0;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow:after {
        content: "\f35f";
        position: absolute;
        top: 0;
        right: 0;
        font: normal normal normal 24px/7px Ionicons;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset #phone {
        padding-left: 80px;
    }
    .multi_step_form #msform fieldset .form-group {
        padding: 0 10px;
    }
    .multi_step_form #msform fieldset .fg_2, .multi_step_form #msform fieldset .fg_3 {
        padding-top: 10px;
        display: block;
        overflow: hidden;
    }
    .multi_step_form #msform fieldset .fg_3 {
        padding-bottom: 70px;
    }
    .multi_step_form #msform fieldset .form-control, .multi_step_form #msform fieldset .product_select {
        border-radius: 3px;
        border: 1px solid #d8e1e7;
        /*padding: 0 20px;
        height: auto;*/
        font: 400 15px/48px "Roboto", sans-serif;
        color: #5f6771;
        box-shadow: none;
        outline: none;
        width: 100%;
    }
    .multi_step_form #msform fieldset .form-control.placeholder, .multi_step_form #msform fieldset .product_select.placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control:-moz-placeholder, .multi_step_form #msform fieldset .product_select:-moz-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control::-moz-placeholder, .multi_step_form #msform fieldset .product_select::-moz-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select::-webkit-input-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control:hover, .multi_step_form #msform fieldset .form-control:focus, .multi_step_form #msform fieldset .product_select:hover, .multi_step_form #msform fieldset .product_select:focus {
        border-color: #5cb85c;
    }
    .multi_step_form #msform fieldset .form-control:focus.placeholder, .multi_step_form #msform fieldset .product_select:focus.placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus:-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus:-moz-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus::-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus::-moz-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select:focus::-webkit-input-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .product_select:after {
        display: none;
    }
    .multi_step_form #msform fieldset .product_select:before {
        content: "\f35f";
        position: absolute;
        top: 0;
        right: 20px;
        font: normal normal normal 24px/48px Ionicons;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .product_select .list {
        width: 100%;
    }
    .multi_step_form #msform fieldset .done_text {
        padding-top: 40px;
    }
    .multi_step_form #msform fieldset .done_text .don_icon {
        height: 36px;
        width: 36px;
        line-height: 36px;
        font-size: 22px;
        margin-bottom: 10px;
        background: #5cb85c;
        display: inline-block;
        border-radius: 50%;
        color: #ffffff;
        text-align: center;
    }
    .multi_step_form #msform fieldset .done_text h6 {
        line-height: 23px;
    }
    .multi_step_form #msform fieldset .code_group {
        margin-bottom: 60px;
    }
    .multi_step_form #msform fieldset .code_group .form-control {
        border: 0;
        border-bottom: 1px solid #a1a7ac;
        border-radius: 0;
        display: inline-block;
        width: 30px;
        font-size: 30px;
        color: #5f6771;
        padding: 0;
        margin-right: 7px;
        text-align: center;
        line-height: 1;
    }
    .multi_step_form #msform fieldset .passport {
        margin-top: -10px;
        padding-bottom: 30px;
        position: relative;
    }
    .multi_step_form #msform fieldset .passport .don_icon {
        height: 36px;
        width: 36px;
        line-height: 36px;
        font-size: 22px;
        position: absolute;
        top: 4px;
        right: 0;
        background: #5cb85c;
        display: inline-block;
        border-radius: 50%;
        color: #ffffff;
        text-align: center;
    }
    .multi_step_form #msform fieldset .passport h4 {
        font: 500 15px/23px "Roboto", sans-serif;
        color: #5f6771;
        padding: 0;
    }
    .multi_step_form #msform fieldset .input-group {
        padding-bottom: 40px;
    }
    .multi_step_form #msform fieldset .input-group .custom-file {
        width: 100%;
        height: auto;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label {
        width: 168px;
        border-radius: 5px;
        cursor: pointer;
        font: 700 14px/40px "Roboto", sans-serif;
        border: 1px solid #99a2a8;
        text-align: center;
        transition: all 300ms linear 0s;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label i {
        font-size: 20px;
        padding-right: 10px;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:hover, .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:focus {
        background: #5cb85c;
        border-color: #5cb85c;
        color: #fff;
    }
    .multi_step_form #msform fieldset .input-group .custom-file input {
        display: none;
    }
    .multi_step_form #msform fieldset .file_added {
        text-align: left;
        padding-left: 190px;
        padding-bottom: 60px;
    }
    .multi_step_form #msform fieldset .file_added li {
        font: 400 15px/28px "Roboto", sans-serif;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .file_added li a {
        color: #5cb85c;
        font-weight: 500;
        display: inline-block;
        position: relative;
        padding-left: 15px;
    }
    .multi_step_form #msform fieldset .file_added li a i {
        font-size: 22px;
        padding-right: 8px;
        position: absolute;
        left: 0;
        transform: rotate(20deg);
    }
    .multi_step_form #msform #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
    }
    .multi_step_form #msform #progressbar li {
        list-style-type: none;
        color: #99a2a8;
        font-size: 9px;
        width: calc(100%/12);
        float: left;
        position: relative;
        font: 500 13px/1 "Roboto", sans-serif;
    }
    .multi_step_form #msform #progressbar li:nth-child(2):before {
        content: "2" !important;
    }
    .multi_step_form #msform #progressbar li:nth-child(3):before {
        content: "3" !important
    }
    .multi_step_form #msform #progressbar li:nth-child(4):before {
        content: "4" !important;
    }
    .multi_step_form #msform #progressbar li:nth-child(5):before {
        content: "5"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(6):before {
        content: "6"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(7):before {
        content: "7"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(8):before {
        content: "8"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(9):before {
        content: "9"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(10):before {
        content: "10"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(11):before {
        content: "11"!important;
    }

    .multi_step_form #msform #progressbar li:nth-child(12):before {
        content: "12"!important;
    }

    .multi_step_form #msform #progressbar li:before {
        content: "1" !important;
        font: normal normal normal 30px/50px simple-line-icons;
        width: 50px;
        height: 50px;
        line-height: 50px;
        display: block;
        background: #eaf0f4;
        border-radius: 50%;
        margin: 0 auto 10px auto;
    }
    .multi_step_form #msform #progressbar li:after {
        content: '';
        width: 100%;
        height: 10px;
        background: #eaf0f4;
        position: absolute;
        left: -50%;
        top: 21px;
        z-index: -1;
    }
    .multi_step_form #msform #progressbar li:last-child:after {
        width: 150%;
    }
    .multi_step_form #msform #progressbar li.active {
        color: #5cb85c;
    }
    .multi_step_form #msform #progressbar li.active:before, .multi_step_form #msform #progressbar li.active:after {
        background: #5cb85c;
        color: white;
    }
    .multi_step_form #msform .action-button {
        background: #5cb85c;
        color: white;
        border: 0 none;
        border-radius: 5px;
        cursor: pointer;
        min-width: 130px;
        font: 700 14px/40px "Roboto", sans-serif;
        border: 1px solid #5cb85c;
        margin: 0 5px;
        text-transform: uppercase;
        display: inline-block;
    }
    .multi_step_form #msform .action-button:hover, .multi_step_form #msform .action-button:focus {
        background: #405867;
        border-color: #405867;
    }
    .multi_step_form #msform .previous_button {
        background: transparent;
        color: #99a2a8;
        border-color: #99a2a8;
    }
    .multi_step_form #msform .previous_button:hover, .multi_step_form #msform .previous_button:focus {
        background: #405867;
        border-color: #405867;
        color: #fff;
    }

</style>
<style type="text/css">



    #signatureparent {
        color:#000000;
        /*color:darkblue;*/
        /*background-color:darkgrey;*/
        max-width:400px;
        padding:20px;
    }

    /*This is the div within which the signature canvas is fitted*/
    #signature {
        border: 2px dotted black;
        background-color:lightgrey;
    }
    .sigRow{
        margin: auto 24%;
    }
    canvas.jSignature{
        height: 160px !important;
        width: 338px !important;

    }


    @media all and (min-width:0px) and (max-width: 801px) {
        .sigRow{
            margin: auto 5%;
        }
        canvas.jSignature{
            height: 160px !important;

        }
        #signatureparent {
            color:#000000;
            max-width:600px;
            padding:20px;
        }
    }



</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.employees.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div>
                        <!-- Multi step form -->
                        <section class="multi_step_form">
                            {!! Form::open(['id'=>'msform','route' => 'admin.employees.storeOdoo']) !!}
                            <div class="form-body">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active">Position</li>
                                    <li>Public Information</li>
                                    <li>Personal Information</li>
                                    <li>Contact Information</li>
                                    <li>Recruitment Information</li>
                                    <li>Benifits</li>
                                    <li>References</li>
                                    <li>Military Record</li>
                                    <li>Employment Record</li>
                                    <li>Documents</li>
                                    <li>Biometric</li>
                                    <li>Summary</li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset id="fs_1">
                                    <h2>Step 1 of 11</h2>
                                    <h3>Position</h3>
                                    <div class="row">


                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Center*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="center_id" id="center_id" class="form-control" required>
                                                    @if($centers->count() > 0)
                                                        <option value=""> Select Center </option>
                                                        @foreach($centers as $centersDataIn)
                                                            <option value="{{$centersDataIn->id}}" name="odoo_center_id">{{$centersDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Center Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        {{--<div class="col-md-6">
                                            <div class="form-group">
                                                <label>Position*</label>
                                                <select name="position_id" id="position_id" class="form-control" required>
                                                    @if($positions->count() > 0)
                                                        <option value=""> Select Position </option>
                                                        @foreach($positions as $positionDataIn)
                                                            <option value="{{$positionDataIn->id}}" name="position">{{$positionDataIn->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Position</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>--}}


                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Division</label>
                                                <select name="divisions" id="divisions" class="form-control divisions">
                                                    @if(count($divisions) > 0)
                                                        <option value="">Select Division</option>
                                                        @foreach($divisions as $division)
                                                            <option value="{{$division->id}}">{{$division->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Division Found</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Department</label>
                                                <select name="departments" id="departments" class="form-control departments">
                                                    @if($departments->count() > 0)
                                                        <option value=""> Select Department</option>
                                                        @foreach($departments as $department)
                                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">Department Not Found</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Position*</label>
                                                <select name="positions" id="positions" class="form-control positions" required>
                                                    @if($positions->count() > 0)
                                                        <option value=""> Select Position </option>
                                                        @foreach($positions as $positionDataIn)
                                                            <option value="{{$positionDataIn->id}}" name="position">{{$positionDataIn->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Position</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    {{--<button type="button" class="action-button previous_button">Back</button>--}}
                                    <button type="button" class="next_fs2 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_2">
                                    {{--<div class="row">--}}
                                    {{--<div class="col-md-2">--}}
                                    {{--<button type="button" class="action-button previous previous_button">Back</button>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <h2>Step 2 of 11</h2>
                                    <h3>Public Information</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="isGuard" name="is_guard" value="true" type="checkbox">
                                                    <label for="is_Guard"> Is Guard ?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tag*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="category_ids" id="category_ids" class="form-control" required>
                                                    <option value="Civil">Civil</option>
                                                    <option value="Full Time">Full Time</option>
                                                    <option value="Part Time">Part Time</option>
                                                    <option value="Army">Army</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>First Name*</label>
                                                <input type="text" name="first_name" id="first_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Middle Name*</label>
                                                <input type="text" name="middle_name" id="middle_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Last Name*</label>
                                                <input type="text" name="last_name" id="last_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Father Name*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="fatherName" id="fatherName" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Mother Name*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="mother_name" id="mother_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nationality*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="nationality" id="nationality" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Home City*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="home_city" id="home_city" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Last Qualification*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="last_qualification" id="last_qualification" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Height(Centimeters)* </label>
                                                <input type="text" name="height" id="height" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Weight(Centimeters)* </label>
                                                <input type="text" name="weight" id="weight" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs3 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_3">
                                    <h2>Step 3 of 11</h2>
                                    <h3>Personal Information</h3>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>CNIC*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="cnic" id="cnic" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>CNIC Expiry*</label>
                                                <input type="text" name="cnic_expiry" id="cnic_expiry" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date Of Birth*</label>
                                                <input type="text" name="birthday" id="birthday" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.gender')*</label>
                                                <select name="gender" id="gender" class="form-control" required>
                                                    <option value="male">@lang('app.male')</option>
                                                    <option value="female">@lang('app.female')</option>
                                                    <option value="others">@lang('app.others')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Marital Status*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="marital_status" id="marital_status" class="form-control" required>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Widower">Widower</option>
                                                    <option value="Divorced">Divorced</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Number Of Dependents*</label>
                                                <input type="text" name="no_of_child" id="no_of_child" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank Name{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="bank_name" id="bank_name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank Branch</label>
                                                <input type="text" name="bank_branch" id="bank_branch" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank City</label>
                                                <input type="text" name="bank_city" id="bank_city" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Account Title</label>
                                                <input type="text" name="account_title" id="account_title" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Account No</label>
                                                <input type="text" name="account_no" id="account_no" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Blood Group</label>
                                                <input type="text" name="blood_group" id="blood_group" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--<div class="col-md-6">--}}
                                        {{--<label>@lang('modules.profile.profilePicture')</label>--}}
                                        {{--<div class="form-group">--}}
                                        {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                                        {{--<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">--}}
                                        {{--<img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"   alt=""/>--}}
                                        {{--</div>--}}
                                        {{--<div class="fileinput-preview fileinput-exists thumbnail"--}}
                                        {{--style="max-width: 200px; max-height: 150px;"></div>--}}
                                        {{--<div>--}}
                                        {{--<span class="btn btn-info btn-file">--}}
                                        {{--<span class="fileinput-new"> @lang('app.selectImage') </span>--}}
                                        {{--<span class="fileinput-exists"> @lang('app.change') </span>--}}
                                        {{--<input type="file" id="image_profile" name="image_profile"> </span>--}}
                                        {{--<a href="javascript:;" class="btn btn-danger fileinput-exists"--}}
                                        {{--data-dismiss="fileinput"> @lang('app.remove') </a>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        {{--</div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload Profile Pics</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_profile" name="image_profile"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    <br>
                                                    <br>
                                                    <div id="my_camera"></div>
                                                    <input type=button value="Load Camera" id="load_cam" class="btn btn-success" onClick="load_camera()">
                                                    <input style="display: none" id="take_pic" type=button value="Take Snapshot" class="btn btn-success" onClick="take_snapshot()">
                                                    <input style="display: none" id="close_cam" type=button value="Close Camera" class="btn btn-success" onClick="close_camera()">

                                                    <input type="hidden" name="image_profile_cam" class="image-tag">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="results">Your captured image will appear here...</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row sigRow" style="">
                                                <br/>
                                                <br/>
                                                <b style="margin: auto 30%;">Signature</b>
                                                <div id="signatureparent" style="">
                                                    <div id="signature"></div></div>

                                                <br/>

                                                <div style="margin: auto 15%;">
                                                    <button type="button" onclick="drawSig()" value="">Draw Signature</button>
                                                    <button type="button" onclick="resetSig()">Reset</button>
                                                    <button type="button" onclick="save()">Save</button>
                                                </div>

                                                <br/>
                                                <br/>
                                            </div>
                                            {{--<div class="form-group">
                                                <label>Signature</label>
                                                <input type="text" name="signature" id="signature" class="form-control">
                                            </div>--}}
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs4 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_4">
                                    <h2>Step 4 of 11</h2>
                                    <h3>Contact Information</h3>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>@lang('app.mobile')*</label>
                                                <input type="tel" name="mobile" id="mobile"  class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Land Line No*</label>
                                                <input type="text" name="land_line_number" id="land_line_number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Personal Email</label>
                                                <input type="email" name="personal_email" id="personal_email" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Permanent @lang('app.address')*</label>
                                                <textarea name="address"  id="address"  rows="1" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Working @lang('app.address')</label>
                                                <textarea name="working_address"  id="working_address"  rows="1" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Name*</label>
                                                <input type="text" name="kin_1_name" id="kin_1_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Number*</label>
                                                <input type="text" name="kin_1_number" id="kin_1_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Relation*</label>
                                                <input type="text" name="kin_1_relation" id="kin_1_relation" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Name</label>
                                                <input type="text" name="kin_2_name" id="kin_2_name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Number</label>
                                                <input type="text" name="kin_2_number" id="kin_2_number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Relation</label>
                                                <input type="text" name="kin_2_relation" id="kin_2_relation" class="form-control">
                                            </div>
                                        </div>
                                    </div>


                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs5 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_5">
                                    <h2>Step 5 of 11</h2>
                                    <h3>Recruitment Information</h3>

                                    <div class="row">
                                        {{--<div class="col-md-3">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Center--}}{{--@lang('modules.employees.jobTitle')--}}{{--</label>--}}
                                        {{--<select name="center_id" id="center_id" class="form-control">--}}
                                        {{--@if($centers->count() > 0)--}}
                                        {{--<option value=""> Select Center </option>--}}
                                        {{--@foreach($centers as $centersDataIn)--}}
                                        {{--<option value="{{$centersDataIn->id}}" name="odoo_center_id">{{$centersDataIn->name}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--@else--}}
                                        {{--<option value="0">No Center Data Import From ERP</option>--}}
                                        {{--@endif--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Basic Monthly Salary*</label>
                                                <input type="text" name="basic_salary" id="basic_salary" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Hours a Day to Work*</label>
                                                <input type="text" name="duty_hours" id="duty_hours" value="8" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Days to Work per month*</label>
                                                <input type="text" name="duty_days" id="duty_days" value="26" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.joiningDate')*</label>
                                                <input type="text" name="joining_date" id="joining_date" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Annual Leaves*</label>
                                                <input type="text" name="annual_leaves" id="annual_leaves" class="form-control" required>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-3">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Air Ticket Benifit</label>--}}
                                        {{--<input type="text" name="air_ticket" id="air_ticket" class="form-control">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Type of Attendance Logging*</label>
                                                <select name="attendance_logging" id="attendance_logging" class="form-control" required>
                                                    <option value="Biometric">Biometric</option>
                                                    <option value="InterCoApp">InterCoApp</option>
                                                    <option value="Manual">Manual</option>
                                                    <option value="Mobile App Based Track">Mobile App Based Track</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Recruitment Type*</label>
                                                <select name="rec_type" id="rec_type" class="form-control" required>
                                                    <option value="Onsite">On Site</option>
                                                    <option value="Offsite">Off Site</option>
                                                    <option value="Remote">Remote</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Travel Benifits*</label>
                                                <input type="text" name="travel_benifits" id="travel_benifits" class="form-control" required>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-3">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Travel Expenses*</label>--}}
                                        {{--<input type="text" name="travel_expenses" id="travel_expenses" class="form-control">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-3">--}}
                                        {{--<div class="form-group">--}}
                                        {{--<label>Over Time Pay*</label>--}}
                                        {{--<input type="text" name="over_time_pay" id="over_time_pay" class="form-control" required>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs6 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_6">
                                    <h2>Step 6 of 11</h2>
                                    <h3>Benifits</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="email_pro" name="email_pro" value="true" type="checkbox" >
                                                    <label for="email_pro"> Email?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="sim" name="sim" value="true" type="checkbox" >
                                                    <label for="sim"> Sim?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="mobile_phone" name="mobile_phone" value="true" type="checkbox" >
                                                    <label for="mobile_phone"> Mobile Phone?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="office" name="office" value="true" type="checkbox" >
                                                    <label for="office"> Office?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="office_stationary" name="office_stationary" value="true" type="checkbox" >
                                                    <label for="office_stationary"> Office Stationary?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="pc" name="pc" value="true" type="checkbox" >
                                                    <label for="pc"> PC?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="laptop" name="laptop" value="true" type="checkbox" >
                                                    <label for="laptop"> Laptop?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="vehicle" name="vehicle" value="true" type="checkbox" >
                                                    <label for="vehicle"> Vehicle?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="uniform" name="uniform" value="true" type="checkbox" >
                                                    <label for="uniform"> Uniform?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="belt" name="belt" value="true" type="checkbox" >
                                                    <label for="belt"> Belt?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="shoes" name="shoes" value="true" type="checkbox" >
                                                    <label for="shoes"> Shoes?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="caps" name="caps" value="true" type="checkbox" >
                                                    <label for="caps"> Caps?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="badges" name="badges" value="true" type="checkbox" >
                                                    <label for="badges"> Badges?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs7 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_7">
                                    <h2>Step 7 of 11</h2>
                                    <h3>References</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 1 Name*</label>
                                                <input type="text" name="ref_1_name" id="ref_1_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 1 Number*</label>
                                                <input type="text" name="ref_1_number" id="ref_1_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Relation to Refree 1* </label>
                                                <input type="text" name="rel_to_ref1" id="rel_to_ref1" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 1 Address*</label>
                                                <input type="text" name="ref_1_address" id="ref_1_address" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 1 CNIC*</label>
                                                <input type="text" name="ref_1_cnic" id="ref_1_cnic" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 2 Name*</label>
                                                <input type="text" name="ref_2_name" id="ref_2_name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 2 Number*</label>
                                                <input type="text" name="ref_2_number" id="ref_2_number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Relation to Refree 2*</label>
                                                <input type="text" name="rel_to_ref2" id="rel_to_ref2" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 2 Address*</label>
                                                <input type="text" name="ref_2_address" id="ref_2_address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 2 CNIC*</label>
                                                <input type="text" name="ref_2_cnic" id="ref_2_cnic" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs8 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_8">
                                    <h2>Step 8 of 11</h2>
                                    <h3>Military Record</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="military_person" name="military_person" value="true" type="checkbox" >
                                                    <label for="military_person"> Ex. Military Personal?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group force_no" style="display: none">
                                                <label>Force No</label>
                                                <input type="text" name="force_no" id="force_no" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group m_joining_date" style="display: none">
                                                <label>Joining Date in Military</label>
                                                <input type="text" name="m_joining_date" id="m_joining_date" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group m_leaving_date" style="display:none;">
                                                <label>Leaving Date in Military</label>
                                                <input type="text" name="m_leaving_date" id="m_leaving_date" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group force_type" style="display: none">
                                                <label>Force Type</label>
                                                <input type="text" name="force_type" id="force_type" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group force_rank" style="display: none">
                                                <label>Force Rank</label>
                                                <input type="text" name="force_rank" id="force_rank" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group last_unit" style="display:none;">
                                                <label>Last Unit</label>
                                                <input type="text" name="last_unit" id="last_unit" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group last_center" style="display:none;">
                                                <label>Last Center</label>
                                                <input type="text" name="last_center" id="last_center" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next_fs9 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_9">
                                    <h2>Step 9 of 11</h2>
                                    <h3>Employment Record</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input type="text" name="last_company_name" id="last_company-name" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Position</label>
                                                <input type="text" name="last_position" id="last_position" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Joining Date</label>
                                                <input type="text" name="last_joining_date" id="last_joining_date" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Leaving Date</label>
                                                <input type="text" name="last_leaving_date" id="last_leaving_date" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" name="last_working_city" id="last_working_city" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" name="last_working_country" id="last_working_country" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next_fs10 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_10">
                                    <h2>Step 10 of 11</h2>
                                    <h3>Documents</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload CNIC Copy*</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_cnic" name="image_cnic" required> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload Police Varification</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_police_varification" name="image_police_varification"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload Nadra Attested</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_nadra" name="image_nadra"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Varification Date</label>
                                                <input type="text" name="varification_date" id="varification_date" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload Referee's Undertaking*</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_referee_undertaking" name="image_referee_undertaking"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload Employment Agreement</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_emp_aggre" name="image_emp_aggre"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload Discharge/Pension Book Copy</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_pen_book" name="image_pen_book"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Education</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_education" name="image_education"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next_fs11 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_11">
                                    <h2>Step 11 of 11</h2>
                                    <h3>Biometric</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 1</label>
                                                <input type="text" name="left_fin_1" id="left_fin_1" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 2</label>
                                                <input type="text" name="left_fin_2" id="left_fin_2" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 3</label>
                                                <input type="text" name="left_fin_3" id="left_fin_3" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 4</label>
                                                <input type="text" name="left_fin_4" id="left_fin_4" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left thumb</label>
                                                <input type="text" name="left_thumb" id="left_thumb" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Thumb</label>
                                                <input type="text" name="right_thumb" id="right_thumb" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Finger 1</label>
                                                <input type="text" name="right_fin_1" id="right_fin_1" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Finger 2</label>
                                                <input type="text" name="right_fin_2" id="right_fin_2" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Right Finger 3</label>
                                                <input type="text" name="right_fin_3" id="right_fin_3" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Right Finger 4</label>
                                                <input type="text" name="right_fin_4" id="right_fin_4" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next_fs12 action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_12">
                                    <h3>Summary</h3>
                                    <div class="row">
                                        @if(isset($fields))
                                            @foreach($fields as $field)
                                                <div class="col-md-6">
                                                    <label>{{ ucfirst($field->label) }}</label>
                                                    <div class="form-group">
                                                        @if( $field->type == 'text')
                                                            <input type="text" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'password')
                                                            <input type="password" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'number')
                                                            <input type="number" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">

                                                        @elseif($field->type == 'textarea')
                                                            <textarea name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" id="{{$field->name}}" cols="3">{{$editUser->custom_fields_data['field_'.$field->id] or ''}}</textarea>

                                                        @elseif($field->type == 'radio')
                                                            <div class="radio-list">
                                                                @foreach($field->values as $key=>$value)
                                                                    <label class="radio-inline @if($key == 0) p-0 @endif">
                                                                        <div class="radio radio-info">
                                                                            <input type="radio" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" id="optionsRadios{{$key.$field->id}}" value="{{$value}}" @if(isset($editUser) && $editUser->custom_fields_data['field_'.$field->id] == $value) checked @elseif($key==0) checked @endif>>
                                                                            <label for="optionsRadios{{$key.$field->id}}">{{$value}}</label>
                                                                        </div>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'select')
                                                            {!! Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                                    $field->values,
                                                                     isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender'])
                                                             !!}

                                                        @elseif($field->type == 'checkbox')
                                                            <div class="mt-checkbox-inline">
                                                                @foreach($field->values as $key => $value)
                                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                                        <input name="custom_fields_data[{{$field->name.'_'.$field->id}}][]" type="checkbox" value="{{$key}}"> {{$value}}
                                                                        <span></span>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'date')
                                                            <input type="text" class="form-control date-picker" size="16" name="custom_fields_data[{{$field->name.'_'.$field->id}}]"
                                                                   value="{{ isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format('m/d/Y')}}">
                                                        @endif
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>

                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button class="action-button">Finish</button>
                                </fieldset>
                            </div>
                            {!! Form::close() !!}
                        </section>
                        <!-- End Multi step form -->
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        (function($) {
            "use strict";

            //* Form js
            function verificationForm(){
                //jQuery time
                var current_fs, next_fs, previous_fs; //fieldsets
                var left, opacity, scale; //fieldset properties which we will animate
                var animating; //flag to prevent quick multi-click glitches

                $(".next_fs2").click(function () {
                    var isAllValid = false;
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }

                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });

                $(".next_fs3").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs4").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs5").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs6").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs7").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs8").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs9").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs10").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs11").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });
                $(".next_fs12").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });

                $(".previous").click(function () {
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    previous_fs = $(this).parent().prev();

                    //de-activate current step on progressbar
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the previous fieldset
                    previous_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale previous_fs from 80% to 100%
                            scale = 0.8 + (1 - now) * 0.2;
                            //2. take current_fs to the right(50%) - from 0%
                            left = ((1 - now) * 50) + "%";
                            //3. increase opacity of previous_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'left': left
                            });
                            previous_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'opacity': opacity
                            });
                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });

                // $(".submit").click(function () {
                //     return false;
                // })
            };

            //* Add Phone no select
            function phoneNoselect(){
                if ( $('#msform').length ){
                    $("#phone").intlTelInput();
                    $("#phone").intlTelInput("setNumber", "+880");
                };
            };
            //* Select js
            function nice_Select(){
                if ( $('.product_select').length ){
                    $('select').niceSelect();
                };
            };
            /*Function Calls*/
            verificationForm ();
            //phoneNoselect ();
            //nice_Select ();
        })(jQuery);
    </script>
    <script>

        $("#joining_date").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#birthday").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#cnic_expiry").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#m_joining_date").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#m_leaving_date").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#last_joining_date").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $("#last_leaving_date").datepicker({
            todayHighlight: true,
            autoclose: true
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.employees.store')}}',
                container: '#msform',
                type: "POST",
                redirect: true,
                file: (document.getElementById("image").files.length == 0) ? false : true,
                data: $('#msform').serialize()
            })
        });

        $('#random_password').change(function () {
            var randPassword = $(this).is(":checked");

            if(randPassword){
                $('#password').val('{{ str_random(8) }}');
                $('#password').attr('readonly', 'readonly');
            }
            else{
                $('#password').val('');
                $('#password').removeAttr('readonly');
            }
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script src="{{asset('jSignature/flashcanvas.js')}}"></script>
    <script src="{{asset('jSignature/jquery.js')}}"></script>

    <script src="{{asset('jSignature/jSignature.min.noconflict.js')}}"></script>

    <script type="text/javascript">
        jQuery.noConflict()
    </script>

    <script>
        $(".divisions").change(function () {
            var divisionId = $(this).val();
            if(divisionId != ''){
                $.get( "{{route('member.positions.ajaxGetDepartmentByDivision')}}/"+divisionId, {  }, function( data ) {
                    if(data.error == 0){
                        var departmentsHtml     = data.data.departments;
                        $(".departments").html('');
                        $(".departments").html(departmentsHtml);
                    }else{
                        var departmentsHtml     = data.data.departments;
                        $(".departments").html('');
                        $(".departments").html('<option value="0">No Related Data Found</option>');
                    }
                }, "json");
            }
        });

        $(".departments").change(function () {
            var departmentId = $(this).val();
            if(departmentId != ''){
                $.get( "{{route('member.positions.ajaxGetPositionByDepartment')}}/"+departmentId, {  }, function( data ) {
                    if(data.error == 0){
                        var positionsHtml     = data.data.positions;
                        $(".positions").html('');
                        $(".positions").html(positionsHtml);
                    }else{
                        var positionsHtml     = data.data.positions;
                        $(".positions").html('');
                        $(".positions").html('<option value="0">No Related Data Found</option>');
                    }
                }, "json");
            }
        });
        var $sigdiv = [];
        (function($){

            $(document).ready(function() {

                // This is the part where jSignature is initialized.
                $sigdiv = $("#signature").jSignature({'UndoButton':false})

                <?php if(isset($id) && $id != '' && $user->directory != ''){?>
                $sigdiv.jSignature("setData", '<?php echo $user->directory?>');
                <?php }?>
            })

        })(jQuery)

    </script>
    <script>
        function drawSig() {
            $sigdiv.jSignature('reset');
            $("#sigImageField").val('');
            $("#sigImage").hide();
            $("#signatureparent").show();
        }
        function resetSig() {
            $sigdiv.jSignature('reset');
        }
        function save() {
            var sigImage =  $sigdiv.jSignature('getData');
            console.log(sigImage);
            jQuery("#sigImageField").val(sigImage);
            jQuery("#sigImage").attr('src', sigImage);
            jQuery("#sigImage").show();
            $sigdiv.jSignature('reset');
            jQuery("#signatureparent").hide();
        }
        Webcam.set({
            width: 300,
            height: 150,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        function load_camera(){
            Webcam.attach('#my_camera');
            $("#load_cam").hide();
            $("#take_pic").show();
        }

        function close_camera() {
            Webcam.reset('#my_camera');
            $("#load_cam").show();
            $("#take_pic").hide();
            $("#close_cam").hide();
        }

        function take_snapshot() {
            $("#close_cam").show();
            Webcam.snap(function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            });

        }

        $('#military_person').on('change', function(){
            if ( $(this).is(':checked') ) {
                $('.force_no').show();
                $('.m_joining_date').show();
                $('.m_leaving_date').show();
                $('.force_type').show();
                $('.force_rank').show();
                $('.last_unit').show();
                $('.last_center').show();
            } else {
                $('.force_no').hide();
                $('.m_joining_date').hide();
                $('.m_leaving_date').hide();
                $('.force_type').hide();
                $('.force_rank').hide();
                $('.last_unit').hide();
                $('.last_center').hide();
            }
        });

        function validate_fs1() {

        }
    </script>
@endpush

