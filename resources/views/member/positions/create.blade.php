
<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */
?>
@extends($mUserTypeLayout)

@section('page-title')

    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route($mUserType.'.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($mUserType.'.positions') }}">{{ $pageTitle }}</a></li>
                <li class="active">@if(isset($id) && $id > 0) @lang('app.edit') @else @lang('app.addNew') @endif</li>

            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @if(isset($id) && $id > 0) @lang('app.edit') @else @lang('app.create') @endif Position {{--@lang('modules.client.createTitle')--}}</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createPosition','class'=>'ajax-form','method'=>'POST']) !!}
                        @if(isset($id) && $id > 0)
                            <input type="hidden" name="id" value="{{$id}}">
                        @endif
                        <div class="form-body">
                            <h3 class="box-title">Position Detail{{--@lang('modules.client.companyDetails')--}}</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Is Division Or Position ?</label>
                                    </div>
                                </div>
                                <div class="col-md-8">

                                    <div class="radio-list">
                                        <label class="radio-inline p-0">
                                            <div class="radio radio-info">
                                                <input type="radio" name="isDivision" id="isPosition" @if($isDivision == 0) checked @else checked @endif class="positionType" value="0">
                                                <label for="isPosition">Position</label>
                                            </div>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="radio radio-info">
                                                <input type="radio" name="isDivision" id="isDivision" @if($isDivision == 1) checked @endif class="positionType" value="1">
                                                <label for="isDivision">Division</label>
                                            </div>
                                        </label>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name {{--@lang('modules.client.companyName')--}}</label>
                                        <input type="text" id="name" name="name" value="{{ $name or '' }}" class="form-control" >
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Order {{--@lang('modules.client.companyName')--}}</label>
                                        <input type="number" id="order" name="order" value="{{ $order or '' }}" class="form-control"  >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select Parent</label>
                                        <select name="parent" id="parent" class="form-control">
                                            <option value="">Select Parent</option>
                                            <optgroup label="Divisions">
                                                @if(count($division) > 0)
                                                    @foreach($division as $divisionIn)
                                                        <option data-type="division" value="{{$divisionIn->id}}" @if($divisionIn->id == $divisionParentId) selected @endif>{{$divisionIn->po_level_name}}</option>
                                                    @endforeach
                                                @else
                                                    <option value="0">Division not exist</option>
                                                @endif
                                            </optgroup>
                                            <optgroup label="Positions">
                                                @if($positionsData->count() > 0)
                                                    @foreach($positionsData as $positionsDataIn)
                                                        <option data-type="position" value="{{$positionsDataIn->id}}" @if($positionsDataIn->id == $parentId) selected @endif>{{$positionsDataIn->po_level_name}}</option>
                                                    @endforeach
                                                @else
                                                    <option value="0">Position not exist</option>
                                                @endif
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row" id="divisionDepartmentDiv" style="display: @if($isDivision == 1) block @else none @endif ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="divisionDepartment">Select Department </label>
                                        <select name="divisionDepartment[]" id="divisionDepartment" class="form-control" multiple>
                                            @if(true)
                                                @foreach($department as $departmentIn)
                                                    @if(in_array($departmentIn->id,$divisionDepartmentIds))
                                                        <option value="{{$departmentIn->id}}" selected>{{$departmentIn->name}}</option>
                                                    @else
                                                        <option value="{{$departmentIn->id}}">{{$departmentIn->name}}</option>
                                                    @endif
                                                @endforeach
                                            @else
                                                <option value="0">Division not exist</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row" id="positionDepartmentDiv" style="display: @if($isUnderDepartment == 1) block @else none @endif ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select Department <small>Leave blank if you want to</small></label>
                                        <select name="positionDepartment" id="positionDepartment" class="form-control" >

                                            @if(true)
                                                <option value="0">Select Department</option>
                                                @foreach($divisionAssignedDepartments as $departmentIn)
                                                    <option value="{{$departmentIn->id}}" @if($departmentIn->id == $positionDepartmentId) selected @endif>{{$departmentIn->name}}</option>
                                                @endforeach
                                            @else
                                                <option value="0">Division not exist</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('app.status')</label>
                                        <select name="status" id="status" class="form-control" >
                                            <option @if($status == '1') selected
                                                    @endif value="1">@lang('app.active')</option>
                                            <option @if($status == '0') selected
                                                    @endif value="0">@lang('app.deactive')</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row" id="hasMaxChildDiv" style="display: @if($isDivision == 1) none @else block @endif ">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info">
                                            <input  id="hasMaxChild" name="hasMaxChild" value="1" @if($hasMaxChild == 1) checked @endif type="checkbox"  />
                                            <label for="hasMaxChild"> Is guard under ?</label>
                                        </div>
                                    </div>

                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info">
                                            <input  id="isFirstPosition" name="isFirstPosition" value="1" @if($isFirstPosition == 1) checked @endif type="checkbox"  />
                                            <label for="addMultiple"> Is first position ?</label>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->


                            @if($id == 0)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-info">
                                                <input  id="addMultiple" name="addMultiple" value="1" type="checkbox" >
                                                <label for="addMultiple"> Add Multiple ?</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6" id="addMultipleNumberDiv" style="display: none;">
                                        <div class="form-group">
                                            <label>Add Number</label>
                                            <input type="number" id="addMultipleNumber" name="addMultipleNumber" class="form-control" >
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            @endif



                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        var parentType = null;
        $(function () {


            $('.positionType').click(function() {

                if($(this).val() == 1) {
                    $("#divisionDepartmentDiv").show();
                    $('#positionDepartment').val('');
                    $('#parent').val('');
                    $("#positionDepartmentDiv").hide();
                    $("#hasMaxChildDiv").hide();


                }
                else{
                    $("#divisionDepartmentDiv").hide();
                    $("#hasMaxChildDiv").show();
                }
            });

            $('#parent').change(function() {
                parentType = $('option:selected', this).attr('data-type');
                var divisionId = $(this).val();

                if(parentType == "division") {

                    $.get( "{{route($mUserType.'.positions.ajaxGetDepartmentByDivision')}}/"+divisionId, {  }, function( data ) {

                        if(data.error == 0){
                            var departmentsHtml     = data.data.departments;
                            $("#positionDepartment").html('');
                            $("#positionDepartment").html(departmentsHtml);
                            $("#positionDepartmentDiv").show();
                        }

                    }, "json");
                }
                else{
                    parentType = "position";
                    $("#positionDepartment").html('');
                    $("#positionDepartmentDiv").hide();
                    $.get( "{{route($mUserType.'.positions.ajaxGetDepartments')}}", {  }, function( data ) {

                        if(data.error == 0){
                            var departmentsHtml     = data.data.departments;
                            $("#positionDepartment").html('');
                            $("#positionDepartment").html(departmentsHtml);
                            $("#positionDepartmentDiv").show();
                        }

                    }, "json");

                }
            });

            $('#addMultiple').change(function() {
                if(this.checked) {
                    $("#addMultipleNumberDiv").show();
                }
                else{
                    $("#addMultipleNumberDiv").hide();
                }
            });

        })

        $('#save-form').click(function () {
            /*if($("#isFirstPosition").prop('checked') == true){

            }else{
                alert("not");
            }
            return false;*/
            $.easyAjax({
                url: '{{route($mUserType.'.positions.save')}}',
                container: '#createPosition',
                type: "POST",
                redirect: true,
                data: $('#createPosition').serialize()
            })
        });
    </script>
@endpush

