@extends($mUserTypeLayout)

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route($mUserType.'.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="white-box bg-inverse">
                <h3 class="box-title text-white">Total Position{{--@lang('modules.dashboard.totalPositions')--}}</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-user text-white"></i></li>
                    <li class="text-right"><span id="totalWorkingDays" class="counter text-white">{{ $positions }}</span></li>
                </ul>
            </div>
        </div>

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route($mUserType.'.positions.createOrEditView') }}" class="btn btn-outline btn-success btn-sm">{{$addText}} {{--@lang('modules.positions.addNewPosition') --}}<i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a href="{{ route($mUserType.'.positions.show') }}" class="btn btn-outline btn-success btn-sm">{{$showText}} {{--@lang('modules.positions.addNewPosition') --}}<i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-outline btn-danger btn-sm bulkDelete">{{$deleteBulk}} <i class="fa fa-trash" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" id="selectAll" class="btn btn-outline btn-warning btn-sm selectAll">{{$selectAll}} <i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="positions-table">
                        <thead>
                        <tr>
                            <th>select</th>
                            <th>@lang('app.id')</th>
                            <th>Name</th>
                            <th>@lang('app.status')</th>
                            <th>Employee Name</th>
                            <th>Department</th>
                            <th>Report To</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{asset('js/odoo-xml.js')}}"></script>
    <script>
        $(function() {
            var table = $('#positions-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route($mUserType.'.positions.maxChildData',[$parentId]) !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'select', name: 'select' },
                    { data: 'id', name: 'id' },
                    { data: 'po_level_name', name: 'po_level_name' },
                    { data: 'status', name: 'status' },
                    { data: 'name', name: 'name' },
                    { data: 'department_name', name: 'department_name' },
                    { data: 'parent_name', name: 'parent_name' },
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('position-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route($mUserType.'.positions.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.bulkDelete', function(){
                var positionIds = [];
                $(".bulkIds:checked").each(function()
                {
                    positionIds.push($(this).val());
                });
                if(!(positionIds.length > 0)){
                    alert('No record selected');
                    return false;
                }
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route($mUserType.'.positions.bulkDelete') }}";

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST', 'positionsIds':JSON.stringify(positionIds)},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    table._fnDraw();
                                    $("#selectAll").addClass('selectAll');
                                    $("#selectAll").removeClass('unSelectAll');
                                    $("#selectAll").html('Select All');
                                }
                            }
                        });
                    }
                });
            });
        });

        $(document).on('click', '.selectAll', function () {
            $(".bulkIds").prop('checked', true);
            $(this).addClass('unSelectAll');
            $(this).removeClass('selectAll');
            $(this).html('Unselect All');
        })
        $(document).on('click', '.unSelectAll', function () {

            $(".bulkIds").prop('checked', false);
            $(this).addClass('selectAll');
            $(this).removeClass('unSelectAll');
            $(this).html('Select All');
        })

    </script>
@endpush