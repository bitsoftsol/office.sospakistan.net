<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */
?>
@extends($mUserTypeLayout)

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route($mUserType.'.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($mUserType.'.positions') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.show')</li>
            </ol>
        </div>

        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
            @if($firstPositionId == 0) <h3 class="text-center text-danger"> First position not define.</h3> @endif
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>
        .counter{
            font-size: large;
        }
    </style>

    <style>

        /*Now the CSS*/
        /*.mainOH * {margin: 0; padding: 0;}*/

        .tree ul {
            padding-top: 20px; position: relative;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        .tree li {
            float: left; text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*We will use ::before and ::after to draw the connectors*/

        .tree li::before, .tree li::after{
            content: '';
            position: absolute; top: 0; right: 50%;
            border-top: 1px solid #ccc;
            width: 50%; height: 20px;
        }
        .tree li::after{
            right: auto; left: 50%;
            border-left: 1px solid #ccc;
        }

        /*We need to remove left-right connectors from elements without
        any siblings*/
        .tree li:only-child::after, .tree li:only-child::before {
            display: none;
        }

        /*Remove space from the top of single children*/
        .tree li:only-child{ padding-top: 0;}

        /*Remove left connector from first child and
        right connector from last child*/
        .tree li:first-child::before, .tree li:last-child::after{
            border: 0 none;
        }
        /*Adding back the vertical connector to the last nodes*/
        .tree li:last-child::before{
            border-right: 1px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
        }
        .tree li:first-child::after{
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
        }

        /*Time to add downward connectors from parents*/
        .tree ul ul::before{
            content: '';
            position: absolute; top: 0; left: 50%;
            border-left: 1px solid #ccc;
            width: 0; height: 20px;
        }

        .tree li a{
            border: 1px solid #ccc;
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;

            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*Time for some hover effects*/
        /*We will apply the hover effect the the lineage of the element also*/
        .tree li a:hover, .tree li a:hover+ul li a {
            background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
        }
        /*Connector styles on hover*/
        .tree li a:hover+ul li::after,
        .tree li a:hover+ul li::before,
        .tree li a:hover+ul::before,
        .tree li a:hover+ul ul::before{
            border-color:  #94a0b4;
        }

        /*Thats all. I hope you enjoyed it.
        Thanks :)*/

    </style>



    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <!-- .row -->
    <input id="test" min="1" max="10" value='10' step="1" onchange="showVal(this.value)" type="range"/>
    <div class="row mainOH" style="overflow-y: auto;">
        <div class="col-md-12 col-xs-12 white-box " style=" width: 750%; overflow-y: scroll;">
            <div class="">
                <div class="tree zoom">
                    @php echo $HierarchyHTML
                    @endphp
                </div>
            </div>



        </div>
    </div>
    <!-- /.row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script>

        function focusToFirst() {
            var $this = $(this),
                $toElement      = '.tree',
                $focusElement   = ".firstMember",
                $offset         = $this.attr('data-scroll-offset') * 1 || 0,
                $speed          = $this.attr('data-scroll-speed') * 1 || 500;

            $('html, body').animate({
                scrollTop: $($toElement).offset().top + $offset
            }, $speed);

            if ($focusElement) $($focusElement).focus();
        }
        $(function () {
            focusToFirst()
        })
    </script>

    <script>
        function setZoom(zoom,el) {

            transformOrigin = [0,0];
            el = el || instance.getContainer();
            var p = ["webkit", "moz", "ms", "o"],
                s = "scale(" + zoom + ")",
                oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%";

            for (var i = 0; i < p.length; i++) {
                el.style[p[i] + "Transform"] = s;
                el.style[p[i] + "TransformOrigin"] = oString;
            }

            el.style["transform"] = s;
            el.style["transformOrigin"] = oString;

            focusToFirst();
        }

        function showVal(a){
            var zoomScale = Number(a)/10;
            setZoom(zoomScale,document.getElementsByClassName('zoom')[0])
        }


    </script>

@endpush

