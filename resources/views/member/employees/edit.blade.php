@extends($mUserTypeLayout)

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route($mUserType.'.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($mUserType.'.employees.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300i,400,400i,500,700,900">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .multi_step_form {
            /*background: #f6f9fb;*/
            display: block;
            overflow: hidden;
        }
        .multi_step_form #msform {
            text-align: center;
            position: relative;
            padding-top: 50px;
            min-height: 820px;
            /*max-width: 810px;*/
            margin: 0 auto;
            background: #ffffff;
            z-index: 1;
        }
        .multi_step_form #msform .tittle {
            text-align: center;
            padding-bottom: 55px;
        }
        .multi_step_form #msform .tittle h2 {
            font: 500 24px/35px "Roboto", sans-serif;
            color: #3f4553;
            padding-bottom: 5px;
        }
        .multi_step_form #msform .tittle p {
            font: 400 16px/28px "Roboto", sans-serif;
            color: #5f6771;
        }
        .multi_step_form #msform fieldset {
            border: 0;
            padding: 20px 105px 0;
            position: relative;
            width: 100%;
            left: 0;
            right: 0;
        }
        .multi_step_form #msform fieldset:not(:first-of-type) {
            display: none;
        }
        .multi_step_form #msform fieldset h3 {
            font: 500 18px/35px "Roboto", sans-serif;
            color: #3f4553;
        }
        .multi_step_form #msform fieldset h6 {
            font: 400 15px/28px "Roboto", sans-serif;
            color: #5f6771;
            padding-bottom: 30px;
        }
        .multi_step_form #msform fieldset .intl-tel-input {
            display: block;
            background: transparent;
            border: 0;
            box-shadow: none;
            outline: none;
        }
        .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag {
            padding: 0 20px;
            background: transparent;
            border: 0;
            box-shadow: none;
            outline: none;
            width: 65px;
        }
        .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow {
            border: 0;
        }
        .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow:after {
            content: "\f35f";
            position: absolute;
            top: 0;
            right: 0;
            font: normal normal normal 24px/7px Ionicons;
            color: #5f6771;
        }
        .multi_step_form #msform fieldset #phone {
            padding-left: 80px;
        }
        .multi_step_form #msform fieldset .form-group {
            padding: 0 10px;
        }
        .multi_step_form #msform fieldset .fg_2, .multi_step_form #msform fieldset .fg_3 {
            padding-top: 10px;
            display: block;
            overflow: hidden;
        }
        .multi_step_form #msform fieldset .fg_3 {
            padding-bottom: 70px;
        }
        .multi_step_form #msform fieldset .form-control, .multi_step_form #msform fieldset .product_select {
            border-radius: 3px;
            border: 1px solid #d8e1e7;
            /*padding: 0 20px;
            height: auto;*/
            font: 400 15px/48px "Roboto", sans-serif;
            color: #5f6771;
            box-shadow: none;
            outline: none;
            width: 100%;
        }
        .multi_step_form #msform fieldset .form-control.placeholder, .multi_step_form #msform fieldset .product_select.placeholder {
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .form-control:-moz-placeholder, .multi_step_form #msform fieldset .product_select:-moz-placeholder {
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .form-control::-moz-placeholder, .multi_step_form #msform fieldset .product_select::-moz-placeholder {
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .form-control::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select::-webkit-input-placeholder {
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .form-control:hover, .multi_step_form #msform fieldset .form-control:focus, .multi_step_form #msform fieldset .product_select:hover, .multi_step_form #msform fieldset .product_select:focus {
            border-color: #5cb85c;
        }
        .multi_step_form #msform fieldset .form-control:focus.placeholder, .multi_step_form #msform fieldset .product_select:focus.placeholder {
            color: transparent;
        }
        .multi_step_form #msform fieldset .form-control:focus:-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus:-moz-placeholder {
            color: transparent;
        }
        .multi_step_form #msform fieldset .form-control:focus::-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus::-moz-placeholder {
            color: transparent;
        }
        .multi_step_form #msform fieldset .form-control:focus::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select:focus::-webkit-input-placeholder {
            color: transparent;
        }
        .multi_step_form #msform fieldset .product_select:after {
            display: none;
        }
        .multi_step_form #msform fieldset .product_select:before {
            content: "\f35f";
            position: absolute;
            top: 0;
            right: 20px;
            font: normal normal normal 24px/48px Ionicons;
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .product_select .list {
            width: 100%;
        }
        .multi_step_form #msform fieldset .done_text {
            padding-top: 40px;
        }
        .multi_step_form #msform fieldset .done_text .don_icon {
            height: 36px;
            width: 36px;
            line-height: 36px;
            font-size: 22px;
            margin-bottom: 10px;
            background: #5cb85c;
            display: inline-block;
            border-radius: 50%;
            color: #ffffff;
            text-align: center;
        }
        .multi_step_form #msform fieldset .done_text h6 {
            line-height: 23px;
        }
        .multi_step_form #msform fieldset .code_group {
            margin-bottom: 60px;
        }
        .multi_step_form #msform fieldset .code_group .form-control {
            border: 0;
            border-bottom: 1px solid #a1a7ac;
            border-radius: 0;
            display: inline-block;
            width: 30px;
            font-size: 30px;
            color: #5f6771;
            padding: 0;
            margin-right: 7px;
            text-align: center;
            line-height: 1;
        }
        .multi_step_form #msform fieldset .passport {
            margin-top: -10px;
            padding-bottom: 30px;
            position: relative;
        }
        .multi_step_form #msform fieldset .passport .don_icon {
            height: 36px;
            width: 36px;
            line-height: 36px;
            font-size: 22px;
            position: absolute;
            top: 4px;
            right: 0;
            background: #5cb85c;
            display: inline-block;
            border-radius: 50%;
            color: #ffffff;
            text-align: center;
        }
        .multi_step_form #msform fieldset .passport h4 {
            font: 500 15px/23px "Roboto", sans-serif;
            color: #5f6771;
            padding: 0;
        }
        .multi_step_form #msform fieldset .input-group {
            padding-bottom: 40px;
        }
        .multi_step_form #msform fieldset .input-group .custom-file {
            width: 100%;
            height: auto;
        }
        .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label {
            width: 168px;
            border-radius: 5px;
            cursor: pointer;
            font: 700 14px/40px "Roboto", sans-serif;
            border: 1px solid #99a2a8;
            text-align: center;
            transition: all 300ms linear 0s;
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label i {
            font-size: 20px;
            padding-right: 10px;
        }
        .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:hover, .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:focus {
            background: #5cb85c;
            border-color: #5cb85c;
            color: #fff;
        }
        .multi_step_form #msform fieldset .input-group .custom-file input {
            display: none;
        }
        .multi_step_form #msform fieldset .file_added {
            text-align: left;
            padding-left: 190px;
            padding-bottom: 60px;
        }
        .multi_step_form #msform fieldset .file_added li {
            font: 400 15px/28px "Roboto", sans-serif;
            color: #5f6771;
        }
        .multi_step_form #msform fieldset .file_added li a {
            color: #5cb85c;
            font-weight: 500;
            display: inline-block;
            position: relative;
            padding-left: 15px;
        }
        .multi_step_form #msform fieldset .file_added li a i {
            font-size: 22px;
            padding-right: 8px;
            position: absolute;
            left: 0;
            transform: rotate(20deg);
        }
        .multi_step_form #msform #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
        }
        .multi_step_form #msform #progressbar li {
            list-style-type: none;
            color: #99a2a8;
            font-size: 9px;
            width: calc(100%/12);
            float: left;
            position: relative;
            font: 500 13px/1 "Roboto", sans-serif;
        }
        .multi_step_form #msform #progressbar li:nth-child(2):before {
            content: "2" !important;
        }
        .multi_step_form #msform #progressbar li:nth-child(3):before {
            content: "3" !important
        }
        .multi_step_form #msform #progressbar li:nth-child(4):before {
            content: "4" !important;
        }
        .multi_step_form #msform #progressbar li:nth-child(5):before {
            content: "5"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(6):before {
            content: "6"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(7):before {
            content: "7"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(8):before {
            content: "8"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(9):before {
            content: "9"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(10):before {
            content: "10"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(11):before {
            content: "11"!important;
        }

        .multi_step_form #msform #progressbar li:nth-child(12):before {
            content: "12"!important;
        }

        .multi_step_form #msform #progressbar li:before {
            content: "1" !important;
            font: normal normal normal 30px/50px simple-line-icons;
            width: 50px;
            height: 50px;
            line-height: 50px;
            display: block;
            background: #eaf0f4;
            border-radius: 50%;
            margin: 0 auto 10px auto;
        }
        .multi_step_form #msform #progressbar li:after {
            content: '';
            width: 100%;
            height: 10px;
            background: #eaf0f4;
            position: absolute;
            left: -50%;
            top: 21px;
            z-index: -1;
        }
        .multi_step_form #msform #progressbar li:last-child:after {
            width: 150%;
        }
        .multi_step_form #msform #progressbar li.active {
            color: #5cb85c;
        }
        .multi_step_form #msform #progressbar li.active:before, .multi_step_form #msform #progressbar li.active:after {
            background: #5cb85c;
            color: white;
        }
        .multi_step_form #msform .action-button {
            background: #5cb85c;
            color: white;
            border: 0 none;
            border-radius: 5px;
            cursor: pointer;
            min-width: 130px;
            font: 700 14px/40px "Roboto", sans-serif;
            border: 1px solid #5cb85c;
            margin: 0 5px;
            text-transform: uppercase;
            display: inline-block;
        }
        .multi_step_form #msform .action-button:hover, .multi_step_form #msform .action-button:focus {
            background: #405867;
            border-color: #405867;
        }
        .multi_step_form #msform .previous_button {
            background: transparent;
            color: #99a2a8;
            border-color: #99a2a8;
        }
        .multi_step_form #msform .previous_button:hover, .multi_step_form #msform .previous_button:focus {
            background: #405867;
            border-color: #405867;
            color: #fff;
        }

    </style>
    <style type="text/css">



        #signatureparent {
            color:#000000;
            /*color:darkblue;*/
            /*background-color:darkgrey;*/
            max-width:400px;
            padding:20px;
        }

        /*This is the div within which the signature canvas is fitted*/
        #signature {
            border: 2px dotted black;
            background-color:lightgrey;
        }
        .sigRow{
            margin: auto 24%;
        }
        canvas.jSignature{
            height: 160px !important;
            width: 338px !important;

        }


        @media all and (min-width:0px) and (max-width: 801px) {
            .sigRow{
                margin: auto 5%;
            }
            canvas.jSignature{
                height: 160px !important;

            }
            #signatureparent {
                color:#000000;
                max-width:600px;
                padding:20px;
            }
        }



    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> {{$panelHeading}}</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div>
                        <!-- Multi step form -->
                        <section class="multi_step_form">
                            {!! Form::open(['id'=>'msform','class'=>'ajax-form','method'=>'PUT','enctype'=>'multipart/form-data']) !!}
                            <div class="form-body">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active">Position</li>
                                    <li>Public Information</li>
                                    <li>Personal Information</li>
                                    <li>Contact Information</li>
                                    <li>Recruitment Information</li>
                                    <li>Benifits</li>
                                    <li>References</li>
                                    <li>Military Record</li>
                                    <li>Employment Record</li>
                                    <li>Documents</li>
                                    <li>Biometric</li>
                                    <li>Summary</li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset id="fs_1">
                                    <h2>Step 1 of 11</h2>
                                    <h3>Employee and ERP Type</h3>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Select ERP</label>
                                                <select name="ERPType" id="ERPType" class="form-control" required>
                                                    <option value="">Select</option>
                                                    <option value="odoo" @if($employeeDetail->erp_type == 'odoo') {{'selected'}} @endif>Odoo</option>
                                                    <option value="net" @if($employeeDetail->erp_type == 'net') {{'selected'}} @endif>.net</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Select employee type</label>
                                                <select name="employeeType" id="employeeType" class="form-control" required>
                                                    <option value="">Select</option>
                                                    <option value="guard" @if($employeeDetail->employee_type == 'guard') {{'selected'}} @endif > Guard </option>
                                                    <option value="staff" @if($employeeDetail->employee_type == 'staff') {{'selected'}} @endif > Staff </option>
                                                    <option value="CITStaff" @if($employeeDetail->employee_type == 'CITStaff') {{'selected'}} @endif > CIT staff </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Position</h3>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Center*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="center_id" id="center_id" class="form-control" required>
                                                    @if($centers->count() > 0)
                                                        <option value=""> Select Center </option>
                                                        @foreach($centers as $centersDataIn)
                                                            <option value="{{$centersDataIn->odoo_id}}" name="odoo_center_id" @if($employeeDetail->odoo_center_id == $centersDataIn->odoo_id) {{'selected'}} @endif>{{$centersDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Center Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Division</label>
                                                <select name="divisions" id="divisions" class="form-control divisions">
                                                    @if(count($divisions) > 0)
                                                        <option value="">Select Division</option>
                                                        @foreach($divisions as $division)
                                                            <option value="{{$division->id}}" @if($division->id == $employeeDetail->division_id) {{'selected'}} @endif>{{$division->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Division Found</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Department</label>
                                                <select name="departments" id="departments" class="form-control departments">
                                                    @if($departments->count() > 0)
                                                        <option value=""> Select Department</option>
                                                        @foreach($departments as $department)
                                                            <option value="{{$department->id}}" @if($department->id== $employeeDetail->department_id) {{'selected'}} @endif>{{$department->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">Department Not Found</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        {{--js For max child -- Qasim--}}
                                        <div class="col-md-3" id="maxChildParentDiv" style="display: @if($employeeDetail->employee_type == 'guard') block  @else none @endif;">
                                            <div class="form-group">
                                                <label>Max Child Parent*</label>
                                                <select name="maxChildParent" id="maxChildParent" class="form-control maxChildParent">
                                                    @if($maxParent->count() > 0)
                                                        <option value=""> Select Parent </option>
                                                        @foreach($maxParent as $maxParentIn)
                                                            <option value="{{$maxParentIn->id}}" @if($maxParentIn->id == $employeeDetail->position_parent_id) {{'selected'}} @endif>{{$maxParentIn->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Parent</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        {{--js For max child -- Qasim--}}

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Position*</label>
                                                <select name="positions" id="positions" class="form-control positions" required>
                                                    @if($positions->count() > 0)
                                                        <option value=""> Select Position </option>
                                                        @foreach($positions as $positionDataIn)
                                                            <option value="{{$positionDataIn->id}}" @if($positionDataIn->id == $employeeDetail->position_id) {{'selected'}} @endif>{{$positionDataIn->po_level_name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Position</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                    <button type="button" class="next action-button">Continue</button>
                                    <button type="button" class="action-button" id="skip-save-form">Skip And Save</button>
                                </fieldset>
                                <fieldset id="fs_2">
                                    <h2>Step 2 of 11</h2>
                                    <h3>Public Information</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>First Name*</label>
                                                <input type="text" name="first_name" value="{{ $employeeDetail->first_name }}" id="first_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Middle Name*</label>
                                                <input type="text" name="middle_name" value="{{ $employeeDetail->middle_name }}" id="middle_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Last Name*</label>
                                                <input type="text" name="last_name" value="{{ $employeeDetail->last_name }}" id="last_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Father Name*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="fatherName" value="{{ $employeeDetail->father_name }}" id="fatherName" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Mother Name*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="mother_name" value="{{ $employeeDetail->mother_name }}" id="mother_name" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nationality*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="nationality" value="{{ $employeeDetail->country_name }}" id="nationality" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Home City*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="home_city" value="{{ $employeeDetail->home_city }}" id="home_city" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Last Qualification*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="last_qualification" value="{{ $employeeDetail->last_qualification }}" id="last_qualification" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Height(Centimeters)*</label>
                                                <input type="text" name="height" value="{{ $employeeDetail->height }}" id="height" class="form-control" required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Weight(Kilogram)*  </label>
                                                <input type="text" name="weight" value="{{ $employeeDetail->weight }}" id="weight" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tag*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="category_ids" id="category_ids" class="form-control" required>
                                                    <option @if($employeeDetail->category_ids == 'Civil') selected
                                                            @endif
                                                            value="Civil">Civil</option>
                                                    <option @if($employeeDetail->category_ids == 'Full Time') selected
                                                            @endif
                                                            value="Full Time">Full Time</option>
                                                    <option @if($employeeDetail->category_ids == 'Part Time') selected
                                                            @endif
                                                            value="Part Time">Part Time</option>
                                                    <option @if($employeeDetail->category_ids == 'Army') selected
                                                            @endif
                                                            value="Army">Army</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_3">
                                    <h2>Step 3 of 11</h2>
                                    <h3>Personal Information</h3>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>CNIC*{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="cnic" id="cnic" value="{{ $employeeDetail->employee_cnic }}" class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>CNIC Expiry*</label>
                                                <input type="text" name="cnic_expiry" id="cnic_expiry" value="{{ $employeeDetail->cnic_expiry }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date Of Birth*</label>
                                                <input type="text" name="birthday" id="birthday" value="{{ $employeeDetail->birthday }}" class="form-control" required>

                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.gender')*</label>
                                                <select name="gender" id="gender" class="form-control">
                                                    <option @if($userDetail->gender == 'male') selected
                                                            @endif value="male">@lang('app.male')</option>
                                                    <option @if($userDetail->gender == 'female') selected
                                                            @endif value="female">@lang('app.female')</option>
                                                    <option @if($userDetail->gender == 'others') selected
                                                            @endif value="others">@lang('app.others')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Marital Status*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="marital_status" id="marital_status" class="form-control" required>
                                                    <option @if($employeeDetail->marital_status == 'Single') selected
                                                            @endif
                                                            value="Single">Single</option>
                                                    <option @if($employeeDetail->marital_status == 'Married') selected
                                                            @endif
                                                            value="Married">Married</option>
                                                    <option @if($employeeDetail->marital_status == 'Widower') selected
                                                            @endif
                                                            value="Widower">Widower</option>
                                                    <option @if($employeeDetail->marital_status == 'Divorced') selected
                                                            @endif
                                                            value="Divorced">Divorced</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Number Of Dependents*</label>
                                                <input type="text" name="no_of_child" value="{{ $employeeDetail->number_of_children }}" id="no_of_child" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank Name*{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="bank_name" value="{{ $employeeDetail->bank_name }}" id="bank_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank Branch*</label>
                                                <input type="text" name="bank_branch" value="{{ $employeeDetail->bank_branch }}" id="bank_branch" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Bank City*</label>
                                                <input type="text" name="bank_city" value="{{ $employeeDetail->bank_city }}" id="bank_city" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Account Title*</label>
                                                <input type="text" name="account_title" value="{{ $employeeDetail->bank_account_title }}" id="account_title" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Account No*</label>
                                                <input type="text" name="account_no" value="{{ $employeeDetail->bank_account_number }}" id="account_no" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Blood Group*</label>
                                                <input type="text" name="blood_group" value="{{ $employeeDetail->blood_group }}" id="blood_group" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>



                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_4">
                                    <h2>Step 4 of 11</h2>
                                    <h3>Contact Information</h3>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>@lang('app.mobile')*</label>
                                                <input type="tel" name="mobile" value="{{ $userDetail->mobile }}" id="mobile"  class="form-control" autocomplete="nope" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Land Line No*</label>
                                                <input type="text" name="land_line_number" value="{{ $employeeDetail->land_line_number }}" id="land_line_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Personal Email</label>
                                                <input type="email" name="personal_email" value="{{ $employeeDetail->personal_email }}" id="personal_email" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Permanent @lang('app.address')*</label>
                                                <textarea name="address" id="address"  rows="1" class="form-control" required>{{ $employeeDetail->address }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Working @lang('app.address')</label>
                                                <textarea name="working_address" id="working_address"  rows="1" class="form-control">{{ $employeeDetail->work_address }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Name*</label>
                                                <input type="text" name="kin_1_name" value="{{ $employeeDetail->kin_1_name }}" id="kin_1_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Number*</label>
                                                <input type="text" name="kin_1_number" value="{{ $employeeDetail->kin_1_number }}" id="kin_1_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Next Of Kin 1 Relation*</label>
                                                <input type="text" name="kin_1_relation" value="{{ $employeeDetail->kin_1_relation }}" id="kin_1_relation" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Name*</label>
                                                <input type="text" name="kin_2_name" value="{{ $employeeDetail->kin_2_name }}" id="kin_2_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Number*</label>
                                                <input type="text" name="kin_2_number" value="{{ $employeeDetail->kin_2_number }}" id="kin_2_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Next Of Kin 2 Relation*</label>
                                                <input type="text" name="kin_2_relation" value="{{ $employeeDetail->kin_2_relation }}" id="kin_2_relation" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>


                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_5">
                                    <h2>Step 5 of 11</h2>
                                    <h3>Recruitment Information</h3>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Basic Monthly Salary*</label>
                                                <input type="text" name="basic_salary" value="{{ $employeeDetail->basic_salary }}" id="basic_salary" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Hours a Day to Work*</label>
                                                <input type="text" name="duty_hours" value="{{ $employeeDetail->duty_hours }}" id="duty_hours" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Days to Work per month*</label>
                                                <input type="text" name="duty_days" value="{{ $employeeDetail->duty_days }}" id="duty_days" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.joiningDate')*</label>
                                                <input type="text" name="joining_date" value="{{ $employeeDetail->joining_date }}" id="joining_date" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Annual Leaves*</label>
                                                <input type="text" name="annual_leaves" value="{{ $employeeDetail->annual_leaves }}" id="annual_leaves" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Type of Attendance Logging*</label>
                                                <select name="attendance_logging" id="attendance_logging" class="form-control" required>
                                                    <option @if($employeeDetail->attendance_logging == 'Biometric') selected
                                                            @endif
                                                            value="Biometric">Biometric</option>
                                                    <option @if($employeeDetail->attendance_logging == 'InterCoApp') selected
                                                            @endif
                                                            value="InterCoApp">InterCoApp</option>
                                                    <option @if($employeeDetail->attendance_logging == 'Manual') selected
                                                            @endif
                                                            value="Manual">Manual</option>
                                                    <option @if($employeeDetail->attendance_logging == 'Mobile App Based Track') selected
                                                            @endif
                                                            value="Mobile App Based Track">Mobile App Based Track</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Recruitment Type*</label>
                                                <select name="rec_type" id="rec_type" class="form-control" required>
                                                    <option @if($employeeDetail->rec_type == 'Onsite') selected
                                                            @endif
                                                            value="Onsite">On Site</option>
                                                    <option @if($employeeDetail->rec_type == 'Offsite') selected
                                                            @endif
                                                            value="Offsite">Off Site</option>
                                                    <option @if($employeeDetail->rec_type == 'Remote') selected
                                                            @endif
                                                            value="Remote">Remote</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Travel Benefits*</label>
                                                <input type="text" name="travel_benefits" value="{{ $employeeDetail->travel_benefits }}" id="travel_benefits" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_6">
                                    <h2>Step 6 of 11</h2>
                                    <h3>Benifits</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="email_pro" name="email_pro" value="true" type="checkbox" @if($employeeDetail->email_pro == true) {{'checked'}} @endif>
                                                    <label for="email_pro"> Email?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="sim" name="sim" value="true" type="checkbox" @if($employeeDetail->sim == true) {{'checked'}} @endif>
                                                    <label for="sim"> Sim?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="mobile_phone" name="mobile_phone" value="true" type="checkbox" @if($employeeDetail->mobile_phone == true) {{'checked'}} @endif>
                                                    <label for="mobile_phone"> Mobile Phone?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="office" name="office" value="true" type="checkbox" @if($employeeDetail->office == true) {{'checked'}} @endif>
                                                    <label for="office"> Office?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="office_stationary" name="office_stationary" value="true" type="checkbox" @if($employeeDetail->office_stationary == true) {{'checked'}} @endif>
                                                    <label for="office_stationary"> Office Stationary?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="pc" name="pc" value="true" type="checkbox" @if($employeeDetail->pc == true) {{'checked'}} @endif>
                                                    <label for="pc"> PC?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="laptop" name="laptop" value="true" type="checkbox" @if($employeeDetail->laptop == true) {{'checked'}} @endif>
                                                    <label for="laptop"> Laptop?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="vehicle" name="vehicle" value="true" type="checkbox" @if($employeeDetail->vehicle == true) {{'checked'}} @endif>
                                                    <label for="vehicle"> Vehicle?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="uniform" name="uniform" value="true" type="checkbox" @if($employeeDetail->uniform == true) {{'checked'}} @endif>
                                                    <label for="uniform"> Uniform?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="belt" name="belt" value="true" type="checkbox" @if($employeeDetail->belt == true) {{'checked'}} @endif>
                                                    <label for="belt"> Belt?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="shoes" name="shoes" value="true" type="checkbox" @if($employeeDetail->shoes == true) {{'checked'}} @endif>
                                                    <label for="shoes"> Shoes?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="caps" name="caps" value="true" type="checkbox" @if($employeeDetail->caps == true) {{'checked'}} @endif>
                                                    <label for="caps"> Caps?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="badges" name="badges" value="true" type="checkbox" @if($employeeDetail->badges == true) {{'checked'}} @endif>
                                                    <label for="badges"> Badges?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_7">
                                    <h2>Step 7 of 11</h2>
                                    <h3>References</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 1 Name*</label>
                                                <input type="text" name="ref_1_name" value="{{ $employeeDetail->ref_1_name }}" id="ref_1_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 1 Number*</label>
                                                <input type="text" name="ref_1_number" value="{{ $employeeDetail->ref_1_number }}" id="ref_1_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Relation to Refree 1* </label>
                                                <input type="text" name="rel_to_ref1" value="{{ $employeeDetail->rel_to_ref1 }}" id="rel_to_ref1" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 1 Address*</label>
                                                <input type="text" name="ref_1_address" value="{{ $employeeDetail->ref_1_address }}" id="ref_1_address" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 1 CNIC*</label>
                                                <input type="text" name="ref_1_cnic" value="{{ $employeeDetail->ref_1_cnic }}" id="ref_1_cnic" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 2 Name*</label>
                                                <input type="text" name="ref_2_name" value="{{ $employeeDetail->ref_2_name }}" id="ref_2_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Refree 2 Number*</label>
                                                <input type="text" name="ref_2_number" value="{{ $employeeDetail->ref_2_number }}" id="ref_2_number" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Relation to Refree 2*</label>
                                                <input type="text" name="rel_to_ref2" value="{{ $employeeDetail->rel_to_ref2 }}" id="rel_to_ref2" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 2 Address*</label>
                                                <input type="text" name="ref_2_address" value="{{ $employeeDetail->ref_2_address }}" id="ref_2_address" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Refree 2 CNIC*</label>
                                                <input type="text" name="ref_2_cnic" value="{{ $employeeDetail->ref_2_cnic }}" id="ref_2_cnic" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_8">
                                    <h2>Step 8 of 11</h2>
                                    <h3>Military Record</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="military_person" name="military_person" value="true" type="checkbox" @if($employeeDetail->military_person == true) {{'checked'}} @endif>
                                                    <label for="military_person"> Ex. Military Personal?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group force_no" style="display: {{$isMilitryRecordShow}}">
                                                <label>Force No</label>
                                                <input type="text" name="force_no" value="{{ $employeeDetail->force_no }}" id="force_no" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group m_joining_date" style="display: {{$isMilitryRecordShow}}">
                                                <label>Joining Date in Military</label>
                                                <input type="text" name="m_joining_date" value="{{ $employeeDetail->m_joining_date }}" id="m_joining_date" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group m_leaving_date" style="display: {{$isMilitryRecordShow}};">
                                                <label>Leaving Date in Military</label>
                                                <input type="text" name="m_leaving_date" value="{{ $employeeDetail->m_leaving_date }}" id="m_leaving_date" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group force_type" style="display: {{$isMilitryRecordShow}}">
                                                <label>Force Type</label>
                                                <input type="text" name="force_type" value="{{ $employeeDetail->force_type }}" id="force_type" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group force_rank" style="display: {{$isMilitryRecordShow}}">
                                                <label>Force Rank</label>
                                                <input type="text" name="force_rank" value="{{ $employeeDetail->force_rank }}" id="force_rank" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group last_unit" style="display: {{$isMilitryRecordShow}};">
                                                <label>Last Unit</label>
                                                <input type="text" name="last_unit" value="{{ $employeeDetail->last_unit }}" id="last_unit" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group last_center" style=" display: {{$isMilitryRecordShow}};">
                                                <label>Last Center</label>
                                                <input type="text" name="last_center" value="{{ $employeeDetail->last_center }}" id="last_center" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_9">
                                    <h2>Step 9 of 11</h2>
                                    <h3>Employment Record</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input type="text" name="last_company_name" value="{{ $employeeDetail->last_company_name }}" id="last_company_name" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Position</label>
                                                <input type="text" name="last_position" value="{{ $employeeDetail->last_position }}" id="last_position" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Joining Date</label>
                                                <input type="text" name="last_joining_date" value="{{ $employeeDetail->force_rank }}" id="last_joining_date" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Leaving Date</label>
                                                <input type="text" name="last_leaving_date" value="{{ $employeeDetail->last_leaving_date }}" id="last_leaving_date" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" name="last_working_city" value="{{ $employeeDetail->last_working_city }}" id="last_working_city" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" name="last_working_country" value="{{ $employeeDetail->last_working_country }}" id="last_working_country" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_10">
                                    <h2>Step 10 of 11</h2>
                                    <h3>Documents</h3>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id="results"
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($userDetail->image))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$userDetail->image }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview profileImage fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Profile Pics</label>
                                                        <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_profile" name="image_profile">

                                </span>
                                                            <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                               data-dismiss="fileinput"> @lang('app.remove') </a>
                                                            <span class="">
                                <input type=button value="Load Camera" id="load_cam" class="btn btn-success" onClick="load_camera()">
                                <input style="display: none" id="take_pic" type=button value="Take Snapshot" class="btn btn-success" onClick="take_snapshot()">
                                <input style="display: none" id="close_cam" type=button value="Close Camera" class="btn btn-success" onClick="close_camera()">

                                </span>






                                                            <input type="hidden" name="image_profile_cam" id="image_profile_cam" class="image-tag">

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div id="my_camera"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_cnic))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_cnic }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>

                                                    <div>
                                                        <label>Upload CNIC Copy*</label>
                                                        <span class="btn btn-info btn-file">
                                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                                    <input type="file" id="image_cnic" name="image_cnic" @if(is_null($employeeDetail->image_cnic)) required @endif> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput" >
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_police_varification))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_police_varification }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Police Varification</label>
                                                        <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> @lang('app.selectImage') </span>
                                                <span class="fileinput-exists"> @lang('app.change') </span>
                                                <input type="file" id="image_police_varification" name="image_police_varification">
                                                </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_education))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_education }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Education</label>
                                                        <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_education" name="image_education"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                    </div>



                                    <div class="row">


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_pen_book))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_pen_book }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Discharge/Pension Book Copy</label>
                                                        <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_pen_book" name="image_pen_book"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_referee_undertaking))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_referee_undertaking }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Referee's Undertaking*</label>
                                                        <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_referee_undertaking" @if(is_null($employeeDetail->image_referee_undertaking)) required @endif name="image_referee_undertaking"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>



                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_emp_aggre))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_emp_aggre }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Employment Agreement</label>
                                                        <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_emp_aggre" name="image_emp_aggre"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Varification Date</label>
                                                <input type="text" name="varification_date" value="{{ $employeeDetail->varification_date }}" id="varification_date" class="form-control" >
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail " id=""
                                                         style="width: 200px; height: 150px;">
                                                        @if(is_null($employeeDetail->image_nadra))
                                                            <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"
                                                                 alt=""/>
                                                        @else
                                                            <img src="{{ $employeeFolder.$employeeDetail->image_nadra }}"
                                                                 alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <label>Upload Nadra Attested</label>
                                                        <span class="btn btn-info btn-file">
                                                <span class="fileinput-new"> @lang('app.selectImage') </span>
                                                <span class="fileinput-exists"> @lang('app.change') </span>
                                                <input type="file" id="image_nadra" name="image_nadra"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_11">
                                    <h2>Step 11 of 11</h2>
                                    <h3>Biometric</h3>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 1</label>
                                                <input type="text" name="left_fin_1" value="{{ $employeeDetail->left_fin_1 }}" id="left_fin_1" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 2</label>
                                                <input type="text" name="left_fin_2" value="{{ $employeeDetail->left_fin_2 }}" id="left_fin_2" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 3</label>
                                                <input type="text" name="left_fin_3" value="{{ $employeeDetail->left_fin_3 }}" id="left_fin_3" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left Finger 4</label>
                                                <input type="text" name="left_fin_4" value="{{ $employeeDetail->left_fin_4 }}" id="left_fin_4" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Left thumb</label>
                                                <input type="text" name="left_thumb" value="{{ $employeeDetail->left_thumb }}" id="left_thumb" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Thumb</label>
                                                <input type="text" name="right_thumb" value="{{ $employeeDetail->right_thumb }}" id="right_thumb" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Finger 1</label>
                                                <input type="text" name="right_fin_1" value="{{ $employeeDetail->right_fin_1 }}" id="right_fin_1" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Right Finger 2</label>
                                                <input type="text" name="right_fin_2" value="{{ $employeeDetail->right_fin_2 }}" id="right_fin_2" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Right Finger 3</label>
                                                <input type="text" name="right_fin_3" value="{{ $employeeDetail->right_fin_3 }}" id="right_fin_3" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Right Finger 4</label>
                                                <input type="text" name="right_fin_4" value="{{ $employeeDetail->right_fin_4 }}" id="right_fin_4" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset id="fs_12">
                                    <h3>Summary</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row sigRow" style="">
                                                <br/>
                                                <br/>
                                                <b style="margin: auto 30%;">Signature</b>
                                                <img src="{{$employeeFolder.$employeeDetail->signature_image}}" id="sigImage" alt="Signature Image" style="display: @if($employeeDetail->signature_image != '')block @else none @endif;">
                                                <input type="hidden" name="sigImageInput" id="sigImageInput">
                                                <div id="signatureparent" style="display: @if($employeeDetail->signature_image == '')block @else none @endif;">
                                                    <div id="signature"></div>
                                                </div>

                                                <br/>

                                                <div style="margin: auto 15%;">
                                                    <button type="button" onclick="drawSig()" value="">Draw Signature</button>
                                                    {{--<button type="button" onclick="resetSig()">Reset</button>--}}
                                                    <button type="button" onclick="save()">Save</button>
                                                </div>

                                                <br/>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @if(isset($fields))
                                            @foreach($fields as $field)
                                                <div class="col-md-6">
                                                    <label>{{ ucfirst($field->label) }}</label>
                                                    <div class="form-group">
                                                        @if( $field->type == 'text')
                                                            <input type="text" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'password')
                                                            <input type="password" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'number')
                                                            <input type="number" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">

                                                        @elseif($field->type == 'textarea')
                                                            <textarea name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" id="{{$field->name}}" cols="3">{{$editUser->custom_fields_data['field_'.$field->id] or ''}}</textarea>

                                                        @elseif($field->type == 'radio')
                                                            <div class="radio-list">
                                                                @foreach($field->values as $key=>$value)
                                                                    <label class="radio-inline @if($key == 0) p-0 @endif">
                                                                        <div class="radio radio-info">
                                                                            <input type="radio" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" id="optionsRadios{{$key.$field->id}}" value="{{$value}}" @if(isset($editUser) && $editUser->custom_fields_data['field_'.$field->id] == $value) checked @elseif($key==0) checked @endif>>
                                                                            <label for="optionsRadios{{$key.$field->id}}">{{$value}}</label>
                                                                        </div>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'select')
                                                            {!! Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                                    $field->values,
                                                                     isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender'])
                                                             !!}

                                                        @elseif($field->type == 'checkbox')
                                                            <div class="mt-checkbox-inline">
                                                                @foreach($field->values as $key => $value)
                                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                                        <input name="custom_fields_data[{{$field->name.'_'.$field->id}}][]" type="checkbox" value="{{$key}}"> {{$value}}
                                                                        <span></span>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'date')
                                                            <input type="text" class="form-control date-picker" size="16" name="custom_fields_data[{{$field->name.'_'.$field->id}}]"
                                                                   value="{{ isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format('m/d/Y')}}">
                                                        @endif
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>

                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                    <button type="button" class="action-button previous previous previous_button">Back</button>
                                    <button class="action-button" id="save-button">Finish</button>
                                </fieldset>
                            </div>
                            {!! Form::close() !!}
                        </section>
                        <!-- End Multi step form -->
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
    <script>
        (function($) {
            "use strict";

            /*window.onload = function () {
                var date = '2018-12-25';//new Date().toISOString().substring(0, 10);
                $('#ERPType').val('oodo');
                $('#employeeType').val('CITStaff');
                $('#center_id').val(27);
                $('#divisions').val(127);
                $('#departments').val(3);
                $('#positions').val(182);
                $('#first_name').val('firstNameUpdate');
                $('#middle_name').val('middleNameUpdate');
                $('#last_name').val('lastNameUpdate');
                $('#fatherName').val('fatherNameUpdate');
                $('#mother_name').val('motherNomeUpdate');
                $('#nationality').val('nationalityUpdate');
                $('#home_city').val('homeCityUpdate');
                $('#last_qualification').val('lastQualificationUpdate');
                $('#height').val('1');
                $('#weight').val('2');
                $('#category_ids').val('Full Time');
                $('#cnic').val('cnic');
                $('#cnic_expiry').val(date);
                $('#birthday').val(date);
                $('#gender').val('female');
                $('#marital_status').val('Widower');
                $('#no_of_child').val(5);
                $('#bank_name').val('bankNameUpdate');
                $('#bank_branch').val('bankBranchUpdate');
                $('#bank_city').val('bankCityUpdate');
                $('#account_title').val('accountTitleUpdate');
                $('#account_no').val('accountNoUpdate');
                $('#blood_group').val('bloodGroupUpdate');
                $('#mobile').val('MobileUpdate');
                $('#land_line_number').val('landLineNumberUpdate');
                $('#personal_email').val('personalEmailUpdate@gmail.com');
                $('#address').val('AddressUpdate');
                $('#working_address').val('workingAddressUpdate');
                $('#kin_1_name').val('kin_1_nameUpdate');
                $('#kin_1_number').val('kin_1_numberUpdate');
                $('#kin_1_relation').val('kin_1_relationUpdate');
                $('#kin_2_name').val('kin_2_nameUpdate');
                $('#kin_2_number').val('kin_2_number');
                $('#kin_2_relation').val('kin_2_relationUpdate');
                $('#basic_salary').val('basicSalaryUpdate');
                $('#joining_date').val(date);
                $('#annual_leaves').val('annualLeavesUpdate');
                $('#attendance_logging').val('Manual');
                $('#rec_type').val('Remote');
                $('#travel_benefits').val('travelBenefitsUpdate');
                $('#email_pro').attr('checked',false);
                $('#sim').attr('checked',false);
                $('#mobile_phone').attr('checked',false);
                $('#office').attr('checked',false);
                $('#office_stationary').attr('checked',false);
                $('#pc').attr('checked',false);
                $('#laptop').attr('checked',false);
                $('#vehicle').attr('checked',false);
                $('#uniform').attr('checked',false);
                $('#belt').attr('checked',false);
                $('#shoes').attr('checked',false);
                $('#caps').attr('checked',false);
                $('#badges').attr('checked',false);
                $('#ref_1_name').val('ref_1_nameUpdate');
                $('#ref_1_number').val('ref_1_numberUpdate');
                $('#rel_to_ref1').val('rel_to_ref1Update');
                $('#ref_1_address').val('ref_1_addressUpdate');
                $('#ref_1_cnic').val('ref_1_cnicUpdate');
                $('#ref_2_name').val('ref_2_nameUpdate');
                $('#ref_2_number').val('ref_2_numberUpdate');
                $('#rel_to_ref2').val('rel_to_ref2Update');
                $('#ref_2_address').val('ref_2_addressUpdate');
                $('#ref_2_cnic').val('ref_2_cnicUpdate');
                $('#force_no').val('force_noUpdate');
                $('#m_joining_date').val(date);
                $('#m_leaving_date').val(date);
                $('#force_type').val('force_typeUpdate');
                $('#force_rank').val('force_rankUpdate');
                $('#last_unit').val('last_unitUpdate');
                $('#last_center').val('last_centerUpdate');
                $('#last_company_name').val('last_company_nameUpdate');
                $('#last_position').val('last_positionUpdate');
                $('#last_joining_date').val(date);
                $('#last_leaving_date').val(date);
                $('#last_working_city').val('last_working_cityUpdate');
                $('#last_working_country').val('last_working_countryUpdate');
                $('#varification_date').val(date);
                $('#left_fin_1').val('left_fin_1Update');
                $('#left_fin_2').val('left_fin_2Update');
                $('#left_fin_3').val('left_fin_3Update');
                $('#left_fin_4').val('left_fin_4Update');
                $('#left_thumb').val('left_thumbUpdate');
                $('#right_thumb').val('right_thumbUpdate');
                $('#right_fin_1').val('right_fin_1Update');
                $('#right_fin_2').val('right_fin_2Update');
                $('#right_fin_3').val('right_fin_3Update');
                $('#right_fin_4').val('right_fin_4Update');
            }*/

            //* Form js
            function verificationForm(){
                //jQuery time
                var current_fs, next_fs, previous_fs; //fieldsets
                var left, opacity, scale; //fieldset properties which we will animate
                var animating; //flag to prevent quick multi-click glitches

                $(".next").click(function () {
                    $('#msform').validate();
                    if (!$('#msform').valid()) {
                        return false
                    }

                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                /*'position': 'absolute'*/
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });


                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });


                $(".previous").click(function () {
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    previous_fs = $(this).parent().prev();

                    //de-activate current step on progressbar
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the previous fieldset
                    previous_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale previous_fs from 80% to 100%
                            scale = 0.8 + (1 - now) * 0.2;
                            //2. take current_fs to the right(50%) - from 0%
                            left = ((1 - now) * 50) + "%";
                            //3. increase opacity of previous_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'left': left
                            });
                            previous_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'opacity': opacity
                            });
                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                    $('html, body').animate({
                        scrollTop: "80px"
                    }, 800);
                });

                // $(".submit").click(function () {
                //     return false;
                // })
            };

            //* Add Phone no select
            function phoneNoselect(){
                if ( $('#msform').length ){
                    $("#phone").intlTelInput();
                    $("#phone").intlTelInput("setNumber", "+880");
                };
            };
            //* Select js
            function nice_Select(){
                if ( $('.product_select').length ){
                    $('select').niceSelect();
                };
            };
            /*Function Calls*/
            verificationForm ();
            //phoneNoselect ();
            //nice_Select ();
        })(jQuery);
        $('#save-button').click(function () {
            $.easyAjax({
                url: '{{route($mUserType.'.employees.updateOdoo', [$userDetail->id])}}',
                container: '#msform',
                type: "POST",
                redirect: true,
                file:  true,
                data: $('#msform').serialize()
            })
        });



    </script>
    <script>

        $("#joining_date").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $("#birthday").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        })

        $("#cnic_expiry").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $("#m_joining_date").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $("#m_leaving_date").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $("#last_joining_date").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $("#varification_date").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route($mUserType.'.employees.store')}}',
                container: '#msform',
                type: "POST",
                redirect: true,
                file: (document.getElementById("image").files.length == 0) ? false : true,
                data: $('#msform').serialize()
            })
        });
        $('#skip-save-form').click(function () {
            $.easyAjax({
                url: '{{route($mUserType.'.employees.updateOdoo', [$userDetail->id])}}',
                container: '#msform',
                type: "POST",
                redirect: true,
                file:  true,
                data: $('#msform').serialize()
            })
        });

        $('#random_password').change(function () {
            var randPassword = $(this).is(":checked");

            if(randPassword){
                $('#password').val('{{ str_random(8) }}');
                $('#password').attr('readonly', 'readonly');
            }
            else{
                $('#password').val('');
                $('#password').removeAttr('readonly');
            }
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script src="{{asset('jSignature/flashcanvas.js')}}"></script>
    <script src="{{asset('jSignature/jquery.js')}}"></script>

    <script src="{{asset('jSignature/jSignature.min.noconflict.js')}}"></script>

    <script type="text/javascript">
        jQuery.noConflict()
    </script>

    <script>
        var $sigdiv = [];
        (function($){

            $(document).ready(function() {

                // This is the part where jSignature is initialized.
                $sigdiv = $("#signature").jSignature({'UndoButton':false})

                <?php if(isset($id) && $id != '' && $user->directory != ''){?>
                $sigdiv.jSignature("setData", '<?php echo $user->directory?>');
                <?php }?>
            })

        })(jQuery)

    </script>
    <script>
        function drawSig() {
            $sigdiv.jSignature('reset');
            $("#sigImageInput").val('');
            $("#sigImage").hide();
            $("#signatureparent").show();
        }
        function resetSig() {
            $sigdiv.jSignature('reset');
        }
        function save() {
            var sigImage =  $sigdiv.jSignature('getData');
            jQuery("#sigImageInput").val(sigImage);
            jQuery("#sigImage").attr('src', sigImage);
            jQuery("#sigImage").show();
            $sigdiv.jSignature('reset');
            jQuery("#signatureparent").hide();
        }
        Webcam.set({
            width: 300,
            height: 150,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        function load_camera(){
            Webcam.attach('#my_camera');
            $("#load_cam").hide();
            $("#take_pic").show();
        }

        function close_camera() {
            Webcam.reset('#my_camera');
            $("#load_cam").show();
            $("#take_pic").hide();
            $("#close_cam").hide();
        }

        function take_snapshot() {
            $("#close_cam").show();
            Webcam.snap(function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
                //document.getElementById('#image_profile_cam').value = data_uri;
                $("#image_profile_cam").val(data_uri);

            });

        }

        $('#military_person').on('change', function(){
            if ( $(this).is(':checked') ) {
                $('.force_no').show();
                $('.m_joining_date').show();
                $('.m_leaving_date').show();
                $('.force_type').show();
                $('.force_rank').show();
                $('.last_unit').show();
                $('.last_center').show();
            } else {
                $('.force_no').hide();
                $('.m_joining_date').hide();
                $('.m_leaving_date').hide();
                $('.force_type').hide();
                $('.force_rank').hide();
                $('.last_unit').hide();
                $('.last_center').hide();
            }
        });

        function validate_fs1() {

        }
    </script>
    {{--js For Division Filter start -- Qasim--}}
    <script>
        $(function(){
            $(".divisions").change(function () {
                var divisionId = $(this).val();
                if(divisionId != ''){
                    $.get( "{{route($mUserType.'.positions.ajaxGetDepartmentByDivision')}}/"+divisionId, {  }, function( data ) {
                        if(data.error == 0){
                            var departmentsHtml     = data.data.departments;
                            $(".departments").html('');
                            $(".departments").html(departmentsHtml);
                        }else{
                            var departmentsHtml     = data.data.departments;
                            $(".departments").html('');
                            $(".departments").html(departmentsHtml);
                        }
                    }, "json");
                }
            });

            $(".departments").change(function () {
                var departmentId = $(this).val();
                if(departmentId != ''){
                    $.get( "{{route($mUserType.'.positions.ajaxGetPositionByDepartment')}}/"+departmentId, {  }, function( data ) {
                        if(data.error == 0){
                            var positionsHtml     = data.data.positions;
                            $(".positions").html('');
                            $(".positions").html(positionsHtml);
                        }else{
                            var positionsHtml     = data.data.positions;
                            $(".positions").html('');
                            $(".positions").html(positionsHtml);
                        }
                    }, "json");
                }
            });

        });

        $(function(){
        })
    </script>
    {{--js For Division Filter end--}}

    {{--js For camera and profile picture start -- Qasim--}}
    <script>
        $(function () {
            $('.profileImage').bind("DOMSubtreeModified",function(){
                $('#image_profile_cam').val("");
                $('#results img').attr('src',"https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150");
            });
        })
    </script>
    {{--js For camera and profile picture end--}}

    {{--js For max child -- Qasim--}}
    <script>
        $(function () {
            $('#employeeType').change(function(){
                var val = $(this).val();
                if(val == 'guard'){
                    $("#maxChildParentDiv").show();
                }
                else{
                    $("#maxChildParent").val('');
                    $("#maxChildParentDiv").hide();
                    var departmentId = $('.departments').val();
                    if(departmentId != ''){
                        $.get( "{{route($mUserType.'.positions.ajaxGetPositionByDepartment')}}/"+departmentId, {  }, function( data ) {
                            if(data.error == 0){
                                var positionsHtml     = data.data.positions;
                                $(".positions").html('');
                                $(".positions").html(positionsHtml);
                            }else{
                                var positionsHtml     = data.data.positions;
                                $(".positions").html('');
                                $(".positions").html(positionsHtml);
                            }
                        }, "json");
                    }
                }
            })

            $("#maxChildParent").change(function () {
                var maxChildParentId = $(this).val();
                if(maxChildParentId != ''){
                    $.get( "{{route($mUserType.'.positions.ajaxGetPositionByPrentId')}}/"+maxChildParentId, {  }, function( data ) {
                        if(data.error == 0){
                            var positionsHtml     = data.data.positions;
                            $(".positions").html('');
                            $(".positions").html(positionsHtml);
                        }else{
                            var positionsHtml     = data.data.positions;
                            $(".positions").html('');
                            $(".positions").html(positionsHtml);
                        }
                    }, "json");
                }
            });
        })
    </script>
    {{--js js For max child--}}
@endpush


