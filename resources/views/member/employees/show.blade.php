@extends($mUserTypeLayout)

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route($mUserType.'.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($mUserType.'.employees.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>
        .counter{
            font-size: large;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="white-box">
                <div class="user-bg">
                    <img src="{{$employee->image}}"
                         alt="user" width="100%">

                    <div class="overlay-box">
                        <div class="user-content"> <a href="javascript:void(0)">
                                <img src="{{$employee->image}}"
                                     alt="user" class="thumb-lg img-circle">
                            </a>
                            <h4 class="text-white">{{ ucwords($employee->name) }}</h4>
                            <h5 class="text-white">{{ $position }}</h5>
                            <h5 class="text-white">{{ $age }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if($isGuard)
            <div class="col-md-6 col-xs-12" >
                <div class="white-box" style="padding:6px;">
                    <div class="row row-in">
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Days present</h3>
                                <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $daysPresent }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Days absent</h3>
                                <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $daysAbsent }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br  b-r-none">
                            <div class="col-in row">
                                <h3 class="box-title">Leaves taken</h3>
                                <div class="col-xs-2"><i class="icon-clock text-info"></i></div>
                                <div class="col-xs-10 text-right counter" >{{ count($leaves)}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-in">
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Leaves balance</h3>
                                <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                                <div class="col-xs-8 text-right counter">not Define</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Attendance %</h3>
                                <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $perOfAtt }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br  b-r-none">
                            <div class="col-in row">
                                <h3 class="box-title">Post assigned</h3>
                                <div class="col-xs-4"><i class="icon-logout text-danger"></i></div>
                                <div class="col-xs-8 text-right counter">Not define</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @else
            <div class="col-md-6 col-xs-12" >
                <div class="white-box" style="padding:6px;">
                    <div class="row row-in">
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Tasks assigned</h3>
                                <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $taskPending }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Assignments</h3>
                                <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $projects->count() }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br  b-r-none">
                            <div class="col-in row">
                                <h3 class="box-title">Tasks completed</h3>
                                <div class="col-xs-2"><i class="icon-clock text-info"></i></div>
                                <div class="col-xs-10 text-right counter" >{{ $taskCompleted }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-in">
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Attendance %</h3>
                                <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $perOfAtt }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br">
                            <div class="col-in row">
                                <h3 class="box-title">Leaves taken</h3>
                                <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                                <div class="col-xs-8 text-right counter">{{ count($leaves) }}</div>
                            </div>
                        </div>
                        <div class="col-md-4 row-in-br  b-r-none">
                            <div class="col-in row">
                                <h3 class="box-title">Balance leaves</h3>
                                <div class="col-xs-4"><i class="icon-logout text-danger"></i></div>
                                <div class="col-xs-8 text-right counter">Not define</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab"><a href="#generalProfile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">General Profile</span> </a> </li>
                    <li class="tab"><a href="#HRProfile" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">HR Profile</span> </a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="generalProfile">
                        <ul class="nav nav-tabs tabs customtab">
                            <li class="active tab"><a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('modules.employees.profile')</span> </a> </li>
                            <li class="tab"><a href="#projects" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs">Assignments(@lang('app.menu.projects'))</span> </a> </li>
                            <li class="tab"><a href="#tasks" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-list"></i></span> <span class="hidden-xs">@lang('app.menu.tasks')</span> </a> </li>
                            <li class="tab"><a href="#leaves" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-logout"></i></span> <span class="hidden-xs">@lang('app.menu.leaves')</span> </a> </li>

                            <li class="tab"><a href="#attendance" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">Attendance</span> </a> </li>
                            <li class="tab"><a href="#time-logs" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.timeLogs')</span> </a> </li>
                            <li class="tab"><a href="#activity" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('modules.employees.activity')</span> </a> </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>@lang('modules.employees.fullName')</strong> <br>
                                        <p class="text-muted">{{ ucwords($employee->name) }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('app.email')</strong> <br>
                                        <p class="text-muted">{{ $employee->email }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>@lang('app.mobile')</strong> <br>
                                        <p class="text-muted">{{ $employee->mobile or 'NA'}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Department</strong> <br>
                                        <p class="text-muted">{{$department}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Division</strong> <br>
                                        <p class="text-muted">{{$division}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Region</strong> <br>
                                        <p class="text-muted">{{$region}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('modules.employees.joiningDate')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? $employee->employee[0]->joining_date->format('d M, Y') : 'NA' }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 "> <strong>DOB</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? $employee->employee[0]->birthday : 'NA' }}</p>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="projects">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('app.project')</th>
                                            <th>@lang('app.deadline')</th>
                                            <th>@lang('app.completion')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($projects as $key=>$project)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td><a href="{{ route($mUserType.'.projects.show', $project->id) }}">{{ ucwords($project->project_name) }}</a></td>
                                                <td>{{ $project->deadline->format('d M, y') }}</td>
                                                <td>
                                                    <?php

                                                    if ($project->completion_percent < 50) {
                                                        $statusColor = 'danger';
                                                    }
                                                    elseif ($project->completion_percent >= 50 && $project->completion_percent < 75) {
                                                        $statusColor = 'warning';
                                                    }
                                                    else {
                                                        $statusColor = 'success';
                                                    }
                                                    ?>

                                                    <h5>@lang('app.completed')<span class="pull-right">{{  $project->completion_percent }}%</span></h5><div class="progress">
                                                        <div class="progress-bar progress-bar-{{ $statusColor }}" aria-valuenow="{{ $project->completion_percent }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project->completion_percent }}%" role="progressbar"> <span class="sr-only">{{ $project->completion_percent }}% @lang('app.completed')</span> </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">@lang('messages.noProjectFound')</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tasks">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="checkbox checkbox-info">
                                            <input type="checkbox" id="hide-completed-tasks">
                                            <label for="hide-completed-tasks">@lang('app.hideCompletedTasks')</label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                           id="tasks-table">
                                        <thead>
                                        <tr>
                                            <th>@lang('app.id')</th>
                                            <th>@lang('app.project')</th>
                                            <th>@lang('app.task')</th>
                                            <th>@lang('app.dueDate')</th>
                                            <th>@lang('app.status')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane" id="leaves">
                                <div class="row">

                                    <div class="col-md-6">
                                        <ul class="basic-list">
                                            @forelse($leaveTypes as $leaveType)
                                                <li>{{ ucfirst($leaveType->type_name) }}
                                                    <span class="pull-right label-{{ $leaveType->color }} label">{{ (isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0' }}</span>
                                                </li>
                                            @empty
                                                <li>@lang('messages.noRecordFound')</li>
                                            @endforelse
                                        </ul>
                                    </div>

                                </div>
                                <hr>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table" id="leave-table">
                                            <thead>
                                            <tr>
                                                <th>@lang('modules.leaves.leaveType')</th>
                                                <th>@lang('app.date')</th>
                                                <th>@lang('modules.leaves.reason')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($leaves as $key=>$leave)
                                                <tr>
                                                    <td>
                                                        <label class="label label-{{ $leave->type->color }}">{{ ucwords($leave->type->type_name) }}</label>
                                                    </td>
                                                    <td>
                                                        {{ $leave->leave_date->format('d M, Y') }}
                                                    </td>
                                                    <td>
                                                        {{ $leave->reason }}
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td>@lang('messages.noRecordFound')</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="attendance">
                                <div class="row">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>@lang('app.date')</th>
                                            <th>@lang('app.status')</th>
                                            <th>@lang('modules.attendance.clock_in')</th>
                                            <th>@lang('modules.attendance.clock_out')</th>
                                            <th>@lang('app.others')</th>
                                        </tr>
                                        </thead>
                                        <tbody id="attendanceData">
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="tab-pane" id="time-logs">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="timelog-table">
                                        <thead>
                                        <tr>
                                            <th>@lang('app.id')</th>
                                            <th>@lang('app.project')</th>
                                            <th>@lang('modules.employees.startTime')</th>
                                            <th>@lang('modules.employees.endTime')</th>
                                            <th>@lang('modules.employees.totalHours')</th>
                                            <th>@lang('modules.employees.memo')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>


                            </div>
                            <div class="tab-pane" id="activity">
                                <div class="steamline">
                                    @forelse($activities as $key=>$activity)
                                        <div class="sl-item">
                                            <div class="sl-left">
                                                {!!  ($employee->image) ? '<img src="'.$activity->user->employeeImage.'"
                                                                                alt="user" class="img-circle">' : '<img src="'.$activity->user->employeeImage.'"
                                                                                                                        alt="user" class="img-circle">' !!}
                                            </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"><a href="#" class="text-info">{{ ucwords($employee->name) }}</a> <span  class="sl-date">{{ $activity->created_at->diffForHumans() }}</span>
                                                    <p>{!! ucfirst($activity->activity) !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @if(count($activities) > ($key+1))
                                            <hr>
                                        @endif
                                    @empty
                                        <div>@lang('messages.noActivityByThisUser')</div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="HRProfile">
                        <ul class="nav nav-tabs tabs customtab">

                            <li class="active tab"><a href="#publicInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Public Info</span> </a> </li>
                            <li class="tab"><a href="#personalInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Personal info</span> </a> </li>
                            <li class="tab"><a href="#contactInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Contact info</span> </a> </li>
                            <li class="tab"><a href="#recruitmentInfo" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Recruitment info</span> </a> </li>
                            <li class="tab"><a href="#benefits" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Benefits</span> </a> </li>
                            <li class="tab"><a href="#references" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">References</span> </a> </li>
                            <li class="tab" style="display: @if($military_person == 0) none @else block @endIf "><a href="#militaryRecord" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Military record</span> </a> </li>
                            <li class="tab"><a href="#employmentRecord" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Employment record</span> </a> </li>
                            <li class="tab"><a href="#documents" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Documents</span> </a> </li>
                            <li class="tab" style="display: none;"><a href="#biometricRecord" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Biometric record</span> </a> </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="publicInfo">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>First Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($first_name) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Middle Name</strong> <br>
                                        <p class="text-muted">{{ucwords($middle_name)}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Last Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($last_name) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Father Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($father_name)}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Mother Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($mother_name)}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Country Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($country_name)}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>City</strong> <br>
                                        <p class="text-muted">{{ ucwords($home_city)}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Qualification</strong> <br>
                                        <p class="text-muted">{{ ucwords($last_qualification)}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Height</strong> <br>
                                        <p class="text-muted">{{ ucwords($height)}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Weight</strong> <br>
                                        <p class="text-muted">{{ ucwords($weight)}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Category</strong> <br>
                                        <p class="text-muted">{{ ucwords($category_ids)}}</p>
                                    </div>

                                </div>


                            </div>
                            <div class="tab-pane" id="personalInfo">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>CNIC</strong> <br>
                                        <p class="text-muted">{{ ucwords($employee_cnic) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>CNIC Expiry Date</strong> <br>
                                        <p class="text-muted">{{$cnic_expiry}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>DOB</strong>
                                        <br>
                                        <p class="text-muted">{{$birthday}}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Marital Status</strong>
                                        <br>
                                        <p class="text-muted">{{ucwords($marital_status)}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r">
                                        <strong># of children</strong> <br>
                                        <p class="text-muted">{{ $number_of_children }}</p>
                                    </div>
                                    <div class="col-xs-6">
                                        <strong>Bank Name</strong> <br>
                                        <p class="text-muted">{{ucwords($bank_name)}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Bank Branch</strong> <br>
                                        <p class="text-muted">{{ ucwords($bank_branch) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Bank City</strong> <br>
                                        <p class="text-muted">{{ucwords($bank_city)}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Bank Account Title</strong> <br>
                                        <p class="text-muted">{{ ucwords($bank_account_title) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Bank Account Number</strong> <br>
                                        <p class="text-muted">{{$bank_account_number}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Blood Group</strong> <br>
                                        <p class="text-muted">{{ ucwords($blood_group) }}</p>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="contactInfo">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Mobile</strong> <br>
                                        <p class="text-muted">{{ $mobile }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Land Line Number</strong> <br>
                                        <p class="text-muted">{{ $land_line_number}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Personal Email</strong> <br>
                                        <p class="text-muted">{{ $personal_email }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>@lang('app.address')</strong> <br>
                                        <p class="text-muted">{{ $address}}</p>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Work Address</strong> <br>
                                        <p class="text-muted">{{ $work_address }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Next Of Kin 1 Name</strong> <br>
                                        <p class="text-muted">{{ $kin_1_name }}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Next Of Kin 1 Number</strong> <br>
                                        <p class="text-muted">{{ $kin_1_number }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Next Of Kin 1 Relation</strong> <br>
                                        <p class="text-muted">{{ $kin_1_relation }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Next Of Kin 2 Name</strong> <br>
                                        <p class="text-muted">{{ $kin_2_name }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Next Of Kin 2 Number</strong> <br>
                                        <p class="text-muted">{{ $kin_2_number }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Next Of Kin 2 Relation</strong> <br>
                                        <p class="text-muted">{{ $kin_2_relation }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="recruitmentInfo">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Basic Salary</strong> <br>
                                        <p class="text-muted">{{ $basic_salary }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Duty Hours</strong> <br>
                                        <p class="text-muted">{{ $duty_hours}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Joining Date</strong> <br>
                                        <p class="text-muted">{{ $joining_date }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>annual Leaves</strong> <br>
                                        <p class="text-muted">{{ $annual_leaves}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>attendance Logging</strong> <br>
                                        <p class="text-muted">{{ $attendance_logging }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong> Recruitment Type</strong> <br>
                                        <p class="text-muted">{{ $rec_type }}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Travel Benefits</strong> <br>
                                        <p class="text-muted">{{ $travel_benefits }}</p>
                                    </div>

                                </div>

                            </div>
                            <div class="tab-pane" id="benefits">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Email</strong> <br>
                                        <p class="text-muted">@if($email_pro) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Sim</strong> <br>
                                        <p class="text-muted">@if($sim) Yes @else No @endif </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Mobile Phone</strong> <br>
                                        <p class="text-muted">@if($mobile_phone) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Office</strong> <br>
                                        <p class="text-muted">@if($office) Yes @else No @endif </p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Office Stationary</strong> <br>
                                        <p class="text-muted">@if($office_stationary) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>PC</strong> <br>
                                        <p class="text-muted">@if($pc) Yes @else No @endif </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Laptop</strong> <br>
                                        <p class="text-muted">@if($laptop) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Vehicle</strong> <br>
                                        <p class="text-muted">@if($vehicle) Yes @else No @endif </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Uniform</strong> <br>
                                        <p class="text-muted">@if($uniform) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 "> <strong>Belt</strong> <br>
                                        <p class="text-muted">@if($belt) Yes @else No @endif </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Shoes</strong> <br>
                                        <p class="text-muted">@if($shoes) Yes @else No @endif </p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 "> <strong>Caps</strong> <br>
                                        <p class="text-muted">@if($caps) Yes @else No @endif </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Badges</strong> <br>
                                        <p class="text-muted">@if($badges) Yes @else No @endif </p>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="references">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Refree 1 Name</strong> <br>
                                        <p class="text-muted">{{ $ref_1_name }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Refree 1 Number</strong> <br>
                                        <p class="text-muted">{{ $ref_1_number}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Relation to Refree 1</strong> <br>
                                        <p class="text-muted">{{ $rel_to_ref1 }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Refree 1 Address</strong> <br>
                                        <p class="text-muted">{{ $ref_1_address}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Refree 1 CNIC</strong> <br>
                                        <p class="text-muted">{{ $ref_1_cnic }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong> Refree 2 Name</strong> <br>
                                        <p class="text-muted">{{ $ref_2_name }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Refree 2 Number</strong> <br>
                                        <p class="text-muted">{{ $ref_2_number }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Relation to Refree 2</strong> <br>
                                        <p class="text-muted">{{ $rel_to_ref2 }}</p>
                                    </div>

                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Refree 2 Address</strong> <br>
                                        <p class="text-muted">{{ $ref_2_address }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Refree 2 CNIC</strong> <br>
                                        <p class="text-muted">{{ $ref_2_cnic }}</p>
                                    </div>

                                </div>

                            </div>
                            <div class="tab-pane" id="militaryRecord" style="display: @if($military_person == 0) none @else block @endIf ">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Force No</strong> <br>
                                        <p class="text-muted">{{ $force_no }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Joining Date in Military</strong> <br>
                                        <p class="text-muted">{{ $m_joining_date}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Leaving Date in Military</strong> <br>
                                        <p class="text-muted">{{ $m_leaving_date }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Force Type</strong> <br>
                                        <p class="text-muted">{{$force_type}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Force Rank</strong> <br>
                                        <p class="text-muted">{{ $force_rank }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Last Unit</strong> <br>
                                        <p class="text-muted">{{$last_unit}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Last Center</strong> <br>
                                        <p class="text-muted">{{ $last_center}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="employmentRecord">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>Company Name</strong> <br>
                                        <p class="text-muted">{{ ucwords($employee->last_company_name) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>Position</strong> <br>
                                        <p class="text-muted">{{ $last_position}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Joining Date</strong> <br>
                                        <p class="text-muted">{{ $force_rank }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Leaving Date</strong> <br>
                                        <p class="text-muted">{{$last_leaving_date}}</p>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>City</strong> <br>
                                        <p class="text-muted">{{ $last_working_city}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Country</strong> <br>
                                        <p class="text-muted">{{ $last_working_country}}</p>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="documents">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>CNIC Image</strong> <br>
                                        <img src="{{$image_cnic}}" alt="CNIC Image" width="200"/>
                                    </div>
                                    <div class="col-xs-6"> <strong>Police Verification</strong> <br>
                                        <img src="{{$image_police_varification}}" alt="CNIC Image" width="200"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Education</strong> <br>
                                        <img src="{{$image_education}}" alt="CNIC Image" width="200"/>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Upload Discharge/Pension Book Copy</strong> <br>
                                        <img src="{{$image_referee_undertaking}}" alt="CNIC Image" width="200"/>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Upload Referee's Undertaking</strong> <br>
                                        <img src="{{$image_referee_undertaking}}" alt="CNIC Image" width="200"/>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Upload Employment Agreement</strong> <br>
                                        <img src="{{$image_emp_aggre}}" alt="CNIC Image" width="200"/>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">

                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Upload Nadra Attested</strong> <br>
                                        <img src="{{$image_nadra}}" alt="CNIC Image" width="200"/>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>verification Date</strong> <br>
                                        <p class="text-muted" >{{$varification_date}}</p>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="biometricRecord" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-6 b-r"> <strong>@lang('modules.employees.fullName')</strong> <br>
                                        <p class="text-muted">{{ ucwords($employee->name) }}</p>
                                    </div>
                                    <div class="col-xs-6"> <strong>@lang('app.mobile')</strong> <br>
                                        <p class="text-muted">{{ $employee->mobile or 'NA'}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('app.email')</strong> <br>
                                        <p class="text-muted">{{ $employee->email }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>@lang('app.address')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? $employee->employee[0]->address : 'NA'}}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('modules.employees.jobTitle')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? ucwords($employee->employee[0]->job_title) : 'NA' }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>@lang('modules.employees.hourlyRate')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? $employee->employee[0]->hourly_rate : 'NA' }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('modules.employees.slackUsername')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? '@'.$employee->employee[0]->slack_username : 'NA' }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>@lang('modules.employees.joiningDate')</strong> <br>
                                        <p class="text-muted">{{ (count($employee->employee) > 0) ? $employee->employee[0]->joining_date->format('d M, Y') : 'NA' }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 "> <strong>@lang('modules.employees.gender')</strong> <br>
                                        <p class="text-muted">{{ $employee->gender }}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script>

        $('#leave-table').dataTable({
            responsive: true,
            "columnDefs": [
                { responsivePriority: 1, targets: 0, 'width': '20%' },
                { responsivePriority: 2, targets: 1, 'width': '20%' }
            ],
            "autoWidth" : false,
            searching: false,
            paging: false,
            info: false
        });

        var table;

        function showTable() {

            if ($('#hide-completed-tasks').is(':checked')) {
                var hideCompleted = '1';
            } else {
                var hideCompleted = '0';
            }

            var url = '{{ route($mUserType.'.employees.tasks', [$employee->id, ':hideCompleted']) }}';
            url = url.replace(':hideCompleted', hideCompleted);

            table = $('#tasks-table').dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: url,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'project_name', name: 'projects.project_name', width: '20%'},
                    {data: 'heading', name: 'heading', width: '20%'},
                    {data: 'due_date', name: 'due_date'},
                    {data: 'status', name: 'status'}
                ]
            });
        }

        $('#hide-completed-tasks').click(function () {
            showTable();
        });

        showTable();
        var startDate = '{{ $startDate->format('Y-m-d') }}';
        var endDate = '{{ $endDate->format('Y-m-d') }}';
        function showTableAtt() {

            $('body').block({
                message: '<p style="margin:0;padding:8px;font-size:24px;">Just a moment...</p>'
                , css: {
                    color: '#fff'
                    , border: '1px solid #fb9678'
                    , backgroundColor: '#fb9678'
                }
            });

            var userId = '{{$userId}}';
            if (userId == "") {
                userId = 0;
            }


            //refresh counts
            var url = '{!!  route($mUserType.'.attendances.refreshCount', [':startDate', ':endDate', ':userId']) !!}';
            url = url.replace(':startDate', startDate);
            url = url.replace(':endDate', endDate);
            url = url.replace(':userId', userId);

            $.easyAjax({
                type: 'GET',
                url: url,
                success: function (response) {
                    $('#daysPresent').html(response.daysPresent);
                    $('#daysLate').html(response.daysLate);
                    $('#halfDays').html(response.halfDays);
                    $('#totalWorkingDays').html(response.totalWorkingDays);
                    $('#absentDays').html(response.absentDays);
                    initConter();
                }
            });

            //refresh datatable
            var url2 = '{!!  route($mUserType.'.attendances.employeeData', [':startDate', ':endDate', ':userId']) !!}';

            url2 = url2.replace(':startDate', startDate);
            url2 = url2.replace(':endDate', endDate);
            url2 = url2.replace(':userId', userId);

            $.easyAjax({
                type: 'GET',
                url: url2,
                success: function (response) {
                    $('#attendanceData').html(response.data);
                }
            });
        }
        showTableAtt();
    </script>

    <script>
        var table2;

        function showTable2(){

            var url = '{{ route($mUserType.'.employees.time-logs', [$employee->id]) }}';

            table2 = $('#timelog-table').dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: url,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[ 0, "desc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'project_name', name: 'projects.project_name' },
                    { data: 'start_time', name: 'start_time' },
                    { data: 'end_time', name: 'end_time' },
                    { data: 'total_hours', name: 'total_hours' },
                    { data: 'memo', name: 'memo' }
                ]
            });
        }

        showTable2();
    </script>
@endpush

