@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.tickets.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    {!! Form::open(['id'=>'storeTicket','class'=>'ajax-form','method'=>'POST']) !!}
    <div class="form-body">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('modules.tickets.requestTicket')</div>

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{--@lang('modules.tickets.ticketSubject')--}}
                                        <label class="control-label"> Subject <span class="text-danger">*</span></label>
                                        <input type="text" id="subject" name="subject" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{--@lang('modules.tickets.ticketDescription')--}}
                                        <label class="control-label"> Description <span class="text-danger">*</span></label></label>
                                        <textarea class="textarea_editor form-control" rows="10" name="description"
                                                  id="description"></textarea>
                                    </div>
                                </div>
                                <!--/span-->

                            </div>
                            <!--/row-->

                        </div>
                    </div>

                    <div class="panel-footer text-right">
                        <div class="btn-group dropup">
                            <button class="btn btn-success" id="submit-ticket" type="button">@lang('app.submit')</button>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <div class="panel panel-default">

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tickets.requesterName')</label>
                                        <select  name="user_id" id="user_id" class="select2 form-control" data-style="form-control" >
                                            <option value="">@lang('app.select') @lang('modules.tickets.requesterName')</option>
                                            @foreach($requesters as $requester)
                                                <option value="{{ $requester->id }}">{{ ucwords($requester->name).' ['.$requester->email.']' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{--@lang('modules.tickets.agent')--}}
                                        <label class="control-label"> Assign To: </label>
                                        <select  name="agent_id" id="agent_id" class="select2 form-control" data-style="form-control" >
                                            <option value="">Staff not assigned</option>
                                            @forelse($groups as $group)
                                                @if(count($group->enabled_agents) > 0)
                                                    <optgroup label="{{ ucwords($group->group_name) }}">
                                                        @foreach($group->enabled_agents as $agent)
                                                            <option value="{{ $agent->user->id }}">{{ ucwords($agent->user->name).' ['.$agent->user->email.']' }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            @empty
                                                <option value="">@lang('messages.noGroupAdded')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.type') <a class="btn btn-xs btn-info btn-outline" href="javascript:;" id="add-type"><i class="fa fa-plus"></i> @lang('modules.tickets.addType')</a></label>
                                        <select class="form-control selectpicker add-type" name="type_id" id="type_id" data-style="form-control">
                                            @forelse($types as $type)
                                                <option value="{{ $type->id }}">{{ ucwords($type->type) }}</option>
                                            @empty
                                                <option value="">@lang('messages.noTicketTypeAdded')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.priority') <span class="text-danger">*</span></label>
                                        <select class="form-control selectpicker" name="priority" id="priority" data-style="form-control">
                                            <option value="low">@lang('app.low')</option>
                                            <option value="medium">@lang('app.medium')</option>
                                            <option value="high">@lang('app.high')</option>
                                            <option value="urgent">@lang('app.urgent')</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tickets.channelName') <a class="btn btn-xs btn-info btn-outline" href="javascript:;" id="add-channel"><i class="fa fa-plus"></i> @lang('modules.tickets.addChannel')</a></label>
                                        <select class="form-control selectpicker" name="channel_id" id="channel_id" data-style="form-control">
                                            @forelse($channels as $channel)
                                                <option value="{{ $channel->id }}">{{ ucwords($channel->channel_name) }}</option>
                                            @empty
                                                <option value="">@lang('messages.noTicketChannelAdded')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tickets.tags')</label>
                                        <select multiple data-role="tagsinput" name="tags[]" id="tags">

                                        </select>
                                    </div>
                                </div>

                                <!--/span-->

                            </div>
                            <!--/row-->

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- .row -->
    </div>
    {!! Form::close() !!}

@endsection


@push('footer-script')
<script src="{{ asset('plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
<script src="{{ asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }}"></script>
<script>
    $('.textarea_editor').wysihtml5();

    $('#submit-ticket').click(function () {
        $.easyAjax({
            url: '{{route('member.tickets.store')}}',
            container: '#storeTicket',
            type: "POST",
            data: $('#storeTicket').serialize()
        })
    });

</script>
@endpush