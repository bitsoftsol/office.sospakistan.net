@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <div class="white-box">
        <div class="row m-b-10">
            {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}

            <div class="col-md-6">
                <h5 class="box-title m-t-30">Select Guards</h5>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" data-placeholder="Select Guard" id="guard">
                                <option value=""></option>
                                @foreach($guards as $guard)
                                    <option
                                            value="{{ $guard->id }}">{{ $guard->first_name . '( '. $guard->employee_erp . ' )'}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h5 class="box-title m-t-30">Select Payslip</h5>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" data-placeholder="Select Payslip" id="payslip_id">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')
                </button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h2>Details</h2>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                           id="tasks-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Category</th>
                            <th>Quantity</th>
                            <th>Rate(%)</th>
                            <th>Rule</th>
                            <th>Amount</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>

    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "No Record Found Get From ERP";
            }
        });

        $("#guard").change(function () {
            var employeeId = $(this).val();
            if(employeeId != ''){
                $.get( "{{route('member.guard-payslips.ajax-get-payslips')}}/"+employeeId, {  }, function( data ) {
                    if(data.error == 0){
                        var guard_html     = data.data.guards;
                        $("#payslip_id").html('');
                        $("#payslip_id").html(guard_html);
                    }else{
                        var guard_html     = data.data.guards;
                        $("#payslip_id").html('');
                        $("#payslip_id").html(guard_html);
                    }
                }, "json");
            }
        });

        var table;

        $('#tasks-table').dataTable();
        function showTable() {

            var guard_id = $('#guard').val();
            if(!guard_id) {
                guard_id = 0;
            }

            var slip_id = $('#payslip_id').val();
            if(!slip_id) {
                slip_id = 0;
            }

            var url = '{!!  route('member.guard-payslips.data', [':guard_id',':slip_id']) !!}';

            url = url.replace(':guard_id',guard_id);
            url = url.replace(':slip_id',slip_id);

            table = $('#tasks-table').dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: url,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    {data: 'guards_payslip_line_name', name: 'guards_payslip_line_name'},
                    {data: 'guards_payslip_line_code', name: 'hr_payslip_line_code'},
                    {data: 'guards_payslip_line_category_name', name: 'guards_payslip_line_category_name'},
                    {data: 'guards_payslip_line_quantity', name: 'guards_payslip_line_quantity'},
                    {data: 'guards_payslip_line_rate', name: 'guards_payslip_line_rate'},
                    {data: 'guards_payslip_line_rule_name', name: 'guards_payslip_line_rule_name'},
                    {data: 'guards_payslip_line_amount', name: 'guards_payslip_line_amount'},
                    {data: 'guards_payslip_line_total', name: 'guards_payslip_line_total'},
                ]
            });
        }

        $('#filter-results').click(function () {
            showTable();
        });

    </script>
@endpush
