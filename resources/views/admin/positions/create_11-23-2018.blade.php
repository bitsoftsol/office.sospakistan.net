
<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */
?>
@extends('layouts.app')

@section('page-title')

    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.positions') }}">{{ $pageTitle }}</a></li>
                <li class="active">@if(isset($id) && $id > 0) @lang('app.edit') @else @lang('app.addNew') @endif</li>

            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @if(isset($id) && $id > 0) @lang('app.edit') @else @lang('app.create') @endif Position {{--@lang('modules.client.createTitle')--}}</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createPosition','class'=>'ajax-form','method'=>'POST']) !!}
                        @if(isset($id) && $id > 0)
                            <input type="hidden" name="id" value="{{$id}}">
                        @endif
                        <div class="form-body">
                            <h3 class="box-title">Position Detail{{--@lang('modules.client.companyDetails')--}}</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name {{--@lang('modules.client.companyName')--}}</label>
                                        <input type="text" id="name" name="name" value="{{ $name or '' }}" class="form-control" >
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Order {{--@lang('modules.client.companyName')--}}</label>
                                        <input type="number" id="order" name="order" value="{{ $order or '' }}" class="form-control"  >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info">
                                            <input @if($isDivision) checked @endif id="isDivision" name="isDivision" value="1" type="checkbox" >
                                            <label for="isDivision"> Is Division ?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="userDiv" style="display: @if($isDivision) none @else block @endif" >
                                    <div class="form-group">
                                        <label>Select User{{--@lang('app.status')--}}</label>
                                        <select name="userId" id="userId" class="form-control">
                                            @if($users->count() > 0)
                                                <option value=""> Select User</option>
                                                @foreach($users as $usersIn)
                                                    <option value="{{$usersIn->id}}" @if($usersIn->id == $userId) selected @endif>{{$usersIn->name}}</option>
                                                @endforeach
                                            @else
                                                <option value="0">User not exist</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" id="divisionDiv" style="display: @if($isDivision) block @else none @endif">
                                    <div class="form-group">
                                        <label>Select Parent Division{{--@lang('app.status')--}}</label>
                                        <select name="divisionParentId" id="divisionParentId" class="form-control">
                                            @if($users->count() > 0)
                                                <option value=""> Select Parent Division</option>
                                                @foreach($division as $divisionIn)
                                                    <option value="{{$divisionIn->id}}" @if($divisionIn->id == $divisionParentId) selected @endif>{{$divisionIn->po_level_name}}</option>
                                                @endforeach
                                            @else
                                                <option value="0">User not exist</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Parent{{--@lang('app.status')--}}</label>
                                        <select name="parent" id="parent" class="form-control">
                                            @if($positionsData->count() > 0)
                                                <option value=""> Select Parent</option>
                                                @foreach($positionsData as $positionsDataIn)
                                                <option value="{{$positionsDataIn->id}}" @if($positionsDataIn->id == $parentId) selected @endif>{{$positionsDataIn->po_level_name}}</option>
                                                @endforeach
                                            @else
                                                <option value="0">Position not exist</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('app.status')</label>
                                        <select name="status" id="status" class="form-control" >
                                            <option @if($status == '1') selected
                                                    @endif value="1">@lang('app.active')</option>
                                            <option @if($status == '0') selected
                                                    @endif value="0">@lang('app.deactive')</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            @if($id == 0)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info">
                                            <input  id="addMultiple" name="addMultiple" value="1" type="checkbox" >
                                            <label for="addMultiple"> Add Multiple ?</label>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6" id="addMultipleNumberDiv" style="display: none;">
                                    <div class="form-group">
                                        <label>Add Number</label>
                                        <input type="number" id="addMultipleNumber" name="addMultipleNumber" class="form-control" >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            @endif



                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            $('#isDivision').change(function() {
                if(this.checked) {
                    $("#userDiv").hide();
                    $("#divisionDiv").show();
                }
                else{
                    $("#userDiv").show();
                    $("#divisionDiv").hide();
                }
            });
            $('#addMultiple').change(function() {
                if(this.checked) {
                    $("#addMultipleNumberDiv").show();
                }
                else{
                    $("#addMultipleNumberDiv").hide();
                }
            });

        })
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.positions.save')}}',
                container: '#createPosition',
                type: "POST",
                redirect: true,
                data: $('#createPosition').serialize()
            })
        });
    </script>
@endpush

