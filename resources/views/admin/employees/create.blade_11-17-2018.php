@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300i,400,400i,500,700,900">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .multi_step_form {
        /*background: #f6f9fb;*/
        display: block;
        overflow: hidden;
    }
    .multi_step_form #msform {
        text-align: center;
        position: relative;
        padding-top: 50px;
        min-height: 820px;
        /*max-width: 810px;*/
        margin: 0 auto;
        background: #ffffff;
        z-index: 1;
    }
    .multi_step_form #msform .tittle {
        text-align: center;
        padding-bottom: 55px;
    }
    .multi_step_form #msform .tittle h2 {
        font: 500 24px/35px "Roboto", sans-serif;
        color: #3f4553;
        padding-bottom: 5px;
    }
    .multi_step_form #msform .tittle p {
        font: 400 16px/28px "Roboto", sans-serif;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset {
        border: 0;
        padding: 20px 105px 0;
        position: relative;
        width: 100%;
        left: 0;
        right: 0;
    }
    .multi_step_form #msform fieldset:not(:first-of-type) {
        display: none;
    }
    .multi_step_form #msform fieldset h3 {
        font: 500 18px/35px "Roboto", sans-serif;
        color: #3f4553;
    }
    .multi_step_form #msform fieldset h6 {
        font: 400 15px/28px "Roboto", sans-serif;
        color: #5f6771;
        padding-bottom: 30px;
    }
    .multi_step_form #msform fieldset .intl-tel-input {
        display: block;
        background: transparent;
        border: 0;
        box-shadow: none;
        outline: none;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag {
        padding: 0 20px;
        background: transparent;
        border: 0;
        box-shadow: none;
        outline: none;
        width: 65px;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow {
        border: 0;
    }
    .multi_step_form #msform fieldset .intl-tel-input .flag-container .selected-flag .iti-arrow:after {
        content: "\f35f";
        position: absolute;
        top: 0;
        right: 0;
        font: normal normal normal 24px/7px Ionicons;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset #phone {
        padding-left: 80px;
    }
    .multi_step_form #msform fieldset .form-group {
        padding: 0 10px;
    }
    .multi_step_form #msform fieldset .fg_2, .multi_step_form #msform fieldset .fg_3 {
        padding-top: 10px;
        display: block;
        overflow: hidden;
    }
    .multi_step_form #msform fieldset .fg_3 {
        padding-bottom: 70px;
    }
    .multi_step_form #msform fieldset .form-control, .multi_step_form #msform fieldset .product_select {
        border-radius: 3px;
        border: 1px solid #d8e1e7;
        /*padding: 0 20px;
        height: auto;*/
        font: 400 15px/48px "Roboto", sans-serif;
        color: #5f6771;
        box-shadow: none;
        outline: none;
        width: 100%;
    }
    .multi_step_form #msform fieldset .form-control.placeholder, .multi_step_form #msform fieldset .product_select.placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control:-moz-placeholder, .multi_step_form #msform fieldset .product_select:-moz-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control::-moz-placeholder, .multi_step_form #msform fieldset .product_select::-moz-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select::-webkit-input-placeholder {
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .form-control:hover, .multi_step_form #msform fieldset .form-control:focus, .multi_step_form #msform fieldset .product_select:hover, .multi_step_form #msform fieldset .product_select:focus {
        border-color: #5cb85c;
    }
    .multi_step_form #msform fieldset .form-control:focus.placeholder, .multi_step_form #msform fieldset .product_select:focus.placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus:-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus:-moz-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus::-moz-placeholder, .multi_step_form #msform fieldset .product_select:focus::-moz-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .form-control:focus::-webkit-input-placeholder, .multi_step_form #msform fieldset .product_select:focus::-webkit-input-placeholder {
        color: transparent;
    }
    .multi_step_form #msform fieldset .product_select:after {
        display: none;
    }
    .multi_step_form #msform fieldset .product_select:before {
        content: "\f35f";
        position: absolute;
        top: 0;
        right: 20px;
        font: normal normal normal 24px/48px Ionicons;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .product_select .list {
        width: 100%;
    }
    .multi_step_form #msform fieldset .done_text {
        padding-top: 40px;
    }
    .multi_step_form #msform fieldset .done_text .don_icon {
        height: 36px;
        width: 36px;
        line-height: 36px;
        font-size: 22px;
        margin-bottom: 10px;
        background: #5cb85c;
        display: inline-block;
        border-radius: 50%;
        color: #ffffff;
        text-align: center;
    }
    .multi_step_form #msform fieldset .done_text h6 {
        line-height: 23px;
    }
    .multi_step_form #msform fieldset .code_group {
        margin-bottom: 60px;
    }
    .multi_step_form #msform fieldset .code_group .form-control {
        border: 0;
        border-bottom: 1px solid #a1a7ac;
        border-radius: 0;
        display: inline-block;
        width: 30px;
        font-size: 30px;
        color: #5f6771;
        padding: 0;
        margin-right: 7px;
        text-align: center;
        line-height: 1;
    }
    .multi_step_form #msform fieldset .passport {
        margin-top: -10px;
        padding-bottom: 30px;
        position: relative;
    }
    .multi_step_form #msform fieldset .passport .don_icon {
        height: 36px;
        width: 36px;
        line-height: 36px;
        font-size: 22px;
        position: absolute;
        top: 4px;
        right: 0;
        background: #5cb85c;
        display: inline-block;
        border-radius: 50%;
        color: #ffffff;
        text-align: center;
    }
    .multi_step_form #msform fieldset .passport h4 {
        font: 500 15px/23px "Roboto", sans-serif;
        color: #5f6771;
        padding: 0;
    }
    .multi_step_form #msform fieldset .input-group {
        padding-bottom: 40px;
    }
    .multi_step_form #msform fieldset .input-group .custom-file {
        width: 100%;
        height: auto;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label {
        width: 168px;
        border-radius: 5px;
        cursor: pointer;
        font: 700 14px/40px "Roboto", sans-serif;
        border: 1px solid #99a2a8;
        text-align: center;
        transition: all 300ms linear 0s;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label i {
        font-size: 20px;
        padding-right: 10px;
    }
    .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:hover, .multi_step_form #msform fieldset .input-group .custom-file .custom-file-label:focus {
        background: #5cb85c;
        border-color: #5cb85c;
        color: #fff;
    }
    .multi_step_form #msform fieldset .input-group .custom-file input {
        display: none;
    }
    .multi_step_form #msform fieldset .file_added {
        text-align: left;
        padding-left: 190px;
        padding-bottom: 60px;
    }
    .multi_step_form #msform fieldset .file_added li {
        font: 400 15px/28px "Roboto", sans-serif;
        color: #5f6771;
    }
    .multi_step_form #msform fieldset .file_added li a {
        color: #5cb85c;
        font-weight: 500;
        display: inline-block;
        position: relative;
        padding-left: 15px;
    }
    .multi_step_form #msform fieldset .file_added li a i {
        font-size: 22px;
        padding-right: 8px;
        position: absolute;
        left: 0;
        transform: rotate(20deg);
    }
    .multi_step_form #msform #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
    }
    .multi_step_form #msform #progressbar li {
        list-style-type: none;
        color: #99a2a8;
        font-size: 9px;
        width: calc(100%/5);
        float: left;
        position: relative;
        font: 500 13px/1 "Roboto", sans-serif;
    }
    .multi_step_form #msform #progressbar li:nth-child(2):before {
        content: "\e001" !important;
    }
    .multi_step_form #msform #progressbar li:nth-child(3):before {
        content: "\e069" !important
    }
    .multi_step_form #msform #progressbar li:nth-child(4):before {
        content: "\e040" !important;
    }
    .multi_step_form #msform #progressbar li:nth-child(5):before {
        content: "\e603"!important;
    }
    .multi_step_form #msform #progressbar li:before {
        content: "\e08b" !important;
        font: normal normal normal 30px/50px simple-line-icons;
        width: 50px;
        height: 50px;
        line-height: 50px;
        display: block;
        background: #eaf0f4;
        border-radius: 50%;
        margin: 0 auto 10px auto;
    }
    .multi_step_form #msform #progressbar li:after {
        content: '';
        width: 100%;
        height: 10px;
        background: #eaf0f4;
        position: absolute;
        left: -50%;
        top: 21px;
        z-index: -1;
    }
    .multi_step_form #msform #progressbar li:last-child:after {
        width: 150%;
    }
    .multi_step_form #msform #progressbar li.active {
        color: #5cb85c;
    }
    .multi_step_form #msform #progressbar li.active:before, .multi_step_form #msform #progressbar li.active:after {
        background: #5cb85c;
        color: white;
    }
    .multi_step_form #msform .action-button {
        background: #5cb85c;
        color: white;
        border: 0 none;
        border-radius: 5px;
        cursor: pointer;
        min-width: 130px;
        font: 700 14px/40px "Roboto", sans-serif;
        border: 1px solid #5cb85c;
        margin: 0 5px;
        text-transform: uppercase;
        display: inline-block;
    }
    .multi_step_form #msform .action-button:hover, .multi_step_form #msform .action-button:focus {
        background: #405867;
        border-color: #405867;
    }
    .multi_step_form #msform .previous_button {
        background: transparent;
        color: #99a2a8;
        border-color: #99a2a8;
    }
    .multi_step_form #msform .previous_button:hover, .multi_step_form #msform .previous_button:focus {
        background: #405867;
        border-color: #405867;
        color: #fff;
    }

</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.employees.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div>
                        <!-- Multi step form -->
                        <section class="multi_step_form">
                            {!! Form::open(['id'=>'msform','route' => 'admin.employees.storeOdoo']) !!}
                                <div class="form-body">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active">General Information</li>
                                    <li>Public Information</li>
                                    <li>Personal Information</li>
                                    <li>Documents</li>
                                    <li>Other</li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset>
                                    <h3>General Information</h3>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.employeeName')</label>
                                                <input type="text" name="name" id="name" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Father Name{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="fatherName" id="fatherName" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Tag{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="category_ids" id="category_ids" class="form-control">
                                                    <option value="Civil">Civil</option>
                                                    <option value="Full Time">Full Time</option>
                                                    <option value="Part Time">Part Time</option>
                                                    <option value="Army">Army</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee CNIC{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="cnic" id="cnic" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="isGuard" name="is_guard" value="true" type="checkbox" >
                                                    <label for="is_Guard"> Is Guard ?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Qualification{{--@lang('modules.employees.employeeEmail')--}}</label>
                                                <input type="text" name="education" id="education" class="form-control" autocomplete="nope">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.employeeEmail')</label>
                                                <input type="email" name="email" id="email" class="form-control" autocomplete="nope">
                                                <span class="help-block">@lang('modules.employees.emailNote')</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.employeePassword')</label>
                                                <input type="password" style="display: none">
                                                <input type="password" name="password" id="password" class="form-control" autocomplete="nope">
                                                <span class="help-block"> @lang('modules.employees.passwordNote') </span>
                                            </div>
                                        </div>
                                        <!--/span-->

                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('app.mobile')</label>
                                                <input type="tel" name="mobile" id="mobile"  class="form-control" autocomplete="nope">
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.gender')</label>
                                                <select name="gender" id="gender" class="form-control">
                                                    <option value="male">@lang('app.male')</option>
                                                    <option value="female">@lang('app.female')</option>
                                                    <option value="others">@lang('app.others')</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><i class="fa fa-slack"></i> @lang('modules.employees.slackUsername')</label>
                                                <div class="input-group"> <span class="input-group-addon">@</span>
                                                    <input autocomplete="nope" type="text" id="slack_username" name="slack_username" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.joiningDate')</label>
                                                <input type="text" name="joining_date" id="joining_date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date Of Birth</label>
                                                <input type="text" name="birthday" id="birthday" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Center{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="center_id" id="center_id" class="form-control">
                                                    @if($centers->count() > 0)
                                                        <option value=""> Select Center </option>
                                                        @foreach($centers as $centersDataIn)
                                                            <option value="{{$centersDataIn->id}}" name="odoo_center_id">{{$centersDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Center Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.employees.jobTitle')</label>
                                                <input type="text" name="job_title" id="job_title" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Permanent @lang('app.address')</label>
                                                <textarea name="address"  id="address"  rows="1" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <button type="button" class="action-button previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset>
                                    <h3>Public Information</h3>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Working @lang('app.address')</label>
                                                <textarea name="working_address"  id="working_address"  rows="1" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Work @lang('app.mobile')</label>
                                                <input type="text" name="work_mobile" id="work_mobile" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Work Location</label>
                                                <input type="text" name="work_location" id="work_location" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Work Phone</label>
                                                <input type="text" name="work_phone" id="work_phone" class="form-control" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Segment</label>
                                                <select name="segment_id" id="segment" class="form-control">
                                                    @if($segments->count() > 0)
                                                        <option value=""> Select Segment </option>
                                                        @foreach($segments as $segmentsDataIn)
                                                            <option value="{{$segmentsDataIn->id}}" name="odoo_segment_id">{{$segmentsDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Segments Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sub Segment</label>
                                                <select name="sub_segment_id" id="sub_segment_id" class="form-control">
                                                    @if($subSegments->count() > 0)
                                                        <option value=""> Select Sub Segment </option>
                                                        @foreach($subSegments as $subSegmentDataIn)
                                                            <option value="{{$subSegmentDataIn->id}}" name="odoo_sub_segment_id">{{$subSegmentDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Sub Segments Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <select name="department_id" id="department_id" class="form-control">
                                                    @if($departments->count() > 0)
                                                        <option value=""> Select Departments </option>
                                                        @foreach($departments as $departmentDataIn)
                                                            <option value="{{$departmentDataIn->id}}" name="odoo_department_id">{{$departmentDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Department Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Manager</label>
                                                <select name="manager_id" id="manager_id" class="form-control">
                                                    @if($employees->count() > 0)
                                                        <option value=""> Select Employees </option>
                                                        @foreach($employees as $employeesDataIn)
                                                            <option value="{{$employeesDataIn->id}}" name="odoo_emp_id">{{$employeesDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Employee Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Coach</label>
                                                <select name="coach_id" id="coach_id" class="form-control">
                                                    @if($employees->count() > 0)
                                                        <option value=""> Select Employees </option>
                                                        @foreach($employees as $employeesDataIn)
                                                            <option value="{{$employeesDataIn->id}}" name="odoo_emp_id">{{$employeesDataIn->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="0">No Employee Data Import From ERP</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="checkbox checkbox-info">
                                                    <input id="is_manager" name="is_manager" value="true" type="checkbox" >
                                                    <label for="random_password"> Is Manager ?{{--@lang('modules.client.generateRandomPassword')--}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset>
                                    <h3>Citizenship & Other Information</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nationality {{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="nationality" id="nationality" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Passport No</label>
                                                <input type="text" name="passport_no" id="passport_no" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bank Name{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="bank_name" id="bank_name" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Account Title</label>
                                                <input type="text" name="account_title" id="account_title" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Account No</label>
                                                <input type="text" name="account_no" id="account_no" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Marital Status{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <select name="marital_status" id="marital_status" class="form-control">
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Widower">Widower</option>
                                                    <option value="Divorced">Divorced</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Number Of Children</label>
                                                <input type="text" name="no_of_child" id="no_of_child" class="form-control" >
                                            </div>
                                        </div>

                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset>
                                    <h3>Documents</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Education</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_education" name="image_education"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>CNIC Expiry {{--@lang('modules.employees.jobTitle')--}}</label>
                                                <input type="text" name="cnic_expiry" id="cnic_expiry" class="form-control" >
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload CNIC Copy</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_cnic" name="image_cnic"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload Nadra Attested{{--@lang('modules.employees.jobTitle')--}}</label>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_nadra" name="image_nadra"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> @lang('app.remove') </a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>@lang('modules.profile.profilePicture')</label>
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150"   alt=""/>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> @lang('app.selectImage') </span>
                                    <span class="fileinput-exists"> @lang('app.change') </span>
                                    <input type="file" id="image_profile" name="image_profile"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                           data-dismiss="fileinput"> @lang('app.remove') </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button type="button" class="next action-button">Continue</button>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                        @if(isset($fields))
                                            @foreach($fields as $field)
                                                <div class="col-md-6">
                                                    <label>{{ ucfirst($field->label) }}</label>
                                                    <div class="form-group">
                                                        @if( $field->type == 'text')
                                                            <input type="text" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'password')
                                                            <input type="password" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">
                                                        @elseif($field->type == 'number')
                                                            <input type="number" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" placeholder="{{$field->label}}" value="{{$editUser->custom_fields_data['field_'.$field->id] or ''}}">

                                                        @elseif($field->type == 'textarea')
                                                            <textarea name="custom_fields_data[{{$field->name.'_'.$field->id}}]" class="form-control" id="{{$field->name}}" cols="3">{{$editUser->custom_fields_data['field_'.$field->id] or ''}}</textarea>

                                                        @elseif($field->type == 'radio')
                                                            <div class="radio-list">
                                                                @foreach($field->values as $key=>$value)
                                                                    <label class="radio-inline @if($key == 0) p-0 @endif">
                                                                        <div class="radio radio-info">
                                                                            <input type="radio" name="custom_fields_data[{{$field->name.'_'.$field->id}}]" id="optionsRadios{{$key.$field->id}}" value="{{$value}}" @if(isset($editUser) && $editUser->custom_fields_data['field_'.$field->id] == $value) checked @elseif($key==0) checked @endif>>
                                                                            <label for="optionsRadios{{$key.$field->id}}">{{$value}}</label>
                                                                        </div>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'select')
                                                            {!! Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                                    $field->values,
                                                                     isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender'])
                                                             !!}

                                                        @elseif($field->type == 'checkbox')
                                                            <div class="mt-checkbox-inline">
                                                                @foreach($field->values as $key => $value)
                                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                                        <input name="custom_fields_data[{{$field->name.'_'.$field->id}}][]" type="checkbox" value="{{$key}}"> {{$value}}
                                                                        <span></span>
                                                                    </label>
                                                                @endforeach
                                                            </div>
                                                        @elseif($field->type == 'date')
                                                            <input type="text" class="form-control date-picker" size="16" name="custom_fields_data[{{$field->name.'_'.$field->id}}]"
                                                                   value="{{ isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format('m/d/Y')}}">
                                                        @endif
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>

                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                    <button type="button" class="action-button previous previous_button">Back</button>
                                    <button class="action-button">Finish</button>
                                </fieldset>
                                </div>
                            {!! Form::close() !!}
                        </section>
                        <!-- End Multi step form -->
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    ;(function($) {
        "use strict";

        //* Form js
        function verificationForm(){
            //jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches

            $(".next").click(function () {
                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50) + "%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'transform': 'scale(' + scale + ')',
                            /*'position': 'absolute'*/
                        });
                        next_fs.css({
                            'left': left,
                            'opacity': opacity
                        });


                    },
                    duration: 800,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
                $('html, body').animate({
                    scrollTop: "80px"
                }, 800);
            });

            $(".previous").click(function () {
                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //de-activate current step on progressbar
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1 - now) * 50) + "%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'left': left
                        });
                        previous_fs.css({
                            'transform': 'scale(' + scale + ')',
                            'opacity': opacity
                        });
                    },
                    duration: 800,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
                $('html, body').animate({
                    scrollTop: "80px"
                }, 800);
            });

            // $(".submit").click(function () {
            //     return false;
            // })
        };

        //* Add Phone no select
        function phoneNoselect(){
            if ( $('#msform').length ){
                $("#phone").intlTelInput();
                $("#phone").intlTelInput("setNumber", "+880");
            };
        };
        //* Select js
        function nice_Select(){
            if ( $('.product_select').length ){
                $('select').niceSelect();
            };
        };
        /*Function Calls*/
        verificationForm ();
        //phoneNoselect ();
        //nice_Select ();
    })(jQuery);
</script>
<script>

    $("#joining_date").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    $("#birthday").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    $("#cnic_expiry").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.employees.store')}}',
            container: '#msform',
            type: "POST",
            redirect: true,
            file: (document.getElementById("image").files.length == 0) ? false : true,
            data: $('#msform').serialize()
        })
    });

    $('#random_password').change(function () {
        var randPassword = $(this).is(":checked");

        if(randPassword){
            $('#password').val('{{ str_random(8) }}');
            $('#password').attr('readonly', 'readonly');
        }
        else{
            $('#password').val('');
            $('#password').removeAttr('readonly');
        }
    });
</script>
@endpush

 });


