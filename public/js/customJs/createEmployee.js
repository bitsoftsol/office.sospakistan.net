/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/17/2018
 * Time: 2:41 PM
 */


var $sigdiv = [];
(function($){

    $(document).ready(function() {

        // This is the part where jSignature is initialized.
        $sigdiv = $("#signature").jSignature({'UndoButton':false})

        <?php if(isset($id) && $id != '' && $user->directory != ''){?>
            $sigdiv.jSignature("setData", '<?php echo $user->directory?>');
        <?php }?>
    })

})(jQuery)


/* camera and signature qasim*/
function drawSig() {
    $sigdiv.jSignature('reset');
    $("#sigImageInput").val('');
    $("#sigImage").hide();
    $("#signatureparent").show();
}
function resetSig() {
    $sigdiv.jSignature('reset');
}
function save() {
    var sigImage =  $sigdiv.jSignature('getData');
    jQuery("#sigImageInput").val(sigImage);
    jQuery("#sigImage").attr('src', sigImage);
    jQuery("#sigImage").show();
    $sigdiv.jSignature('reset');
    jQuery("#signatureparent").hide();
}
Webcam.set({
    width: 300,
    height: 150,
    image_format: 'jpeg',
    jpeg_quality: 90
});

function load_camera(){
    Webcam.attach('#my_camera');
    $("#load_cam").hide();
    $("#take_pic").show();
}

function close_camera() {
    Webcam.reset('#my_camera');
    $("#load_cam").show();
    $("#take_pic").hide();
    $("#close_cam").hide();
}

function take_snapshot() {
    $("#close_cam").show();
    Webcam.snap(function(data_uri) {
        $(".image-tag").val(data_uri);
        document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        //document.getElementById('#image_profile_cam').value = data_uri;
        $("#image_profile_cam").val(data_uri);

    });

}
/* camera and signature end*/

$('#military_person').on('change', function(){
    if ( $(this).is(':checked') ) {
        $('.force_no').show();
        $('.m_joining_date').show();
        $('.m_leaving_date').show();
        $('.force_type').show();
        $('.force_rank').show();
        $('.last_unit').show();
        $('.last_center').show();
    } else {
        $('.force_no').hide();
        $('.m_joining_date').hide();
        $('.m_leaving_date').hide();
        $('.force_type').hide();
        $('.force_rank').hide();
        $('.last_unit').hide();
        $('.last_center').hide();
    }
});

function validate_fs1() {

}


/*js For Division Filter start -- Qasim*/

$(function(){
    $(".divisions").change(function () {
        var divisionId = $(this).val();
        if(divisionId != ''){
            $.get( "{{route('admin.positions.ajaxGetDepartmentByDivision')}}/"+divisionId, {  }, function( data ) {
                if(data.error == 0){
                    var departmentsHtml     = data.data.departments;
                    $(".departments").html('');
                    $(".departments").html(departmentsHtml);
                }else{
                    var departmentsHtml     = data.data.departments;
                    $(".departments").html('');
                    $(".departments").html(departmentsHtml);
                }
            }, "json");
        }
    });

    $(".departments").change(function () {
        var departmentId = $(this).val();
        if(departmentId != ''){
            $.get( "{{route('admin.positions.ajaxGetPositionByDepartment')}}/"+departmentId, {  }, function( data ) {
                if(data.error == 0){
                    var positionsHtml     = data.data.positions;
                    $(".positions").html('');
                    $(".positions").html(positionsHtml);
                }else{
                    var positionsHtml     = data.data.positions;
                    $(".positions").html('');
                    $(".positions").html(positionsHtml);
                }
            }, "json");
        }
    });

});
/*js For Division Filter end*/

/* js For camera and profile picture start -- Qasim--*/

$(function () {
    $('.profileImage').bind("DOMSubtreeModified",function(){
        $('#image_profile_cam').val("");
        $('#results img').attr('src',"https://placeholdit.imgix.net/~text?txtsize=25&txt=@lang('modules.profile.uploadPicture')&w=200&h=150");
    });
})
/*js For camera and profile picture end*/
