window.onload = function () {

    CanvasJS.addColorSet("customColorSet", [
        "#393f63",
        "#e5d8B0",
        "#ffb367",
        "#f98461",
        "#d9695f",
        "#e05850",
    ]);
    /**
     * Dashboard tickets
     */
    if(viewDashboardTickets == "true"){
        var dashboardTicketsCanvas = [
            {y:openTicket,label:"Open"},
            {y:pendingTicket,label:"Pending"},
            {y:resolvedTicket,label:"Resolved"},
            {y:closedTicket,label:"Closed"}
        ]
        var dashboardTicketsCanvasToolTipContent = "<b>{label}:{y}</b>";
        if(userTicketsCount <= 0){
            dashboardTicketsCanvas = [
                {y:100,label:"No Ticket"},
            ]
            dashboardTicketsCanvasToolTipContent = "<b>{label}</b>";
        }
        var chart = new CanvasJS.Chart("dashboardTicketsCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",
            title: {
                dockInsidePlotArea: true,
                fontSize: 55,
                fontWeight: "normal",
                horizontalAlign: "center",
                verticalAlign: "center",
                text: userTicketsCount
            },
            data: [{
                type: "doughnut",
                startAngle: 60,
                indexLabelFontSize: 17,
                indexLabel: "{label}",
                toolTipContent: dashboardTicketsCanvasToolTipContent,
                dataPoints: dashboardTicketsCanvas
            }]
        });
        chart.render();
    }

    /**
     * Dashboard tasks canvas
     *
     */

    if(viewDashboardTasks == "true"){
        var dashboardTasksCanvas = [
            {y:completedTaskCount,label:"completed"},
            {y:pendingTaskCount,label:"Pending"},
            {y:incompleteTaskCount,label:"Incomplete"},
        ];
        var indexLabelDashboardTasksCanvas = "{label} {y}";
        var yValueFormatStringDashboardTasksCanvas = "##0\"\"";


        if(userTaskCount <= 0){
            dashboardTasksCanvas = [
                {y:0.001,label:"No Task"},
            ];
            var indexLabelDashboardTasksCanvas = "{label}";
            var yValueFormatStringDashboardTasksCanvas = "##0.00";
        }
        var chart = new CanvasJS.Chart("dashboardTaskCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",
            title: {
                text: ""
            },
            data: [{
                type: "pie",
                startAngle: 240,
                yValueFormatString: yValueFormatStringDashboardTasksCanvas,
                indexLabel: indexLabelDashboardTasksCanvas,
                dataPoints: dashboardTasksCanvas
            }]
        });
        chart.render();
    }


    /**
     * Dashboard guards
     */
    if(viewDashboardGuards == "true"){

        var dashboardGuardsCanvas = [
            { y: guardsCount, name: "Total ("+guardsCount+")" },
            { y: terminatedGuardsCount, name: "Terminated ("+terminatedGuardsCount+")" },
            { y: newGuardsCount, name: "New ("+newGuardsCount+")" },


        ];
        var toolTipContentDashboardGuardsCanvas = "<b>{name}";
        var indexLabelDashboardGuardsCanvas = "{name}";
        if(guardsCount <= 0){
            dashboardGuardsCanvas = [
                { y: 100, name: "No guard" },
            ];
            toolTipContentDashboardGuardsCanvas = "<b>{name}</b>";
            indexLabelDashboardGuardsCanvas = "{name}";
        }


        var chart = new CanvasJS.Chart("dashboardGuardsCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",
            title: {
                dockInsidePlotArea: true,
                fontSize: 30,
                fontWeight: "normal",
                horizontalAlign: "center",
                verticalAlign: "center",
                text: guardsCount
            },
            data: [{
                type: "doughnut",
                innerRadius: 90,
                showInLegend: true,
                toolTipContent: toolTipContentDashboardGuardsCanvas,
                indexLabel: indexLabelDashboardGuardsCanvas,
                dataPoints: dashboardGuardsCanvas
            }]
        });
        chart.render();
    }


    /**
     * Dashboard assignment
     */
    if(viewDashboardAssignments == "true"){
        var chart = new CanvasJS.Chart("dashboardAssignmentCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",

            title:{
                text: ""
            },
            data: [{
                type: "column",
                showInLegend: true,
                legendMarkerColor: "grey",
                legendText: "",
                dataPoints: [
                    { y: userTotalProjectsCount, label: "Total" },
                    { y: completedProjectsCount,  label: "Completed" },
                    { y: inProcessProjectsCount,  label: "InProcess" },
                    { y: overdueProjectsCount,  label: "Overdue" },
                ]
            }]
        });
        chart.render();
    }

    /**
     * Dashboard Salary
     */
    if(viewDashboardSalaries == "true"){
        var chart = new CanvasJS.Chart("dashboardSalaryCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",

            title:{
                text:""
            },
            axisX:{
                interval: 1
            },
            data: [{
                type: "bar",
                name: "companies",
                axisYType: "secondary",
                dataPoints: [
                    { y: salaryOnHold, label: "On Hold" },
                    { y: salaryDeductions, label: "Deductions" },
                    { y: salaryPending, label: "Pending" },
                    { y: salaryPayable, label: "Payable" }
                ]
            }]
        });
        chart.render();
    }

    /**
     * Dashboard store
     */
    if(viewDashboardStore == "true"){
        var chart = new CanvasJS.Chart("dashboardStoreCanvas", {
            animationEnabled: true,
            colorSet: "customColorSet",
            title:{
                text: ""
            },
            axisY: {
                title: "",
                suffix: " m",
                reversed: true
            },
            axisX2: {
                tickThickness: 0,
                labelAngle: 0
            },
            data: [{
                type: "column",
                axisXType: "secondary",
                yValueFormatString: "#,##0",
                dataPoints: [
                    { y: storeTotalItems, label: "Total Items" },
                    { y: storeNewIns, label: "New Ins" },
                    { y: storeNewOuts, label: "New Outs" },
                    { y: storeTotalInStock, label: "Total In Stock" },
                    { y: storeTotalDeployed, label: "Total Deployed" }
                ]
            }]
        });
        chart.render();
    }

}