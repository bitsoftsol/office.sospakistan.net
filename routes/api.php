<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'Api\ApiLoginController@index');
Route::post('employeeDirectory', 'Api\ApiEmployeeDirectoryController@index');
Route::post('employeeData', 'Api\ApiEmployeeController@index');
Route::post('dashboardData', 'Api\ApiDashboardController@index');
Route::post('profileData', 'Api\ApiProfileController@index');
Route::post('profileUpdate', 'Api\ApiProfileController@update');
Route::post('leaveStats', 'Api\ApiLeavesController@index');
Route::post('applyLeave', 'Api\ApiLeavesController@applyLeave');
Route::post('getLeaveTypes', 'Api\ApiLeavesController@getLeaveTypes');
Route::post('leaveDetail', 'Api\ApiLeavesController@leaveDetail');
Route::post('paySlipList', 'Api\ApiPaySlipController@index');
Route::post('paySlipDetail', 'Api\ApiPaySlipController@detail');
Route::post('attendanceStats', 'Api\ApiAttendanceController@index');
Route::post('currentMonthList', 'Api\ApiAttendanceController@currentMonthList');
Route::post('clockIn', 'Api\ApiAttendanceController@clockIn');
Route::post('clockOut', 'Api\ApiAttendanceController@clockOut');

