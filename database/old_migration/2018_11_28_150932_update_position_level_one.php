<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePositionLevelOne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('position_levels', function (Blueprint $table) {
            $table->enum('is_division',['0','1'])->default('0')->after('po_level_user_id')->comment = "1 means active, 0 means inactive";
            $table->integer('division_parent_id')->after('is_division');
            $table->enum('is_under_department',['0','1'])->default('0')->after('division_parent_id')->comment = "0 means no, 1 means yes";
            $table->enum('is_first',['0','1'])->default('0')->after('po_level_status')->comment = "0 means no, 1 means yes";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
