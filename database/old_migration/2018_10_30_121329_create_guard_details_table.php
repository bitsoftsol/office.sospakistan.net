<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuardDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guard_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('address')->nullable();
            $table->integer('employee_erp');
            $table->string('center_name');
            $table->integer('employee_cnic');
            $table->date('cnic_expiry');
            $table->string('current_post');
            $table->integer('odoo_id');
            $table->string('contract');
            $table->integer('mobile');
            $table->enum('gender',['Male','Female','Other']);
            $table->integer('hourly_rate')->nullable();
            $table->timestamps();
        });
        \App\Module::insert([
            ['module_name' => 'guards', 'description' => ''],
        ]);

        $modules = [
            ['module_name' => 'guards', 'status' => 'active'],
        ];

        \App\ModuleSetting::insert($modules);

        \App\Permission::insert([
            ['name' => 'add_guards', 'display_name' => 'Add Guards', 'module_id' => 15],
            ['name' => 'view_guards', 'display_name' => 'View Guards', 'module_id' => 15],
            ['name' => 'edit_guards', 'display_name' => 'Edit Guards', 'module_id' => 15],
            ['name' => 'delete_guards', 'display_name' => 'Delete Guards', 'module_id' => 15],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guard_details');
    }
}
