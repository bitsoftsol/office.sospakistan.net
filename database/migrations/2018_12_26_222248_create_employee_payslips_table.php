<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payslips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('hr_payslip_id')->nullable();
            $table->boolean('hr_payslip_employee_id')->nullable();
            $table->string('hr_payslip_number')->nullable();
            $table->string('hr_payslip_name')->nullable();
            $table->date('hr_payslip_date_from')->nullable();
            $table->date('hr_payslip_date_to')->nullable();
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_updated_on_odoo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payslips');
    }
}
