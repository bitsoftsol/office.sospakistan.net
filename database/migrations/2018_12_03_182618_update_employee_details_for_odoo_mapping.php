<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeeDetailsForOdooMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_details', function ($table) {
            $table->integer('odoo_center_id')->nullable();
            $table->integer('odoo_segment_id')->nullable();
            $table->integer('odoo_sub_segment_id')->nullable();
            $table->integer('odoo_department_id')->nullable();

//            $table->foreign('odoo_center_id')->references('id')->on('sos_centers')->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('odoo_segment_id')->references('id')->on('segments')->onDelete('cascade')->onUpdate('casecade');
//            $table->foreign('odoo_department_id')->references('id')->on('departments')->onDelete('cascade')->onUpdate('casecade');
//            $table->foreign('odoo_sub_segment_id')->references('id')->on('sub_segments')->onDelete('cascade')->onUpdate('casecade');
            $table->string('country_name')->after('country_id')->nullable();
            $table->integer('bank_odoo_id')->after('bank_name')->nullable();
            $table->integer('education_odoo_id')->after('education')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_details', function ($table) {
            $table->dropColumn('odoo_center_id');
            $table->dropColumn('odoo_segment_id');
            $table->dropColumn('odoo_sub_segment_id');
            $table->dropColumn('odoo_department_id');
            $table->dropColumn('country_name');
            $table->dropColumn('bank_odoo_id');
            $table->dropColumn('education_odoo_id');
        });
    }
}
