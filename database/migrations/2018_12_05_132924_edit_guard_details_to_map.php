<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditGuardDetailsToMap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guard_details', function ($table) {
                $table->string('father_name')->nullable();
                $table->boolean('is_guard')->nullable();
                $table->date('birthday')->nullable();
                $table->integer('project_id')->nullable();
                $table->string('project_name')->nullable();
                $table->boolean('nadra_attested')->nullable();
                $table->date('nadra_attest_date')->nullable();
                $table->integer('education_id')->nullable();
                $table->string('education_name')->nullable();
                $table->boolean('edu_certificate')->nullable();
                $table->string('marital')->nullable();
                $table->string('duty_bonus')->nullable();
                $table->string('weapon_allowance')->nullable();
                $table->string('children')->nullable();
                $table->string('discharge_book')->nullable();
                $table->string('unit_name')->nullable();
                $table->string('army_no')->nullable();
                $table->date('discharge_date')->nullable();
                $table->string('work_phone')->nullable();
                $table->string('odoo_department_id')->nullable();
                $table->integer('odoo_bank_id')->nullable();
                $table->string('odoo_bank_name')->nullable();
                $table->string('bank_account_title')->nullable();
                $table->string('bank_account_no')->nullable();
                $table->string('account_owner')->nullable();
                $table->string('account_creation_date')->nullable();
                $table->string('category_ids')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guard_details', function ($table) {
            $table->dropColumn('father_name');
            $table->dropColumn('is_guard');
            $table->dropColumn('birthday');
            $table->dropColumn('project_id');
            $table->dropColumn('project_name');
            $table->dropColumn('nadra_attested');
            $table->dropColumn('nadra_attest_date');
            $table->dropColumn('education_id');
            $table->dropColumn('education_name');
            $table->dropColumn('edu_certificate');
            $table->dropColumn('marital');
            $table->dropColumn('duty_bonus');
            $table->dropColumn('weapon_allowance');
            $table->dropColumn('children');
            $table->dropColumn('discharge_book');
            $table->dropColumn('unit_name');
            $table->dropColumn('army_no');
            $table->dropColumn('discharge_date');
            $table->dropColumn('work_phone');
            $table->dropColumn('odoo_department_id');
            $table->dropColumn('odoo_bank_id');
            $table->dropColumn('odoo_bank_name');
            $table->dropColumn('bank_account_title');
            $table->dropColumn('bank_account_no');
            $table->dropColumn('account_owner');
            $table->dropColumn('account_creation_date');
            $table->dropColumn('category_ids');
        });
    }
}
