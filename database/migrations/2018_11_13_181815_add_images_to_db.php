<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_details', function ($table){
            $table->string('image_education');
            $table->string('image_cnic');
            $table->string('image_nadra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_details', function ($table){
            $table->dropColumn('image_education');
            $table->dropColumn('image_cnic');
            $table->dropColumn('image_nadra');
        });
    }
}
