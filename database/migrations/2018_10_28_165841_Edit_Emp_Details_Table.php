<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEmpDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_details', function ($table){
            $table->integer('employee_cnic')->nullable();
            $table->integer('employee_odoo_id')->nullable();
            $table->integer('employee_erp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_details', function ($table){
            $table->dropColumn('employee_cnic');
            $table->dropColumn('employee_odoo_id');
            $table->dropColumn('employee_erp');
        });
    }
}
