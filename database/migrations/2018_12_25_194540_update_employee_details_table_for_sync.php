<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeeDetailsTableForSync extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_details', function ($table) {
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_changed_in_odoo')->nullable();
            $table->boolean('is_random_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_details', function ($table) {
            $table->dropColumn('is_sync');
            $table->dropColumn('is_changed_in_odoo');
            $table->dropColumn('is_random_email');
        });
    }
}
