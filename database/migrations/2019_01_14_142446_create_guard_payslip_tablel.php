<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuardPayslipTablel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guard_payslips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guard_id')->unsigned();
            $table->foreign('guard_id')->references('id')->on('guard_details')->onDelete('cascade')->onUpdate('cascade');
            $table->date('guards_payslip_date_from')->nullable();
            $table->date('guards_payslip_date_to')->nullable();
            $table->string('guards_payslip_contract_id')->nullable();
            $table->string('guards_payslip_contract_name')->nullable();
            $table->string('guards_payslip_number')->nullable();
            $table->string('guards_payslip_struct_id')->nullable();
            $table->string('guards_payslip_struct_name')->nullable();
            $table->string('guards_payslip_name')->nullable();
            $table->string('guards_payslip_bank')->nullable();
            $table->string('guards_payslip_bankacctitle')->nullable();
            $table->string('guards_payslip_bankacc')->nullable();
            $table->string('guards_payslip_id')->nullable();
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_updated_on_odoo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guard_payslips');
    }
}
