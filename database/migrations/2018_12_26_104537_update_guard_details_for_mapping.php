<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGuardDetailsForMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guard_details', function ($table) {
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_changed_in_odoo')->nullable();
            $table->integer('odoo_current_post_id')->nullable();
            $table->string('odoo_current_post_name')->nullable();
            $table->integer('odoo_contract_id')->nullable();
            $table->string('odoo_contract_name')->nullable();
            $table->string('mobile')->nullable();
            $table->string('profile_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guard_details', function ($table) {
            $table->dropColumn('is_sync')->nullable();
            $table->dropColumn('is_changed_in_odoo')->nullable();
            $table->dropColumn('odoo_current_post_id')->nullable();
            $table->dropColumn('odoo_current_post_name')->nullable();
            $table->dropColumn('odoo_contract_id')->nullable();
            $table->dropColumn('odoo_contract_name')->nullable();
            $table->dropColumn('mobile')->nullable();
            $table->dropColumn('profile_image')->nullable();
        });
    }
}
