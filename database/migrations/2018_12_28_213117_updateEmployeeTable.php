<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('employee_details', function ($table) {
            $table->integer('position_parent_id')->default('0')->after('position_id');
        });


        //ALTER TABLE `employee_details` ADD `position_parent_id` INT(152) NOT NULL DEFAULT '0' AFTER `position_id`;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
