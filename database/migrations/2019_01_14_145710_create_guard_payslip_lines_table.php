<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuardPayslipLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guard_payslip_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guard_payslips_id')->unsigned();
            $table->foreign('guard_payslips_id')->references('id')->on('guard_payslips')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('guards_payslip_line_id')->nullable();
            $table->string('guards_payslip_line_name')->nullable();
            $table->string('guards_payslip_line_post_id')->nullable();
            $table->string('guards_payslip_line_post_name')->nullable();
            $table->integer('guards_payslip_line_sequence')->nullable();
            $table->string('guards_payslip_line_code')->nullable();
            $table->decimal('guards_payslip_line_rate')->nullable();
            $table->string('guards_payslip_line_quantity')->nullable();
            $table->string('guards_payslip_line_category_id')->nullable();
            $table->string('guards_payslip_line_category_name')->nullable();
            $table->string('guards_payslip_line_total')->nullable();
            $table->string('guards_payslip_line_rule_id')->nullable();
            $table->string('guards_payslip_line_rule_name')->nullable();
            $table->decimal('guards_payslip_line_amount')->nullable();
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_updated_on_odoo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guard_payslip_lines');
    }
}
