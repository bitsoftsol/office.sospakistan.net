
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEmpDetailsInLinkWithOdoo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_details', function ($table){
            $table->string('father_name')->nullable();
            $table->enum('category_ids',['Civil','Full Time','Part Time','Army'])->nullable();
            $table->string('work_address')->nullable();
            $table->string('work_location')->nullable();
            $table->string('work_phone')->nullable();
            $table->integer('parent_coach_user_id')->nullable();
            $table->boolean('is_manager')->nullable()->default(false);
            $table->string('country_name')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_account_title')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->enum('marital_status',['Single,Married,Widower,Divorced'])->nullable();
            $table->integer('number_of_children')->nullable();
            $table->date('birthday')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('resign_date')->nullable();
            $table->date('cnic_expiry')->nullable();
            $table->boolean('cnic_copy')->default(false);
            $table->boolean('nadra_attested')->default(false);
            $table->string('education')->nullable();
            $table->boolean('education_certificate')->default(false);
            $table->boolean('emp_edu_certificate')->default(false);
            $table->boolean('emp_cv')->default(false);
            $table->boolean('emp_photographs')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_details', function ($table){
            $table->dropColumn('employee_cnic');
            $table->dropColumn('employee_odoo_id');
            $table->dropColumn('employee_erp');
        });
    }
}
