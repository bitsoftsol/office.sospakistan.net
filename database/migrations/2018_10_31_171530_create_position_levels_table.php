<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_level_name');
            $table->integer('po_level_order');
            $table->integer('po_level_parent_id')->default('0');
            $table->integer('po_level_user_id')->default('0');
            $table->enum('po_level_status',['1','0'])->default('1')->comment = "1=Active and 0=deactivate";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_levels');
    }
}
