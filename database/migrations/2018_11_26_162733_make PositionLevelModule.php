<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePositionLevelModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Module::insert([
            ['module_name' => 'position_levels', 'description' => ''],
        ]);

        $modules = [
            ['module_name' => 'position_levels', 'status' => 'active'],
        ];

        \App\ModuleSetting::insert($modules);

        \App\Permission::insert([
            ['name' => 'add_positions', 'display_name' => 'Add Positions', 'module_id' => 16],
            ['name' => 'view_positions', 'display_name' => 'View Positions', 'module_id' => 16],
            ['name' => 'edit_positions', 'display_name' => 'Edit Positions', 'module_id' => 16],
            ['name' => 'delete_positions', 'display_name' => 'Delete Positions', 'module_id' => 16],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
