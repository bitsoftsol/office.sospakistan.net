<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePayslipLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payslip_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payslip_id')->unsigned();
            $table->foreign('payslip_id')->references('id')->on('employee_payslips')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('hr_payslip_line_id')->nullable();
            $table->string('hr_payslip_line_code')->nullable();
            $table->string('hr_payslip_line_name')->nullable();
            $table->decimal('hr_payslip_line_rate')->nullable();
            $table->integer('hr_payslip_line_category_id')->nullable();
            $table->string('hr_payslip_line_category_name')->nullable();
            $table->decimal('hr_payslip_line_amount')->nullable();
            $table->decimal('hr_payslip_line_total')->nullable();
            $table->integer('hr_payslip_line_salary_rule_id')->nullable();
            $table->string('hr_payslip_line_salary_rule_name')->nullable();
            $table->decimal('hr_payslip_line_quantity')->nullable();
            $table->boolean('is_sync')->nullable();
            $table->boolean('is_updated_on_odoo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payslip_lines');
    }
}
