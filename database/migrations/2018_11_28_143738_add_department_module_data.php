<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentModuleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Module::insert([
            ['module_name' => 'departments', 'description' => ''],
        ]);

        $modules = [
            ['module_name' => 'departments', 'status' => 'active'],
        ];

        \App\ModuleSetting::insert($modules);

        \App\Permission::insert([
            ['name' => 'add_departments', 'display_name' => 'Add Departments', 'module_id' => 17],
            ['name' => 'view_departments', 'display_name' => 'View Departments', 'module_id' => 17],
            ['name' => 'edit_departments', 'display_name' => 'Edit Departments', 'module_id' => 17],
            ['name' => 'delete_departments', 'display_name' => 'Delete Departments', 'module_id' => 17],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
