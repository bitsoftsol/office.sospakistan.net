<?php
/**
 * Created by PhpStorm.
 * User: hafiz
 * Date: 12/29/18
 * Time: 10:09 AM
 */

namespace App\Repositories;


interface LeaveRepositoryInterface
{
    public function get($id);
}