<?php
/**
 * Created by PhpStorm.
 * User: hafiz
 * Date: 12/26/18
 * Time: 9:33 PM
 */

namespace App\Repositories;


use App\EmployeeDetails;
use Edujugon\Laradoo\Odoo;

class PayslipRepository implements PayslipRepositoryInterface
{

    protected $model;
    protected $odoo;

    public function __construct()
    {
        $this->model = 'hr.payslip';
        $this->odoo = new Odoo();
    }

    public function get_payslips_by_user($id)
    {
        return $this->odoo->where('employee_id', $id)->fields(
            'date_from','date_to','line_ids','number','name','employee_id')->get($this->model);
    }

    public function get_payslip_by_id($id,$date_from,$date_to)
    {
        return $this->odoo->where('employee_id', $id)->where('date_from',$date_from)->where('date_to',$date_to)->fields(
            'date_from','date_to','line_ids','number','name','employee_id')->get($this->model);
    }

    public function get_pay_slip_lines($id)
    {
        return $this->odoo->where('id',$id)->fields('name','code','category_id','rate',
            'quantity','salary_rule_id','amount','total')->get('hr.payslip.line');
    }

    public function get_payslips_by_guard($id)
    {
        return $this->odoo->where('employee_id',$id)->fields('date_from','date_to','contract_id','number','struct_id',
            'name','bank','bankacctitle','bankacc','line_ids','id')->get('guards.payslip');
    }

    public function get_payslips_by_guard_by_date($id, $date_from, $date_to)
    {
        return $this->odoo->where('employee_id',$id)
            ->where('date_from','>=',$date_from)
            ->where('date_to','<=',$date_to)
            ->fields('date_from','date_to','contract_id','number','struct_id',
            'name','bank','bankacctitle','bankacc','line_ids','id')->get('guards.payslip');
    }

    public function get_guard_payslip_lines($id)
    {
        return $this->odoo->where('id',$id)->fields('id','name','post_id','sequence','code','rate','quantity','category_id',
        'amount','total','salary_rule_id')->get('guards.payslip.line');
    }
}