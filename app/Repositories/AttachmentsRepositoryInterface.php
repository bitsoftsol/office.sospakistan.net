<?php
/**
 * Created by PhpStorm.
 * User: Hafiz Siddiq
 * Date: 12/1/2018
 * Time: 7:19 PM
 */

namespace App\Repositories;


interface AttachmentsRepositoryInterface
{
    public function getAttachmentByEmployee($id);
}