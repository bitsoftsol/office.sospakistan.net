<?php
/**
 * Created by PhpStorm.
 * User: Hafiz Siddiq
 * Date: 10/28/2018
 * Time: 2:45 PM
 */

namespace App\Repositories;


use App\EmployeeDetails;
use App\GuardDetails;
use App\User;
use Carbon\Carbon;
use Edujugon\Laradoo\Odoo;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    protected $model;
    protected $odoo;

    public function __construct($model)
    {
        $this->model = $model;
        $this->odoo = new Odoo();
    }

    public function allemployees(array $fields)
    {
        $active = $fields['active'];
        $is_guard = $fields['is_guard'];
        return $this->odoo->where('active', $active)->where('is_guard',$is_guard)->fields(
            'reference','appointmentdate','cnic','job_id','mobile_phone',
            'name','fathername','work_location','work_phone','parent_id','coach_id','center_id','category_ids',
            'work_email','gender','address_home_id','is_guard','segment_id','department_id','manager','country_id','passport_id',
            'bank_id','bankacc','bankacctitle','birthday','marital','children','cnic_expiry','cniccopy','nadraattest','education_id',
            'educertificate','emp_exp_certificate','emp_cv','emp_photographs','sub_segment_id'
            )->get($this->model);
    }

    public function get_details_by_id($id)
    {
        return $this->odoo->where('id',$id)->fields(
            'name','fathername','mothername','country_id','education_id',
            'height','weight','category_ids','gender','cnic','cnic_expiry','birthday','marital','childno','bank_id',
            'branch','bankacctitle','bankacc','bloodgroup_id','center_id','is_guard','mobile_phone','street','street2','kin','basicpay','appointmentdate',
            'ref_ids','armyno','joindate','dischargedate','forcetype','rank','lastunit','lastcenter','image','image_medium','l_index_finger',
            'l_middle_finger','l_ring_finger','l_baby_finger','l_thumb','r_thumb','r_index_finger','r_middle_finger','r_ring_finger','r_baby_finger')->get($this->model);
    }

    public function odoo_inter_co_employees(array $fields)
    {
        $active = $fields['active'];
        $is_guard = $fields['is_guard'];
        return $this->odoo->where('active', $active)->where('is_guard',$is_guard)->fields(
            'reference','name','work_email','mobile_phone','gender')->get($this->model);
    }

    public function odoo_inter_co_guards(array $fields)
    {
        $active = $fields['active'];
        $current = $fields['current'];
        $is_guard = $fields['is_guard'];
        return $this->odoo->where('active',$active)->where('is_guard',$is_guard)->where('current',$current)->fields(
            'current_post_id', 'reference','name','id','center_id','category_ids','guard_contract_id','mobile_phone','gender','appointmentdate')->get($this->model);
    }

    public function allguards(array $fields)
    {
        //    'address_home_id','name','fathername','center_id','cnic','cnic_expiry','is_guard','birthday','project_id','street',
//            'street1','mothername','education_id','height','weight','marital','basicpay','duty_bonas','wpallowance',
//            'childno','dischargebook','bloodgroup_id', 'educertificate','policeverification','nadraattest','cniccopy',
//            'nadraattestdate','joindate','unitname','armyno','lastunit','lastcenter','dischargedate','rank','ref_ids',
//            'l_thumb','l_index_finger','l_middle_finger','l_ring_finger','l_baby_finger',
//            'r_thumb','r_index_finger','r_middle_finger','r_ring_finger','r_babay_finger',
//            'work_phone','department_id','bank_id','bankacctitle','bankacc','branch','acc_owner','acc_creation_date',
        return $this->odoo->where('active',true)->where('is_guard',true)->where('current',true)->fields(
            'current_post_id', 'reference','name','id','center_id','category_ids','contract_id','mobile_phone','gender','appointmentdate')->get($this->model);
    }

    public function getCenters()
    {
        $model = 'sos.center';
        return $this->odoo->fields('id','name')->get($model);
    }

    public function getSegments()
    {
        $model = 'hr.segmentation';
        return $this->odoo->fields('id','name')->get($model);
    }

    public function getSubSegments()
    {
        $model = 'hr.sub.segmentation';
        return $this->odoo->fields('id','name')->get($model);
    }

    public function addEmployee(array $fields)
    {
       return $this->odoo->create($this->model,['name' => $fields['name'],
            'fathername' => $fields['fathername'],
            'is_guard' => $fields['is_guard'],
            'work_email' => $fields['work_email'],
            'mobile_phone' => $fields['mobile_phone'],
            'gender' => $fields['gender'],
            'appointmentdate' => $fields['appointmentdate'],
           // 'center_id' => $fields['center_id'],
           // 'job_id' => $fields['job_id'],
           // 'address_home_id' => $fields['address_home_id'],
            'work_location' => $fields['work_location'],
            'work_phone' => $fields['work_phone'],
            'segment_id' => $fields['segment_id'],
            'sub_segment_id' => $fields['sub_segment_id'],
          //  'department_id' => $fields['department_id'],
            'parent_id' => $fields['parent_id'],
            'coach_id' => $fields['coach_id'],
            'manager' => $fields['manager'],
           // 'country_id' => $fields['country_id'],
            'passport_id' => $fields['passport_id'],
           // 'bank_id' => $fields['bank_id'],
            'bankacctitle' => $fields['bankacctitle'],
            'bankacc' => $fields['bankacc'],
           // 'marital' => $fields['marital'],
            'children' => $fields['children'],
            'cnic_expiry' => $fields['cnic_expiry'],
            'educertificate' => $fields['educertificate'],
            'nadraattest' => $fields['nadraattest'],
            'emp_photographs' => $fields['emp_photographs'],
            'current' => true,
            'active' => true,
            ]);
    }

    public function save_employee(EmployeeDetails $emp, User $user)
    {
        $this->odoo->create($this->model,[
            'name' => $emp->first_name,
            'fathername' => $emp->father_name,
            'mothername' => $emp->mother_name,
            'height' => $emp->height,
            'weight' => $emp->weight,
            'cnic' => $emp->employee_cnic,
            'cnic_expiry' => $emp->cnic_expiry,
            'birthday' => $emp->birthday,
            'bankacctitle' => $emp->bank_account_title,
            'bankacc' => $emp->bank_account_number,
            'childno' => $emp->number_of_children,
            'mobile_phone' => $user->mobile,
            'street' => $emp->address,
            'basicpay' => $emp->basic_salary,
            'appointmentdate' => $emp->joining_date,
            'armyno' => $emp->force_no,
            'joindate' => $emp->m_joining_date,
            'dischargedate' => $emp->m_leaving_date,
            'forcetype' => $emp->force_type,
            'rank' => $emp->force_rank,
            'lastunit' => $emp->last_unit,
            'lastcenter' => $emp->last_center,
            'current' => true,
            'active' => true,
            'is_guard' => false,
        ]);
    }

    public function add_employeeTest()
    {
        $cnic = '36302-6676768-1';
        $center_id = 95;
        $id = $this->odoo->create($this->model,['name' => 'Farooq Arif',
            'fathername' => 'Siddiq',
            'cnic' => $cnic,
            'center_id' => $center_id,
            'is_guard' => false,
            'current' => true,
            'active' => true,
        ]);
    }

    public function get_emp_erp($id)
    {
       return $this->odoo->where('id','=',$id)->fields('reference')->get($this->model);
    }

    public function getDepartments()
    {
        $model = 'hr.department';
        return $this->odoo->fields('id','name')->get($model);
    }

    public function insertAttendance()
    {
//        $model = 'sos.guard.attendance1';
//       return $this->odoo->create($model,['employee_id' => 13792, 'name'=> Carbon::now()->toDateTimeString(),'center_id'=>22, 'current_action' => 'present',
//            'post_id' => 132, 'state'=> 'draft', 'action' => 'in', 'project_id' => 28, 'device_id' => 181, 'device_datetime' => Carbon::now()->toDateTimeString(),
//            ]);
        $active = true;
        $is_guard = true;
        $current = true;
        return $this->odoo->where('active',$active)->where('is_guard',$is_guard)->where('current',$current)->fields('address_home_id','name','center_id','cnic','cnic_expiry',
            'current_post_id', 'reference', 'id','category_ids','mobile_phone','gender','appointmentdate')->get($this->model);

    }

    public function editEmployee(array $fields)
    {
        return $this->odoo->where('id', '=',$fields['id'])->update($this->model,['name' => $fields['name'],
        'fathername' => $fields['fathername'],
        'is_guard' => $fields['is_guard'],
        'work_email' => $fields['work_email'],
        'mobile_phone' => $fields['mobile_phone'],
        'gender' => $fields['gender'],
        'appointmentdate' => $fields['appointmentdate'],
        // 'center_id' => $fields['center_id'],
        // 'job_id' => $fields['job_id'],
        // 'address_home_id' => $fields['address_home_id'],
        'work_location' => $fields['work_location'],
        'work_phone' => $fields['work_phone'],
        'segment_id' => $fields['segment_id'],
        'sub_segment_id' => $fields['sub_segment_id'],
        //  'department_id' => $fields['department_id'],
        'parent_id' => $fields['parent_id'],
        'coach_id' => $fields['coach_id'],
        'manager' => $fields['manager'],
        // 'country_id' => $fields['country_id'],
        'passport_id' => $fields['passport_id'],
        // 'bank_id' => $fields['bank_id'],
        'bankacctitle' => $fields['bankacctitle'],
        'bankacc' => $fields['bankacc'],
        // 'marital' => $fields['marital'],
        'children' => $fields['children'],
        'cnic_expiry' => $fields['cnic_expiry'],
        'educertificate' => $fields['educertificate'],
        'nadraattest' => $fields['nadraattest'],
        'emp_photographs' => $fields['emp_photographs'],
        'current' => true,
        'active' => true,
    ]);
    }

    public function edit_employee(EmployeeDetails $emp, User $user)
    {
        $odoo_id = $emp->employee_odoo_id;
       return $this->odoo->where('id', '=',$odoo_id)->update($this->model,[
            'name' => $emp->first_name,
            'fathername' => $emp->father_name,
            'mothername' => $emp->mother_name,
            'height' => $emp->height,
            'weight' => $emp->weight,
            'cnic' => $emp->employee_cnic,
            'cnic_expiry' => $emp->cnic_expiry,
            'birthday' => $emp->birthday,
            'bankacctitle' => $emp->bank_account_title,
            'bankacc' => $emp->bank_account_number,
            'childno' => $emp->number_of_children,
            'mobile_phone' => $user->mobile,
            'street' => $emp->address,
            'basicpay' => $emp->basic_salary,
            'appointmentdate' => $emp->joining_date,
            'armyno' => $emp->force_no,
            'joindate' => $emp->m_joining_date,
            'dischargedate' => $emp->m_leaving_date,
            'forcetype' => $emp->force_type,
            'rank' => $emp->force_rank,
            'lastunit' => $emp->last_unit,
            'lastcenter' => $emp->last_center,
        ]);
        // 'marital' => $emp->marital_status,
        //$this->employeeDetail->blood_group = $update_data['bloodgroup_id'] == null ? null : $update_data['bloodgroup_id'][1];
        //$this->employeeDetail->bank_odoo_id = $update_data['bank_id'] == null ? null : $update_data['bank_id'][0];
        //$this->employeeDetail->bank_name = $update_data['bank_id'] == null ? null : $update_data['bank_id'][1];
        //$this->employeeDetail->bank_branch = $update_data['branch'];
        // $this->employeeDetail->country_id = $update_data['country_id'] == null ? null : $update_data['country_id'][0];
        //$this->employeeDetail->country_name = $update_data['country_id'] == null ? null : $update_data['country_id'][1];
        //$this->employeeDetail->last_qualification = $update_data['education_id'] == null ? null : $update_data['education_id'][1];
//        if($update_data['image'] != null){
//            $this->employeeDir($this->userDetail->id);
//            $imageName              = md5(rand(100,10000));
//            $camProfileImage        = $update_data['image'];
//            $data                   = base64_decode($camProfileImage);
//            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
//            $this->userDetail->image      = $imageName.".png";
//        }
//        $this->employeeDetail->left_fin_1 = $update_data['l_index_finger'];
//        $this->employeeDetail->left_fin_2 = $update_data['l_middle_finger'];
//        $this->employeeDetail->left_fin_3 = $update_data['l_ring_finger'];
//        $this->employeeDetail->left_fin_4 = $update_data['l_baby_finger'];
//        $this->employeeDetail->left_thumb = $update_data['l_thumb'];
//        $this->employeeDetail->right_thumb = $update_data['r_thumb'];
//        $this->employeeDetail->right_fin_1 = $update_data['r_index_finger'];
//        $this->employeeDetail->right_fin_2 = $update_data['r_middle_finger'];
//        $this->employeeDetail->right_fin_3 = $update_data['r_ring_finger'];
//        $this->employeeDetail->right_fin_4 = $update_data['r_baby_finger'];
//        $this->employeeDetail->odoo_center_id = $update_data['center_id'] == null ? null : $update_data['center_id'][0];
//        $this->employeeDetail->is_guard = $update_data['is_guard'];
//        $this->employeeDetail->erp_type = 'odoo';
//        $this->employeeDetail->employee_type = $update_data['is_guard'] == false ? 'staff' : 'guard';
    }

    public function edit_guard(GuardDetails $emp)
    {
        $odoo_id = $emp->employee_odoo_id;
        return $this->odoo->where('id', '=',$odoo_id)->update($this->model,[
            'name' => $emp->first_name,
            'fathername' => $emp->father_name,
            'mothername' => $emp->mother_name,
            'height' => $emp->height,
            'weight' => $emp->weight,
            'cnic' => $emp->employee_cnic,
            'cnic_expiry' => $emp->cnic_expiry,
            'birthday' => $emp->birthday,
            'bankacctitle' => $emp->bank_account_title,
            'bankacc' => $emp->bank_account_number,
            'childno' => $emp->number_of_children,
            'mobile_phone' => $emp->mobile,
            'street' => $emp->address,
            'basicpay' => $emp->basic_salary,
            'appointmentdate' => $emp->joining_date,
            'armyno' => $emp->force_no,
            'joindate' => $emp->m_joining_date,
            'dischargedate' => $emp->m_leaving_date,
            'forcetype' => $emp->force_type,
            'rank' => $emp->force_rank,
            'lastunit' => $emp->last_unit,
            'lastcenter' => $emp->last_center,
        ]);
    }
}