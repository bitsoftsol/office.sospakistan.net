<?php
/**
 * Created by PhpStorm.
 * User: hafiz
 * Date: 12/29/18
 * Time: 10:10 AM
 */

namespace App\Repositories;


use Edujugon\Laradoo\Odoo;

class LeaveRepository implements LeaveRepositoryInterface
{
    protected $odoo;

    public function __construct()
    {
        $this->model = 'hr.holidays.status';
        $this->odoo = new Odoo();
    }

    public function get($id)
    {
        return $this->odoo->fields('max_leaves','name')->get($this->model);
    }
}