<?php
/**
 * Created by PhpStorm.
 * User: Hafiz Siddiq
 * Date: 12/1/2018
 * Time: 7:23 PM
 */

namespace App\Repositories;


use Edujugon\Laradoo\Odoo;

class AttachmentsRepository implements AttachmentsRepositoryInterface
{
    protected $model;
    protected $odoo;

    public function __construct($model)
    {
        $this->model = $model;
        $this->odoo = new Odoo();
    }

    public function getAttachmentByEmployee($id)
    {
        return $this->odoo->where('')->limit(3)->get($this->model);
    }
}