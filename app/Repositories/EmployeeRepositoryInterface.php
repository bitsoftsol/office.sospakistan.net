<?php
/**
 * Created by PhpStorm.
 * User: Hafiz Siddiq
 * Date: 10/28/2018
 * Time: 2:44 PM
 */

namespace App\Repositories;


use App\EmployeeDetails;
use App\GuardDetails;
use App\User;

interface EmployeeRepositoryInterface
{
    public function allemployees(array $fields);
    public function odoo_inter_co_employees(array $fields);
    public function odoo_inter_co_guards(array $fields);
    public function allguards(array $fields);
    public function getCenters();
    public function getSegments();
    public function getSubSegments();
    public function addEmployee(array $fields);
    public function editEmployee(array $fields);
    public function edit_employee(EmployeeDetails $emp,User $user);
    public function edit_guard(GuardDetails $emp);
    public function save_employee(EmployeeDetails $emp,User $user);
    public function get_emp_erp($id);
    public function get_details_by_id($id);
    public function getDepartments();
    public function insertAttendance();
    public function add_employeeTest();
}