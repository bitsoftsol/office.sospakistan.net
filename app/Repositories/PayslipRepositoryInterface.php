<?php
/**
 * Created by PhpStorm.
 * User: hafiz
 * Date: 12/26/18
 * Time: 9:32 PM
 */

namespace App\Repositories;


use App\EmployeeDetails;

interface PayslipRepositoryInterface
{
    public function get_payslips_by_user($id);
    public function get_payslip_by_id($id,$date_from,$date_to);
    public function get_pay_slip_lines($id);
    public function get_payslips_by_guard($id);
    public function get_guard_payslip_lines($id);
    public function get_payslips_by_guard_by_date($id,$date_from,$date_to);
}