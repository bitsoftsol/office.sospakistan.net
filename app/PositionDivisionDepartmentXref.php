<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class PositionDivisionDepartmentXref extends Model
{
    use Notifiable;

    const Table = 'position_division_department_xrefs';

    public function deleteByDivision($parentId){
        $query                      = "Delete from ".self::Table." WHERE division_id = $parentId ";
        $results                    = DB::select( DB::raw($query));
        return true;
    }

    static public function getByDivision($parentId){
        $query                      = "Select * from ".self::Table." WHERE division_id = $parentId ";
        $results                    = DB::select( DB::raw($query));
        return $results;
    }



}
