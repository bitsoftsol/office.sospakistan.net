<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 10:57 AM
 */

namespace App\Helper;
class ApiResponse {

    const ApiErrorInvalidData           = '-5';
    const ApiErrorDataNotAvailable      = '-3';
    const ApiSuccess                    = '200';

    public static function createResponse($code, $data, $message = '')
    {
        $response                   = array();
        $response['code']           = $code;
        $response['data']           = $data;

        switch ($code){
            case -5:
                $response['message']        = 'Invalid data in request';
                break;
            case -3;
                $response['message']        = 'Data Not Available';
                break;
            case 200;
                $response['message']        = 'Ok!';
                break;

        }
        if($message != ''){
            $response['message'] = $message;
        }
        return $response;
    }

    public static function getEmployeeDirPath($id)
    {
        return asset('/employee/' . $id);
    }
}