<?php

namespace App;

use App\Traits\CustomFieldsTrait;
use Illuminate\Database\Eloquent\Model;

class OdooInterCoEmployee extends Model
{
    use CustomFieldsTrait;

    protected $table = 'odoo_inter_co_employees';

    protected $dates = ['joining_date'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withoutGlobalScopes(['active']);
    }
}
