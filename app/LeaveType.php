<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Leave;

class LeaveType extends Model
{
    public function leaves()
    {
        return $this->hasMany(Leave::class, 'leave_type_id');
    }

    public function leavesCount()
    {
        return $this->leaves()
            ->selectRaw('leave_type_id, count(*) as count')
            ->groupBy('leave_type_id');
    }

    public static function byUser($userId){
        $setting = Setting::first();
        $user = User::findOrFail($userId);

        if($setting->leaves_start_from == 'joining_date'){
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'approved');
            }])
                ->get();
        }
        else{
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'approved');
            }])
                ->get();
        }

    }

    public static function byUserPending($userId){
        $setting = Setting::first();
        $user = User::findOrFail($userId);

        if($setting->leaves_start_from == 'joining_date'){
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'pending');
            }])
                ->get();
        }
        else{
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'pending');
            }])
                ->get();
        }

    }

    public static function byUserRejected($userId){
        $setting = Setting::first();
        $user = User::findOrFail($userId);

        if($setting->leaves_start_from == 'joining_date'){
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'rejected');
            }])
                ->get();
        }
        else{
            return LeaveType::with(['leavesCount' => function($q) use ($user, $userId){
                $q->where('leaves.user_id', $userId);
                $q->where('leaves.leave_date','<=', $user->employee[0]->joining_date->format((Carbon::now()->year+1).'-m-d'));
                $q->where('leaves.status', 'rejected');
            }])
                ->get();
        }

    }


    public static function getAllLeaveStats ($userId)
    {
        $data                               = array();
        $approvedLeaves                     = array();
        $pendingLeaves                      = array();
        $rejectedLeaves                     = array();
        $data['totalLeaves']                = 0;
        $data['approvedLeavesCount']        = 0;
        $data['pendingLeavesCount']         = 0;
        $data['rejectedLeavesCount']        = 0;
        $data['remainingLeavesCount']       = 0;
        $approvedLeavesCount                = 0;
        $pendingLeavesCount                 = 0;
        $rejectedLeavesCount                = 0;

        $approvedLeavesWithTypes = LeaveType::byUser($userId);
        foreach($approvedLeavesWithTypes as $leaveType){
            $temp                   = array();
            $temp['id']             = $leaveType->id;
            $temp['typeName']       = $leaveType->type_name;
            $temp['color']          = $leaveType->color;
            $temp['count']          = (isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0';
            if(isset($leaveType->leavesCount[0])){
                $currentLeaveTypeRecord = Leave::where('user_id','=',$userId)
                                            ->where('leave_type_id','=', $leaveType->id)
                                            ->where('status','=', 'approved')
                                            ->get();
                $currentLeaveTypeRecord = $currentLeaveTypeRecord->toArray();
                $temp['LeaveTypeRecord'] = $currentLeaveTypeRecord;
            }
            else{
                $temp['LeaveTypeRecord'] = array();
            }
            if(isset($leaveType->leavesCount[0])){
                $approvedLeavesCount    += $leaveType->leavesCount[0]->count;
            }
            $approvedLeaves[]           = $temp;
        }

        $pendingLeavesWithTypes = LeaveType::byUserPending($userId);
        foreach($pendingLeavesWithTypes as $leaveType){
            $temp                   = array();
            $temp['id']             = $leaveType->id;
            $temp['typeName']       = $leaveType->type_name;
            $temp['color']          = $leaveType->color;
            $temp['count']          = (isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0';
            if(isset($leaveType->leavesCount[0])){
                $currentLeaveTypeRecord = Leave::where('user_id','=',$userId)
                    ->where('leave_type_id','=', $leaveType->id)
                    ->where('status','=', 'pending')
                    ->get();
                $currentLeaveTypeRecord = $currentLeaveTypeRecord->toArray();
                $temp['LeaveTypeRecord'] = $currentLeaveTypeRecord;
            }
            else{
                $temp['LeaveTypeRecord'] = array();
            }
            if(isset($leaveType->leavesCount[0])){
                $pendingLeavesCount    += $leaveType->leavesCount[0]->count;
            }
            $pendingLeaves[]           = $temp;
        }

        $rejectedLeavesWithTypes = LeaveType::byUserRejected($userId);
        foreach($rejectedLeavesWithTypes as $leaveType){
            $temp                   = array();
            $temp['id']             = $leaveType->id;
            $temp['typeName']       = $leaveType->type_name;
            $temp['color']          = $leaveType->color;
            $temp['count']          = (isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0';
            if(isset($leaveType->leavesCount[0])){
                $currentLeaveTypeRecord = Leave::where('user_id','=',$userId)
                    ->where('leave_type_id','=', $leaveType->id)
                    ->where('status','=', 'rejected')
                    ->get();
                $currentLeaveTypeRecord = $currentLeaveTypeRecord->toArray();
                $temp['LeaveTypeRecord'] = $currentLeaveTypeRecord;
            }
            else{
                $temp['LeaveTypeRecord'] = array();
            }
            if(isset($leaveType->leavesCount[0])){
                $rejectedLeavesCount        += $leaveType->leavesCount[0]->count;
            }
            $rejectedLeaves[]               = $temp;
        }

        $totalLeaves                        = (int)LeaveType::sum('no_of_leaves');
        $data['totalLeaves']                = $totalLeaves;
        $data['approvedLeaves']             = $approvedLeaves;
        $data['pendingLeaves']              = $pendingLeaves;
        $data['rejectedLeaves']             = $rejectedLeaves;
        $data['approvedLeavesCount']        = $approvedLeavesCount;
        $data['pendingLeavesCount']         = $pendingLeavesCount;
        $data['rejectedLeavesCount']        = $rejectedLeavesCount;
        $data['remainingLeavesCount']       = $totalLeaves - $approvedLeavesCount;
        return $data;
    }
}
