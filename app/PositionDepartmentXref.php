<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class PositionDepartmentXref extends Model
{
    use Notifiable;

    const Table = 'position_department_xrefs';

    public function deleteByPosition($parentId){
        $query                      = "Delete from ".self::Table." WHERE position_id = $parentId ";
        $results                    = DB::select( DB::raw($query));
        return true;
    }

    static public function getByPosition($parentId){
        $query                      = "Select * from ".self::Table." WHERE position_id = $parentId ";
        $results                    = DB::select( DB::raw($query));
        return $results;
    }



}
