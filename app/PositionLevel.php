<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 10/22/2018
 * Time: 8:37 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use app\User;
use app\EmployeeDetails;
use App\PositionDivisionDepartmentXref;
use App\PositionDepartmentXref;
class PositionLevel extends Model
{
    use Notifiable;
    public $tableName = '';
    public function __construct()
    {
        parent::__construct();

        $this->tableName = $this->getTable();

    }


    public function getHierarchy(){
        $results = DB::select( DB::raw("select  id, po_level_name, po_level_parent_id 
                                        from    (select * from position_levels order by po_level_parent_id, id) position_levels_t,
                                        (select @pv := '0') initialisation
                                          where   find_in_set(po_level_parent_id, @pv)
                                          and     length(@pv := concat(@pv, ',', id))"));
        return $results;
        //echo "<pre>"; print_r($results); exit;
    }

    public function getChildren($parentId ){
        $return                     = array();
        //$results                    = DB::select( DB::raw("Select * from position_levels  GROUP BY po_level_parent_id HAVING po_level_parent_id = '$parentId' order by po_level_parent_id, id"));

        $results                    = DB::select( DB::raw("Select * from position_levels WHERE po_level_parent_id = '$parentId' order by po_level_parent_id, id"));
        $count = count($results);

        /*if($count > 50){

            $tempResult = array();
            $tempResult[] = $results[0];
            $tempResult[] = $results[1];
            $tempResult[] = $results[2];
            $results[3]->po_level_name  = "View List";
            $results[3]->toList         = true;
            $results[3]->listURL         = route('admin.positions.maxChildList', [$results[3]->po_level_parent_id]) ;
            $tempResult[] = $results[3];
            $results = $tempResult;
        }*/

        foreach ($results as $key => $result){
            if($result->po_level_user_id > 0){
                $userId         = $result->po_level_user_id;
                $userInfo       = User::where('id','=',$userId)->first();
                if(isset($userInfo->name)){
                    $results[$key]->userName = $userInfo->name;
                }
            }



            $children               = $this->getChildren($result->id);
            if($result->has_max_child == '1'){
                $results[$key]->po_level_name   = $result->po_level_name."<br/>View List";
                $results[$key]->toList          = true;
                $results[$key]->listURL         = route('admin.positions.maxChildList', [$result->id]) ;
                $children                       = array();

            }
            $result->children       = (!empty($children))?$children:array();
            $return                 = $results;
        }
        return $return;
    }

    public static function division(){
        $results                    = DB::select( DB::raw("Select * from position_levels WHERE is_division = '1'"));
        return $results;
    }

    public static function divisionExceptSelf($id){
        $results                    = DB::select( DB::raw("Select * from position_levels WHERE is_division = '1' AND id != '$id'"));
        return $results;
    }

    public static function customList(){

        // records who parent have more then 50 child
        /*$query                  = 'Select *, count(*) as position_count from position_levels GROUP BY po_level_parent_id HAVING count(*) > 50';
        $results                = DB::select( DB::raw($query));
        $maxChildParentIds      = array();
        foreach ($results as $result){
            $maxChildParentIds[]     = $result->po_level_parent_id;
        }*/
        $maxChildParentIds          = array();
        //record who max child flag is 1
        $query                      = 'Select * from position_levels where has_max_child = "1"';
        $results                    = DB::select( DB::raw($query));
        foreach ($results as $result){
            $maxChildParentIds[]    = $result->id;
        }

        $minChileResult         = self::select('*')->whereNotIn('po_level_parent_id', $maxChildParentIds)->whereNotIn('id', $maxChildParentIds)->where('is_division','=','0')->get();
        $maxChildList           = self::select((DB::raw('* , "position_count" as position_count')))->whereIn('id', $maxChildParentIds)->where('is_division','=','0')->get();
        $result                 = $minChileResult->merge($maxChildList);

        return $result;

    }


    public static function maxChildList($parentId)
    {
        $result = self::where('po_level_parent_id','=',$parentId)->get();
        return $result;
    }


    public function positionAssignToUser($positionId, $userId)
    {
        /*updating position start*/
        $this->where('po_level_user_id', '=', $userId)->update(['po_level_user_id' => 0]);
        $position                   = $this->findOrFail($positionId);
        $position->po_level_user_id = $userId;
        $position->save();
        /*updating position end*/

        EmployeeDetails::where('position_id', '=', $positionId)->update(['position_id' => 0]);
        $employeeInfo                       = EmployeeDetails::where('user_id','=', $userId)->first();
        if($employeeInfo != ''){
            $employeeInfo->position_id         = $positionId;
            $employeeInfo->save();
        }
        return true;
    }

    public function makeIsFirstToZero()
    {
        self::where('is_first', '=', '1')->update(['is_first' => 0]);
    }

    public function getFirstPositionData($positionId)
    {
        $results                    = DB::select( DB::raw("Select * from position_levels WHERE id = '$positionId'"));
        return $results;
    }

    public function removeAndAssignParent($parentId, $newParentId = 0)
    {
        self::where('po_level_parent_id', '=', $parentId)->update(['po_level_parent_id' => $newParentId]);
    }



    public function getPositionRelation($positionId)
    {
        $positionData       = $this->where('id', '=', $positionId)->first();
        if($positionData){
            $isDivision         = $positionData->is_division;
            $parentId           = $positionData->po_level_parent_id;

            $divisionData       = $this->getDivision($parentId);
            $departmentData     = $this->getDepartment($positionId,$isDivision);
            $parentData         = $this->getFirstParent($positionId);



            $data                           = array();
            $data['divisionData']           = $divisionData;
            $data['departmentData']         = $departmentData;
            $data['parentData']             = $parentData;
        }
        else{

            $data                           = array();
            $data['divisionData']           = array('error'=>'Position not Exist','id'=>'0');
            $data['departmentData']         = array('error'=>'Position not Exist','id'=>'0');
            $data['parentData']             = array('error'=>'Position not Exist','id'=>'0');
        }


        return $data;

    }

    public function getFirstParent($positionId)
    {
        $positionData               = $this->where('id', '=', $positionId)->first();
        $parentId                   = $positionData->po_level_parent_id;
        if($parentId > 0){
            $parentData             = $this->where('id', '=', $parentId)->first();

            if($parentData != ''){
                $isDivision             = $parentData->is_division;
                if($isDivision == '1'){
                    return  $this->getDivisionParent($parentData->id);
                }
                $userId = $parentData->po_level_user_id;
                if($userId > 0){
                    $userInfo                       = User::where('id','=', $userId)->first();
                    if($userInfo != ''){
                        $parentData                 = $parentData->toArray();
                        $parentData['userName']     = $userInfo->name;
                        $return                     =  $parentData;
                    }
                    else{
                        $return                     = array('error'=>'User deleted of position '.$parentData->po_level_name,'id'=>'0');
                    }
                }
                else{
                    $return                     = array('error'=>'User not assign to position '.$parentData->po_level_name,'id'=>'0');
                }
            }
            else{
                $return                     = array('error'=>'Parent position deleted','id'=>'0');
            }
        }
        else{
            $return                     = array('error'=>'Parent position not assign','id'=>'0');
        }

        return $return;

    }

    public function getDepartment($positionId, $isDivision)
    {
        if($isDivision == '1'){
            $departmentInfo  = PositionDivisionDepartmentXref::join('departments', 'departments.id', '=', 'position_division_department_xrefs.department_id')

                ->select('*')

                ->where('position_division_department_xrefs.division_id', '=', $positionId)

                ->first();
        }
        else{
            $departmentInfo  = PositionDepartmentXref::join('departments', 'departments.id', '=', 'position_department_xrefs.department_id')
                ->select('departments.name', 'departments.id')

                ->where('position_department_xrefs.position_id', '=', $positionId)

                ->first();
        }

        if($departmentInfo){
            return $departmentInfo->toArray();
        }
        else{
            return array('error'=>'Department not assign','id'=>'0');
        }

    }

    public function getDivisionParent($positionId)
    {
        if( $positionId == 0){
            return array('error'=>'parent division not assign or Deleted','id'=>'0');
        }
        $positionData               = $this->where('id', '=', $positionId)->first();
        $isDivision                 = $positionData->is_division;
        $id                         = $positionData->po_level_parent_id;
        if($isDivision == '1'){
            return $this->getDivisionParent($id);
        }
        else{
            $return                 = array();
            $parentData = $positionData;
            if($parentData != ''){
                $userId = $parentData->po_level_user_id;

                if($userId > 0){
                    $userInfo                       = User::where('id','=', $userId)->first();
                    if($userInfo != ''){
                        $parentData                 = $parentData->toArray();
                        $parentData['userName']     = $userInfo->name;
                        $return                 = $parentData;
                    }
                    else{
                        //$return                 = array('error'=>'User deleted of position '.$parentData->po_level_name,'id'=>'0');
                        $return                 = array('error'=>$parentData->po_level_name,'id'=>'0');
                    }
                }
                else{
                    //$return                     = array('error'=>'User not assign to position to position '.$parentData->po_level_name,'id'=>'0');
                    $return                     = array('error'=>$parentData->po_level_name,'id'=>'0');
                }
            }
            else{
                $return                         = array('error'=>'Parent position deleted','id'=>'0');
            }
            return $return;
        }
    }

    public function getDivision($positionId)
    {
        if( $positionId == 0){
            return array('error'=>'Parent division not assign','id'=>'0');
        }
        $positionData               = $this->where('id', '=', $positionId)->first();
        if($positionData != ''){
            $isDivision                 = $positionData->is_division;
            $id                         = $positionData->po_level_parent_id;

            if($isDivision == '0'){
                return $this->getDivisionParent($id);
            }
            else{
                $return                 = array();
                $parentData = $positionData;
                if($parentData != ''){
                    $parentData                 = $parentData->toArray();
                    $return                     = $parentData;
                }
                else{
                    $return                         = array('error'=>'Parent division deleted','id'=>'0');
                }

            }
        }
        else{
            $return                         = array('error'=>'Parent division deleted','id'=>'0');
        }
        return $return;

    }




}
