<?php

namespace App;

use App\Traits\CustomFieldsTrait;
use Illuminate\Database\Eloquent\Model;

class GuardDetails extends Model
{
    use CustomFieldsTrait;

    protected $table = 'guard_details';
    protected $fillable = ['joining_date', 'address', 'guard_name', 'employee_erp', 'center_name', 'employee_cnic',
        'cnic_expiry', 'current_post', 'odoo_id', 'contract', 'mobile', 'gender'];
    protected $dates = ['joining_date'];

}
