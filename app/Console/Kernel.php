<?php

namespace App\Console;

use App\EmployeeDetails;
use App\EmployeePayslips;
use App\GuardDetails;
use App\GuardPayslipLines;
use App\GuardPayslips;
use App\Http\Controllers\Admin\ManagePayslipsController;
use App\Repositories\PayslipRepository;
use App\User;
use function foo\func;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\NewVersion::class,
        Commands\UpdateExchangeRates::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('update-exchange-rate')->daily();
        $schedule->call(function(){
            $odoo_ids = EmployeeDetails::all()->pluck('employee_odoo_id')->toArray();
            $this->payslip = new PayslipRepository();
            if($odoo_ids != null) {
                for($k = 0; $k<count($odoo_ids); $k++)
                {
                    $payslips = $this->payslip->get_payslips_by_user($odoo_ids[$k]);
                    for($i=0; $i<count($payslips);$i++)
                    {
                        $newpayslip = new EmployeePayslips();
                        $user_id = EmployeeDetails::where('employee_odoo_id','=',$odoo_ids[$k])->first();
                        if($user_id) {
                            $payslip = EmployeeDetails::where('hr_payslip_id','=',$payslips[$i]['id']);
                            if($payslip){
                                $newpayslip->user_id = $user_id->user_id;
                                $newpayslip->hr_payslip_date_from = $payslips[$i]['date_from'];
                                $newpayslip->hr_payslip_date_to = $payslips[$i]['date_to'];
                                $newpayslip->hr_payslip_employee_id = $payslips[$i]['employee_id'][0];
                                $newpayslip->hr_payslip_number = $payslips[$i]['number'];
                                $newpayslip->hr_payslip_name = $payslips[$i]['name'];
                                $newpayslip->hr_payslip_id = $payslips[$i]['id'];
                                $newpayslip->is_sync = true;
                                $newpayslip->save();
                                for ($j = 0; $j<count($payslips[$i]['line_ids']); $j++)
                                {
                                    $new_line = new EmployeePayslipLines();
                                    $new_line->payslip_id = $newpayslip->id;
                                    $new_line->hr_payslip_line_id = $payslips[$i]['line_ids'][$j];
                                    $new_line->save();
                                }
                            }
                        }
                    }
                }
            }
        })->dailyAt('01:00');
    }

    protected function get_guard_payslip_cron(Schedule $schedule)
    {
        $schedule->call(function(){
            $odoo_ids = GuardDetails::all()->pluck('employee_odoo_id')->toArray();
            $date_from = Carbon::now()->startofMonth()->subMonth(3)->startOfMonth()->toDateString();
            $date_to = Carbon::now()->startofMonth()->subMonth()->endOfMonth()->toDateString();
            $this->payslip = new PayslipRepository();
            if($odoo_ids != null) {
                for($k = 0; $k<count($odoo_ids); $k++)
                {
                    $payslips = $this->payslip->get_payslips_by_guard_by_date($odoo_ids[$k],$date_from,$date_to);
                    for($i=0; $i<count($payslips);$i++)
                    {
                        $newpayslip = new GuardPayslips();
                        $guard_id = GuardDetails::where('employee_odoo_id','=',$odoo_ids[$k])->first();
                        if($guard_id) {
                            $payslip = GuardPayslips::where('id','=',$payslips[$i]['id'])->where('guard_id','=',$guard_id->id)->get();
                            if($payslip){
                                $newpayslip->guard_id = $guard_id->id;
                                $newpayslip->guards_payslip_date_from = $payslips[$i]['date_from'];
                                $newpayslip->guards_payslip_date_to = $payslips[$i]['date_to'];
                                $newpayslip->guards_payslip_contract_id = $payslips[$i]['contract_id'] == null ? null : $payslips[$i]['contract_id'][0];
                                $newpayslip->guards_payslip_contract_name = $payslips[$i]['contract_id'] == null ? null : $payslips[$i]['contract_id'][1];
                                $newpayslip->guards_payslip_number = $payslips[$i]['number'];
                                $newpayslip->guards_payslip_struct_id = $payslips[$i]['struct_id'] == null ? null : $payslips[$i]['struct_id'][0];
                                $newpayslip->guards_payslip_struct_name = $payslips[$i]['struct_id'] == null ? null : $payslips[$i]['struct_id'][1];
                                $newpayslip->guards_payslip_name = $payslips[$i]['name'];
                                $newpayslip->guards_payslip_bank = $payslips[$i]['bank'] == null ? null : $payslips[$i]['bank'][1];
                                $newpayslip->guards_payslip_bankacctitle = $payslips[$i]['bankacctitle'];
                                $newpayslip->guards_payslip_bankacc = $payslips[$i]['bankacc'];
                                $newpayslip->guards_payslip_id = $payslips[$i]['id'];
                                $newpayslip->is_sync = true;
                                $newpayslip->save();
                                for ($j = 0; $j<count($payslips[$i]['line_ids']); $j++)
                                {
                                    $new_line = new GuardPayslipLines();
                                    $new_line->guard_payslips_id = $newpayslip->id;
                                    $new_line->guards_payslip_line_id = $payslips[$i]['line_ids'][$j];
                                    $new_line->save();
                                }
                            }
                        }
                    }
                }
            }
        })->dailyAt('04:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
    
}
