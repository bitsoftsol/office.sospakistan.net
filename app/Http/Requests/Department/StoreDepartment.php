<?php
/**
 * Created by PhpStorm.
 * User: Hafiz Siddiq
 * Date: 11/24/2018
 * Time: 10:21 AM
 */

namespace App\Http\Requests\Department;


use App\Http\Requests\CoreRequest;

class StoreDepartment extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}