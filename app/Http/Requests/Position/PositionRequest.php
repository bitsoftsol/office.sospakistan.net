<?php

namespace App\Http\Requests\Position;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class PositionRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredData                   = array('name' => 'required', 'order' => 'required', 'status' => 'required');
        if(!isset($_REQUEST['isFirstPosition'])){
            $requiredData['parent']         = 'required';
        }
        return $requiredData;
    }
}
