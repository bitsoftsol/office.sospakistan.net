<?php

namespace App\Http\Controllers\Member;

use App\EmployeeDetails;
use App\EmployeePayslipLines;
use App\EmployeePayslips;
use App\GuardDetails;
use App\GuardPayslipLines;
use App\GuardPayslips;
use App\Helper\Reply;
use App\Http\Requests\ProjectMembers\StoreProjectMembers;
use App\Http\Requests\Tasks\StoreTask;
use App\Http\Requests\User\UpdateProfile;
use App\Issue;
use App\ModuleSetting;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Project;
use App\ProjectActivity;
use App\ProjectMember;
use App\ProjectTimeLog;
use App\Repositories\PayslipRepository;
use App\Task;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Symfony\Component\VarDumper\Cloner\Data;
use Yajra\Datatables\Facades\Datatables;

class MemberManageGuardPayslipsController extends MemberBaseController

{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Guard Payslips';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->payslip = new PayslipRepository();
    }

    public function index()
    {
        $this->guards = GuardDetails::all();
        return view('member.guard-payslips.index', $this->data);
    }

    public function ajax_get_payslips($id)
    {
        $return = array();

        $return['error'] = 1;

        $return['msg'] = 'Data Not Found';

        $return['data'] = array();

        $guards = GuardPayslips::where('guard_id','=',$id)->get();

        if (!empty($guards)) {

            $guards_html = $this->guards_html($guards);

            $return['error'] = 0;

            $return['msg'] = 'Data Found';

            $return['data'] = array('guards' => $guards_html);

        }
        echo json_encode($return);
        exit;
    }

    public function guards_html($data)
    {

        $html = '';

        $count = 0;

        foreach ($data as $dataIn) {

            if ($count == 0 && isset($dataIn->id) && $dataIn->id != '') {

                $html .= "<option value=''>Select Payslips</option>";

            }

            $html .= "<option value='" . $dataIn->id . "'>" . $dataIn->guards_payslip_name . "</option>";

            $count++;
        }
        return $html;
    }

    public function get_odoo()
    {
        EmployeePayslips::where('user_id','=',$this->user->id)->delete();
        $odoo_id = EmployeeDetails::where('user_id', $this->user->id)->pluck('employee_odoo_id')->toArray();
        if($odoo_id != null)
        {
            $payslips = $this->payslip->get_payslips_by_user($odoo_id);
            for($i=0; $i<count($payslips);$i++)
            {
                $newpayslip = new EmployeePayslips();
                $newpayslip->user_id = $this->user->id;
                $newpayslip->hr_payslip_date_from = $payslips[$i]['date_from'];
                $newpayslip->hr_payslip_date_to = $payslips[$i]['date_to'];
                $newpayslip->hr_payslip_employee_id = $payslips[$i]['employee_id'][0];
                $newpayslip->hr_payslip_number = $payslips[$i]['number'];
                $newpayslip->hr_payslip_name = $payslips[$i]['name'];
                $newpayslip->hr_payslip_id = $payslips[$i]['id'];
                $newpayslip->is_sync = true;
                $newpayslip->save();
                for ($j = 0; $j<count($payslips[$i]['line_ids']); $j++)
                {
                    $new_line = new EmployeePayslipLines();
                    $new_line->payslip_id = $newpayslip->id;
                    $new_line->hr_payslip_line_id = $payslips[$i]['line_ids'][$j];
                    $new_line->save();
                }
            }
        }
        return redirect(route('member.payslips.index'));
    }

    public function data($guard_id,$slip_id)
    {
        if($slip_id != null){
            $pay_slip_lines = GuardPayslipLines::where('guard_payslips_id','=',$slip_id)->get();
            foreach ($pay_slip_lines as $line)
            {
                if($line->is_sync != true)
                {
                    $odoo_line = $this->payslip->get_guard_payslip_lines($line->guards_payslip_line_id);
                    $existing_line = GuardPayslipLines::find($line->id);
                    if($existing_line)
                    {
                        for ($i = 0; $i<count($odoo_line); $i++)
                        {
                            $existing_line->guards_payslip_line_name = $odoo_line[$i]['name'];
                            $existing_line->guards_payslip_line_post_id = $odoo_line[$i]['post_id'] == null ? null : $odoo_line[$i]['post_id'][0];
                            $existing_line->guards_payslip_line_post_name = $odoo_line[$i]['post_id'] == null ? null : $odoo_line[$i]['post_id'][1];
                            $existing_line->guards_payslip_line_sequence = $odoo_line[$i]['sequence'];
                            $existing_line->guards_payslip_line_code = $odoo_line[$i]['code'];
                            $existing_line->guards_payslip_line_rate = $odoo_line[$i]['rate'];
                            $existing_line->guards_payslip_line_quantity = $odoo_line[$i]['quantity'];
                            $existing_line->guards_payslip_line_category_id = $odoo_line[$i]['category_id'] == null ? null : $odoo_line[$i]['category_id'][0];
                            $existing_line->guards_payslip_line_category_name = $odoo_line[$i]['category_id'] == null ? null : $odoo_line[$i]['category_id'][1];
                            $existing_line->guards_payslip_line_rule_id = $odoo_line[$i]['salary_rule_id'] == null ? null : $odoo_line[$i]['salary_rule_id'][0];
                            $existing_line->guards_payslip_line_rule_name = $odoo_line[$i]['salary_rule_id'] == null ? null : $odoo_line[$i]['salary_rule_id'][1];
                            $existing_line->guards_payslip_line_amount = $odoo_line[$i]['amount'];
                            $existing_line->guards_payslip_line_total = $odoo_line[$i]['total'];
                            $existing_line->is_sync = true;
                            $existing_line->save();
                        }
                    }
                }
            }

            $final_lines = GuardPayslipLines::where('guard_payslips_id','=',$slip_id)->get();
            return Datatables::of($final_lines)->make(true);
        }
    }

}
