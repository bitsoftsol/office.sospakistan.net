<?php

namespace App\Http\Controllers\Member;

use App\Department;
use App\EmployeeDetails;
use App\GuardDetails;
use App\Helper\Reply;
use App\Http\Controllers\Member\MemberBaseController;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateEmployee;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\PositionLevel;
use App\Project;
use App\ProjectTimeLog;
use App\Repositories\EmployeeRepository;
use App\Role;
use App\SosCenter;
use App\Task;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;


class MemberGuardsController extends MemberBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.employees');
        $this->odooModel = 'hr.employee';
        $this->model = new EmployeeRepository($this->odooModel);
        $this->pageIcon = 'icon-user';
        $this->mUserType = 'member';
        if(!ModuleSetting::checkModule('guards')){
            abort(403);
        }
    }

    public function index() {
//        if(!($this->user->hasRole('employee') || $this->user->can('view_guards'))){
//            abort(403);
//        }
        $this->totalGaurds = count(GuardDetails::all());
        return view('member.guards.index', $this->data);
    }

    public function create() {
      return 'Create';
    }

    public function store(StoreUser $request) {
        return 'Store';
    }

    public function show($id) {
        return 'Show';
    }

    public function edit($id) {
        if(!$this->user->can('edit_guards')){
            abort(403);
        }
        $this->employeeDetail = GuardDetails::where('id', '=', $id)->first();
        //Updated By Hafiz For Integration
        if($this->employeeDetail->is_sync == false){
            $odoo_id = $this->employeeDetail->employee_odoo_id;
            $update_data = $odoo_id == null ? null : $this->model->get_details_by_id($odoo_id)->first();
            if($update_data != null){
                $this->employeeDetail->first_name = $update_data['name'];
                $this->employeeDetail->father_name = $update_data['fathername'];
                $this->employeeDetail->mother_name = $update_data['mothername'];
                $this->employeeDetail->country_id = $update_data['country_id'] == null ? null : $update_data['country_id'][0];
                $this->employeeDetail->country_name = $update_data['country_id'] == null ? null : $update_data['country_id'][1];
                $this->employeeDetail->last_qualification = $update_data['education_id'] == null ? null : $update_data['education_id'][1];
                $this->employeeDetail->height = $update_data['height'];
                $this->employeeDetail->weight = $update_data['weight'];
                $this->employeeDetail->employee_cnic = $update_data['cnic'];
                $this->employeeDetail->cnic_expiry = $update_data['cnic_expiry'];
                $this->employeeDetail->birthday = $update_data['birthday'];
                $this->employeeDetail->marital_status = $update_data['marital'];
                $this->employeeDetail->bank_odoo_id = $update_data['bank_id'] == null ? null : $update_data['bank_id'][0];
                $this->employeeDetail->bank_name = $update_data['bank_id'] == null ? null : $update_data['bank_id'][1];
                $this->employeeDetail->bank_branch = $update_data['branch'];
                $this->employeeDetail->bank_account_title = $update_data['bankacctitle'];
                $this->employeeDetail->bank_account_number = $update_data['bankacc'];
                $this->employeeDetail->number_of_children = $update_data['childno'];
                $this->employeeDetail->blood_group = $update_data['bloodgroup_id'] == null ? null : $update_data['bloodgroup_id'][1];
                $this->employeeDetail->address = $update_data['street'] .' '.$update_data['street2'];
                $this->employeeDetail->basic_salary = $update_data['basicpay'];
                $this->employeeDetail->joining_date = $update_data['appointmentdate'];
                $this->employeeDetail->force_no = $update_data['armyno'];
                $this->employeeDetail->m_joining_date = $update_data['joindate'];
                $this->employeeDetail->m_leaving_date = $update_data['dischargedate'];
                $this->employeeDetail->force_type = $update_data['forcetype'];
                $this->employeeDetail->force_rank = $update_data['rank'];
                $this->employeeDetail->last_unit = $update_data['lastunit'];
                $this->employeeDetail->last_center = $update_data['lastcenter'];
                if($update_data['image_medium'] != null){
                    $this->employeeDir($this->employeeDetail->id);
                    $imageName              = md5(rand(100,10000));
                    $camProfileImage        = $update_data['image_medium'];
                    $data                   = base64_decode($camProfileImage);
                    file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
                    $this->employeeDetail->profile_image      = $imageName.".png";
                }
                $this->employeeDetail->left_fin_1 = $update_data['l_index_finger'];
                $this->employeeDetail->left_fin_2 = $update_data['l_middle_finger'];
                $this->employeeDetail->left_fin_3 = $update_data['l_ring_finger'];
                $this->employeeDetail->left_fin_4 = $update_data['l_baby_finger'];
                $this->employeeDetail->left_thumb = $update_data['l_thumb'];
                $this->employeeDetail->right_thumb = $update_data['r_thumb'];
                $this->employeeDetail->right_fin_1 = $update_data['r_index_finger'];
                $this->employeeDetail->right_fin_2 = $update_data['r_middle_finger'];
                $this->employeeDetail->right_fin_3 = $update_data['r_ring_finger'];
                $this->employeeDetail->right_fin_4 = $update_data['r_baby_finger'];
                $this->employeeDetail->is_guard = $update_data['is_guard'];
                $this->employeeDetail->erp_type = 'odoo';
                $this->employeeDetail->employee_type = $update_data['is_guard'] == false ? 'staff' : 'guard';
                $this->employeeDetail->is_sync = true;
                $this->employeeDetail->save();
            }
        }
        //End
        $divisions = PositionLevel::division();
        $this->divisions = $divisions;
        $departments = Department::all();
        $this->departments = $departments;
        $this->positions = PositionLevel::all();
        $this->employeeFolder = asset('employee/'.$id).'/';
        $this->isMilitryRecordShow = (isset($this->employeeDetail->military_person) && $this->employeeDetail->military_person == 1)?'block':'none';

        /*        if(!is_null($this->employeeDetail)){
                    $this->employeeDetail = $this->employeeDetail->withCustomFields();
                    $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
                }*/
        return view('member.guards.edit', $this->data);
    }

    public function updateOdoo(UpdateEmployee $request, $id)
    {

        ini_set('max_input_vars','2000' );
        config(['filesystems.default' => 'local']);
        echo "<pre>";
        print_r($id);
        exit;
        $guard = GuardDetails::find($id);
        $guard->first_name = $request->input('first_name');
        $guard->mobile = $request->input('mobile');
        $guard->gender = $request->input('gender');

        $this->employeeDir($id);

        if($request->input('image_profile_cam') == ''){

            if ($request->hasFile('image_profile')) {
                File::delete($this->employeeDirPath . '/' . $user->image);
                $user->image = $request->image_profile->hashName();
                $request->image_profile->store($this->employeeDirPath);
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $user->image);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }
        }
        else{
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $request->input('image_profile_cam');
            $base_to_php            = explode(',', $camProfileImage);
            $data                   = base64_decode($base_to_php[1]);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $guard->profile_image      = $imageName.".png";
        }

        $is_cnic = $guard->cnic_copy;


        $is_nadra = $guard->nadra_attested;
        $is_education = $guard->emp_edu_certificate;

        $guard->job_title = $request->input('job_title');

        $guard->address = $request->input('address');

        $guard->hourly_rate = $request->input('hourly_rate');

        $guard->slack_username = $request->input('slack_username');

        $guard->joining_date = Carbon::parse($request->input('joining_date'))->format('Y-m-d');

        if($request->input('sigImageInput') != ''){
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $request->input('sigImageInput');
            $base_to_php            = explode(',', $camProfileImage);
            $data                   = base64_decode($base_to_php[1]);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $guard->signature_image      = $imageName.".png";
        }
        if ($request->hasFile('image_cnic')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_cnic);

            $guard->image_cnic = $request->image_cnic->hashName();
            $request->image_cnic->store($this->employeeDirPath);
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_cnic);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_cnic = true;
        }

        if ($request->hasFile('image_police_varification')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_police_varification);

            $guard->image_police_varification = $request->image_police_varification->hashName();
            $request->image_police_varification->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_police_varification);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_referee_undertaking')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_referee_undertaking);

            $guard->image_referee_undertaking = $request->image_referee_undertaking->hashName();
            $request->image_referee_undertaking->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_referee_undertaking);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }        if ($request->hasFile('image_emp_aggre')) {
        File::delete($this->employeeDirPath . '/' . $guard->image_emp_aggre);

        $guard->image_emp_aggre = $request->image_emp_aggre->hashName();
        $request->image_emp_aggre->store($this->employeeDirPath);

        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img = Image::make($this->employeeDirPath . '/' . $guard->image_emp_aggre);
        $img->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save();
    }


        if ($request->hasFile('image_pen_book')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_pen_book);

            $guard->image_pen_book = $request->image_pen_book->hashName();
            $request->image_pen_book->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_pen_book);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_nadra')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_nadra);

            $guard->image_nadra = $request->image_nadra->hashName();
            $request->image_nadra->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_nadra);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_nadra = true;
        }

        if ($request->hasFile('image_education')) {
            File::delete($this->employeeDirPath . '/' . $guard->image_education);

            $guard->image_education = $request->image_cnic->hashName();
            $request->image_education->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $guard->image_education);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_education = true;
        }

        $guard->erp_type = $request->input('ERPType');
        $guard->employee_type = $request->input('employeeType');
        $guard->odoo_center_id = $request->input('center_id');
        $guard->division_id = $request->input('divisions');
        $guard->department_id = $request->input('departments');
        $guard->position_id = $request->input('positions');
        $guard->employee_cnic = $request->input('cnic');
        $guard->marital_status = $request->input('marital_status');
        $guard->number_of_children = $request->input('no_of_child');
        $guard->bank_account_number = $request->input('account_no');
        $guard->air_ticket = $request->input('air_ticket');
        $guard->father_name = $request->input('fatherName');
        $guard->category_ids = $request->input('category_ids');
        $guard->work_address = $request->input('working_address');
        $guard->work_location = $request->input('work_location');
        $guard->work_phone = $request->input('work_phone');
        $guard->parent_coach_user_id = $request->input('manager_id');
        $guard->is_manager = ($request->input('is_manager') != '') ? true : false;
        $guard->country_name = $request->input('nationality');
        $guard->passport_number = $request->input('passport_no');
        $guard->bank_name = $request->input('bank_name');
        $guard->bank_account_title = $request->input('account_title');
        $guard->place_of_birth = $request->input('nationality');
        $guard->cnic_expiry = $request->input('cnic_expiry');
        $guard->birthday = $request->input('birthday');
        $guard->cnic_copy = $is_cnic;
        $guard->education = $request->input('education');
        $guard->first_name = $request->input('first_name');
        $guard->middle_name = $request->input('middle_name');
        $guard->last_name = $request->input('last_name');
        $guard->mother_name = $request->input('mother_name');
        $guard->home_city = $request->input('home_city');
        $guard->last_qualification = $request->input('last_qualification');
        $guard->height = $request->input('height');
        $guard->weight = $request->input('weight');
        $guard->bank_branch = $request->input('bank_branch');
        $guard->bank_city = $request->input('bank_city');
        $guard->blood_group = $request->input('blood_group');
        $guard->land_line_number = $request->input('land_line_number');
        $guard->personal_email = $request->input('personal_email');
        $guard->kin_1_name = $request->input('kin_1_name');
        $guard->kin_1_number = $request->input('kin_1_number');
        $guard->kin_1_relation = $request->input('kin_1_relation');
        $guard->kin_2_name = $request->input('kin_2_name');
        $guard->kin_2_number = $request->input('kin_2_number');
        $guard->kin_2_relation = $request->input('kin_2_relation');
        $guard->basic_salary = $request->input('basic_salary');
        $guard->duty_hours = $request->input('duty_hours');
        $guard->duty_days = $request->input('duty_days');
        $guard->annual_leaves = $request->input('annual_leaves');
        $guard->air_ticket = $request->input('air_ticket');
        $guard->attendance_logging = $request->input('attendance_logging');
        $guard->rec_type = $request->input('rec_type');
        $guard->travel_benefits = $request->input('travel_benefits');
        $guard->travel_expenses = $request->input('travel_expenses');
        $guard->over_time_pay = $request->input('over_time_pay');
        $guard->email_pro = ($request->input('email_pro') != '') ? true : false;
        $guard->sim = ($request->input('sim') != '') ? true : false;
        $guard->mobile_phone = ($request->input('mobile_phone') != '') ? true : false;
        $guard->office = ($request->input('office') != '') ? true : false;
        $guard->office_stationary = ($request->input('office_stationary') != '') ? true : false;
        $guard->pc = ($request->input('pc') != '') ? true : false;
        $guard->laptop = ($request->input('laptop') != '') ? true : false;
        $guard->vehicle = ($request->input('vehicle') != '') ? true : false;
        $guard->uniform = ($request->input('uniform') != '') ? true : false;
        $guard->belt = ($request->input('belt') != '') ? true : false;
        $guard->shoes = ($request->input('shoes') != '') ? true : false;
        $guard->caps = ($request->input('caps') != '') ? true : false;
        $guard->badges = ($request->input('badges') != '') ? true : false;
        $guard->ref_1_name = $request->input('ref_1_name');
        $guard->ref_1_number = $request->input('ref_1_number');
        $guard->rel_to_ref1 = $request->input('rel_to_ref1');
        $guard->ref_1_address = $request->input('ref_1_address');
        $guard->ref_1_cnic = $request->input('ref_1_cnic');
        $guard->ref_2_name = $request->input('ref_2_name');
        $guard->ref_2_number = $request->input('ref_2_number');
        $guard->rel_to_ref2 = $request->input('rel_to_ref2');
        $guard->ref_2_address = $request->input('ref_2_address');
        $guard->ref_2_cnic = $request->input('ref_2_cnic');
        $guard->military_person = ($request->input('military_person') != '') ? true : false;
        $guard->force_no = $request->input('force_no');
        $guard->m_joining_date = $request->input('m_joining_date');
        $guard->m_leaving_date = $request->input('m_leaving_date');
        $guard->force_type = $request->input('force_type');
        $guard->force_rank = $request->input('force_rank');
        $guard->last_unit = $request->input('last_unit');
        $guard->last_center = $request->input('last_center');
        $guard->last_company_name = $request->input('last_company_name');
        $guard->last_position = $request->input('last_position');
        $guard->last_joining_date = $request->input('last_joining_date');
        $guard->last_leaving_date = $request->input('last_leaving_date');
        $guard->last_working_city = $request->input('last_working_city');
        $guard->last_working_country = $request->input('last_working_country');
        $guard->varification_date = $request->input('varification_date');
        $guard->left_fin_1 = $request->input('left_fin_1');
        $guard->left_fin_2 = $request->input('left_fin_2');
        $guard->left_fin_3 = $request->input('left_fin_3');
        $guard->left_fin_4 = $request->input('left_fin_4');
        $guard->left_thumb = $request->input('left_thumb');
        $guard->right_thumb = $request->input('right_thumb');
        $guard->right_fin_1 = $request->input('right_fin_1');
        $guard->right_fin_2 = $request->input('right_fin_2');
        $guard->right_fin_3 = $request->input('right_fin_3');
        $guard->right_fin_4 = $request->input('right_fin_4');
        $guard->nadra_attested = $is_nadra;
        $guard->emp_edu_certificate = $is_education;
        $guard->is_guard = ($request->input('is_guard') != '') ? true : false;
        $return = $this->model->edit_guard($guard);
        /*fields that are not filling
        hourly_rate
        slack_username
        employee_odoo_id
        employee_erp
        is_guard
        work_location
        work_phone
        parent_coach_user_id
        passport_number
        bank_odoo_id
        resign_date
        education
        education_odoo_id
        education_certificate
        emp_cv
        emp_photographs
        country_id
        image_reference_undertaking
        air_ticket
        travel_expenses
        over_time_pay
        odoo_segment_id
        odoo_sub_segment_id
        odoo_department_id
        signature
        */
        $guard->save();

        /*add user id to position qasim*/
        $positionId = $request->input('positions');
        if($positionId != ''){
            $positionLevel      = new PositionLevel();
            $positionLevel->positionAssignToUser($positionId, $user->id);
        }

        /*add user id to position end*/

        // To add custom fields data

        return Reply::redirect(route($this->mUserType.'.guards.index'), __('messages.employeeUpdated'));
    }

    public function updateGuard(UpdateEmployee $request, $id)
    {
        $guard = GuardDetails::findOrFail($id);

        $guard->name = $request->input('name');

        $guard->mobile = $request->input('mobile');

        $guard->gender = $request->input('gender');

        $is_cnic = false;

        $guard->address = $request->address;

        $guard->hourly_rate = $request->hourly_rate;

        $guard->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');

        $guard->employee_cnic = $request->cnic;

        $guard->father_name = $request->fatherName;

        $guard->contract = $request->contract;

        $guard->work_address = $request->working_address;

        $guard->work_location = $request->work_location;

        $guard->work_phone = $request->work_phone;

        $guard->parent_coach_user_id = $request->manager_id;

        $guard->is_manager = isset($request['is_manager']) ? true : false;

        $guard->country_name = $request->nationality;

        $guard->passport_number = $request->passport_no;

        $guard->bank_name = $request->bank_name;

        $guard->bank_account_title = $request->account_title;

        $guard->place_of_birth = $request->nationality;

        $guard->cnic_expiry = $request->cnic_expiry;

        $guard->cnic_copy = $is_cnic;

        $guard->education = $request->education;

        $guard->position_id = $request->position_id;

        $guard->first_name = $request->first_name;

        $guard->middle_name = $request->middle_name;

        $guard->last_name = $request->last_name;

        $guard->mother_name = $request->mother_name;

        $guard->home_city = $request->home_city;

        $guard->last_qualification = $request->last_qualification;

        $request->height = $request->height;

        $guard->weight = $request->weight;

        $guard->bank_branch = $request->bank_branch;

        $guard->bank_city = $request->bank_city;

        $guard->blood_group = $request->blood_group;

        // $employee->signature = $request->signature;

        $guard->land_line_number = $request->land_line_number;

        $guard->personal_email = $request->personal_email;

        $guard->kin_1_name = $request->kin_1_name;

        $guard->kin_1_number = $request->kin_1_number;

        $guard->kin_1_relation = $request->kin_1_relation;

        $guard->kin_2_name = $request->kin_2_name;

        $guard->kin_2_number = $request->kin_2_number;

        $guard->kin_2_relation = $request->kin_2_relation;

        $guard->basic_salary = $request->basic_salary;

        $guard->duty_hours = $request->duty_hours;

        $guard->duty_days = $request->duty_days;

        $guard->annual_leaves = $request->annual_leaves;

        $guard->air_ticket = $request->air_ticket;

        $guard->attendance_logging = $request->attendance_logging;

        $guard->rec_type = $request->rec_type;

        $guard->travel_benifits = $request->travel_benifits;

        $guard->travel_expenses = $request->travel_expensis;

        $guard->over_time_pay = $request->over_time_pay;

        $guard->email_pro = isset($request->email_pro) ? true : false;

        $guard->sim = isset($request->sim) ? true : false;

        $guard->mobile_phone = isset($request->mobile_phone) ? true : false;

        $guard->office = isset($request->office) ? true : false;

        $guard->office_stationary = isset($request->office_stationary) ? true : false;

        $guard->pc = isset($request->pc) ? true : false;

        $guard->laptop = isset($request->laptop) ? true : false;

        $guard->vehicle = isset($request->vehicle) ? true : false;

        $guard->ref_1_name = $request->ref_1_name;

        $guard->ref_1_number = $request->ref_1_number;

        $guard->rel_to_ref1 = $request->rel_to_ref1;

        $guard->ref_1_address = $request->ref_1_address;

        $guard->ref_1_cnic = $request->ref_1_cnic;

        $guard->ref_2_name = $request->ref_2_name;

        $guard->ref_2_number = $request->ref_2_number;

        $guard->rel_to_ref2 = $request->rel_to_ref2;

        $guard->ref_2_address = $request->ref_2_address;

        $guard->ref_2_cnic = $request->ref_2_cnic;

        $guard->military_person = isset($request->military_person) ? true : false;

        $guard->force_no = $request->force_no;

        $guard->m_joining_date = $request->m_joining_daate;

        $guard->m_leaving_date = $request->m_leaving_date;

        $guard->force_type = $request->force_type;

        $guard->force_rank = $request->force_rank;

        $guard->last_unit = $request->last_unit;

        $guard->last_center = $request->last_center;

        $guard->last_company_name = $request->last_company_name;

        $guard->last_position = $request->last_position;

        $guard->last_joining_date = $request->last_joining_date;

        $guard->last_leaving_date = $request->last_leaving_date;

        $guard->last_working_city = $request->last_working_city;

        $guard->last_working_country = $request->last_working_country;

        if ($request->hasFile('image_police_varification')) {

            File::delete('user-uploads/avatar/'.$guard->image_police_varification);



            $guard->image_police_varification = $request->image_police_varification->hashName();

            $request->image_police_varification->store('user-uploads/avatar');



            // resize the image to a width of 300 and constrain aspect ratio (auto height)

            $img = Image::make('user-uploads/avatar/'.$guard->image_police_varification);

            $img->resize(300, null, function ($constraint) {

                $constraint->aspectRatio();

            });

            $img->save();

        }

        $guard->varification_date = $request->varification_date;



        if ($request->hasFile('image_referee_undertaking')) {

            File::delete('user-uploads/avatar/'.$guard->image_referee_undertaking);



            $guard->image_referee_undertaking = $request->image_referee_undertaking->hashName();

            $request->image_referee_undertaking->store('user-uploads/avatar');



            // resize the image to a width of 300 and constrain aspect ratio (auto height)

            $img = Image::make('user-uploads/avatar/'.$guard->image_referee_undertaking);

            $img->resize(300, null, function ($constraint) {

                $constraint->aspectRatio();

            });

            $img->save();

        }



        if ($request->hasFile('image_emp_aggre')) {

            File::delete('user-uploads/avatar/'.$guard->image_emp_aggre);



            $guard->image_emp_aggre = $request->image_emp_aggre->hashName();

            $request->image_emp_aggre->store('user-uploads/avatar');



            // resize the image to a width of 300 and constrain aspect ratio (auto height)

            $img = Image::make('user-uploads/avatar/'.$guard->image_emp_aggre);

            $img->resize(300, null, function ($constraint) {

                $constraint->aspectRatio();

            });

            $img->save();

        }



        if ($request->hasFile('image_pen_book')) {

            File::delete('user-uploads/avatar/'.$guard->image_pen_book);



            $guard->image_pen_book = $request->image_pen_book->hashName();

            $request->image_pen_book->store('user-uploads/avatar');



            // resize the image to a width of 300 and constrain aspect ratio (auto height)

            $img = Image::make('user-uploads/avatar/'.$guard->image_pen_book);

            $img->resize(300, null, function ($constraint) {

                $constraint->aspectRatio();

            });

            $img->save();

        }



        $guard->image_fin_1 = $request->image_fin_1;

        $guard->image_fin_2 = $request->image_fin_2;

        $guard->image_fin_3 = $request->image_fin_3;

        $guard->image_fin_4 = $request->image_fin_4;

        $guard->left_thumb = $request->left_thumb;

        $guard->right_thumb = $request->right_thumb;

        $guard->right_fin_1 = $request->right_fin_1;

        $guard->right_fin_2 = $request->right_fin_2;

        $guard->right_fin_3 = $request->right_fin_3;

        $guard->right_fin_4 = $request->right_fin_4;

        $guard->military_person = isset($request->military_person) ? true : false;

        $guard->force_no = $request->force_no;

        $guard->m_joining_date = $request->m_joining_date;

        $guard->m_leaving_date = $request->m_leaving_date;

        $guard->force_type = $request->force_type;

        $guard->force_rank = $request->force_rank;

        $guard->last_unit = $request->last_unit;

        $guard->last_center = $request->last_center;

        $guard->save();

        return Reply::redirect(route('member.guards.index'), __('messages.employeeUpdated'));

    }

    public function update(UpdateEmployee $request, $id) {
        return 'Update';
    }

    public function destroy($id) {
       return 'Destroy';
    }

    public function data() {
        ini_set('memory_limit','1024M');
        $guards = GuardDetails::all();
        $total_collumns = DB::select(DB::raw("SELECT count(*) as total_count FROM information_schema.columns WHERE table_name = 'guard_details'"));
        foreach ($guards as $guard){
            $filled_values = DB::select(DB::raw("SELECT 
                                count(division_id) + count(department_id) + count(job_title) + count(address)
                              + count(joining_date) + count(father_name)
                              + count(category_ids) + count(work_address) + count(work_location) + count(work_phone) + count(parent_coach_user_id)
                              + count(country_name) + count(passport_number) + count(bank_name)
                              + count(bank_account_title) + count(bank_account_number) + count(marital_status) + count(number_of_children) + count(birthday) 
                              + count(place_of_birth) + count(cnic_expiry) + count(cnic_copy) + count(nadra_attested) + count(education)
                              + count(emp_edu_certificate) + count(emp_cv) + count(emp_photographs)
                              + count(image_education) + count(image_cnic) + count(image_nadra) + count(position_id) + count(first_name) + count(middle_name)
                              + count(last_name) + count(mother_name) + count(home_city) + count(last_qualification) + count(height) + count(weight) + count(bank_branch)
                              + count(bank_city) + count(blood_group) + count(signature) + count(land_line_number) + count(personal_email) + count(kin_1_name)
                              + count(kin_1_number) + count(kin_1_relation) + count(kin_2_name) + count(kin_2_number) + count(kin_2_relation) + count(basic_salary)
                              + count(duty_hours) + count(duty_days) + count(annual_leaves) + count(air_ticket) + count(attendance_logging) + count(rec_type) 
                              + count(travel_expenses) + count(over_time_pay) + count(ref_1_name) + count(ref_1_number) + count(rel_to_ref1) + count(ref_1_address) + count(ref_1_cnic) 
                              + count(ref_2_name) + count(ref_2_number) + count(rel_to_ref2) + count(ref_2_address) + count(ref_2_cnic) + count(military_person)
                              + count(force_no) + count(m_joining_date) + count(m_leaving_date) + count(force_type) + count(force_rank) + count(last_unit) 
                              + count(last_center) + count(last_company_name) + count(last_position) + count(last_joining_date) + count(last_leaving_date)
                              + count(last_working_city) + count(last_working_country) + count(image_police_varification) + count(varification_date) 
                              + count(image_referee_undertaking) + count(image_emp_aggre) + count(image_pen_book) + count(left_fin_1)
                              + count(left_fin_2) + count(left_fin_3) + count(left_fin_4) + count(left_thumb) + count(right_thumb)
                              + count(right_fin_1) + count(right_fin_2) + count(right_fin_3) + count(right_fin_4) + count(employee_cnic) + count(employee_odoo_id)
                              + count(employee_erp) + count(odoo_center_id) 
                              AS total_not_null_count FROM guard_details where id =".$guard->id));
            $completion_percent = ($filled_values[0]->total_not_null_count/$total_collumns[0]->total_count)*100;
            $guard->setAttribute('completion_precent', number_format($completion_percent,2,'.',''));
        }
        return Datatables::of($guards)
            ->addColumn('action', function ($row) {
                if($this->user->can('edit_guards')){
                    return '<a href="' . route('member.guards.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }
                return 'No Action';
            })
            ->addColumn('odoo_center_name', function($row){
                $center_name = SosCenter::where('odoo_id','=',$row->odoo_center_id)->first();
                if($center_name){
                    return $center_name->name;
                }
                return 'No Center';
            })
            ->addColumn('erp',function ($row){
                return 'Odoo';
            })
            ->editColumn('completion_precent',function ($row){
                if ($row->completion_precent < 50) {
                    $statusColor = 'danger';
                }
                elseif ($row->completion_precent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                }
                else {
                    $statusColor = 'success';
                }
                $style = 'style="width: ' . $row->completion_precent . '%"';
                return '<h5>' . $row->completion_precent . '%<span class="pull-right"></span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_precent . '" aria-valuemin="0" aria-valuemax="100" '.$style.' role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->editColumn('first_name', function ($row) {
                $getEmployeeImage = $this->get_guard_image($row->id);
                $image = '<img src="' . $getEmployeeImage . '" alt="user" class="img-circle" width="30"> ';
                return '<a href="' . route('member.guards.show', $row->id) . '">' . $image . $row->first_name. ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-success">' . __('app.employee') . '</label>';
            })
            ->rawColumns(['first_name', 'action','completion_precent'])
            ->make(true);
    }

    public function export() {
        $rows = GuardDetails::all();

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Address', 'Name', 'ERP NO', 'Center Name', 'CNIC', 'CNIC Expiry', 'Current Post', 'Odoo ID', 'Contract', 'Mobile','Gender','Hourly Rate', 'Created At','Updated At', 'Joining Date'];


        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Employees', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Guards Data');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Guards File');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('guard_details', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });


        })->download('xlsx');
    }

    public function importOdoo()
    {
        set_time_limit(1000);
        GuardDetails::truncate();
        $fields = array('is_guard' => true, 'active' => true, 'current' => true);
        $guards = $this->model->allguards($fields);
        $laravelGuards = count(GuardDetails::all());
        for($i=0; $i<count($guards);$i++)
        {
            $newgaurd = new GuardDetails();
            $newgaurd->joining_date = $guards[$i]['appointmentdate'];
            $newgaurd->address = $guards[$i]['address_home_id'] != false ? $guards[$i]['address_home_id'][1] : "No Address Defined in Odoo";
            $newgaurd->guard_name = $guards[$i]['name'];
            $newgaurd->employee_erp = $guards[$i]['reference'];
            $newgaurd->center_name = $guards[$i]['center_id'] != false ? $guards[$i]['center_id'][1] : "No Center is defined in Odoo";
            $newgaurd->employee_cnic = $guards[$i]['cnic'];
            $newgaurd->cnic_expiry = $guards[$i]['cnic_expiry'];
            $newgaurd->current_post = $guards[$i]['current_post_id'] != false ? $guards[$i]['center_id'][1] : "No Current Post is Defined in Odoo";
            $newgaurd->odoo_id = $guards[$i]['id'];
            $newgaurd->contract = $guards[$i]['guard_contract_id'] != false ? $guards[$i]['guard_contract_id'][1] : "No Contract is Defined in Odoo";
            $newgaurd->mobile = $guards[$i]['mobile_phone'];
            $newgaurd->gender = $guards[$i]['gender'];
            $newgaurd->save();
        }
        return redirect(route('admin.guards.index'));
    }


}

