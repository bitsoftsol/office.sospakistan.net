<?php

namespace App\Http\Controllers\Member;

use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\Project\StoreProject;
use App\Http\Requests\User\UpdateProfile;
use App\Issue;
use App\ModuleSetting;
use App\Project;
use App\ProjectActivity;
use App\ProjectCategory;
use App\ProjectFile;
use App\ProjectMember;
use App\ProjectTimeLog;
use App\Task;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Maatwebsite\Excel\Facades\Excel;
/**
 * Class MemberProjectsController
 * @package App\Http\Controllers\Member
 */
class MemberProjectsController extends MemberBaseController
{
    use ProjectProgress;

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Assignments';
        $this->pageIcon = 'icon-layers';

        if(!ModuleSetting::checkModule('projects')){
            abort(403);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if(!$this->user->can('add_projects')) {
            $this->totalProjects = Project::join('project_members','projects.id','=','project_members.user_id')
                ->where('project_members.user_id','=',$this->user->id)
                ->get()->count();
            $this->completedProjects = Project::completed()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$this->user->id)->count();
            $this->inProcessProjects = Project::inProcess()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$this->user->id)->count();
            $this->overdueProjects = Project::overdue()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$this->user->id)->count();
        }
        else {
            $this->totalProjects = Project::all()->count();
            $this->completedProjects = Project::completed()->count();
            $this->inProcessProjects = Project::inProcess()->count();
            $this->overdueProjects = Project::overdue()->count();
        }

        $this->clients = User::allAssignees();
        return view('member.projects.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->clients = User::allAssignees();
        $this->categories = ProjectCategory::all();
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        return view('member.projects.edit', $this->data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->project = Project::findOrFail($id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? "0" : (count($this->openTasks) / count($this->project->tasks)) * 100);

        if($this->project->deadline->isPast()){
            $this->daysLeft = 0;
        }
        else{
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d')+($this->project->deadline->diff(Carbon::now())->format('%m')*30)+($this->project->deadline->diff(Carbon::now())->format('%y')*12);
        }

        $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d')+($this->project->deadline->diff($this->project->start_date)->format('%m')*30)+($this->project->deadline->diff($this->project->start_date)->format('%y')*12);

        $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        $this->hoursLogged = ProjectTimeLog::projectTotalHours($this->project->id);
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id','desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($id, 10);
//        $this->completedTasks = Task::projectCompletedTasks($this->project->id);
        return view('member.projects.show',$this->data);
    }

    public function data(Request $request) {
        if(!$this->user->can('add_projects')){
            $projects = Project::join('project_members','projects.id','project_members.project_id')
                ->where('project_members.user_id','=',$this->user->id)->
                select('projects.id', 'projects.project_name', 'projects.start_date', 'projects.deadline', 'projects.client_id', 'projects.completion_percent');
        }
        else {
            $projects = Project::select('projects.id', 'projects.project_name', 'projects.start_date', 'projects.deadline', 'projects.client_id', 'projects.completion_percent');
        }

        if(!is_null($request->status)) {
            if($request->status == 'incomplete') {
                $projects->where('completion_percent', '<','100');
            } elseif ($request->status == 'complete') {
                $projects->where('completion_percent','=', '100');
            }
        }

        if(!is_null($request->client_id)) {
            $projects->where('client_id', $request->client_id);
        }

        $projects->get();

        return Datatables::of($projects)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('member.projects.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('member.projects.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Project Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->addColumn('members', function ($row) {
                $members = '';

                if (count($row->members) > 0) {
                    foreach ($row->members as $member) {
                        $getEmployeeImage = $this->getEmployeeImage($member->user_id);
                        $members .= ($member->user->image) ? '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . $getEmployeeImage . '"
                        
                        alt="user" class="img-circle" width="30"> ' : '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . $getEmployeeImage. '"
                        alt="user" class="img-circle" width="30"> ';
                    }
                }
                else{
                    $members.= 'No member added to this assignment';
                }

                if($member->user->can('add_projects')){
                    $members.= '<br><br><a class="font-12" href="'.route('member.project-members.show', $row->id).'"><i class="fa fa-plus"></i> Add Assignment Members</a>';
                }
                return $members;
            })
            ->editColumn('project_name', function ($row) {
                return '<a href="' . route('member.projects.show', $row->id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->editColumn('start_date', function ($row) {
                return $row->start_date->format('d M, Y');
            })
            ->editColumn('deadline', function ($row) {
                return $row->deadline->format('d M, Y');
            })
            ->editColumn('client_id', function ($row) {
                if(is_null($row->client_id)){
                    return "";
                }
                return ucwords($row->client->name);
            })
            ->editColumn('completion_percent', function ($row) {
                if ($row->completion_percent < 50) {
                    $statusColor = 'danger';
                }
                elseif ($row->completion_percent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                }
                else {
                    $statusColor = 'success';
                }

                return '<h5>'.__('app.completed').'<span class="pull-right">' . $row->completion_percent . '%</span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_percent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $row->completion_percent . '%" role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->rawColumns(['project_name', 'action', 'completion_percent', 'members'])
            ->removeColumn('project_summary')
            ->removeColumn('notes')
            ->removeColumn('category_id')
            ->removeColumn('feedback')
            ->removeColumn('start_date')
            ->make(true);
    }

    public function export() {
        $rows = Project::leftJoin('users', 'users.id', '=', 'projects.client_id')
            ->leftJoin('project_category', 'project_category.id', '=', 'projects.category_id')
            ->select(
                'projects.id',
                'projects.project_name',
                'users.name',
                'project_category.category_name',
                'projects.start_date',
                'projects.deadline',
                'projects.completion_percent',
                'projects.created_at'
            )
            ->get();

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Assignment Name', 'Assignee Name', 'Category', 'Start Date', 'Deadline', 'Completion Percent', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Projects', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Projects');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Projects file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));

                });

            });



        })->download('xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProject $request, $id) {
        $project = Project::findOrFail($id);
        $project->project_name = $request->project_name;
        if ($request->project_summary != '') {
            $project->project_summary = $request->project_summary;
        }
        $project->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $project->deadline = Carbon::parse($request->deadline)->format('Y-m-d');
        if ($request->notes != '') {
            $project->notes = $request->notes;
        }
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        $project->client_id = $request->client_id;
        $project->feedback = $request->feedback;

        if($request->calculate_task_progress){
            $project->calculate_task_progress = $request->calculate_task_progress;
            $project->completion_percent = $this->calculateProjectProgress($id);
        }
        else{
            $project->calculate_task_progress = "false";
            $project->completion_percent = $request->completion_percent;
        }

        if($request->client_view_task){
            $project->client_view_task = 'enable';
        }
        else{
            $project->client_view_task = "disable";
        }

        if($request->manual_timelog){
            $project->manual_timelog = 'enable';
        }
        else{
            $project->manual_timelog = "disable";
        }

        $project->save();

        $this->logProjectActivity($project->id, ucwords($project->project_name) . __('modules.projects.projectUpdated'));
        return Reply::redirect(route('member.projects.edit', $id), __('messages.projectUpdated'));
    }

    public function create() {
        if(!$this->user->can('add_projects')){
            abort(403);
        }

        $this->clients = User::allAssignees();
        $this->categories = ProjectCategory::all();

        $project = new Project();
        $this->fields = $project->getCustomFieldGroupsWithFields()->fields;
        return view('member.projects.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProject $request) {
        $project = new Project();
        $project->project_name = $request->project_name;
        if ($request->project_summary != '') {
            $project->project_summary = $request->project_summary;
        }
        $project->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $project->deadline = Carbon::parse($request->deadline)->format('Y-m-d');
        if ($request->notes != '') {
            $project->notes = $request->notes;
        }
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        $project->client_id = $request->client_id;

        if($request->client_view_task){
            $project->client_view_task = 'enable';
        }
        else{
            $project->client_view_task = "disable";
        }

        if($request->manual_timelog){
            $project->manual_timelog = 'enable';
        }
        else{
            $project->manual_timelog = "disable";
        }

        $project->save();

        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $project->updateCustomFieldData($request->get('custom_fields_data'));
        }

        $this->logSearchEntry($project->id, 'Project: '.$project->project_name, 'admin.projects.show');

        $this->logProjectActivity($project->id, ucwords($project->project_name) . ' '. __("messages.addedAsNewProject"));
        return Reply::redirect(route('member.projects.index'), __('modules.projects.projectUpdated'));
    }

    public function destroy($id) {
        Project::destroy($id);
        return Reply::success(__('messages.projectDeleted'));
    }

}
