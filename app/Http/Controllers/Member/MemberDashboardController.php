<?php

namespace App\Http\Controllers\Member;

use App\Attendance;
use App\EmployeeLoyality;
use App\Issue;
use App\ModuleSetting;
use App\PositionLevel;
use App\Project;
use App\ProjectActivity;
use App\ProjectTimeLog;
use App\Task;
use App\Ticket;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Psy\Input\CodeArgument;
use App\GuardDetails;

class MemberDashboardController extends MemberBaseController
{
    public function __construct() {
        parent::__construct();

        $this->pageTitle = __('app.menu.dashboard');
        $this->pageIcon = 'icon-speedometer';
    }

    public function index() {


        $position = PositionLevel::where('po_level_user_id','=',$this->user->id)->first();
        $is_parent = $position == null ? null : PositionLevel::where('po_level_parent_id','=',$position->id)->first();
        $this->can_approve_leave = $is_parent == null ? false : true;
        $date = Carbon::now();
        $this->is_accepted = EmployeeLoyality::where('user_id','=',Auth::user()->id)->where('date','=',$date->toDateString())->
        where('is_accepted','=',true)->first() == null ? 0 : 1;
        $this->totalProjects = Project::select('projects.id')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $this->user->id)->count();
        $this->counts = DB::table('users')
            ->select(
                DB::raw('(select IFNULL(sum(project_time_logs.total_minutes),0) from `project_time_logs` where user_id = '.$this->user->id.') as totalHoursLogged '),
                DB::raw('(select count(tasks.id) from `tasks` where status="completed" and user_id = '.$this->user->id.') as totalCompletedTasks'),
                DB::raw('(select count(tasks.id) from `tasks` where status="incomplete" and user_id = '.$this->user->id.') as totalPendingTasks')
            )
            ->first();

        $timeLog = intdiv($this->counts->totalHoursLogged, 60).' hrs ';

        if(($this->counts->totalHoursLogged % 60) > 0){
            $timeLog.= ($this->counts->totalHoursLogged % 60).' mins';
        }

        $this->counts->totalHoursLogged = $timeLog;

        $this->projectActivities = ProjectActivity::join('projects', 'projects.id', '=', 'project_activity.project_id')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $this->user->id)
            ->select('projects.project_name', 'project_activity.created_at', 'project_activity.activity', 'project_activity.project_id')
            ->limit(15)->orderBy('project_activity.id', 'desc')->get();

        $this->userActivities = UserActivity::limit(15)->orderBy('id', 'desc')->where('user_id', $this->user->id)->get();

        foreach ($this->userActivities  as  $key => $userActivities){
            $userId = $userActivities->user->id;
            $getEmployeeImage = $this->getEmployeeImage($userId);
            $this->userActivities[$key]->user->setAttribute('employeeImage',$getEmployeeImage);
        }

        $this->pendingTasks = Task::where('status', 'incomplete')
            ->where(DB::raw('DATE(due_date)'), '<=', Carbon::today()->format('Y-m-d'))
            ->where('user_id', $this->user->id)
            ->get();
        $this->todayAttendance = Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon::today()->format('Y-m-d'))
            ->where('user_id', $this->user->id)->first();
        $userId = $this->user->id;

        /**
         * Ticket stats
         */
        $this->viewDashboardTickets     = (auth()->user()->can('view_dashboard_tickets'))?"true":"false";
        $this->userTicketsCount         = Ticket::where("user_id", $userId)->count();
        $this->openTicketsCount         = Ticket::where('status', 'open')->where("user_id", $userId)->count();
        $this->pendingTicketsCount      = Ticket::where('status', 'pending')->where("user_id", $userId)->count();
        $this->resolvedTicketsCount     = Ticket::where('status', 'resolved')->where("user_id", $userId)->count();
        $this->closedTicketsCount       = Ticket::where('status', 'closed')->where("user_id", $userId)->count();

        /**
         * Tasks stats
         */
        $this->viewDashboardTasks           = (auth()->user()->can('view_dashboard_tasks'))?"true":"false";
        $this->userTaskCount                = Task::where("user_id", $userId)->count();
        $this->completedTaskCount           = Task::where("user_id", $userId)->where("status", 'completed')->count();
        $this->incompleteTaskCount          = Task::where("user_id", $userId)->where("status", 'incomplete')->count();
        $this->pendingTaskCount             = Task::where('status', 'incomplete')->where(DB::raw('DATE(due_date)'), '<=', Carbon::today()->format('Y-m-d'))->where('user_id', $userId)->count();

        /**
         * Guard stats
         */
        $this->viewDashboardGuards          = (auth()->user()->can('view_dashboard_guards'))?"true":"false";
        $this->guardsCount                  = GuardDetails::count();
        $dateS                              = Carbon::now();
        $dateE                              = Carbon::now()->subDays(5);
        $this->newGuardsCount               = GuardDetails::where("created_at", "<=", $dateS->format('Y-m-d')." 00:00:00")
                                                ->where("created_at",">=",$dateE->format('Y-m-d')." 23:59:59")
                                                ->get()->count();
        $this->terminatedGuardsCount        = 0;

        /**
         * Assignment stats
         */
        $this->viewDashboardAssignments     = (auth()->user()->can('view_dashboard_assignments'))?"true":"false";
        $this->userTotalProjectsCount       = Project::join('project_members','projects.id','=','project_members.user_id')->where('project_members.user_id','=',$userId)->get()->count();
        $this->completedProjectsCount       = Project::completed()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$userId)->count();
        $this->inProcessProjectsCount       = Project::inProcess()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$userId)->count();
        $this->overdueProjectsCount         = Project::overdue()->join('project_members','projects.id','project_members.project_id')->where('project_members.user_id',$userId)->count();

        /**
         * Salaries stats
         */
        $this->viewDashboardSalaries        = (auth()->user()->can('view_dashboard_salaries'))?"true":"false";
        $this->salaryDeductions             = '200';
        $this->salaryPayable                = '500';
        $this->salaryPending                = '240';
        $this->salaryOnHold                 = '0';

        /**
         * Store stats
         */
        $this->viewDashboardStore           = (auth()->user()->can('view_dashboard_store'))?"true":"false";
        $this->storeTotalItems              = '1000';
        $this->storeNewIns                  = '130';
        $this->storeNewOuts                 = '70';
        $this->storeTotalInStock            = '300';
        $this->storeTotalDeployed           = '500';



        return view('member.dashboard.index', $this->data);
    }

    public function store(Request $request)
    {
        $loyality = new EmployeeLoyality();
        $today = Carbon::now();
        if(isset($request->is_accepted))
        {
            $loyality->user_id = Auth::user()->id;
            $loyality->date = $today->toDateString();
            $loyality->is_accepted = true;
            $loyality->save();
        }
        else {
            $loyality->user_id = Auth::user()->id;
            $loyality->date = $today->toDateString();
            $loyality->save();
            Auth::logout();
            return redirect('/login');
        }
        return redirect(route('member.dashboard'));
    }
}
