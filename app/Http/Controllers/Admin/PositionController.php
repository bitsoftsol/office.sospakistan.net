<?php
namespace App\Http\Controllers\Admin;
use App\PositionLevel;

use App\PositionDivisionDepartmentXref;

use App\PositionDepartmentXref;

use App\Department;

use Carbon\Carbon;

use App\Helper\Reply;

use App\Http\Requests\Position\PositionRequest;

use Yajra\Datatables\Datatables;

use App\User;
use Illuminate\Http\Request;
class PositionController extends AdminBaseController

{

    public function __construct()

    {
        parent::__construct();

        $this->pageTitle = "Positions";//__('app.menu.positions');

        $this->pageIcon = 'ti-menu';

        $this->addText = 'Add';//__('app.menu.positions');;

        $this->showText     = 'Show';//__('app.menu.positions');;
        $this->deleteBulk   = 'Delete Bulk';
        $this->selectAll   = 'Select All';
        $this->mUserType = 'admin';
        $this->mUserTypeLayout      = 'layouts.app';

        $getFirstPosition = PositionLevel::where('is_first', '=', '1')->first();

        if ($getFirstPosition) {
            $this->firstPositionId = $getFirstPosition->id;
        } else {
            $this->firstPositionId = 0;
        }

    }

    public function index()
    {

        $this->positionsData = PositionLevel::all();

        $this->positions = count($this->positionsData);

        return view($this->mUserType.'.positions.index', $this->data);

    }

    public function createOrEditView($positionID = false){
        if($this->user->roles->first()->name == 'employee'){
            abort(403);
        }
        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();
        $division = PositionLevel::division();

        $this->status = 1;

        $this->parentId = 0;

        $this->userId = 0;

        $this->id = 0;

        $this->divisionParentId = 0;

        $this->isUnderDepartment = 0;

        $this->hasMaxChild = 0;

        $this->isDivision = false;
        $this->isFirstPosition = 0;

        $this->divisionAssignedDepartments = array();

        $divisionDepartmentIds = array();

        $divisionDepartment = array();

        $departmentIds = array();
        $this->positionDepartmentId = '';
        $this->positionsData = PositionLevel::where("is_division", "=", "0")->get();

        if ($positionID) {

            $this->positionData = PositionLevel::findOrFail($positionID);

            $this->id = $positionID;

            $this->name = $this->positionData->po_level_name;

            $this->order = $this->positionData->po_level_order;

            $this->parentId = $this->positionData->po_level_parent_id;

            $this->userId = $this->positionData->po_level_user_id;

            $this->hasMaxChild = $this->positionData->has_max_child;

            $this->isFirstPosition = $this->positionData->is_first;

            $this->status = $this->positionData->po_level_status;

            $this->divisionParentId = $this->positionData->po_level_parent_id;//$this->positionData->division_parent_id;

            $this->isUnderDepartment = $this->positionData->is_under_department;
            $this->divisionAssignedDepartments = Department::all();
            $checkDivision = isset($this->positionData->is_division) ? $this->positionData->is_division : 0;

            if ($checkDivision == 1) {

                $this->isDivision = true;

                $divisionDepartment = PositionDivisionDepartmentXref::getByDivision($positionID);

                foreach ($divisionDepartment as $divisionDepartmentIn) {

                    $divisionDepartmentIds[] = $divisionDepartmentIn->department_id;

                }

            }
            if ($this->isUnderDepartment == 1) {

                $departmentRefIds = PositionDivisionDepartmentXref::getByDivision($this->parentId);
                if (!empty($departmentRefIds)) {

                    foreach ($departmentRefIds as $departmentRefId) {

                        $departmentIds[] = $departmentRefId->department_id;

                    }
                    $this->divisionAssignedDepartments = Department::whereIn('id', $departmentIds)->get();

                }
                $positionDepartmentData = PositionDepartmentXref::where("position_id", "=", $positionID)->first();

                $this->positionDepartmentId = isset($positionDepartmentData->department_id) ? $positionDepartmentData->department_id : 0;

            }
            $division = PositionLevel::divisionExceptSelf($positionID);

            $this->positionsData = PositionLevel::where("is_division", "=", "0")->where('id', '!=', $positionID)->get();

        }
        $this->division = $division;

        $this->users = $users;

        $this->divisionDepartmentIds = $divisionDepartmentIds;
        $department = Department::all();

        $this->department                       = $department;
        return view($this->mUserType.'.positions.create', $this->data);

    }

    public function save(PositionRequest $request)
    {

        $count = 0;

        do {

            $name = $request->input('name');

            $id = $request->input('id');

            $order = $request->input('order');

            $parentId = $request->input('parent');

            $userId = $request->input('userId');

            $divisionParentId = $request->input('divisionParentId');

            $status = $request->input('status');

            $isDivision = $request->input('isDivision');

            $addMultiple = $request->input('addMultiple');

            $addMultipleNumber = $request->input('addMultipleNumber');

            $divisionDepartments = $request->input('divisionDepartment');

            $positionDepartment = $request->input('positionDepartment');

            $hasMaxChild = $request->input('hasMaxChild');

            $isFirstPosition = $request->input('isFirstPosition');

            if ($id) {

                $position = PositionLevel::findOrFail($id);

            } else {

                $position = new PositionLevel();

            }
            $position->po_level_name = $name;

            $position->po_level_order = $order;

            $position->po_level_parent_id = ($parentId != '') ? $parentId : 0;

            //$position->po_level_user_id     = ($userId != '')?$userId:0;

            $position->is_division = ($isDivision != '') ? $isDivision : 0;

            if (($isFirstPosition != '')) {
                $position->makeIsFirstToZero();
                $position->is_first = $isFirstPosition;
            } else {
                $position->is_first = '0';
            }


            if ($isDivision == 1) {

                $position->division_parent_id = ($parentId != '') ? $parentId : 0;
                $position->has_max_child = 0;

            } else {

                $position->division_parent_id = 0;
                $position->has_max_child = ($hasMaxChild != '') ? $hasMaxChild : 0;

            }
            if ($positionDepartment > 0) {

                $position->is_under_department = '1';

            } else {

                $position->is_under_department = '0';

            }
            $position->po_level_status = $status;

            $position->save();

            $lastInsertId = $position->id;
            /*

             * relation with department

             * */

            if ($isDivision == 1) {

                if (!empty($divisionDepartments)) {

                    $PositionDivisionDepartmentXref = new PositionDivisionDepartmentXref();

                    $PositionDivisionDepartmentXref->deleteByDivision($lastInsertId);

                    foreach ($divisionDepartments as $divisionDepartment) {

                        $PositionDivisionDepartmentXref = new PositionDivisionDepartmentXref();

                        $PositionDivisionDepartmentXref->division_id = $lastInsertId;

                        $PositionDivisionDepartmentXref->department_id = $divisionDepartment;

                        $PositionDivisionDepartmentXref->save();

                    }

                }

            } else {

                if ($positionDepartment > 0) {

                    $PositionDepartmentXref = new PositionDepartmentXref();

                    $PositionDepartmentXref->deleteByPosition($lastInsertId);
                    $PositionDepartmentXref = new PositionDepartmentXref();

                    $PositionDepartmentXref->position_id = $lastInsertId;

                    $PositionDepartmentXref->division_id = $parentId;

                    $PositionDepartmentXref->department_id = $positionDepartment;

                    $PositionDepartmentXref->save();

                }

            }
            $count++;

        }

        while($addMultiple != '' && $addMultipleNumber > $count);
        return Reply::redirect(route($this->mUserType.'.positions'));
    }

    public function show()
    {

        $PositionLevel = new PositionLevel();
        $hierarchy = $PositionLevel->getChildren($this->firstPositionId);

        if ($this->firstPositionId != 0) {
            $getFirstPositionData = $PositionLevel->getFirstPositionData($this->firstPositionId);
            $getFirstPositionData[0]->children = $hierarchy;
            $hierarchy = $getFirstPositionData;
        }


        $this->hierarchy = $hierarchy;

        $this->HierarchyHTML = $this->makeHierarchyHTML($this->hierarchy);


        return view($this->mUserType.'.positions.show', $this->data);

    }

    public function showAll() {

        $PositionLevel          = new PositionLevel();
        $hierarchy              = $PositionLevel->getChildren($this->firstPositionId);

        if($this->firstPositionId != 0){
            $getFirstPositionData                   = $PositionLevel->getFirstPositionData($this->firstPositionId);
            $getFirstPositionData[0]->children      = $hierarchy;
            $hierarchy                              = $getFirstPositionData;
        }


        $this->hierarchy        = $hierarchy;

        $this->HierarchyHTML    = $this->makeHierarchyHTML($this->hierarchy);


        return view($this->mUserType.'.positions.show', $this->data);

    }

    public function makeHierarchyHTML($hierarchy, $first = 1)
    {
        $HTML = '';

        foreach ($hierarchy as $index) {
            $style = '';
            $userName = '';
            if($this->user->roles->first()->name == 'employee'){
                $route = "javascript:void(0)";
            }else{
                $route = route($this->mUserType.'.positions.createOrEditView', [$index->id]);
            }
            if(isset($index->toList))
            {
                $style = 'style="background-color: #00c292;"';
            }
            if (isset($index->listURL)) {

                $route = $index->listURL;
            }
            $firstClass = '';
            if ($first == 1) {
                $firstClass = 'firstMember';
            }
            if (isset($index->userName)) {
                $userName = $index->userName . '<br/>';
            }
            $HTML .= '<li><a class="' . $firstClass . '" href="' . $route . '" ' . $style . '>' . $userName . $index->po_level_name . '</a>' . $this->makeHierarchyHTML($index->children, 0) . '</li>';
        }

        if ($HTML != '') {

            return '<ul>' . $HTML . '</ul>';

        }

    }

    public function data()
    {

        $positionObj = new PositionLevel();
        $position = PositionLevel::customList();

        foreach ($position as $positionIn) {

            $relationData = $positionObj->getPositionRelation($positionIn->id);

            if (isset($relationData['parentData']['error'])) {
                $report_to = $relationData['parentData']['error'];
            } else {
                $report_to = $relationData['parentData']['po_level_name'] . "(" . $relationData['parentData']['userName'] . ")";
            }
            if (isset($relationData['departmentData']['error'])) {
                $departmentName = $relationData['departmentData']['error'];
            } else {
                $departmentName = $relationData['departmentData']['name'];
            }

            $positionIn->setAttribute('parent_po_name', $report_to);

            $positionIn->setAttribute('department_name', $departmentName);

            $empName = 'Not assign';
            if ($positionIn->po_level_user_id > 0) {

                $empId = $positionIn->po_level_user_id;

                $userInfo = User::where('id', '=', $empId)->first();

                if ($userInfo) {

                    $empName = $userInfo->name;

                }

            }

            $positionIn->setAttribute('name', $empName);

        }

        return Datatables::of($position)
            ->addColumn('action', function ($row) {

                $HTML = '<a href="' . route($this->mUserType.'.positions.createOrEditView', [$row->id]) . '" class="btn btn-info btn-circle"

                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"

                      data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';

                if(isset($row->position_count)){
                    $HTML .= '<a href="'.route($this->mUserType.'.positions.maxChildList',[$row->id]).'" class="btn btn-warning " style="margin-left: 4px;" data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Go To List">Go To List</a>';

                }else if($row->has_max_child == 1){
                    $HTML .= '<a href="'.route($this->mUserType.'.positions.maxChildList',[$row->id]).'" class="btn btn-warning " style="margin-left: 4px;" data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Go To List">Go To List</a>';
                }
                return $HTML;

            })
            ->addColumn('select', function ($row) {
                $HTML = '<input class="bulkIds"   name="bulkIds[]" value="' . $row->id . '" type="checkbox" >';
                return $HTML;
            })
            ->editColumn(

                'po_level_name',

                function ($row) {
                    $positionLevelName = $row->po_level_name;

                    if (isset($row->position_count)) {
                        $positionLevelName = $row->po_level_name;
                    }
                    return $positionLevelName;
                })
            ->editColumn(

                'created_at',

                function ($row) {

                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');

                })
            ->editColumn(

                'status',

                function ($row) {

                    if ($row->po_level_status == '1') {

                        return '<label class="label label-success">' . __('app.active') . '</label>';

                    } else {

                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';

                    }

                }

            )
            ->rawColumns(['name', 'action', 'status', 'select'])
            ->make(true);

    }

    public function destroy($id)

    {

        $PositionLevel = new PositionLevel();
        $PositionLevel->removeAndAssignParent($id);

        PositionLevel::destroy($id);

        return Reply::success(__('messages.positionDeleted'));

    }

    public function ajaxGetDepartmentByDivision($divisionId)

    {

        $return = array();

        $return['error'] = 1;

        $return['msg'] = 'Data Not Found';
        $return['data'] = array('departments' => '<option value="">Data Not Found</option>');

        $departmentIds = array();

        $departmentRefIds = PositionDivisionDepartmentXref::getByDivision($divisionId);
        if (!empty($departmentRefIds)) {

            foreach ($departmentRefIds as $departmentRefId) {

                $departmentIds[] = $departmentRefId->department_id;

            }

            $departments = Department::whereIn('id', $departmentIds)->get();

            if (!empty($departments)) {

                $departmentHTML = $this->departmentHTML($departments);

                $return['error'] = 0;

                $return['msg'] = 'Data Found';

                $return['data'] = array('departments' => $departmentHTML);

            }

        }
        echo json_encode($return);
        exit;

    }

    public function ajaxGetDepartments()

    {

        $return = array();

        $return['error'] = 1;

        $return['msg'] = 'Data Not Found';

        $return['data'] = array();

        $departments = Department::all();

        if (!empty($departments)) {

            $departmentHTML = $this->departmentHTML($departments);

            $return['error'] = 0;

            $return['msg'] = 'Data Found';

            $return['data'] = array('departments' => $departmentHTML);

        }
        echo json_encode($return);
        exit;

    }

    public function departmentHTML($data)

    {

        $html = '';

        $count = 0;

        foreach ($data as $dataIn) {

            if ($count == 0 && isset($dataIn->id) && $dataIn->id != '') {

                $html .= "<option value=''>Select Department</option>";

            }

            $html .= "<option value='" . $dataIn->id . "'>" . $dataIn->name . "</option>";

            $count++;
        }
        return $html;
    }

    public function ajaxGetPositionByDepartment($departmentId)
    {
        $return = array();
        $return['error'] = 1;
        $return['msg'] = 'Data Not Found';
        $return['data'] = array('positions' => '<option value="">Data Not Found</option>');

        $positionIds = array();
        $positionRefIds = PositionDepartmentXref::where('department_id', '=', $departmentId)->get();
        if (!empty($positionRefIds)) {
            foreach ($positionRefIds as $positionRefId) {
                $positionIds[] = $positionRefId->position_id;
            }
            $positions = PositionLevel::whereIn('id', $positionIds)->get();
            if ($positions->count() > 0) {
                $positionHTML = $this->positionHTML($positions);
                $return['error'] = 0;
                $return['msg'] = 'Data Found';
                $return['data'] = array('positions' => $positionHTML);
            }
        }

        echo json_encode($return);
        exit;
    }

    public function ajaxGetPositionByPrentId($parentId)
    {
        $return             = array();
        $return['error']    = 1;
        $return['msg']      = 'Data Not Found';
        $return['data']     = array('positions'=>'<option value="">Data Not Found</option>');

        $positions                  = PositionLevel::maxChildList($parentId);
        if($positions->count() > 0){

            $positionHTML           = $this->positionHTML($positions);
            $return['error']        = 0;
            $return['msg']          = 'Data Found';
            $return['data']         = array('positions'=>$positionHTML);
        }


        echo json_encode($return); exit;
    }

    public function positionHTML($data)
    {
        $html = '';
        $count = 0;
        foreach ($data as $dataIn) {
            if ($count == 0 && isset($dataIn->id) && $dataIn->id != '') {
                $html .= "<option value=''>Select Position</option>";
            }
            $html .= "<option value='" . $dataIn->id . "'>" . $dataIn->po_level_name . "</option>";

            $count++;

        }

        return $html;

    }

    public function divisionList()

    {

        $this->pageTitle = "Divisions";

        $this->pageIcon = 'ti-menu';

        $this->divisionData = PositionLevel::where('is_division', '=', '1')->get();

        $this->divisions = count($this->divisionData);

        return view($this->mUserType.'.positions.divisionList', $this->data);

    }

    public function divisionData()
    {
        $division = PositionLevel::where('is_division', '=', '1')->get();

        return Datatables::of($division)
            ->addColumn('action', function ($row) {

                if (!$this->user->hasRole('employee')) {
                $HTML = '<a href="' . route($this->mUserType.'.positions.createOrEditView', [$row->id]) . '" class="btn btn-info btn-circle
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"

                      data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                } else {
                    $HTML = 'No Action';
                }
                return $HTML;
            })
            ->editColumn(

                'created_at',

                function ($row) {

                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');

                })
            ->editColumn(

                'status',

                function ($row) {

                    if ($row->po_level_status == '1') {

                        return '<label class="label label-success">' . __('app.active') . '</label>';

                    } else {

                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';

                    }

                }

            )
            ->rawColumns(['po_level_name', 'action', 'status'])
            ->make(true);

    }

    public function maxChildList($parentId)
    {

        $this->positionsData = PositionLevel::maxChildList($parentId);

        $this->positions = count($this->positionsData);
        $this->parentId = $parentId;

        return view($this->mUserType.'.positions.maxChildList', $this->data);

    }

    public function maxChildData($parentId)
    {
        $positionObj = new PositionLevel();
        $position = PositionLevel::maxChildList($parentId);

        foreach ($position as $positionIn) {

            $parent_po_ID = $positionIn->po_level_parent_id;

            $parentInfo = PositionLevel::where('po_level_parent_id', '=', $parent_po_ID)->first();

            $depart_id = PositionDepartmentXref::where('position_id', '=', $positionIn->id)->first();

            $depart_name = null;

            if ($depart_id) {

                $depart_name = Department::where('id', '=', $depart_id->department_id)->first();

            }


            $relationData = $positionObj->getPositionRelation($positionIn->id);

            if (isset($relationData['parentData']['error'])) {
                $report_to = $relationData['parentData']['error'];
            } else {
                $report_to = $relationData['parentData']['po_level_name'] . "(" . $relationData['parentData']['userName'] . ")";
            }

            /*$parent_name = User::where('id', '=', $parentInfo->po_level_user_id)->first();

            if ($parent_name) {

                $report_to = $parent_name->name . "if";

            } else {

                $report_to = 'No Parent is Defined' . "else";

            }*/

            $departmentName = $depart_name == null ? 'No Department' : $depart_name->name;

            $positionIn->setAttribute('parent_name', $report_to);

            $positionIn->setAttribute('department_name', $departmentName);

            $empName = 'Not assign';
            if ($positionIn->po_level_user_id > 0) {

                $empId = $positionIn->po_level_user_id;

                $userInfo = User::where('id', '=', $empId)->first();

                if ($userInfo) {

                    $empName = $userInfo->name;

                }

            }

            $positionIn->setAttribute('name', $empName);

        }

        return Datatables::of($position)
            ->addColumn('action', function ($row) {

                $HTML = '<a href="' . route($this->mUserType.'.positions.createOrEditView', [$row->id]) . '" class="btn btn-info btn-circle"

                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"

                      data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                return $HTML;

            })
            ->addColumn('select', function ($row) {
                $HTML = '<input class="bulkIds"   name="bulkIds[]" value="' . $row->id . '" type="checkbox" >';
                return $HTML;
            })
            ->editColumn(

                'po_level_name',

                function ($row) {
                    $positionLevelName = $row->po_level_name;
                    return $positionLevelName;
                })
            ->editColumn(

                'created_at',

                function ($row) {

                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');

                })
            ->editColumn(

                'status',

                function ($row) {

                    if ($row->po_level_status == '1') {

                        return '<label class="label label-success">' . __('app.active') . '</label>';

                    } else {

                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';

                    }

                }

            )
            ->rawColumns(['name', 'action', 'status', 'select'])
            ->make(true);

    }

    public function bulkDelete(Request $request)
    {
        $positionsIds = $request->positionsIds;
        $positionsIds = json_decode($positionsIds);
        foreach ($positionsIds as $positionsId) {
            $PositionLevel = new PositionLevel();
            $PositionLevel->removeAndAssignParent($positionsId);
            PositionLevel::destroy($positionsId);
        }
        return Reply::success(__('messages.positionDeleted'));
    }
}