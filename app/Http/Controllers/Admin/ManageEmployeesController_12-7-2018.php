<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateEmployee;
use App\Leave;
use App\LeaveType;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectTimeLog;
use App\Repositories\EmployeeRepository;
use App\Repositories\Repository;
use App\Role;
use App\RoleUser;
use App\Segment;
use App\SosCenter;
use App\SubSegment;
use App\Task;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Edujugon\Laradoo\Odoo;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Ripcord\Providers\Laravel\Ripcord;

use App\PositionLevel;

use App\PositionDepartmentXref;

use App\PositionDivisionDepartmentXref;

class ManageEmployeesController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.employees');
        $this->pageIcon = 'icon-user';
        $this->odooModel = 'hr.employee';
        $this->model = new EmployeeRepository($this->odooModel);
        if(!ModuleSetting::checkModule('employees')){
            abort(403);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->totalEmployees = count(User::allEmployees());
        return view('admin.employees.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $employee               = new EmployeeDetails();
        $this->fields           = $employee->getCustomFieldGroupsWithFields()->fields;
        $divisions              = PositionLevel::division();
        $this->divisions        = $divisions;
        $departments            = Department::all();
        $this->departments      = $departments;
        $this->positions        = PositionLevel::all();
        return view('admin.employees.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request) {
        $validate = Validator::make(['job_title' => $request->job_title, 'hourly_rate' => $request->hourly_rate, 'joining_date' => $request->joining_date], [

            'job_title' => 'required',

            'hourly_rate' => 'numeric',

            'joining_date' => 'required'        ]);        if ($validate->fails()) {
            return Reply::formErrors($validate);
        }

        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');
        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/'.$user->image);
            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save();

        }
        $user->save();

        if ($user->id) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
            $employee->job_title = $request->job_title;
            $employee->address = $request->address;
            $employee->hourly_rate = $request->hourly_rate;
            $employee->slack_username = $request->slack_username;
            $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');
            $employee->save();
        }
        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        $user->attachRole(2);
        // Notify User
        $user->notify(new NewUser($request->input('password')));
        $this->logSearchEntry($user->id, $user->name, 'admin.employees.show');
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeAdded'));
    }

    public function storeOdoo(StoreUser $request)
    {
        echo "<pre>"; print_r($request); exit;
        $segment = Segment::where('id', '=',$request['segment_id'])->first();
        $subSegment = SubSegment::where('id','=',$request['sub_segment_id'])->first();
        $department = Department::where('id','=',$request['department_id'])->first();
        $parent_id = EmployeeDetails::where('user_id','=',$request['manager_id'])->first();
        $coach_id = EmployeeDetails::where('user_id','=',$request['coach_id'])->first();
        $center_id = SosCenter::where('id','=',$request['center_id'])->first();
        $fields = array(
            'name' => $request['name'],
            'fathername' => $request['fatherName'],
            'cnic' => $request['cnic'],
            'is_guard' => isset($request['is_guard']) ? true : false,
            'work_email' => $request['email'],
            'mobile_phone' => $request['mobile'],
            'gender' => $request['gender'],
            'appointmentdate' => $request['joining_date'],
            'center_id' => $center_id['odoo_id'],
            'job_id' => $request['job_title'],
            'address_home_id' => $request['address'],
            'work_location' => $request['working_address'],
            'work_phone' => $request['work_mobile'],
            'segment_id' => $segment['odoo_id'],
            'sub_segment_id' => $subSegment['odoo_id'],
            'department_id' => $department['odoo_id'],
            'parent_id' => $parent_id['employee_odoo_id'],
            'coach_id' => $coach_id['employee_odoo_id'],
            'manager' => isset($request['is_manager']) ? true : false,
            'country_id' => $request['nationality'],
            'passport_id' => $request['passport_no'],
            'bank_id' => $request['bank_name'],
            'bankacctitle' => $request['account_title'],
            'bankacc' => $request['account_no'],
            'marital' => $request['marital_status'],
            'children' => $request['children'],
            'cnic_expiry' => $request['cnic_expiry'],
            'cniccopy' => $request->hasFile('image_cnic') ? true : false,
            'educertificate' => $request->hasCookie('image_education') ? true : false,
            'nadraattest' => $request->hasFile('image_nadra') ? true : false,
            'emp_photographs' => $request->hasFile('image_profile') ? true : false
        );
        $validate = Validator::make(['job_title' => $request->job_title, 'joining_date' => $request->joining_date], [

            //'job_title' => 'required',
            'joining_date' => 'required'
        ]);

        if ($validate->fails()) {

            $request->session()->flash('alert-danger', 'There is a validation Error');
            return redirect(route('admin.employees.create'));
            
        }

        $id = $this->model->addEmployee($fields);
        $reference = $this->model->searchById($id);

        $user = new User();
        $user->name = $request->input('name');
        //$user->email = $request->input('email');
        //$user->password = Hash::make($request->input('password'));
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');

        if ($request->hasFile('image_profile')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image_profile->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_profile_cam')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image_profile->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $user->save();

        if ($user->id) {
            $is_cnic = false;
            $is_nadra = false;
            $is_education = false;
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
            $employee->job_title = $request->job_title;
            $employee->address = $request->address;
            $employee->hourly_rate = $request->hourly_rate;
            $employee->slack_username = $request->slack_username;
            $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');
            if ($request->hasFile('image_cnic')) {
                File::delete('user-uploads/avatar/'.$employee->image_cnic);

                $employee->image_cnic = $request->image_cnic->hashName();
                $request->image_cnic->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_cnic);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_cnic = true;
            }
            $employee->employee_cnic = $request->cnic;
            $employee->employee_odoo_id = $id;
            $employee->employee_erp = $reference;
            $employee->father_name = $request->fatherName;
            $employee->category_ids = $request->category_ids;
            $employee->work_address = $request->working_address;
            $employee->work_location = $request->work_location;
            $employee->work_phone = $request->work_phone;
            $employee->parent_coach_user_id = $request->manager_id;
            $employee->is_manager = isset($request['is_manager']) ? true : false;
            $employee->country_name = $request->nationality;
            $employee->passport_number = $request->passport_no;
            $employee->bank_name = $request->bank_name;
            $employee->bank_account_title = $request->account_title;
            $employee->place_of_birth = $request->nationality;
            $employee->cnic_expiry = $request->cnic_expiry;
            $employee->cnic_copy = $is_cnic;
            $employee->education = $request->education;
            $employee->position_id = $request->position_id;
            $employee->first_name = $request->first_name;
            $employee->middle_name = $request->middle_name;
            $employee->last_name = $request->last_name;
            $employee->mother_name = $request->mother_name;
            $request->home_city = $request->home_city;
            $request->last_qualification = $request->last_qualification;
            $request->height = $request->height;
            $employee->weight = $request->weight;
            $employee->bank_branch = $request->bank_branch;
            $employee->bank_city = $request->bank_city;
            $employee->blood_group = $request->blood_group;
            // $employee->signature = $request->signature;
            $employee->land_line_number = $request->land_line_number;
            $employee->personal_email = $request->personal_email;
            $employee->kin_1_name = $request->kin_1_name;
            $employee->kin_1_number = $request->kin_1_number;
            $employee->kin_1_relation = $request->kin_1_relation;
            $employee->kin_2_name = $request->kin_2_name;
            $employee->kin_2_number = $request->kin_2_number;
            $employee->kin_2_relation = $request->kin_2_relation;
            $employee->basic_salary = $request->basic_salary;
            $employee->duty_hours = $request->duty_hours;
            $employee->duty_days = $request->duty_days;
            $employee->annual_leaves = $request->annual_leaves;
            $employee->air_ticket = $request->air_ticket;
            $employee->attendance_logging = $request->attendance_logging;
            $employee->rec_type = $request->rec_type;
            $employee->travel_benifits = $request->travel_benifits;
            $employee->travel_expenses = $request->travel_expensis;
            $employee->over_time_pay = $request->over_time_pay;
            $employee->email_pro = isset($request->email_pro) ? true : false;
            $employee->sim = isset($request->sim) ? true : false;
            $employee->mobile_phone = isset($request->mobile_phone) ? true : false;
            $employee->office = isset($request->office) ? true : false;
            $employee->office_stationary = isset($request->office_stationary) ? true : false;
            $employee->pc = isset($request->pc) ? true : false;
            $employee->laptop = isset($request->laptop) ? true : false;
            $employee->vehicle = isset($request->vehicle) ? true : false;
            $employee->ref_1_name = $request->ref_1_name;
            $employee->ref_1_number = $request->ref_1_number;
            $employee->rel_to_ref1 = $request->rel_to_ref1;
            $employee->ref_1_address = $request->ref_1_address;
            $employee->ref_1_cnic = $request->ref_1_cnic;
            $employee->ref_2_name = $request->ref_2_name;
            $employee->ref_2_number = $request->ref_2_number;
            $employee->rel_to_ref2 = $request->rel_to_ref2;
            $employee->ref_2_address = $request->ref_2_address;
            $employee->ref_2_cnic = $request->ref_2_cnic;
            $employee->military_person = isset($request->military_person) ? true : false;
            $employee->force_no = $request->force_no;
            $employee->m_joining_date = $request->m_joining_daate;
            $employee->m_leaving_date = $request->m_leaving_date;
            $employee->force_type = $request->force_type;
            $employee->force_rank = $request->force_rank;
            $employee->last_unit = $request->last_unit;
            $employee->last_center = $request->last_center;
            $employee->last_company_name = $request->last_company_name;
            $employee->last_position = $request->last_position;
            $employee->last_joining_date = $request->last_joining_date;
            $employee->last_leaving_date = $request->last_leaving_date;
            $employee->last_working_city = $request->last_working_city;
            $employee->last_working_country = $request->last_working_country;

            if ($request->hasFile('image_police_varification')) {
                File::delete('user-uploads/avatar/'.$employee->image_police_varification);

                $employee->image_police_varification = $request->image_police_varification->hashName();
                $request->image_police_varification->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_police_varification);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }
            $employee->varification_date = $request->varification_date;

            if ($request->hasFile('image_referee_undertaking')) {
                File::delete('user-uploads/avatar/'.$employee->image_referee_undertaking);

                $employee->image_referee_undertaking = $request->image_referee_undertaking->hashName();
                $request->image_referee_undertaking->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_referee_undertaking);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }

            if ($request->hasFile('image_emp_aggre')) {
                File::delete('user-uploads/avatar/'.$employee->image_emp_aggre);

                $employee->image_emp_aggre = $request->image_emp_aggre->hashName();
                $request->image_emp_aggre->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_emp_aggre);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }

            if ($request->hasFile('image_pen_book')) {
                File::delete('user-uploads/avatar/'.$employee->image_pen_book);

                $employee->image_pen_book = $request->image_pen_book->hashName();
                $request->image_pen_book->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_pen_book);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }

            $employee->image_fin_1 = $request->image_fin_1;
            $employee->image_fin_2 = $request->image_fin_2;
            $employee->image_fin_3 = $request->image_fin_3;
            $employee->image_fin_4 = $request->image_fin_4;
            $employee->left_thumb = $request->left_thumb;
            $employee->right_thumb = $request->right_thumb;
            $employee->right_fin_1 = $request->right_fin_1;
            $employee->right_fin_2 = $request->right_fin_2;
            $employee->right_fin_3 = $request->right_fin_3;
            $employee->right_fin_4 = $request->right_fin_4;

            if ($request->hasFile('image_nadra')) {
                File::delete('user-uploads/avatar/'.$employee->image_nadra);

                $employee->image_nadra = $request->image_nadra->hashName();
                $request->image_nadra->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_nadra);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_nadra = true;
            }
            $employee->nadra_attested = $is_nadra;
            if ($request->hasFile('image_education')) {
                File::delete('user-uploads/avatar/'.$employee->image_education);

                $employee->image_education = $request->image_cnic->hashName();
                $request->image_education->store('user-uploads/avatar');

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make('user-uploads/avatar/'.$employee->image_education);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_education = true;
            }
            $employee->emp_edu_certificate = $is_education;
            $employee->is_guard = isset($request['is_guard']) ? true : false;
            $employee->save();
        }

        // To add custom fields data What are custom Fields?
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }        $user->attachRole(2);

        // Notify User
        $user->notify(new NewUser($request->input('password')));

        $this->logSearchEntry($user->id, $user->name, 'admin.employees.show');

        $request->session()->flash('alert-success', 'Data has been Saved Successfully Odoo ERP No '.$reference);

        return redirect(route('admin.employees.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $this->employee = User::withoutGlobalScope('active')->findOrFail($id);
        $this->taskCompleted = Task::where('user_id', $id)->where('status', 'completed')->count();
        $hoursLogged = ProjectTimeLog::where('user_id', $id)->sum('total_minutes');

        $timeLog = intdiv($hoursLogged, 60).' hrs ';

        if(($hoursLogged % 60) > 0){
            $timeLog.= ($hoursLogged % 60).' mins';
        }

        $this->hoursLogged = $timeLog;

        $this->activities = UserActivity::where('user_id', $id)->orderBy('id', 'desc')->get();
        $this->projects = Project::select('projects.id', 'projects.project_name', 'projects.deadline', 'projects.completion_percent')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $id)
            ->get();
        $this->leaves = Leave::byUser($id);

        $this->leaveTypes = LeaveType::byUser($id);
        $this->allowedLeaves = LeaveType::sum('no_of_leaves');

        return view('admin.employees.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();

        if(!is_null($this->employeeDetail)){
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }

        $divisions              = PositionLevel::division();
        $this->divisions        = $divisions;
        $departments            = Department::all();
        $this->departments      = $departments;
        $this->positions        = PositionLevel::all();

        /*if($this->employeeDetail->division_id > 0){

            $divisionId         = $this->employeeDetail->division_id;
            $departmentIds      = array();
            $departmentRefIds   = PositionDivisionDepartmentXref::getByDivision($divisionId);

            if(!empty($departmentRefIds)){
                foreach ($departmentRefIds as $departmentRefId){
                    $departmentIds[]        = $departmentRefId->department_id;
                }
                $this->departments          = Department::whereIn('id', $departmentIds)->get();
            }
        }

        if($this->employeeDetail->department_id > 0){
            $departmentId     = $this->employeeDetail->department_id;
            $positionIds      = array();
            $positionRefIds   = PositionDepartmentXref::where('department_id','=',$departmentId)->get();

            if(!empty($positionRefIds)){
                foreach ($positionRefIds as $positionRefId){
                    $positionIds[]          = $positionRefId->position_id;
                }
                $this->positions            = PositionLevel::whereIn('id', $positionIds)->get();
            }
        }*/

        return view('admin.employees.edit', $this->data);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function employeeAccessControl($id) {
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();

        if(!is_null($this->employeeDetail)){
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }

        return view('admin.employees.editAccess', $this->data);

    }    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateAccessControl(UpdateEmployee $request, $id) {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->password != '') {
            $user->password = Hash::make($request->input('password'));
        }
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');
        $user->status = $request->input('status');

        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $user->save();

        $validate = Validator::make(['job_title' => $request->job_title], [
            'job_title' => 'required'
        ]);

        if ($validate->fails()) {
            return Reply::formErrors($validate);
        }

        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployee $request, $id) {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->password != '') {
            $user->password = Hash::make($request->input('password'));
        }
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');
        $user->status = $request->input('status');

        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $user->save();

        $validate = Validator::make(['job_title' => $request->job_title], [
            'job_title' => 'required'
        ]);

        if ($validate->fails()) {
            return Reply::formErrors($validate);
        }

        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        $employee->job_title = $request->job_title;
        $employee->address = $request->address;
        $employee->hourly_rate = $request->hourly_rate;
        $employee->slack_username = $request->slack_username;
        $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');;
        $employee->save();

        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }

        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }

    public function employeeDir($id)
    {
        $this->employeeDirPath = public_path().'/employee';
        if(!File::exists($this->employeeDirPath)) {
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }
        $this->employeeDirPath = public_path().'/employee/'.$id;
        if(!File::exists($this->employeeDirPath)){
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }

        $this->employeeDirPath = 'employee/'.$id;
        return $this->employeeDirPath;

    }
    public function updateOdoo(UpdateEmployee $request, $id) {

        $user                   = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name             = $request->input('first_name').' '.$request->input('last_name');
        $user->mobile           = $request->input('mobile');
        $user->gender           = $request->input('gender');

        $this->employeeDir($id);

        if ($request->hasFile('image_profile')) {
            File::delete($this->employeeDirPath.'/'.$user->image);

            $user->image = $request->image_profile->hashName();
            $request->image_profile->store($this->employeeDirPath);
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }



        if ($request->hasFile('image_profile_cam')) {
            File::delete($this->employeeDirPath.'/'.$user->image);

            $user->image = $request->image_profile_cam->hashName();
            $request->image_profile_cam->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }
        $user->save();


        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        $is_cnic = false;
        $is_nadra = false;
        $is_education = false;
        $employee = new EmployeeDetails();
        $employee->user_id = $user->id;
        $employee->job_title = $request->job_title;
        $employee->address = $request->address;
        $employee->hourly_rate = $request->hourly_rate;
        $employee->slack_username = $request->slack_username;
        $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');
        if ($request->hasFile('image_cnic')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_cnic);

            $employee->image_cnic = $request->image_cnic->hashName();
            $request->image_cnic->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_cnic);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_cnic = true;
        }
        $employee->employee_cnic = $request->cnic;
        $employee->employee_odoo_id = $id;
        $employee->father_name = $request->fatherName;
        $employee->category_ids = $request->category_ids;
        $employee->work_address = $request->working_address;
        $employee->work_location = $request->work_location;
        $employee->work_phone = $request->work_phone;
        $employee->parent_coach_user_id = $request->manager_id;
        $employee->is_manager = isset($request['is_manager']) ? true : false;
        $employee->country_name = $request->nationality;
        $employee->passport_number = $request->passport_no;
        $employee->bank_name = $request->bank_name;
        $employee->bank_account_title = $request->account_title;
        $employee->place_of_birth = $request->nationality;
        $employee->cnic_expiry = $request->cnic_expiry;
        $employee->cnic_copy = $is_cnic;
        $employee->education = $request->education;
        $employee->position_id = $request->position_id;
        $employee->first_name = $request->first_name;
        $employee->middle_name = $request->middle_name;
        $employee->last_name = $request->last_name;
        $employee->mother_name = $request->mother_name;
        $request->home_city = $request->home_city;
        $request->last_qualification = $request->last_qualification;
        $request->height = $request->height;
        $employee->weight = $request->weight;
        $employee->bank_branch = $request->bank_branch;
        $employee->bank_city = $request->bank_city;
        $employee->blood_group = $request->blood_group;
        // $employee->signature = $request->signature;
        $employee->land_line_number = $request->land_line_number;
        $employee->personal_email = $request->personal_email;
        $employee->kin_1_name = $request->kin_1_name;
        $employee->kin_1_number = $request->kin_1_number;
        $employee->kin_1_relation = $request->kin_1_relation;
        $employee->kin_2_name = $request->kin_2_name;
        $employee->kin_2_number = $request->kin_2_number;
        $employee->kin_2_relation = $request->kin_2_relation;
        $employee->basic_salary = $request->basic_salary;
        $employee->duty_hours = $request->duty_hours;
        $employee->duty_days = $request->duty_days;
        $employee->annual_leaves = $request->annual_leaves;
        $employee->air_ticket = $request->air_ticket;
        $employee->attendance_logging = $request->attendance_logging;
        $employee->rec_type = $request->rec_type;
        $employee->travel_benifits = $request->travel_benifits;
        $employee->travel_expenses = $request->travel_expensis;
        $employee->over_time_pay = $request->over_time_pay;
        $employee->email_pro = isset($request->email_pro) ? true : false;
        $employee->sim = isset($request->sim) ? true : false;
        $employee->mobile_phone = isset($request->mobile_phone) ? true : false;
        $employee->office = isset($request->office) ? true : false;
        $employee->office_stationary = isset($request->office_stationary) ? true : false;
        $employee->pc = isset($request->pc) ? true : false;
        $employee->laptop = isset($request->laptop) ? true : false;
        $employee->vehicle = isset($request->vehicle) ? true : false;
        $employee->ref_1_name = $request->ref_1_name;
        $employee->ref_1_number = $request->ref_1_number;
        $employee->rel_to_ref1 = $request->rel_to_ref1;
        $employee->ref_1_address = $request->ref_1_address;
        $employee->ref_1_cnic = $request->ref_1_cnic;
        $employee->ref_2_name = $request->ref_2_name;
        $employee->ref_2_number = $request->ref_2_number;
        $employee->rel_to_ref2 = $request->rel_to_ref2;
        $employee->ref_2_address = $request->ref_2_address;
        $employee->ref_2_cnic = $request->ref_2_cnic;
        $employee->military_person = isset($request->military_person) ? true : false;
        $employee->force_no = $request->force_no;
        $employee->m_joining_date = $request->m_joining_daate;
        $employee->m_leaving_date = $request->m_leaving_date;
        $employee->force_type = $request->force_type;
        $employee->force_rank = $request->force_rank;
        $employee->last_unit = $request->last_unit;
        $employee->last_center = $request->last_center;
        $employee->last_company_name = $request->last_company_name;
        $employee->last_position = $request->last_position;
        $employee->last_joining_date = $request->last_joining_date;
        $employee->last_leaving_date = $request->last_leaving_date;
        $employee->last_working_city = $request->last_working_city;
        $employee->last_working_country = $request->last_working_country;

        if ($request->hasFile('image_police_varification')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_police_varification);

            $employee->image_police_varification = $request->image_police_varification->hashName();
            $request->image_police_varification->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_police_varification);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }
        $employee->varification_date = $request->varification_date;

        if ($request->hasFile('image_referee_undertaking')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_referee_undertaking);

            $employee->image_referee_undertaking = $request->image_referee_undertaking->hashName();
            $request->image_referee_undertaking->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_referee_undertaking);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_emp_aggre')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_emp_aggre);

            $employee->image_emp_aggre = $request->image_emp_aggre->hashName();
            $request->image_emp_aggre->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_emp_aggre);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_pen_book')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_pen_book);

            $employee->image_pen_book = $request->image_pen_book->hashName();
            $request->image_pen_book->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_pen_book);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $employee->image_fin_1 = $request->image_fin_1;
        $employee->image_fin_2 = $request->image_fin_2;
        $employee->image_fin_3 = $request->image_fin_3;
        $employee->image_fin_4 = $request->image_fin_4;
        $employee->left_thumb = $request->left_thumb;
        $employee->right_thumb = $request->right_thumb;
        $employee->right_fin_1 = $request->right_fin_1;
        $employee->right_fin_2 = $request->right_fin_2;
        $employee->right_fin_3 = $request->right_fin_3;
        $employee->right_fin_4 = $request->right_fin_4;
        $employee->military_person = isset($request->military_person) ? true : false;
        $employee->force_no = $request->force_no;
        $employee->m_joining_date = $request->m_joining_date;
        $employee->m_leaving_date = $request->m_leaving_date;
        $employee->force_type = $request->force_type;
        $employee->force_rank = $request->force_rank;
        $employee->last_unit = $request->last_unit;
        $employee->last_center = $request->last_center;
        if ($request->hasFile('image_nadra')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_nadra);

            $employee->image_nadra = $request->image_nadra->hashName();
            $request->image_nadra->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_nadra);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_nadra = true;
        }
        $employee->nadra_attested = $is_nadra;
        if ($request->hasFile('image_education')) {
            File::delete($this->employeeDirPath.'/'.$employee->image_education);

            $employee->image_education = $request->image_cnic->hashName();
            $request->image_education->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath.'/'.$employee->image_education);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_education = true;
        }
        $employee->emp_edu_certificate = $is_education;
        $employee->is_guard = isset($request['is_guard']) ? true : false;
        $employee->save();
        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }

        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = User::withoutGlobalScope('active')->findOrFail($id);

        if ($user->id == 1) {
            return Reply::error(__('messages.adminCannotDelete'));
        }

        User::destroy($id);
        return Reply::success(__('messages.employeeDeleted'));
    }

    public function data() {
        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();

        $roles = Role::where('name', '<>', 'client')->get();

        return Datatables::of($users)
            ->addColumn('role', function ($row) use ($roles) {
                $roleRow = '';
                if ($row->id != 1) {

                    $flag = 0;
                    foreach($roles as $role){

                        $roleRow .= '<div class="radio radio-info">
                              <input type="radio" name="role_' . $row->id . '" class="assign_role" data-user-id="' . $row->id . '"';

                        foreach($row->role as $urole){

                            if($role->id == $urole->role_id && $flag == 0){
                                $roleRow .= ' checked ';

                                if($role->name == 'admin'){
                                    $flag = 1; //do not check any other role for user if is admin
                                }
                            }

                        }

                        if($role->id <= 3){
                            $roleRow.= 'id="none_role_' . $row->id .$role->id. '" data-role-id="'.$role->id.'" value="'.$role->id.'"> <label for="none_role_' . $row->id .$role->id . '" data-role-id="'.$role->id.'" data-user-id="' . $row->id . '">'.__('app.'.$role->name).'</label></div>';
                        }
                        else{
                            $roleRow.= 'id="none_role_' . $row->id .$role->id. '" data-role-id="'.$role->id.'" value="'.$role->id.'"> <label for="none_role_' . $row->id .$role->id . '" data-role-id="'.$role->id.'" data-user-id="' . $row->id . '">'.ucwords($role->name).'</label></div>';
                        }

                        $roleRow .= '<br>';

                    }
                    return $roleRow;

                }
                else{
                    return __('messages.roleCannotChange');
                }

            })
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 'active'){
                        return '<label class="label label-success">'.__('app.active').'</label>';
                    }
                    else{
                        return '<label class="label label-danger">'.__('app.deactive').'</label>';
                    }
                }
            )
            ->editColumn('name', function ($row) use ($roles) {
                $image =  ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'"
                                                            alt="user" class="img-circle" width="30"> ': '<img src="'.asset('default-profile-2.png').'"
                                                            alt="user" class="img-circle" width="30"> ';
                if ($row->hasRole('admin')) {

                    return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-danger">'.__('app.admin').'</label>';
                }
                else{
                    foreach($roles as $role){
                        foreach($row->role as $urole){

                            if($role->id == $urole->role_id && $role->id != 2){
                                return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-info">'.ucwords($role->name).'</label>';
                            }

                        }
                    }
                    return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-warning">'.__('app.employee').'</label>';
                }
                return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a>';
            })
            ->rawColumns(['name', 'action', 'role', 'status'])
            ->removeColumn('roleId')
            ->removeColumn('roleName')
            ->make(true);
    }
    public function filter($centerID = null, $departmentID = null, $segmentID = null)

    {

        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();

        $roles = Role::where('name', '<>', 'client')->get();

        return Datatables::of($users)
            ->addColumn('role', function ($row) use ($roles) {
                $roleRow = '';
                if ($row->id != 1) {

                    $flag = 0;
                    foreach($roles as $role){

                        $roleRow .= '<div class="radio radio-info">
                              <input type="radio" name="role_' . $row->id . '" class="assign_role" data-user-id="' . $row->id . '"';

                        foreach($row->role as $urole){

                            if($role->id == $urole->role_id && $flag == 0){
                                $roleRow .= ' checked ';

                                if($role->name == 'admin'){
                                    $flag = 1; //do not check any other role for user if is admin
                                }
                            }

                        }

                        if($role->id <= 3){
                            $roleRow.= 'id="none_role_' . $row->id .$role->id. '" data-role-id="'.$role->id.'" value="'.$role->id.'"> <label for="none_role_' . $row->id .$role->id . '" data-role-id="'.$role->id.'" data-user-id="' . $row->id . '">'.__('app.'.$role->name).'</label></div>';
                        }
                        else{
                            $roleRow.= 'id="none_role_' . $row->id .$role->id. '" data-role-id="'.$role->id.'" value="'.$role->id.'"> <label for="none_role_' . $row->id .$role->id . '" data-role-id="'.$role->id.'" data-user-id="' . $row->id . '">'.ucwords($role->name).'</label></div>';
                        }

                        $roleRow .= '<br>';

                    }
                    return $roleRow;

                }
                else{
                    return __('messages.roleCannotChange');
                }

            })
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 'active'){
                        return '<label class="label label-success">'.__('app.active').'</label>';
                    }
                    else{
                        return '<label class="label label-danger">'.__('app.deactive').'</label>';
                    }
                }
            )
            ->editColumn('name', function ($row) use ($roles) {
                $image =  ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'"
                                                            alt="user" class="img-circle" width="30"> ': '<img src="'.asset('default-profile-2.png').'"
                                                            alt="user" class="img-circle" width="30"> ';
                if ($row->hasRole('admin')) {

                    return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-danger">'.__('app.admin').'</label>';
                }
                else{
                    foreach($roles as $role){
                        foreach($row->role as $urole){

                            if($role->id == $urole->role_id && $role->id != 2){
                                return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-info">'.ucwords($role->name).'</label>';
                            }

                        }
                    }
                    return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a><br><br> <label class="label label-warning">'.__('app.employee').'</label>';
                }
                return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image.' '.ucwords($row->name) . '</a>';
            })
            ->rawColumns(['name', 'action', 'role', 'status'])
            ->removeColumn('roleId')
            ->removeColumn('roleName')
            ->make(true);

    }
    public function tasks($userId, $hideCompleted) {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->select('tasks.id', 'projects.project_name', 'tasks.heading', 'tasks.due_date', 'tasks.status', 'tasks.project_id')
            ->where('tasks.user_id', $userId);

        if ($hideCompleted == '1') {
            $tasks->where('tasks.status', '=', 'incomplete');
        }

        $tasks->get();

        return Datatables::of($tasks)
            ->editColumn('due_date', function ($row) {
                if ($row->due_date->isPast()) {
                    return '<span class="text-danger">' . $row->due_date->format('d M, y') . '</span>';
                }
                return '<span class="text-success">' . $row->due_date->format('d M, y') . '</span>';
            })
            ->editColumn('heading', function ($row) {
                return ucfirst($row->heading);
            })
            ->editColumn('status', function ($row) {
                if ($row->status == 'incomplete') {
                    return '<label class="label label-danger">'.__('app.incomplete').'</label>';
                }
                return '<label class="label label-success">'.__('app.complete').'</label>';
            })
            ->editColumn('project_name', function ($row) {
                if(!is_null($row->project_name)){
                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
                }
            })
            ->rawColumns(['status', 'project_name', 'due_date'])
            ->removeColumn('project_id')
            ->make(true);
    }

    public function timeLogs($userId) {
        $timeLogs = ProjectTimeLog::join('projects', 'projects.id', '=', 'project_time_logs.project_id')
            ->select('project_time_logs.id', 'projects.project_name', 'project_time_logs.start_time', 'project_time_logs.end_time', 'project_time_logs.total_hours', 'project_time_logs.memo', 'project_time_logs.project_id', 'project_time_logs.total_minutes')
            ->where('project_time_logs.user_id', $userId);
        $timeLogs->get();

        return Datatables::of($timeLogs)
            ->editColumn('start_time', function ($row) {
                return $row->start_time->timezone($this->global->timezone)->format('d M, Y h:i A');
            })
            ->editColumn('end_time', function ($row) {
                if (!is_null($row->end_time)) {
                    return $row->end_time->timezone($this->global->timezone)->format('d M, Y h:i A');
                }
                else {
                    return "<label class='label label-success'>Active</label>";
                }
            })
            ->editColumn('project_name', function ($row) {
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->editColumn('total_hours', function($row){
                $timeLog = intdiv($row->total_minutes, 60).' hrs ';

                if(($row->total_minutes % 60) > 0){
                    $timeLog.= ($row->total_minutes % 60).' mins';
                }

                return $timeLog;
            })
            ->rawColumns(['end_time', 'project_name'])
            ->removeColumn('project_id')
            ->make(true);
    }

    public function export() {
        $rows = User::leftJoin('employee_details', 'users.id', '=', 'employee_details.user_id')
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'users.mobile',
                'employee_details.job_title',
                'employee_details.address',
                'employee_details.hourly_rate',
                'users.created_at'
            )
            ->get();

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Job Title', 'Address', 'Hourly Rate', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Employees', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Employees');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Employees file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });        })->download('xlsx');
    }

    public function importOdoo()
    {
        $fields = array('active' => true, 'is_guard' => false);
        $emp = $this->model->allemployees($fields);
        foreach ($emp as $item)
        {
            $is_exist = 0;
            $odoo_emp = EmployeeDetails::where('employee_odoo_id', $item['id'])->pluck('employee_odoo_id')->toArray();
            if($odoo_emp != null){
                $is_exist = 1;
            }
            if($is_exist == 0) {
                $user = new User();
                $user->name = $item['name'];
                $user->email = $item['reference'].'erp@sospakistan.com';
                $user->password = Hash::make($item['mobile_phone'] != false ? $item['mobile_phone'] : 'password');
                $user->mobile = $item['mobile_phone'];
                $user->gender = $item['gender'];
                $user->save();
                if ($user->id) {
                    $employee = new EmployeeDetails();
                    $employee->user_id = $user->id;
                    $employee->job_title = $item['job_id'] != false ? $item['job_id'][1] : "No Job Title is Defined in Odoo ERP";
                    $employee->address = $item['address_home_id'] != false ? $item['address_home_id'][1] : "No Home Address is Defined in Odoo ERP";
                    $employee->joining_date = Carbon::parse($item['appointmentdate'])->format('Y-m-d');
                    $employee->employee_cnic = $item['cnic'];
                    $employee->employee_odoo_id = $item['id'];
                    $employee->employee_erp = $item['reference'];
                    $employee->save();
                    $user->attachRole(2);

                    // Notify User
                    $user->notify(new NewUser('password'));
                    $this->logSearchEntry($user->id, $user->name, 'admin.employees.show');
                }
            }
        }
        return redirect(route('admin.employees.index'));
    }

    public function importCenters()
    {
        SosCenter::truncate();
        $centers = $this->model->getCenters();
        foreach ($centers as $item) {
            $center = new SosCenter();
            $center->odoo_id = $item['id'];
            $center->name = $item['name'];
            $center->save();
        }
        return redirect(route('admin.employees.index'));
    }

    public function importSegments()
    {
        Segment::truncate();
        $segments = $this->model->getSegments();
        foreach ($segments as $item1) {
            $segment = new Segment();
            $segment->odoo_id = $item1['id'];
            $segment->name = $item1['name'];
            $segment->save();
        }
        return redirect(route('admin.employees.index'));
    }

    public function importSubSegments()
    {
        SubSegment::truncate();
        $subSegments = $this->model->getSubSegments();
        foreach ($subSegments as $subSeg) {
            $subSegment = new SubSegment();
            $subSegment->odoo_id = $subSeg['id'];
            $subSegment->name = $subSeg['name'];
            $subSegment->save();
        }
        return redirect(route('admin.employees.index'));
    }

    public function importDepartments()
    {
        Department::truncate();
        $departments = $this->model->getDepartments();
        foreach ($departments as $dept)
        {
            $department = new Department();
            $department->odoo_id = $dept['id'];
            $department->name = $dept['name'];
            $department->save();
        }
        return redirect(route('admin.employees.index'));
    }

    public function assignRole(Request $request) {
        $userId = $request->userId;
        $roleId = $request->role;
        $employeeRole = Role::where('name', 'employee')->first();
        $user = User::findOrFail($userId);

        RoleUser::where('user_id', $user->id)->delete();
        $user->roles()->attach($employeeRole->id);
        if($employeeRole->id != $roleId){
            $user->roles()->attach($roleId);
        }

        return Reply::success(__('messages.roleAssigned'));
    }

    public function assignProjectAdmin(Request $request) {
        $userId = $request->userId;
        $projectId = $request->projectId;
        $project = Project::findOrFail($projectId);
        $project->project_admin = $userId;
        $project->save();

        return Reply::success(__('messages.roleAssigned'));
    }

}
