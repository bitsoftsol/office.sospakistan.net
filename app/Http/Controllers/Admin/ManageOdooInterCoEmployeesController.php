<?php

namespace App\Http\Controllers\Admin;

use App\Department;


use App\EmployeeDepartment;
use App\EmployeeDetails;


use App\Helper\Reply;


use App\Http\Requests\User\StoreUser;


use App\Http\Requests\User\UpdateEmployee;


use App\Leave;


use App\LeaveType;


use App\ModuleSetting;


use App\Notifications\NewUser;


use App\OdooInterCoEmployee;
use App\Project;


use App\ProjectTimeLog;


use App\Repositories\AttachmentsRepository;
use App\Repositories\EmployeeRepository;


use App\Repositories\Repository;


use App\Role;


use App\RoleUser;


use App\Segment;


use App\SosCenter;


use App\SubSegment;


use App\Task;


use App\User;


use App\UserActivity;


use Carbon\Carbon;


use Edujugon\Laradoo\Odoo;


use function foo\func;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


use Illuminate\Http\Request;


use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Hash;


use Illuminate\Support\Facades\Validator;


use Intervention\Image\Facades\Image;


use Maatwebsite\Excel\Facades\Excel;


use Yajra\Datatables\Facades\Datatables;


use Ripcord\Providers\Laravel\Ripcord;

use App\PositionLevel;

use App\PositionDepartmentXref;

use App\PositionDivisionDepartmentXref;

class ManageOdooInterCoEmployeesController extends AdminBaseController
{    public function __construct()
    {        parent::__construct();
            $this->pageTitle = __('app.menu.employees');
            $this->pageIcon = 'icon-user';
            $this->odooModel = 'hr.employee';
            $this->model = new EmployeeRepository($this->odooModel);
            if (!ModuleSetting::checkModule('employees'))
            {
                abort(403);
            }
    }    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $this->totalEmployees = count(User::all_odoo_inter_co_employees());
        $divisions = PositionLevel::division();
        $this->divisions = $divisions;
//        $this->model_attachment = new AttachmentsRepository('ir.attachment');
//
//        return $this->model_attachment->getAttachmentByEmployee(22751);
        return view('admin.odoo-inter-co-employees.index', $this->data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        $employee = new EmployeeDetails();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $divisions = PositionLevel::division();
        $this->divisions = $divisions;
        $departments = Department::all();
        $this->departments = $departments;
        return view('admin.employees.create', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    public function store(StoreUser $request)
    {


        $validate = Validator::make(['job_title' => $request->job_title, 'hourly_rate' => $request->hourly_rate, 'joining_date' => $request->joining_date], [


            'job_title' => 'required',


            'hourly_rate' => 'numeric',


            'joining_date' => 'required'


        ]);


        if ($validate->fails()) {


            return Reply::formErrors($validate);


        }


        $user = new User();


        $user->name = $request->input('name');


        $user->email = $request->input('email');


        $user->password = Hash::make($request->input('password'));


        $user->mobile = $request->input('mobile');


        $user->gender = $request->input('gender');

        if ($request->hasFile('image')) {


            File::delete('user-uploads/avatar/' . $user->image);


            $user->image = $request->image->hashName();


            $request->image->store('user-uploads/avatar');


            // resize the image to a width of 300 and constrain aspect ratio (auto height)


            $img = Image::make('user-uploads/avatar/' . $user->image);


            $img->resize(300, null, function ($constraint) {


                $constraint->aspectRatio();


            });


            $img->save();


        }


        $user->save();


        if ($user->id) {


            $employee = new EmployeeDetails();


            $employee->user_id = $user->id;


            $employee->job_title = $request->job_title;


            $employee->address = $request->address;


            $employee->hourly_rate = $request->hourly_rate;


            $employee->slack_username = $request->slack_username;


            $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');


            $employee->save();


        }


        // To add custom fields data


        if ($request->get('custom_fields_data')) {


            $employee->updateCustomFieldData($request->get('custom_fields_data'));


        }


        $user->attachRole(2);


        // Notify User


        $user->notify(new NewUser($request->input('password')));


        $this->logSearchEntry($user->id, $user->name, 'admin.employees.show');


        return Reply::redirect(route('admin.employees.index'), __('messages.employeeAdded'));


    }


    public function storeOdoo(StoreUser $request)
    {
        config(['filesystems.default' => 'local']);

        $validate = Validator::make(['job_title' => $request->job_title, 'joining_date' => $request->joining_date], [

            'joining_date' => 'required'

        ]);


        if ($validate->fails()) {

            $request->session()->flash('alert-danger', 'There is a validation Error');
            return redirect(route('admin.employees.create'));

        }

        $user = new User();

        $user->name = $request->input('first_name');

        $user->email = $request->input('first_name').rand(100,1000).'@sospakistan.com';

        $user->mobile = $request->input('mobile');

        $user->gender = $request->input('gender');

        $user->save();

        $this->employeeDir($user->id);

        $updateUser                   = User::findOrFail($user->id);

        if($request->input('image_profile_cam') == ''){

            if ($request->hasFile('image_profile')) {
                File::delete($this->employeeDirPath . '/' . $updateUser->image);
                $updateUser->image = $request->image_profile->hashName();
                $request->image_profile->store($this->employeeDirPath);
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $updateUser->image);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }
        }
        else{
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $request->input('image_profile_cam');
            $base_to_php            = explode(',', $camProfileImage);
            $data                   = base64_decode($base_to_php[1]);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $updateUser->image      = $imageName.".png";
        }

        $updateUser->save();
        if ($user->id) {
            $is_cnic = false;


            $is_nadra = false;
            $is_education = false;
            $employee = new EmployeeDetails();

            $employee->user_id = $user->id;
            $employee->job_title = $request->input('job_title');

            $employee->address = $request->input('address');

            $employee->hourly_rate = $request->input('hourly_rate');

            $employee->slack_username = $request->input('slack_username');

            $employee->joining_date = Carbon::parse($request->input('joining_date'))->format('Y-m-d');

            if($request->input('sigImageInput') != ''){
                $imageName              = md5(rand(100,10000));
                $camProfileImage        = $request->input('sigImageInput');
                $base_to_php            = explode(',', $camProfileImage);
                $data                   = base64_decode($base_to_php[1]);
                file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
                $employee->signature_image      = $imageName.".png";
            }
            if ($request->hasFile('image_cnic')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_cnic);

                $employee->image_cnic = $request->image_cnic->hashName();
                $request->image_cnic->store($this->employeeDirPath);
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_cnic);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_cnic = true;
            }

            if ($request->hasFile('image_police_varification')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_police_varification);

                $employee->image_police_varification = $request->image_police_varification->hashName();
                $request->image_police_varification->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_police_varification);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }

            if ($request->hasFile('image_referee_undertaking')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_referee_undertaking);

                $employee->image_referee_undertaking = $request->image_referee_undertaking->hashName();
                $request->image_referee_undertaking->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_referee_undertaking);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }            if ($request->hasFile('image_emp_aggre')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_emp_aggre);

                $employee->image_emp_aggre = $request->image_emp_aggre->hashName();
                $request->image_emp_aggre->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_emp_aggre);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }


            if ($request->hasFile('image_pen_book')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_pen_book);

                $employee->image_pen_book = $request->image_pen_book->hashName();
                $request->image_pen_book->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_pen_book);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }

            if ($request->hasFile('image_nadra')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_nadra);

                $employee->image_nadra = $request->image_nadra->hashName();
                $request->image_nadra->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_nadra);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_nadra = true;
            }

            if ($request->hasFile('image_education')) {
                File::delete($this->employeeDirPath . '/' . $employee->image_education);

                $employee->image_education = $request->image_cnic->hashName();
                $request->image_education->store($this->employeeDirPath);

                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $employee->image_education);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
                $is_education = true;
            }

            $employee->erp_type = $request->input('ERPType');
            $employee->employee_type = $request->input('employeeType');
            $employee->odoo_center_id = $request->input('center_id');
            $employee->division_id = $request->input('divisions');
            $employee->department_id = $request->input('departments');
            $employee->position_id = $request->input('positions');
            $employee->employee_cnic = $request->input('cnic');
            $employee->marital_status = $request->input('marital_status');
            $employee->number_of_children = $request->input('no_of_child');
            $employee->bank_account_number = $request->input('account_no');
            $employee->air_ticket = $request->input('air_ticket');
            $employee->father_name = $request->input('fatherName');
            $employee->category_ids = $request->input('category_ids');
            $employee->work_address = $request->input('working_address');
            $employee->work_location = $request->input('work_location');
            $employee->work_phone = $request->input('work_phone');
            $employee->parent_coach_user_id = $request->input('manager_id');
            $employee->is_manager = ($request->input('is_manager') != '') ? true : false;
            $employee->country_name = $request->input('nationality');
            $employee->passport_number = $request->input('passport_no');
            $employee->bank_name = $request->input('bank_name');
            $employee->bank_account_title = $request->input('account_title');
            $employee->place_of_birth = $request->input('nationality');
            $employee->cnic_expiry = $request->input('cnic_expiry');
            $employee->birthday = $request->input('birthday');
            $employee->cnic_copy = $is_cnic;
            $employee->education = $request->input('education');
            $employee->first_name = $request->input('first_name');
            $employee->middle_name = $request->input('middle_name');
            $employee->last_name = $request->input('last_name');
            $employee->mother_name = $request->input('mother_name');
            $employee->home_city = $request->input('home_city');
            $employee->last_qualification = $request->input('last_qualification');
            $employee->height = $request->input('height');
            $employee->weight = $request->input('weight');
            $employee->bank_branch = $request->input('bank_branch');
            $employee->bank_city = $request->input('bank_city');
            $employee->blood_group = $request->input('blood_group');
            $employee->land_line_number = $request->input('land_line_number');
            $employee->personal_email = $request->input('personal_email');
            $employee->kin_1_name = $request->input('kin_1_name');
            $employee->kin_1_number = $request->input('kin_1_number');
            $employee->kin_1_relation = $request->input('kin_1_relation');
            $employee->kin_2_name = $request->input('kin_2_name');
            $employee->kin_2_number = $request->input('kin_2_number');
            $employee->kin_2_relation = $request->input('kin_2_relation');
            $employee->basic_salary = $request->input('basic_salary');
            $employee->duty_hours = $request->input('duty_hours');
            $employee->duty_days = $request->input('duty_days');
            $employee->annual_leaves = $request->input('annual_leaves');
            $employee->air_ticket = $request->input('air_ticket');
            $employee->attendance_logging = $request->input('attendance_logging');
            $employee->rec_type = $request->input('rec_type');
            $employee->travel_benefits = $request->input('travel_benefits');
            $employee->travel_expenses = $request->input('travel_expenses');
            $employee->over_time_pay = $request->input('over_time_pay');
            $employee->email_pro = ($request->input('email_pro') != '') ? true : false;
            $employee->sim = ($request->input('sim') != '') ? true : false;
            $employee->mobile_phone = ($request->input('mobile_phone') != '') ? true : false;
            $employee->office = ($request->input('office') != '') ? true : false;
            $employee->office_stationary = ($request->input('office_stationary') != '') ? true : false;
            $employee->pc = ($request->input('pc') != '') ? true : false;
            $employee->laptop = ($request->input('laptop') != '') ? true : false;
            $employee->vehicle = ($request->input('vehicle') != '') ? true : false;
            $employee->uniform = ($request->input('uniform') != '') ? true : false;
            $employee->belt = ($request->input('belt') != '') ? true : false;
            $employee->shoes = ($request->input('shoes') != '') ? true : false;
            $employee->caps = ($request->input('caps') != '') ? true : false;
            $employee->badges = ($request->input('badges') != '') ? true : false;
            $employee->ref_1_name = $request->input('ref_1_name');
            $employee->ref_1_number = $request->input('ref_1_number');
            $employee->rel_to_ref1 = $request->input('rel_to_ref1');
            $employee->ref_1_address = $request->input('ref_1_address');
            $employee->ref_1_cnic = $request->input('ref_1_cnic');
            $employee->ref_2_name = $request->input('ref_2_name');
            $employee->ref_2_number = $request->input('ref_2_number');
            $employee->rel_to_ref2 = $request->input('rel_to_ref2');
            $employee->ref_2_address = $request->input('ref_2_address');
            $employee->ref_2_cnic = $request->input('ref_2_cnic');
            $employee->military_person = ($request->input('military_person') != '') ? true : false;
            $employee->force_no = $request->input('force_no');
            $employee->m_joining_date = $request->input('m_joining_date');
            $employee->m_leaving_date = $request->input('m_leaving_date');
            $employee->force_type = $request->input('force_type');
            $employee->force_rank = $request->input('force_rank');
            $employee->last_unit = $request->input('last_unit');
            $employee->last_center = $request->input('last_center');
            $employee->last_company_name = $request->input('last_company_name');
            $employee->last_position = $request->input('last_position');
            $employee->last_joining_date = $request->input('last_joining_date');
            $employee->last_leaving_date = $request->input('last_leaving_date');
            $employee->last_working_city = $request->input('last_working_city');
            $employee->last_working_country = $request->input('last_working_country');
            $employee->varification_date = $request->input('varification_date');
            $employee->left_fin_1 = $request->input('left_fin_1');
            $employee->left_fin_2 = $request->input('left_fin_2');
            $employee->left_fin_3 = $request->input('left_fin_3');
            $employee->left_fin_4 = $request->input('left_fin_4');
            $employee->left_thumb = $request->input('left_thumb');
            $employee->right_thumb = $request->input('right_thumb');
            $employee->right_fin_1 = $request->input('right_fin_1');
            $employee->right_fin_2 = $request->input('right_fin_2');
            $employee->right_fin_3 = $request->input('right_fin_3');
            $employee->right_fin_4 = $request->input('right_fin_4');
            $employee->nadra_attested = $is_nadra;
            $employee->emp_edu_certificate = $is_education;
            $employee->is_guard = ($request->input('is_guard') != '') ? true : false;

            /*fields that are not filling
            hourly_rate
            slack_username
            employee_odoo_id
            employee_erp
            is_guard
            work_location
            work_phone
            parent_coach_user_id
            passport_number
            bank_odoo_id
            resign_date
            education
            education_odoo_id
            education_certificate
            emp_cv
            emp_photographs
            country_id
            image_reference_undertaking
            air_ticket
            travel_expenses
            over_time_pay
            odoo_segment_id
            odoo_sub_segment_id
            odoo_department_id
            signature
            */
            $employee->save();

            /*add user id to position qasim*/
            $positionId             = $request->input('positions');
            if($positionId != ''){
                $positionLevel      = new PositionLevel();
                $positionLevel->positionAssignToUser($positionId, $user->id);
            }
            /*add user id to position end*/


        }
        // To add custom fields data What are custom Fields?

        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        $user->attachRole(2);
        // Notify User
        //$user->notify(new NewUser($request->input('password')));
        $this->logSearchEntry($user->id, $user->name, 'admin.employees.show');
        //$request->session()->flash('alert-success', 'Data has been Saved Successfully Odoo ERP No ');
        //return redirect(route('admin.employees.index'));
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeAdded'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {


        $this->employee = User::withoutGlobalScope('active')->findOrFail($id);


        $this->taskCompleted = Task::where('user_id', $id)->where('status', 'completed')->count();


        $hoursLogged = ProjectTimeLog::where('user_id', $id)->sum('total_minutes');


        $timeLog = intdiv($hoursLogged, 60) . ' hrs ';


        if (($hoursLogged % 60) > 0) {


            $timeLog .= ($hoursLogged % 60) . ' mins';


        }


        $this->hoursLogged = $timeLog;


        $this->activities = UserActivity::where('user_id', $id)->orderBy('id', 'desc')->get();


        $this->projects = Project::select('projects.id', 'projects.project_name', 'projects.deadline', 'projects.completion_percent')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $id)
            ->get();


        $this->leaves = Leave::byUser($id);


        $this->leaveTypes = LeaveType::byUser($id);


        $this->allowedLeaves = LeaveType::sum('no_of_leaves');


        return view('admin.employees.show', $this->data);


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = OdooInterCoEmployee::where('user_id', '=', $this->userDetail->id)->first();
        $odoo_id = $this->employeeDetail->employee_odoo_id;
        $update_data = $this->model->get_details_by_id($odoo_id)->first();
        $this->employeeDetail->first_name = $update_data['name'];
        $this->employeeDetail->father_name = $update_data['fathername'];
        $this->employeeDetail->mother_name = $update_data['mothername'];
        $this->employeeDetail->country_id = $update_data['country_id'] == null ? null : $update_data['country_id'][0];
        $this->employeeDetail->country_name = $update_data['country_id'] == null ? null : $update_data['country_id'][1];
        $this->employeeDetail->last_qualification = $update_data['education_id'] == null ? null : $update_data['education_id'][1];
        $this->employeeDetail->height = $update_data['height'];
        $this->employeeDetail->weight = $update_data['weight'];
        $this->employeeDetail->employee_cnic = $update_data['cnic'];
        $this->employeeDetail->cnic_expiry = $update_data['cnic_expiry'];
        $this->employeeDetail->birthday = $update_data['birthday'];
        $this->employeeDetail->marital_status = $update_data['marital'];
        $this->employeeDetail->bank_odoo_id = $update_data['bank_id'] == null ? mull : $update_data['bank_id'][0];
        $this->employeeDetail->bank_name = $update_data['bank_id'] == null ? null : $update_data['bank_id'][1];
        $this->employeeDetail->bank_branch = $update_data['branch'];
        $this->employeeDetail->bank_account_title = $update_data['bankacctitle'];
        $this->employeeDetail->bank_account_number = $update_data['bankacc'];
        $this->employeeDetail->number_of_children = $update_data['childno'];
        $this->employeeDetail->blood_group = $update_data['bloodgroup_id'] == null ? null : $update_data['bloodgroup_id'][1];
        $this->employeeDetail->mobile_phone = $update_data['mobile_phone'];
        $this->employeeDetail->address = $update_data['street'] .' '.$update_data['street2'];
        $this->employeeDetail->basic_salary = $update_data['basicpay'];
        $this->employeeDetail->joining_date = $update_data['appointmentdate'];
        $this->employeeDetail->force_no = $update_data['armyno'];
        $this->employeeDetail->m_joining_date = $update_data['joindate'];
        $this->employeeDetail->m_leaving_date = $update_data['dischargedate'];
        $this->employeeDetail->force_type = $update_data['forcetype'];
        $this->employeeDetail->force_rank = $update_data['rank'];
        $this->employeeDetail->last_unit = $update_data['lastunit'];
        $this->employeeDetail->last_center = $update_data['lastcenter'];
        if($update_data['image'] != null){
            $this->employeeDir($this->userDetail->id);
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $update_data['image'];
            $data                   = base64_decode($camProfileImage);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $this->userDetail->image      = $imageName.".png";
        }
        $this->employeeDetail->left_fin_1 = $update_data['l_index_finger'];
        $this->employeeDetail->left_fin_2 = $update_data['l_middle_finger'];
        $this->employeeDetail->left_fin_3 = $update_data['l_ring_finger'];
        $this->employeeDetail->left_fin_4 = $update_data['l_baby_finger'];
        $this->employeeDetail->left_thumb = $update_data['l_thumb'];
        $this->employeeDetail->right_thumb = $update_data['r_thumb'];
        $this->employeeDetail->right_fin_1 = $update_data['r_index_finger'];
        $this->employeeDetail->right_fin_2 = $update_data['r_middle_finger'];
        $this->employeeDetail->right_fin_3 = $update_data['r_ring_finger'];
        $this->employeeDetail->right_fin_4 = $update_data['r_baby_finger'];
        $this->employeeDetail->save();
        $this->userDetail->save();
//         if (!is_null($this->employeeDetail)) {
//            $this->employeeDetail = $this->employeeDetail->withCustomFields();
//            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
//        }

        $divisions = PositionLevel::division();
        $this->divisions = $divisions;
        $departments = Department::all();
        $this->departments = $departments;
        $this->positions = PositionLevel::all();
        $this->employeeFolder = asset('employee/'.$id).'/';
        $this->isMilitryRecordShow = (isset($this->employeeDetail->military_person) && $this->employeeDetail->military_person == 1)?'block':'none';
        /*if($this->employeeDetail->division_id > 0){

            $divisionId         = $this->employeeDetail->division_id;
            $departmentIds      = array();
            $departmentRefIds   = PositionDivisionDepartmentXref::getByDivision($divisionId);

            if(!empty($departmentRefIds)){
                foreach ($departmentRefIds as $departmentRefId){
                    $departmentIds[]        = $departmentRefId->department_id;
                }
                $this->departments          = Department::whereIn('id', $departmentIds)->get();
            }
        }

        if($this->employeeDetail->department_id > 0){
            $departmentId     = $this->employeeDetail->department_id;
            $positionIds      = array();
            $positionRefIds   = PositionDepartmentXref::where('department_id','=',$departmentId)->get();

            if(!empty($positionRefIds)){
                foreach ($positionRefIds as $positionRefId){
                    $positionIds[]          = $positionRefId->position_id;
                }
                $this->positions            = PositionLevel::whereIn('id', $positionIds)->get();
            }
        }*/

        return view('admin.odoo-inter-co-employees.edit', $this->data);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function employeeAccessControl($id)
    {
        $this->userDetail = User::withoutGlobalScope('active')
            ->where('users.id','=',$id)
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('role_user.user_id','users.name','users.email','users.password',
                'users.remember_token','users.image','users.mobile','users.gender','users.locale',
                'users.status','users.created_at','users.updated_at', 'role_user.role_id as roleId'
            )->first();
        $this->roles = Role::where('name','!=','client')
            ->where('name','!=','admin')->get();
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->user_id)->first();


        if (!is_null($this->employeeDetail)) {


            $this->employeeDetail = $this->employeeDetail->withCustomFields();


            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;


        }

        return view('admin.employees.editAccess', $this->data);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function updateAccessControl(UpdateEmployee $request, $id)
    {


        $user = User::withoutGlobalScope('active')->findOrFail($id);


        $user->name = $request->input('name');


        $user->email = $request->input('email');


        if ($request->password != '') {


            $user->password = Hash::make($request->input('password'));


        }


        $user->mobile = $request->input('mobile');


        $user->gender = $request->input('gender');


        $user->status = $request->input('status');


        if ($request->hasFile('image')) {


            File::delete('user-uploads/avatar/' . $user->image);


            $user->image = $request->image->hashName();


            $request->image->store('user-uploads/avatar');


            // resize the image to a width of 300 and constrain aspect ratio (auto height)


            $img = Image::make('user-uploads/avatar/' . $user->image);


            $img->resize(300, null, function ($constraint) {


                $constraint->aspectRatio();


            });


            $img->save();


        }


        $user->save();


//        $validate = Validator::make(['job_title' => $request->job_title], [
//
//
//            'job_title' => 'required'
//
//
//        ]);
//
//
//        if ($validate->fails()) {
//
//
//            return Reply::formErrors($validate);
//
//
//        }


        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();


        if (empty($employee)) {


            $employee = new EmployeeDetails();


            $employee->user_id = $user->id;


        }


        // To add custom fields data


        if ($request->get('custom_fields_data')) {


            $employee->updateCustomFieldData($request->get('custom_fields_data'));


        }


        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function update(UpdateEmployee $request, $id)
    {


        $user = User::withoutGlobalScope('active')->findOrFail($id);


        $user->name = $request->input('name');


        $user->email = $request->input('email');


        if ($request->password != '') {


            $user->password = Hash::make($request->input('password'));


        }


        $user->mobile = $request->input('mobile');


        $user->gender = $request->input('gender');


        $user->status = $request->input('status');


        if ($request->hasFile('image')) {


            File::delete('user-uploads/avatar/' . $user->image);


            $user->image = $request->image->hashName();


            $request->image->store('user-uploads/avatar');


            // resize the image to a width of 300 and constrain aspect ratio (auto height)


            $img = Image::make('user-uploads/avatar/' . $user->image);


            $img->resize(300, null, function ($constraint) {


                $constraint->aspectRatio();


            });


            $img->save();


        }


        $user->save();


        $validate = Validator::make(['job_title' => $request->job_title], [


            'job_title' => 'required'


        ]);


        if ($validate->fails()) {


            return Reply::formErrors($validate);


        }


        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();


        if (empty($employee)) {


            $employee = new EmployeeDetails();


            $employee->user_id = $user->id;


        }


        $employee->job_title = $request->job_title;


        $employee->address = $request->address;


        $employee->hourly_rate = $request->hourly_rate;


        $employee->slack_username = $request->slack_username;


        $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');;


        $employee->save();


        // To add custom fields data


        if ($request->get('custom_fields_data')) {


            $employee->updateCustomFieldData($request->get('custom_fields_data'));


        }
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));


    }

    //Edited By Hafiz
    public function updateEmp(UpdateEmployee $request, $id)
    {
        config(['filesystems.default' => 'local']);
        $userId = $id;

        $roleId = $request->role_id;

        $user = User::findOrFail($userId);

        RoleUser::where('user_id', $user->id)->delete();

        $role_user = new RoleUser();
        $role_user->user_id = $user->id;
        $role_user->role_id = $roleId;
        $role_user->save();
        $user->name = $request->input('name');


        $user->email = $request->input('email');


        if ($request->password != '') {


            $user->password = Hash::make($request->input('password'));


        }


        $user->mobile = $request->input('mobile');


        $user->gender = $request->input('gender');


        $user->status = $request->input('status');

        $this->employeeDir($userId);

        if ($request->hasFile('image')) {
            File::delete($this->employeeDirPath . '/' . $user->image);
            $user->image = $request->image->hashName();
            $request->image->store($this->employeeDirPath);
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        /* if ($request->hasFile('image')) {
             File::delete('user-uploads/avatar/' . $user->image);
             $user->image = $request->image->hashName();


             $request->image->store('user-uploads/avatar');


             // resize the image to a width of 300 and constrain aspect ratio (auto height)


             $img = Image::make('user-uploads/avatar/' . $user->image);


             $img->resize(300, null, function ($constraint) {


                 $constraint->aspectRatio();


             });


             $img->save();


         }*/


        $user->save();


        $validate = Validator::make(['job_title' => $request->job_title], [


            'job_title' => 'required'


        ]);


        if ($validate->fails()) {


            return Reply::formErrors($validate);


        }
        if($request->password != ''){
            $user->notify(new NewUser($request->input('password')));
        }

        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();


        if (empty($employee)) {


            $employee = new EmployeeDetails();


            $employee->user_id = $user->id;


        }


        $employee->job_title = $request->job_title;


        $employee->address = $request->address;


        $employee->hourly_rate = $request->hourly_rate;


        $employee->slack_username = $request->slack_username;


        $employee->joining_date = Carbon::parse($request->joining_date)->format('Y-m-d');;


        $employee->save();


        // To add custom fields data


        if ($request->get('custom_fields_data')) {


            $employee->updateCustomFieldData($request->get('custom_fields_data'));


        }
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }

    public function updateOdoo(UpdateEmployee $request, $id)
    {

        config(['filesystems.default' => 'local']);

        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');

        $this->employeeDir($id);

        if($request->input('image_profile_cam') == ''){

            if ($request->hasFile('image_profile')) {
                File::delete($this->employeeDirPath . '/' . $user->image);
                $user->image = $request->image_profile->hashName();
                $request->image_profile->store($this->employeeDirPath);
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img = Image::make($this->employeeDirPath . '/' . $user->image);
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save();
            }
        }
        else{
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $request->input('image_profile_cam');
            $base_to_php            = explode(',', $camProfileImage);
            $data                   = base64_decode($base_to_php[1]);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $user->image      = $imageName.".png";
        }


        $user->save();


        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        $is_cnic = $employee->cnic_copy;


        $is_nadra = $employee->nadra_attested;
        $is_education = $employee->emp_edu_certificate;

        $employee->user_id = $user->id;
        $employee->job_title = $request->input('job_title');

        $employee->address = $request->input('address');

        $employee->hourly_rate = $request->input('hourly_rate');

        $employee->slack_username = $request->input('slack_username');

        $employee->joining_date = Carbon::parse($request->input('joining_date'))->format('Y-m-d');

        if($request->input('sigImageInput') != ''){
            $imageName              = md5(rand(100,10000));
            $camProfileImage        = $request->input('sigImageInput');
            $base_to_php            = explode(',', $camProfileImage);
            $data                   = base64_decode($base_to_php[1]);
            file_put_contents($this->employeeDirPath.'/'.$imageName.'.png',$data);
            $employee->signature_image      = $imageName.".png";
        }
        if ($request->hasFile('image_cnic')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_cnic);

            $employee->image_cnic = $request->image_cnic->hashName();
            $request->image_cnic->store($this->employeeDirPath);
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_cnic);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_cnic = true;
        }

        if ($request->hasFile('image_police_varification')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_police_varification);

            $employee->image_police_varification = $request->image_police_varification->hashName();
            $request->image_police_varification->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_police_varification);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_referee_undertaking')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_referee_undertaking);

            $employee->image_referee_undertaking = $request->image_referee_undertaking->hashName();
            $request->image_referee_undertaking->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_referee_undertaking);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }        if ($request->hasFile('image_emp_aggre')) {
        File::delete($this->employeeDirPath . '/' . $employee->image_emp_aggre);

        $employee->image_emp_aggre = $request->image_emp_aggre->hashName();
        $request->image_emp_aggre->store($this->employeeDirPath);

        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img = Image::make($this->employeeDirPath . '/' . $employee->image_emp_aggre);
        $img->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save();
    }


        if ($request->hasFile('image_pen_book')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_pen_book);

            $employee->image_pen_book = $request->image_pen_book->hashName();
            $request->image_pen_book->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_pen_book);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        if ($request->hasFile('image_nadra')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_nadra);

            $employee->image_nadra = $request->image_nadra->hashName();
            $request->image_nadra->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_nadra);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_nadra = true;
        }

        if ($request->hasFile('image_education')) {
            File::delete($this->employeeDirPath . '/' . $employee->image_education);

            $employee->image_education = $request->image_cnic->hashName();
            $request->image_education->store($this->employeeDirPath);

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make($this->employeeDirPath . '/' . $employee->image_education);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
            $is_education = true;
        }

        $employee->erp_type = $request->input('ERPType');
        $employee->employee_type = $request->input('employeeType');
        $employee->odoo_center_id = $request->input('center_id');
        $employee->division_id = $request->input('divisions');
        $employee->department_id = $request->input('departments');
        $employee->position_id = $request->input('positions');
        $employee->employee_cnic = $request->input('cnic');
        $employee->marital_status = $request->input('marital_status');
        $employee->number_of_children = $request->input('no_of_child');
        $employee->bank_account_number = $request->input('account_no');
        $employee->air_ticket = $request->input('air_ticket');
        $employee->father_name = $request->input('fatherName');
        $employee->category_ids = $request->input('category_ids');
        $employee->work_address = $request->input('working_address');
        $employee->work_location = $request->input('work_location');
        $employee->work_phone = $request->input('work_phone');
        $employee->parent_coach_user_id = $request->input('manager_id');
        $employee->is_manager = ($request->input('is_manager') != '') ? true : false;
        $employee->country_name = $request->input('nationality');
        $employee->passport_number = $request->input('passport_no');
        $employee->bank_name = $request->input('bank_name');
        $employee->bank_account_title = $request->input('account_title');
        $employee->place_of_birth = $request->input('nationality');
        $employee->cnic_expiry = $request->input('cnic_expiry');
        $employee->birthday = $request->input('birthday');
        $employee->cnic_copy = $is_cnic;
        $employee->education = $request->input('education');
        $employee->first_name = $request->input('first_name');
        $employee->middle_name = $request->input('middle_name');
        $employee->last_name = $request->input('last_name');
        $employee->mother_name = $request->input('mother_name');
        $employee->home_city = $request->input('home_city');
        $employee->last_qualification = $request->input('last_qualification');
        $employee->height = $request->input('height');
        $employee->weight = $request->input('weight');
        $employee->bank_branch = $request->input('bank_branch');
        $employee->bank_city = $request->input('bank_city');
        $employee->blood_group = $request->input('blood_group');
        $employee->land_line_number = $request->input('land_line_number');
        $employee->personal_email = $request->input('personal_email');
        $employee->kin_1_name = $request->input('kin_1_name');
        $employee->kin_1_number = $request->input('kin_1_number');
        $employee->kin_1_relation = $request->input('kin_1_relation');
        $employee->kin_2_name = $request->input('kin_2_name');
        $employee->kin_2_number = $request->input('kin_2_number');
        $employee->kin_2_relation = $request->input('kin_2_relation');
        $employee->basic_salary = $request->input('basic_salary');
        $employee->duty_hours = $request->input('duty_hours');
        $employee->duty_days = $request->input('duty_days');
        $employee->annual_leaves = $request->input('annual_leaves');
        $employee->air_ticket = $request->input('air_ticket');
        $employee->attendance_logging = $request->input('attendance_logging');
        $employee->rec_type = $request->input('rec_type');
        $employee->travel_benefits = $request->input('travel_benefits');
        $employee->travel_expenses = $request->input('travel_expenses');
        $employee->over_time_pay = $request->input('over_time_pay');
        $employee->email_pro = ($request->input('email_pro') != '') ? true : false;
        $employee->sim = ($request->input('sim') != '') ? true : false;
        $employee->mobile_phone = ($request->input('mobile_phone') != '') ? true : false;
        $employee->office = ($request->input('office') != '') ? true : false;
        $employee->office_stationary = ($request->input('office_stationary') != '') ? true : false;
        $employee->pc = ($request->input('pc') != '') ? true : false;
        $employee->laptop = ($request->input('laptop') != '') ? true : false;
        $employee->vehicle = ($request->input('vehicle') != '') ? true : false;
        $employee->uniform = ($request->input('uniform') != '') ? true : false;
        $employee->belt = ($request->input('belt') != '') ? true : false;
        $employee->shoes = ($request->input('shoes') != '') ? true : false;
        $employee->caps = ($request->input('caps') != '') ? true : false;
        $employee->badges = ($request->input('badges') != '') ? true : false;
        $employee->ref_1_name = $request->input('ref_1_name');
        $employee->ref_1_number = $request->input('ref_1_number');
        $employee->rel_to_ref1 = $request->input('rel_to_ref1');
        $employee->ref_1_address = $request->input('ref_1_address');
        $employee->ref_1_cnic = $request->input('ref_1_cnic');
        $employee->ref_2_name = $request->input('ref_2_name');
        $employee->ref_2_number = $request->input('ref_2_number');
        $employee->rel_to_ref2 = $request->input('rel_to_ref2');
        $employee->ref_2_address = $request->input('ref_2_address');
        $employee->ref_2_cnic = $request->input('ref_2_cnic');
        $employee->military_person = ($request->input('military_person') != '') ? true : false;
        $employee->force_no = $request->input('force_no');
        $employee->m_joining_date = $request->input('m_joining_date');
        $employee->m_leaving_date = $request->input('m_leaving_date');
        $employee->force_type = $request->input('force_type');
        $employee->force_rank = $request->input('force_rank');
        $employee->last_unit = $request->input('last_unit');
        $employee->last_center = $request->input('last_center');
        $employee->last_company_name = $request->input('last_company_name');
        $employee->last_position = $request->input('last_position');
        $employee->last_joining_date = $request->input('last_joining_date');
        $employee->last_leaving_date = $request->input('last_leaving_date');
        $employee->last_working_city = $request->input('last_working_city');
        $employee->last_working_country = $request->input('last_working_country');
        $employee->varification_date = $request->input('varification_date');
        $employee->left_fin_1 = $request->input('left_fin_1');
        $employee->left_fin_2 = $request->input('left_fin_2');
        $employee->left_fin_3 = $request->input('left_fin_3');
        $employee->left_fin_4 = $request->input('left_fin_4');
        $employee->left_thumb = $request->input('left_thumb');
        $employee->right_thumb = $request->input('right_thumb');
        $employee->right_fin_1 = $request->input('right_fin_1');
        $employee->right_fin_2 = $request->input('right_fin_2');
        $employee->right_fin_3 = $request->input('right_fin_3');
        $employee->right_fin_4 = $request->input('right_fin_4');
        $employee->nadra_attested = $is_nadra;
        $employee->emp_edu_certificate = $is_education;
        $employee->is_guard = ($request->input('is_guard') != '') ? true : false;

        /*fields that are not filling
        hourly_rate
        slack_username
        employee_odoo_id
        employee_erp
        is_guard
        work_location
        work_phone
        parent_coach_user_id
        passport_number
        bank_odoo_id
        resign_date
        education
        education_odoo_id
        education_certificate
        emp_cv
        emp_photographs
        country_id
        image_reference_undertaking
        air_ticket
        travel_expenses
        over_time_pay
        odoo_segment_id
        odoo_sub_segment_id
        odoo_department_id
        signature
        */
        $employee->save();

        /*add user id to position qasim*/
        $positionId = $request->input('positions');
        if($positionId != ''){
            $positionLevel      = new PositionLevel();
            $positionLevel->positionAssignToUser($positionId, $user->id);
        }

        /*add user id to position end*/

        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }

        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }

    public function employeeDir($id)
    {
        $this->employeeDirPath = public_path() . '/employee';
        if (!File::exists($this->employeeDirPath)) {
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }
        $this->employeeDirPath = public_path() . '/employee/' . $id;
        if (!File::exists($this->employeeDirPath)) {
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }

        $this->employeeDirPath = 'employee/' . $id;
        return $this->employeeDirPath;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {


        $user = User::withoutGlobalScope('active')->findOrFail($id);


        if ($user->id == 1) {


            return Reply::error(__('messages.adminCannotDelete'));


        }


        User::destroy($id);


        return Reply::success(__('messages.employeeDeleted'));


    }


    public function data()
    {
        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('odoo_inter_co_employees','odoo_inter_co_employees.user_id','users.id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId',
                'users.image', 'users.status','odoo_inter_co_employees.employee_erp')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();
        $roles = Role::where('name', '<>', 'client')->get();

        $total_collumns = DB::select(DB::raw("SELECT count(*) as total_count FROM information_schema.columns WHERE table_name = 'odoo_inter_co_employees'"));
        foreach ($users as $user){
            $filled_values = DB::select(DB::raw("SELECT 
                                count(division_id) + count(department_id) + count(job_title) + count(address)
                              + count(joining_date) + count(father_name)
                              + count(category_ids) + count(work_address) + count(work_location) + count(work_phone) + count(parent_coach_user_id)
                              + count(country_name) + count(passport_number) + count(bank_name)
                              + count(bank_account_title) + count(bank_account_number) + count(marital_status) + count(number_of_children) + count(birthday) 
                              + count(place_of_birth) + count(cnic_expiry) + count(cnic_copy) + count(nadra_attested) + count(education)
                              + count(emp_edu_certificate) + count(emp_cv) + count(emp_photographs)
                              + count(image_education) + count(image_cnic) + count(image_nadra) + count(position_id) + count(first_name) + count(middle_name)
                              + count(last_name) + count(mother_name) + count(home_city) + count(last_qualification) + count(height) + count(weight) + count(bank_branch)
                              + count(bank_city) + count(blood_group) + count(signature) + count(land_line_number) + count(personal_email) + count(kin_1_name)
                              + count(kin_1_number) + count(kin_1_relation) + count(kin_2_name) + count(kin_2_number) + count(kin_2_relation) + count(basic_salary)
                              + count(duty_hours) + count(duty_days) + count(annual_leaves) + count(air_ticket) + count(attendance_logging) + count(rec_type) 
                              + count(travel_expenses) + count(over_time_pay) + count(ref_1_name) + count(ref_1_number) + count(rel_to_ref1) + count(ref_1_address) + count(ref_1_cnic) 
                              + count(ref_2_name) + count(ref_2_number) + count(rel_to_ref2) + count(ref_2_address) + count(ref_2_cnic) + count(military_person)
                              + count(force_no) + count(m_joining_date) + count(m_leaving_date) + count(force_type) + count(force_rank) + count(last_unit) 
                              + count(last_center) + count(last_company_name) + count(last_position) + count(last_joining_date) + count(last_leaving_date)
                              + count(last_working_city) + count(last_working_country) + count(image_police_varification) + count(varification_date) 
                              + count(image_reference_undertaking) + count(image_emp_aggre) + count(image_pen_book) + count(left_fin_1)
                              + count(left_fin_2) + count(left_fin_3) + count(left_fin_4) + count(left_thumb) + count(right_thumb)
                              + count(right_fin_1) + count(right_fin_2) + count(right_fin_3) + count(right_fin_4) + count(employee_cnic) + count(employee_odoo_id)
                              + count(employee_erp) + count(odoo_center_id) 
                              AS total_not_null_count FROM odoo_inter_co_employees where id =".$user->id));
            $completion_percent = ($filled_values[0]->total_not_null_count/$total_collumns[0]->total_count)*100;
            $user->setAttribute('completion_precent', number_format($completion_percent,2,'.',''));
        }
        return Datatables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.odoo-inter-co-employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="' . route('admin.odoo-inter-co-employees.employeeAccessControl', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit System Profile"><i class="fa fa-pencil" style="color: red" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->addColumn('erp',function ($row){
                return 'Odoo';
            })
            ->editColumn('completion_precent',function ($row){
                if ($row->completion_precent < 50) {
                    $statusColor = 'danger';
                }
                elseif ($row->completion_precent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                }
                else {
                    $statusColor = 'success';
                }
                $style = 'style="width: ' . $row->completion_precent . '%"';
                return '<h5>'.__('app.completed').'<span class="pull-right">' . $row->completion_precent . '%</span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_precent . '" aria-valuemin="0" aria-valuemax="100" '.$style.' role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->editColumn(


                'status',


                function ($row) {


                    if ($row->status == 'active') {


                        return '<label class="label label-success">' . __('app.active') . '</label>';


                    } else {


                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';


                    }


                }


            )
            ->editColumn('name', function ($row) use ($roles) {

                $this->employeeDir($row->id);
                $path = $this->employeeDirPath.'/'.$row->image;
                if(file_exists($path)){
                    $image = '<img src="' . asset('employee/'.$row->id.'/'. $row->image) . '" alt="user" class="img-circle" width="30"> ';

                }
                else{
                    $image = ($row->image) ?
                        '<img src="' . asset('user-uploads/avatar/' . $row->image) . '" alt="user" class="img-circle" width="30"> '
                        :
                        '<img src="' . asset('default-profile-2.png') . '" alt="user" class="img-circle" width="30"> ';
                }

                if ($row->hasRole('admin')) {


                    return '<a href="' . route('admin.odoo-inter-co-employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-danger">' . __('app.admin') . '</label>';


                } else {

                    foreach ($roles as $role) {


                        foreach ($row->role as $urole) {


                            if ($role->id == $urole->role_id && $role->id != 2) {


                                return '<a href="' . route('admin.odoo-inter-co-employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-info">' . ucwords($role->name) . '</label>';


                            }


                        }


                    }


                    return '<a href="' . route('admin.odoo-inter-co-employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-warning">' . __('app.employee') . '</label>';

                }

                return '<a href="' . route('admin.odoo-inter-co-employees.employeeAccessControl', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a>';


            })
            ->rawColumns(['name', 'action', 'role', 'status','completion_precent'])
            ->removeColumn('roleId')
            ->removeColumn('roleName')
            ->make(true);


    }


    public function filter($centerID = null, $departmentID = null, $segmentID = null)

    {
        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->join('employee_details','employee_details.user_id','=','users.id')
            ->where('employee_details.odoo_center_id','=',$centerID)
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status','employee_details.employee_erp')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();

        $roles = Role::where('name', '<>', 'client')->get();

        $total_collumns = DB::select(DB::raw("SELECT count(*) as total_count FROM information_schema.columns WHERE table_name = 'employee_details'"));
        foreach ($users as $user){
            $filled_values = DB::select(DB::raw("SELECT 
                                count(division_id) + count(department_id) + count(job_title) + count(address)
                              + count(joining_date) + count(father_name)
                              + count(category_ids) + count(work_address) + count(work_location) + count(work_phone) + count(parent_coach_user_id)
                              + count(country_name) + count(passport_number) + count(bank_name)
                              + count(bank_account_title) + count(bank_account_number) + count(marital_status) + count(number_of_children) + count(birthday) 
                              + count(place_of_birth) + count(cnic_expiry) + count(cnic_copy) + count(nadra_attested) + count(education)
                              + count(emp_edu_certificate) + count(emp_cv) + count(emp_photographs)
                              + count(image_education) + count(image_cnic) + count(image_nadra) + count(position_id) + count(first_name) + count(middle_name)
                              + count(last_name) + count(mother_name) + count(home_city) + count(last_qualification) + count(height) + count(weight) + count(bank_branch)
                              + count(bank_city) + count(blood_group) + count(signature) + count(land_line_number) + count(personal_email) + count(kin_1_name)
                              + count(kin_1_number) + count(kin_1_relation) + count(kin_2_name) + count(kin_2_number) + count(kin_2_relation) + count(basic_salary)
                              + count(duty_hours) + count(duty_days) + count(annual_leaves) + count(air_ticket) + count(attendance_logging) + count(rec_type) 
                              + count(travel_expenses) + count(over_time_pay) + count(ref_1_name) + count(ref_1_number) + count(rel_to_ref1) + count(ref_1_address) + count(ref_1_cnic) 
                              + count(ref_2_name) + count(ref_2_number) + count(rel_to_ref2) + count(ref_2_address) + count(ref_2_cnic) + count(military_person)
                              + count(force_no) + count(m_joining_date) + count(m_leaving_date) + count(force_type) + count(force_rank) + count(last_unit) 
                              + count(last_center) + count(last_company_name) + count(last_position) + count(last_joining_date) + count(last_leaving_date)
                              + count(last_working_city) + count(last_working_country) + count(image_police_varification) + count(varification_date) 
                              + count(image_reference_undertaking) + count(image_emp_aggre) + count(image_pen_book) + count(left_fin_1)
                              + count(left_fin_2) + count(left_fin_3) + count(left_fin_4) + count(left_thumb) + count(right_thumb)
                              + count(right_fin_1) + count(right_fin_2) + count(right_fin_3) + count(right_fin_4) + count(employee_cnic) + count(employee_odoo_id)
                              + count(employee_erp) + count(odoo_center_id) 
                              AS total_not_null_count FROM employee_details where id =".$user->id));
            $completion_percent = ($filled_values[0]->total_not_null_count/$total_collumns[0]->total_count)*100;
            $user->setAttribute('completion_precent', number_format($completion_percent,2,'.',''));
        }
        return Datatables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="' . route('admin.employees.employeeAccessControl', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit System Profile"><i class="fa fa-pencil" style="color: red" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->addColumn('erp',function ($row){
                return 'Odoo';
            })
            ->editColumn('completion_precent',function ($row){
                if ($row->completion_precent < 50) {
                    $statusColor = 'danger';
                }
                elseif ($row->completion_precent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                }
                else {
                    $statusColor = 'success';
                }
                $style = 'style="width: ' . $row->completion_precent . '%"';
                return '<h5>'.__('app.completed').'<span class="pull-right">' . $row->completion_precent . '%</span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_precent . '" aria-valuemin="0" aria-valuemax="100" '.$style.' role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->editColumn(


                'created_at',


                function ($row) {


                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');


                }


            )
            ->editColumn(


                'status',


                function ($row) {


                    if ($row->status == 'active') {


                        return '<label class="label label-success">' . __('app.active') . '</label>';


                    } else {


                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';


                    }


                }


            )
            ->editColumn('name', function ($row) use ($roles) {


                $image = ($row->image) ? '<img src="' . asset('user-uploads/avatar/' . $row->image) . '"                                                            alt="user" class="img-circle" width="30"> ' : '<img src="' . asset('default-profile-2.png') . '"                                                            alt="user" class="img-circle" width="30"> ';


                if ($row->hasRole('admin')) {


                    return '<a href="' . route('admin.employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-danger">' . __('app.admin') . '</label>';


                } else {


                    foreach ($roles as $role) {


                        foreach ($row->role as $urole) {


                            if ($role->id == $urole->role_id && $role->id != 2) {


                                return '<a href="' . route('admin.employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-info">' . ucwords($role->name) . '</label>';


                            }


                        }


                    }


                    return '<a href="' . route('admin.employees.show', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a><br><br> <label class="label label-warning">' . __('app.employee') . '</label>';


                }


                return '<a href="' . route('admin.employees.employeeAccessControl', $row->id) . '">' . $image . ' ' . ucwords($row->name) . '</a>';


            })
            ->rawColumns(['name', 'action', 'role', 'status','completion_precent'])
            ->removeColumn('roleId')
            ->removeColumn('roleName')
            ->make(true);

    }


    public function tasks($userId, $hideCompleted)
    {


        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->select('tasks.id', 'projects.project_name', 'tasks.heading', 'tasks.due_date', 'tasks.status', 'tasks.project_id')
            ->where('tasks.user_id', $userId);


        if ($hideCompleted == '1') {


            $tasks->where('tasks.status', '=', 'incomplete');


        }


        $tasks->get();


        return Datatables::of($tasks)
            ->editColumn('due_date', function ($row) {


                if ($row->due_date->isPast()) {


                    return '<span class="text-danger">' . $row->due_date->format('d M, y') . '</span>';


                }


                return '<span class="text-success">' . $row->due_date->format('d M, y') . '</span>';


            })
            ->editColumn('heading', function ($row) {


                return ucfirst($row->heading);


            })
            ->editColumn('status', function ($row) {


                if ($row->status == 'incomplete') {


                    return '<label class="label label-danger">' . __('app.incomplete') . '</label>';


                }


                return '<label class="label label-success">' . __('app.complete') . '</label>';


            })
            ->editColumn('project_name', function ($row) {


                if (!is_null($row->project_name)) {


                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';


                }


            })
            ->rawColumns(['status', 'project_name', 'due_date'])
            ->removeColumn('project_id')
            ->make(true);


    }


    public function timeLogs($userId)
    {


        $timeLogs = ProjectTimeLog::join('projects', 'projects.id', '=', 'project_time_logs.project_id')
            ->select('project_time_logs.id', 'projects.project_name', 'project_time_logs.start_time', 'project_time_logs.end_time', 'project_time_logs.total_hours', 'project_time_logs.memo', 'project_time_logs.project_id', 'project_time_logs.total_minutes')
            ->where('project_time_logs.user_id', $userId);


        $timeLogs->get();


        return Datatables::of($timeLogs)
            ->editColumn('start_time', function ($row) {


                return $row->start_time->timezone($this->global->timezone)->format('d M, Y h:i A');


            })
            ->editColumn('end_time', function ($row) {


                if (!is_null($row->end_time)) {


                    return $row->end_time->timezone($this->global->timezone)->format('d M, Y h:i A');


                } else {


                    return "<label class='label label-success'>Active</label>";


                }


            })
            ->editColumn('project_name', function ($row) {


                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';


            })
            ->editColumn('total_hours', function ($row) {


                $timeLog = intdiv($row->total_minutes, 60) . ' hrs ';


                if (($row->total_minutes % 60) > 0) {


                    $timeLog .= ($row->total_minutes % 60) . ' mins';


                }


                return $timeLog;


            })
            ->rawColumns(['end_time', 'project_name'])
            ->removeColumn('project_id')
            ->make(true);


    }


    public function export()
    {


        $rows = User::leftJoin('employee_details', 'users.id', '=', 'employee_details.user_id')
            ->select(


                'users.id',


                'users.name',


                'users.email',


                'users.mobile',


                'employee_details.job_title',


                'employee_details.address',


                'employee_details.hourly_rate',


                'users.created_at'


            )
            ->get();


        // Initialize the array which will be passed into the Excel


        // generator.


        $exportArray = [];


        // Define the Excel spreadsheet headers


        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Job Title', 'Address', 'Hourly Rate', 'Created at'];


        // Convert each member of the returned collection into an array,


        // and append it to the payments array.


        foreach ($rows as $row) {


            $exportArray[] = $row->toArray();


        }


        // Generate and return the spreadsheet


        Excel::create('Employees', function ($excel) use ($exportArray) {


            // Set the spreadsheet title, creator, and description


            $excel->setTitle('Employees');


            $excel->setCreator('Worksuite')->setCompany($this->companyName);


            $excel->setDescription('Employees file');


            // Build the spreadsheet, passing in the payments array


            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {


                $sheet->fromArray($exportArray, null, 'A1', false, false);


                $sheet->row(1, function ($row) {


                    // call row manipulation methods


                    $row->setFont(array(


                        'bold' => true


                    ));


                });


            });


        })->download('xlsx');


    }


    public function importOdoo()
    {
        $fields = array('active' => true, 'is_guard' => false);

        set_time_limit(1000);

        $emp = $this->model->odoo_inter_co_employees($fields);
        foreach ($emp as $item) {
            $is_exist = 0;
            $odoo_emp = OdooInterCoEmployee::where('employee_odoo_id', $item['id'])->pluck('employee_odoo_id')->toArray();
            if ($odoo_emp != null) {

                $is_exist = 1;
            }

            if ($is_exist == 0) {

                $user = new User();

                $user->name = $item['name'];

                $email_exist = User::where('email','=',$item['work_email'])->first();
                if($item['work_email'] != null && $email_exist == null && $item['work_email'] != '-' && $item['work_email'] != 'N/A' && $item['work_email'] != 'NA' && $item['work_email'] != 'N/ A' && $item['work_email'] != 'Nill' && $item['work_email'] != 'Nil'){
                    $user->email = $item['work_email'];
                }
                else {
                    $user->email = $item['reference'] . 'erp@sospakistan.com';
                }
                $user->password = Hash::make('password');

                $user->mobile = $item['mobile_phone'];

                $user->gender = $item['gender'];

                $user->save();

                if ($user->id) {
                    $employee = new OdooInterCoEmployee();
                    $employee->user_id = $user->id;
                    $employee->first_name = $item['name'];
                    $employee->employee_odoo_id = $item['id'];
                    $employee->employee_erp = $item['reference'];
                    $employee->save();

                    $user->attachRole(2);
                    // Notify User
                    $user->notify(new NewUser('password'));
                    $this->logSearchEntry($user->id, $user->name, 'admin.odoo-inter-co-employees.show');
                }
            }

        }

        return redirect(route('admin.odoo-inter-co-employees.index'));

    }


    public function importCenters()
    {
        SosCenter::truncate();


        $centers = $this->model->getCenters();


        foreach ($centers as $item) {


            $center = new SosCenter();


            $center->odoo_id = $item['id'];


            $center->name = $item['name'];


            $center->save();


        }


        return redirect(route('admin.employees.index'));


    }


    public function importSegments()
    {


        Segment::truncate();


        $segments = $this->model->getSegments();


        foreach ($segments as $item1) {


            $segment = new Segment();


            $segment->odoo_id = $item1['id'];


            $segment->name = $item1['name'];


            $segment->save();


        }


        return redirect(route('admin.employees.index'));


    }


    public function importSubSegments()
    {


        SubSegment::truncate();


        $subSegments = $this->model->getSubSegments();


        foreach ($subSegments as $subSeg) {


            $subSegment = new SubSegment();


            $subSegment->odoo_id = $subSeg['id'];


            $subSegment->name = $subSeg['name'];


            $subSegment->save();


        }


        return redirect(route('admin.employees.index'));


    }


    public function importDepartments()
    {


        Department::truncate();


        $departments = $this->model->getDepartments();


        foreach ($departments as $dept) {


            $department = new Department();


            $department->odoo_id = $dept['id'];


            $department->name = $dept['name'];


            $department->save();


        }


        return redirect(route('admin.employees.index'));


    }


    public function assignRole(Request $request)
    {


        $userId = $request->userId;


        $roleId = $request->role;


        $employeeRole = Role::where('name', 'employee')->first();


        $user = User::findOrFail($userId);


        RoleUser::where('user_id', $user->id)->delete();


        $user->roles()->attach($employeeRole->id);


        if ($employeeRole->id != $roleId) {


            $user->roles()->attach($roleId);


        }


        return Reply::success(__('messages.roleAssigned'));


    }


    public function assignProjectAdmin(Request $request)
    {


        $userId = $request->userId;


        $projectId = $request->projectId;


        $project = Project::findOrFail($projectId);


        $project->project_admin = $userId;


        $project->save();


        return Reply::success(__('messages.roleAssigned'));


    }

}
