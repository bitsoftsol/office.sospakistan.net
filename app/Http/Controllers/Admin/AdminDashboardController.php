<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\GuardDetails;
use App\Issue;
use App\LeadFollowUp;
use App\Leave;
use App\Project;
use App\ProjectActivity;
use App\Task;
use App\Ticket;
use App\Traits\CurrencyExchange;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AdminDashboardController extends AdminBaseController
{
    use CurrencyExchange;

    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.dashboard');
        $this->pageIcon = 'icon-speedometer';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $client = new Client();
        $res = $client->request('GET', config('laraupdater.update_baseurl').'/laraupdater.json', ['verify' => false]);
        $lastVersion = $res->getBody();
        $lastVersion = json_decode($lastVersion, true);

        if ( $lastVersion['version'] > File::get('version.txt') ){
            $this->lastVersion = $lastVersion['version'];
        }

        $this->counts = DB::table('users')
            ->select(
                DB::raw('(select count(users.id) from `users` inner join role_user on role_user.user_id=users.id inner join roles on roles.id=role_user.role_id WHERE roles.name = "client") as totalClients'),
                DB::raw('(select count(*) from employee_details ) as totalEmployees'),
                DB::raw('(select count(projects.id) from `projects`) as totalProjects'),
                DB::raw('(select count(invoices.id) from `invoices` where status = "unpaid") as totalUnpaidInvoices'),
                DB::raw('(select sum(project_time_logs.total_minutes) from `project_time_logs`) as totalHoursLogged'),
                DB::raw('(select count(tasks.id) from `tasks` where status="completed") as totalCompletedTasks'),
                DB::raw('(select count(tasks.id) from `tasks` where status="incomplete") as totalPendingTasks'),
                DB::raw('(select count(attendances.id) from `attendances` where DATE(attendances.clock_in_time) = CURDATE()) as totalTodayAttendance'),
//                DB::raw('(select count(issues.id) from `issues` where status="pending") as totalPendingIssues'),
                DB::raw('(select count(tickets.id) from `tickets` where (status="open" or status="pending")) as totalUnResolvedTickets'),
                DB::raw('(select count(tickets.id) from `tickets` where (status="resolved" or status="closed")) as totalResolvedTickets')
            )
            ->first();

        $timeLog = intdiv($this->counts->totalHoursLogged, 60).' hrs ';
        $this->counts->totalGuards = count(GuardDetails::all());
        if(($this->counts->totalHoursLogged % 60) > 0){
            $timeLog.= ($this->counts->totalHoursLogged % 60).' mins';
        }

        $this->counts->totalHoursLogged = $timeLog;

        $this->pendingTasks = Task::where('status', 'incomplete')
            ->where(DB::raw('DATE(due_date)'), '<=', Carbon::today()->format('Y-m-d'))
            ->orderBy('due_date', 'desc')
            ->get();
        $this->pendingLeadFollowUps = LeadFollowUp::where(DB::raw('DATE(next_follow_up_date)'), '<=', Carbon::today()->format('Y-m-d'))
            ->join('leads', 'leads.id', 'lead_follow_up.lead_id')
            ->where('leads.next_follow_up', 'yes')
            ->get();

        $this->newTickets = Ticket::where('status', 'open')->orderBy('id', 'desc')->get();

        $this->projectActivities = ProjectActivity::limit(15)->orderBy('id', 'desc')->get();
        $this->userActivities = UserActivity::limit(15)->orderBy('id', 'desc')->get();

        foreach ($this->userActivities  as  $key => $userActivities){
            $userId = $userActivities->user->id;
            $getEmployeeImage = $this->getEmployeeImage($userId);
            $this->userActivities[$key]->user->setAttribute('employeeImage',$getEmployeeImage);
        }

        $this->feedbacks = Project::whereNotNull('feedback')->limit(5)->get();

        $locale = strtolower($this->global->locale);

        if($locale == 'pt-br'){
            $locale = 'en';
        }

        if(!is_null($this->global->latitude)){
            // get current weather
            $client = new Client();
            $res = $client->request('GET', 'https://api.darksky.net/forecast/9f7190aeb882036f098ba016003ab300/'.$this->global->latitude.','.$this->global->longitude.'?units=auto&exclude=minutely,daily&lang='.$locale, ['verify' => false]);
            $weather = $res->getBody();
            $this->weather = json_decode($weather, true);
        }

        // earning chart
        $this->currencies = Currency::all();
        $this->currentCurrencyId = $this->global->currency_id;

        $this->fromDate = Carbon::today()->timezone($this->global->timezone)->subDays(180);
        $this->toDate = Carbon::today()->timezone($this->global->timezone);
        $invoices = DB::table('payments')
            ->join('currencies', 'currencies.id', '=', 'payments.currency_id')
            ->where('paid_on', '>=', $this->fromDate)
            ->where('paid_on', '<=', $this->toDate)
            ->where('payments.status', 'complete')
            ->groupBy('paid_on')
            ->orderBy('paid_on', 'ASC')
            ->get([
                DB::raw('DATE_FORMAT(paid_on,"%Y-%m-%d") as date'),
                DB::raw('sum(amount) as total'),
                'currencies.currency_code',
                'currencies.is_cryptocurrency',
                'currencies.usd_price',
                'currencies.exchange_rate'
            ]);

        $chartData = array();
        foreach($invoices as $chart) {
            if($chart->currency_code != $this->global->currency->currency_code){
                if($chart->is_cryptocurrency == 'yes'){
                    if($chart->exchange_rate == 0){
                        if($this->updateExchangeRates()){
                            $usdTotal = ($chart->total*$chart->usd_price);
                            $chartData[] = ['date' => $chart->date, 'total' => floor($usdTotal / $chart->exchange_rate)];
                        }
                    }
                    else{
                        $usdTotal = ($chart->total*$chart->usd_price);
                        $chartData[] = ['date' => $chart->date, 'total' => floor($usdTotal / $chart->exchange_rate)];
                    }
                }
                else{
                    if($chart->exchange_rate == 0){
                       if($this->updateExchangeRates()){
                           $chartData[] = ['date' => $chart->date, 'total' => floor($chart->total / $chart->exchange_rate)];
                       }
                    }
                    else{
                        $chartData[] = ['date' => $chart->date, 'total' => floor($chart->total / $chart->exchange_rate)];
                    }
                }
            }
            else{
                $chartData[] = ['date' => $chart->date, 'total' => $chart->total];
            }
        }

        $this->chartData = json_encode($chartData);
        $this->leaves = Leave::where('status', '<>', 'rejected')->get();

        /**
         * Ticket stats
         */
        $this->viewDashboardTickets     = "true";
        $this->userTicketsCount         = Ticket::count();
        $this->openTicketsCount         = Ticket::where('status', 'open')->count();
        $this->pendingTicketsCount      = Ticket::where('status', 'pending')->count();
        $this->resolvedTicketsCount     = Ticket::where('status', 'resolved')->count();
        $this->closedTicketsCount       = Ticket::where('status', 'closed')->count();

        /**
         * Tasks stats
         */
        $this->viewDashboardTasks           = "true";
        $this->userTaskCount                = Task::count();
        $this->completedTaskCount           = Task::where("status", 'completed')->count();
        $this->incompleteTaskCount          = Task::where("status", 'incomplete')->count();
        $this->pendingTaskCount             = Task::where('status', 'incomplete')->where(DB::raw('DATE(due_date)'), '<=', Carbon::today()->format('Y-m-d'))->count();

        /**
         * Guard stats
         */
        $this->viewDashboardGuards          = "true";
        $this->guardsCount                  = GuardDetails::count();
        $dateS                              = Carbon::now();
        $dateE                              = Carbon::now()->subDays(5);
        $this->newGuardsCount               = GuardDetails::where("created_at", "<=", $dateS->format('Y-m-d')." 00:00:00")
            ->where("created_at",">=",$dateE->format('Y-m-d')." 23:59:59")
            ->get()->count();
        $this->terminatedGuardsCount        = 0;

        /**
         * Assignment stats
         */
        $this->viewDashboardAssignments     = "true";
        $this->userTotalProjectsCount       = Project::count();
        $this->completedProjectsCount       = Project::completed()->join('project_members','projects.id','project_members.project_id')->count();
        $this->inProcessProjectsCount       = Project::inProcess()->join('project_members','projects.id','project_members.project_id')->count();
        $this->overdueProjectsCount         = Project::overdue()->join('project_members','projects.id','project_members.project_id')->count();

        /**
         * Salaries stats
         */
        $this->viewDashboardSalaries        = "true";
        $this->salaryDeductions             = '200';
        $this->salaryPayable                = '500';
        $this->salaryPending                = '240';
        $this->salaryOnHold                 = '0';

        /**
         * Store stats
         */
        $this->viewDashboardStore           = "true";
        $this->storeTotalItems              = '1000';
        $this->storeNewIns                  = '130';
        $this->storeNewOuts                 = '70';
        $this->storeTotalInStock            = '300';
        $this->storeTotalDeployed           = '500';

        return view('admin.dashboard.index', $this->data);
    }
}
