<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!session_start()) {
    session_start();
}

class shiptime extends CI_Controller
{
    private  $error         = array();
    private  $success       = array();
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('Template');
        $this->load->library('session');
        $this->load->library('adminUser');
        $this->load->library('shiptime_custom');
        //Check user Login
        $this->load->library('check_user_login');
        $this->check_user_login->checkLogin();
        $this->load->model("general_model");
        $this->load->model("shiptime/shiptime_signup_model");
        $this->load->model("shiptime/config_model");
    }

    public function insert_edit()
    {
        $data                                   = $_POST;
        $country                                = $this->general_model->getMyData('countries', array('countries_id'=>$data['shipTimeCountry']), 'array');
        if(!empty($country)){
            $data['shipTimeCountry']            = $country[0]['countries_iso_code_2'];
        }

        if(isset($data['encryptedUsername']) && $data['encryptedUsername'] == ''){
            $shipTimeResponse                   = $this->shipTimeSignUp($data);

            if(isset($shipTimeResponse['error']) && $shipTimeResponse['error'] == 1){
                $this->error                    = array_merge($this->error, $shipTimeResponse['msg']);
                $errorMassage                   = join(" ",$this->error);
                $this->session->set_flashdata('errorMsg',$errorMassage);
                redirect(site_url().'/package/shop/main_index');
            }

            $shipTimeResponse                   = $shipTimeResponse['data'];

            if($shipTimeResponse->key->EncryptedUsername != ''){
                $data['encryptedUsername']      = $shipTimeResponse->key->EncryptedUsername;
                $data['encryptedPassword']      = $shipTimeResponse->key->EncryptedPassword;
            }
        }
        $data['shipTimeCountry']                = $country[0]['countries_id'];

        if(isset($data['id']) && $data['id'] != ''){
            $this->shiptime_signup_model->updateRecord($data);
        }else{
            $this->shiptime_signup_model->insertRecord($data);
        }

        $this->session->set_flashdata('successMsg','You have successfully save details');
        redirect(site_url().'/package/shop/main_index');
    }

    Private function shipTimeSignUp($data)
    {
        return $this->shiptime_custom->signUp($data);
    }

    public function config()
    {
        $data               = array();
        $data['action']     = base_url().'/package/shiptime/update_config';
        $data['errorMsg']   = '';
        $this->services($data);
        if(isset($this->error) && !empty($this->error)){
            $errorMassage                   = join(" ",$this->error);
            $data['errorMsg']               = $errorMassage;
        }
        $this->template->write_view('content', 'adminUser/package/shiptime/config', $data);
        $this->template->render();
    }

    public function services(&$data)
    {

        $configData                                     = $this->config_model->getShipTimeConfig();
        if(!empty($configData)) {

            $id                                         = $configData['id'];
            $data['settingId']                          = $configData['id'];
            if($configData['turnaround_days'] !=''){
                $data['configure_turnaround_days']      = json_decode($configData['turnaround_days']);
            }
            $data['configure_shipping_threshold']       = $configData['shipping_threshold'];
            $data['configure_internation_shipping']     = $configData['intl_shipping'];
            $data['configure_fallback_type']            = $configData['fallback_type'];
            $data['configure_fallback_fee']             = $configData['fallback_fee'];
            $data['configure_fallback_max']             = $configData['fallback_max'];
            $data['service_array']                      = json_decode($configData['services'], true);
            $data['customer_delivery_list_type']        = $configData['customer_delivery_list_type'];
        }


        $profile                        = $this->shiptime_signup_model->getActiveRecord();
        if(count($profile) > 0) {
            $profile                    = $profile[0];
            $encryptedUsername          = $profile->encrypted_username;
            $encryptedPassword          = $profile->encrypted_password;

            $shipTimeResponse           = $this->shiptime_custom->gerServices($encryptedUsername, $encryptedPassword);

            if(isset($shipTimeResponse['error']) && $shipTimeResponse['error'] == 1){
                $this->error            = array_merge($this->error, $shipTimeResponse['msg']);
                return false;
            }

            $shipTimeResponse           = $shipTimeResponse['data'];
            foreach ($shipTimeResponse->ServiceOptions as $serviceOption) {
                $shipping_services[$serviceOption->ServiceId] = array(
                    'carrier_id'    => $serviceOption->CarrierId,
                    'carrier_name'  => $serviceOption->CarrierName,
                    'service_name'  => $serviceOption->ServiceName,
                    'service_type'  => $serviceOption->ServiceType
                );
            }

            if(is_array($shipping_services)) {
                $data['shipping_services'] = $shipping_services;
            }
        }
    }


    public function update_config()
    {
        if(!empty($_POST)){
            $data           = $_POST;
            (isset($data['settingId']) && $data['settingId'] != '')? $this->config_model->updateRecord($data): $this->config_model->insertRecord($data);
        }

        redirect(site_url().'/package/shiptime/config');
    }
}