<?php

namespace App\Http\Controllers\Admin;

use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\TicketAgentGroups\StoreAgentGroup;
use App\TicketAgentGroups;
use App\TicketGroup;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageTicketAgentsController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Ticket Admins'; //__('app.menu.ticketAgents');
        $this->pageIcon = 'ti-headphone-alt';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->agents = TicketAgentGroups::all();
        $this->employees = User::all_ticket_admins();
        $this->groups = TicketGroup::all();
        foreach ($this->agents as $agent) {
            $getEmployeeImage = $this->getEmployeeImage($agent->user->id);
            $agent->user->image = $getEmployeeImage;
        }
        return view('admin.manage-ticket-settings.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgentGroup $request)
    {
        $users = $request->user_id;

        foreach($users as $user){
            $agent = new TicketAgentGroups();
            $agent->agent_id = $user;
            $agent->group_id = $request->group_id;
            $agent->save();

//            $notifyUser = User::findOrFail($user);
//            $notifyUser->notify(new NewProjectMember($member));

//            $this->logProjectActivity($request->project_id, ucwords($agent->user->name).__('messages.isAddedAsProjectMember'));
        }

        return Reply::success(__('messages.agentAddedSuccessfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = TicketAgentGroups::findOrFail($id);
        $agent->status = $request->status;
        $agent->save();

        return Reply::success(__('messages.statusUpdatedSuccessfully'));
    }

    public function updateGroup(Request $request, $id)
    {
        $agent = TicketAgentGroups::findOrFail($id);
        $agent->group_id = $request->groupId;
        $agent->save();

        return Reply::success(__('messages.groupUpdatedSuccessfully'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TicketAgentGroups::destroy($id);

        return Reply::success(__('messages.agentRemoveSuccess'));
    }
}
