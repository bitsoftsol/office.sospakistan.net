<?php
namespace App\Http\Controllers\Admin;
use App\Department;

use App\EmailNotificationSetting;
use App\GuardDetails;
use Illuminate\Support\Facades\File;
use App\LanguageSetting;

use App\Notification;
use App\PositionLevel;
use App\ProjectActivity;
use App\Segment;

use App\Setting;

use App\SosCenter;

use App\StickyNote;

use App\SubSegment;

use App\Traits\FileSystemSettingTrait;

use App\UniversalSearch;

use App\User;

use App\UserActivity;

use App\UserChat;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Validator;

use SebastianBergmann\CodeCoverage\Report\Xml\Project;

use App\ThemeSetting;
class AdminBaseController extends Controller

{

    use FileSystemSettingTrait;
    /**

     * @var array

     */

    public $data = [];
    /**

     * @param $name

     * @param $value

     */

    public function __set($name, $value)

    {

        $this->data[$name] = $value;

    }
    /**

     * @param $name

     * @return mixed

     */

    public function __get($name)

    {

        return $this->data[$name];

    }
    /**

     * @param $name

     * @return bool

     */

    public function __isset($name)

    {

        return isset($this->data[ $name ]);

    }
    /**

     * UserBaseController constructor.

     */

    public function __construct()

    {

        // Inject currently logged in user object into every view of user dashboard

        $this->odooModel = '';

        $this->totalGaurds = 0;

        $this->global = Setting::first();

        $this->emailSetting = EmailNotificationSetting::all();

        $this->departments = Department::all();

        $this->centers = SosCenter::all();

        $this->segments = Segment::all();

        $this->employees = User::with('role')

            ->withoutGlobalScope('active')

            ->join('role_user', 'role_user.user_id', '=', 'users.id')

            ->join('roles', 'roles.id', '=', 'role_user.role_id')

            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status')

            ->where('roles.name', '<>', 'client')

            ->groupBy('users.id')

            ->get();

        $this->subSegments = SubSegment::all();

        $this->companyName = $this->global->company_name;

        $this->positions = PositionLevel::where('po_level_user_id','=',0)->get();

        $this->adminTheme = ThemeSetting::where('panel', 'admin')->first();

        $this->languageSettings = LanguageSetting::where('status', 'enabled')->get();
        App::setLocale($this->global->locale);

        Carbon::setLocale($this->global->locale);

        setlocale(LC_TIME,$this->global->locale.'_'.strtoupper($this->global->locale));

        $this->setFileSystemConfigs();
        $this->middleware(function ($request, $next) {

            $this->user = auth()->user();

            $this->user->image  = $this->getEmployeeImage($this->user->id);

            $this->unreadMessageCount = UserChat::where('to', $this->user->id)->where('message_seen', 'no')->count();

            $this->unreadTicketCount = Notification::where('notifiable_id', $this->user->id)

                ->where('type', 'App\Notifications\NewTicket')

                ->whereNull('read_at')

                ->count();

            $this->unreadExpenseCount = Notification::where('notifiable_id', $this->user->id)

                ->where('type', 'App\Notifications\NewExpenseAdmin')

                ->whereNull('read_at')

                ->count();

            $this->unreadIssuesCount = Notification::where('notifiable_id', $this->user->id)

                ->where('type', 'App\Notifications\NewIssue')

                ->whereNull('read_at')

                ->count();
            $this->stickyNotes = StickyNote::where('user_id', $this->user->id)->orderBy('updated_at', 'desc')->get();

            return $next($request);

        });    }
    public function logProjectActivity($projectId, $text) {

        $activity = new ProjectActivity();

        $activity->project_id = $projectId;

        $activity->activity = $text;

        $activity->save();

    }
    public function logUserActivity($userId, $text) {

        $activity = new UserActivity();

        $activity->user_id = $userId;

        $activity->activity = $text;

        $activity->save();

    }
    public function logSearchEntry($searchableId, $title, $route) {

        $search = new UniversalSearch();

        $search->searchable_id = $searchableId;

        $search->title = $title;

        $search->route_name = $route;

        $search->save();

    }

    public function employeeDir($id)
    {
        $this->employeeDirPath = public_path() . '/employee';
        if (!File::exists($this->employeeDirPath)) {
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }
        $this->employeeDirPath = public_path() . '/employee/' . $id;
        if (!File::exists($this->employeeDirPath)) {
            File::makeDirectory($this->employeeDirPath, $mode = 0777, true, true);
        }

        $this->employeeDirPath = public_path() .'/employee/' . $id;
        return $this->employeeDirPath;

    }

    public function getEmployeeImage($id)
    {
        $path = asset('default-profile-2.png');
        $this->employeeDir($id);
        $userInfo = User::where('id', '=', $id)->first();
        if($userInfo != ''){

            if($userInfo->image != ''){
                $path = $this->employeeDirPath.'/'.$userInfo->image;
                if(file_exists($path) && is_file($path)){
                    $path = asset('employee/'.$userInfo->id.'/'. $userInfo->image);
                }
                else{
                    $path = (file_exists(asset('user-uploads/avatar/' . $userInfo->image)) && is_file(asset('user-uploads/avatar/' . $userInfo->image))) ? asset('user-uploads/avatar/' . $userInfo->image):asset('default-profile-2.png');
                }
            }

        }
        return $path;
    }

    public function  get_guard_image($id)
    {
        $path = asset('default-profile-2.png');
        $this->employeeDir($id);
        $emp_info = GuardDetails::where('id', '=', $id)->first();
        if($emp_info != ''){
            $path = $this->employeeDirPath.'/'.$emp_info->profile_image;

            if(file_exists($path) && is_file($path)){
                $path = asset('employee/'.$emp_info->id.'/'. $emp_info->profile_image);
            }
            else{
                $path = ($emp_info->profile_image) ? asset('user-uploads/avatar/' . $emp_info->profile_image):asset('default-profile-2.png');
            }
        }
        return $path;
    }

}

