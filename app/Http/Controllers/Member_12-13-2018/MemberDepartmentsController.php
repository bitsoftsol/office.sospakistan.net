<?php

namespace App\Http\Controllers\Member;

use App\Department;
use App\EmployeeDepartment;
use App\EmployeeTeam;
use App\Helper\Reply;
use App\Http\Requests\Department\StoreDepartment;
use App\ModuleSetting;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class MemberDepartmentsController extends MemberBaseController

{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Departments';
        $this->pageIcon = 'icon-user';

        if(!ModuleSetting::checkModule('employees')){
            abort(403);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->groups = Department::all();
        return view('member.departments.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departments.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartment $request)
    {
        $group = new Department();
        $group->name = $request->name;
        $group->save();
        // Create Department In Odoo As Well
        return Reply::redirect(route('admin.departments.index'), 'Department created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->group = Department::findOrFail($id);
        $this->employees = User::doesntHave('department', 'and', function($query) use ($id){
            $query->where('department_id', $id);
        })
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();
        return view('member.departments.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->group = Department::findOrFail($id);
        $this->employees = User::doesntHave('department', 'and', function($query) use ($id){
            $query->where('department_id', $id);
        })
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();
        return view('admin.departments.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Department::find($id);
        $group->name = $request->name;
        $group->save();

        if(!empty($users = $request->user_id)){
            foreach($users as $user){
                $member = new EmployeeDepartment();
                $member->user_id = $user;
                $member->department_id = $id;
                $member->save();
            }
        }


        return Reply::redirect(route('admin.departments.index'), 'Department Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Department::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }
}
