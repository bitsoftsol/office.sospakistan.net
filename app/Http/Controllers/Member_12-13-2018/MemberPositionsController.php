<?php

namespace App\Http\Controllers\Member;

use App\PositionLevel;
use App\PositionDivisionDepartmentXref;
use App\PositionDepartmentXref;
use App\Department;
use Carbon\Carbon;
use App\Helper\Reply;
use App\Http\Requests\Position\PositionRequest;
use Yajra\Datatables\Datatables;
use App\User;

class MemberPositionsController extends MemberBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle    = "Positions";//__('app.menu.positions');
        $this->pageIcon     = 'ti-menu';
        $this->addText      = 'Add';//__('app.menu.positions');;
        $this->showText     = 'Show';//__('app.menu.positions');;
    }

    public function index(){
        if(!$this->user->can('view_positions')){
            abort(403);
        }
        $this->positionsData    = PositionLevel::all();
        $this->positions        = count($this->positionsData);
        return view('member.positions.index', $this->data);
    }

    public function createOrEditView($positionID = false){

        if(!$this->user->can('add_positions')){
            abort(403);
        }
        $users = User::with('role')
            ->withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status')
            ->where('roles.name', '<>', 'client')
            ->groupBy('users.id')
            ->get();

        $division                       = PositionLevel::division();
        $this->status                   = 1;
        $this->parentId                 = 0;
        $this->userId                   = 0;
        $this->id                       = 0;
        $this->divisionParentId         = 0;
        $this->isUnderDepartment        = 0;
        $this->isDivision               = false;
        $this->divisionAssignedDepartments      = array();
        $divisionDepartmentIds          = array();
        $divisionDepartment             = array();
        $departmentIds                  = array();
        $this->positionDepartmentId     = '';
        $this->positionsData            = PositionLevel::where("is_division","=","0")->get();
        if($positionID){
            $this->positionData         = PositionLevel::findOrFail($positionID);
            $this->id                   = $positionID;
            $this->name                 = $this->positionData->po_level_name;
            $this->order                = $this->positionData->po_level_order;
            $this->parentId             = $this->positionData->po_level_parent_id;
            $this->userId               = $this->positionData->po_level_user_id;
            $this->status               = $this->positionData->po_level_status;
            $this->divisionParentId     = $this->positionData->po_level_parent_id;//$this->positionData->division_parent_id;
            $this->isUnderDepartment    = $this->positionData->is_under_department;
            $this->divisionAssignedDepartments      = Department::all();
            $checkDivision              = isset($this->positionData->is_division)?$this->positionData->is_division:0;
            if($checkDivision == 1){
                $this->isDivision               = true;
                $divisionDepartment             = PositionDivisionDepartmentXref::getByDivision($positionID);
                foreach ($divisionDepartment as $divisionDepartmentIn){
                    $divisionDepartmentIds[]    = $divisionDepartmentIn->department_id;
                }
            }

            if($this->isUnderDepartment == 1){
                $departmentRefIds   = PositionDivisionDepartmentXref::getByDivision($this->parentId);

                if(!empty($departmentRefIds)) {
                    foreach ($departmentRefIds as $departmentRefId) {
                        $departmentIds[]                    = $departmentRefId->department_id;
                    }

                    $this->divisionAssignedDepartments      = Department::whereIn('id', $departmentIds)->get();
                }

                $positionDepartmentData                     = PositionDepartmentXref::where("position_id","=",$positionID)->first();
                $this->positionDepartmentId                 = isset($positionDepartmentData->department_id)?$positionDepartmentData->department_id:0;
            }

            $division                           = PositionLevel::divisionExceptSelf($positionID);
            $this->positionsData                = PositionLevel::where("is_division","=","0")->where('id', '!=', $positionID)->get();
        }

        $this->division                         = $division;
        $this->users                            = $users;
        $this->divisionDepartmentIds            = $divisionDepartmentIds;

        $department                             = Department::all();
        $this->department                       = $department;

        return view('member.positions.create', $this->data);
    }


    public function save(PositionRequest $request){
        $count                          = 0;
        do{
            $name                       = $request->input('name');
            $id                         = $request->input('id');
            $order                      = $request->input('order');
            $parentId                   = $request->input('parent');
            $userId                     = $request->input('userId');
            $divisionParentId           = $request->input('divisionParentId');
            $status                     = $request->input('status');
            $isDivision                 = $request->input('isDivision');
            $addMultiple                = $request->input('addMultiple');
            $addMultipleNumber          = $request->input('addMultipleNumber');
            $divisionDepartments        = $request->input('divisionDepartment');
            $positionDepartment         = $request->input('positionDepartment');


            if($id){
                $position                   = PositionLevel::findOrFail($id);
            }
            else{
                $position                   = new PositionLevel();
            }

            $position->po_level_name        = $name;
            $position->po_level_order       = $order;
            $position->po_level_parent_id   = ($parentId != '')?$parentId:0;
            $position->po_level_user_id     = ($userId != '')?$userId:0;
            $position->is_division          = ($isDivision != '')?$isDivision:0;
            if($isDivision == 1){
                $position->division_parent_id   = ($parentId != '')?$parentId:0;
            }
            else{
                $position->division_parent_id   = 0;
            }

            if($positionDepartment > 0){
                $position->is_under_department   = '1';
            }
            else{
                $position->is_under_department   = '0';
            }

            $position->po_level_status          = $status;
            $position->save();
            $lastInsertId                       = $position->id;

            /*
             * relation with department
             * */
            if($isDivision == 1){
                if(!empty($divisionDepartments)){
                    $PositionDivisionDepartmentXref                         = new PositionDivisionDepartmentXref();
                    $PositionDivisionDepartmentXref->deleteByDivision($lastInsertId);
                    foreach ($divisionDepartments as $divisionDepartment){
                        $PositionDivisionDepartmentXref                     = new PositionDivisionDepartmentXref();
                        $PositionDivisionDepartmentXref->division_id        = $lastInsertId;
                        $PositionDivisionDepartmentXref->department_id      = $divisionDepartment;
                        $PositionDivisionDepartmentXref->save();
                    }
                }
            }
            else{
                if($positionDepartment > 0){
                    $PositionDepartmentXref                     = new PositionDepartmentXref();
                    $PositionDepartmentXref->deleteByPosition($lastInsertId);

                    $PositionDepartmentXref                     = new PositionDepartmentXref();
                    $PositionDepartmentXref->position_id        = $lastInsertId;
                    $PositionDepartmentXref->division_id        = $parentId;
                    $PositionDepartmentXref->department_id      = $positionDepartment;
                    $PositionDepartmentXref->save();
                }
            }

            $count++;
        }
        while($addMultiple != '' && $addMultipleNumber > $count);

        return Reply::redirect(route('member.positions'));

    }

    public function show() {
        $PositionLevel          = new PositionLevel();
        $hierarchy              = $PositionLevel->getChildren(0);
        $this->hierarchy        = $hierarchy;
        $this->HierarchyHTML    = $this->makeHierarchyHTML($this->hierarchy);
        return view('member.positions.show', $this->data);
    }

    public function showToAll()
    {
        $PositionLevel          = new PositionLevel();
        $hierarchy              = $PositionLevel->getChildren(0);
        $this->hierarchy        = $hierarchy;
        $this->HierarchyHTML    = $this->makeHierarchyHTML($this->hierarchy);
        return view('member.positions.show', $this->data);
    }

    public function makeHierarchyHTML($hierarchy){
        $HTML       = '';
        foreach ($hierarchy as $index){
            $HTML   .= '<li><a href="'.route('member.positions.createOrEditView', [$index->id]).'">'.$index->po_level_name.'</a>'.$this->makeHierarchyHTML($index->children).'</li>';
        }
        if($HTML != ''){
            return '<ul>'.$HTML.'</ul>';
        }

    }

    public function data() {
        $position               = PositionLevel::all();
        foreach ($position as $positionIn){
            $empName            = 'Not assign';
            if($positionIn->po_level_user_id > 0){
                $empId          = $positionIn->po_level_user_id;
                $userInfo       = User::where('id', '=', $empId)->first();
                if($userInfo){
                    $empName    = $userInfo->name;
                }
            }
            $positionIn->setAttribute('name',$empName);
        }

        return Datatables::of($position)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('member.positions.createOrEditView', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');
                })
            ->editColumn(
                'status',
                function ($row) {
                    if($row->po_level_status == '1'){
                        return '<label class="label label-success">'.__('app.active').'</label>';
                    }
                    else{
                        return '<label class="label label-danger">'.__('app.deactive').'</label>';
                    }
                }
            )
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function destroy($id)
    {
        PositionLevel::destroy($id);
        return Reply::success(__('messages.positionDeleted'));
    }

    public function ajaxGetDepartmentByDivision($divisionId)
    {
        $return             = array();
        $return['error']    = 1;
        $return['msg']      = 'Data Not Found';
        $return['data']     = array();

        $departmentIds      = array();
        $departmentRefIds   = PositionDivisionDepartmentXref::getByDivision($divisionId);


        if(!empty($departmentRefIds)){
            foreach ($departmentRefIds as $departmentRefId){
                $departmentIds[]        = $departmentRefId->department_id;
            }
            $departments                = Department::whereIn('id', $departmentIds)->get();
            if(!empty($departments)){
                $departmentHTML         = $this->departmentHTML($departments);
                $return['error']        = 0;
                $return['msg']          = 'Data Found';
                $return['data']         = array('departments'=>$departmentHTML);
            }
        }

        echo json_encode($return); exit;
    }

    public function ajaxGetDepartments()
    {
        $return             = array();
        $return['error']    = 1;
        $return['msg']      = 'Data Not Found';
        $return['data']     = array();
        $departments                = Department::all();
        if(!empty($departments)){
            $departmentHTML         = $this->departmentHTML($departments);
            $return['error']        = 0;
            $return['msg']          = 'Data Found';
            $return['data']         = array('departments'=>$departmentHTML);
        }

        echo json_encode($return); exit;
    }

    public function departmentHTML($data)
    {
        $html           = '';
        $count          = 0;
        foreach ($data as $dataIn){
            if($count == 0 && isset($dataIn->id) && $dataIn->id != ''){
                $html       .= "<option value=''>Select Department</option>";
            }
            $html       .= "<option value='".$dataIn->id."'>".$dataIn->name."</option>";

            $count++;
        }
        return $html;
    }

    public function divisionList()
    {
        $this->pageTitle        = "Divisions";
        $this->pageIcon         = 'ti-menu';
        $this->divisionData     = PositionLevel::where('is_division','=','1')->get();
        $this->divisions        = count($this->divisionData);
        return view('admin.positions.divisionList', $this->data);
    }

    public function divisionData() {

        $division               = PositionLevel::where('is_division','=','1')->get();
        //echo "<pre>"; print_r($division); exit;

        return Datatables::of($division)
            ->addColumn('action', function ($row) {

                return "No Action Yet";
                /*return '<a href="' . route('admin.positions.createOrEditView', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-position-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';*/
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->formatLocalized('%B %d, %Y');
                })
            ->editColumn(
                'status',
                function ($row) {
                    if($row->po_level_status == '1'){
                        return '<label class="label label-success">'.__('app.active').'</label>';
                    }
                    else{
                        return '<label class="label label-danger">'.__('app.deactive').'</label>';
                    }
                }
            )
            ->rawColumns(['po_level_name', 'action', 'status'])
            ->make(true);
    }
}
