<?php

namespace App\Http\Controllers\Member;

use App\EmployeeDetails;
use App\GuardDetails;
use App\Helper\Reply;
use App\Http\Controllers\Member\MemberBaseController;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateEmployee;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectTimeLog;
use App\Repositories\EmployeeRepository;
use App\Role;
use App\Task;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;


class MemberGuardsController extends MemberBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.employees');
        $this->odooModel = 'hr.employee';
        $this->model = new EmployeeRepository($this->odooModel);
        $this->pageIcon = 'icon-user';

        if(!ModuleSetting::checkModule('guards')){
            abort(403);
        }
    }

    public function index() {
        if(!$this->user->can('view_guards')){
            abort(403);
        }
        $this->totalGaurds = count(GuardDetails::all());
        return view('member.guards.index', $this->data);
    }

    public function create() {
      return 'Create';
    }

    public function store(StoreUser $request) {
        return 'Store';
    }

    public function show($id) {
        return 'Show';
    }

    public function edit($id) {
        return 'Edit';
    }

    public function update(UpdateEmployee $request, $id) {
        return 'Update';
    }

    public function destroy($id) {
       return 'Destroy';
    }

    public function data() {

        $gurads = GuardDetails::all();
        return Datatables::of($gurads)
            ->addColumn('action', function ($row) {
                return '
                      <a href="' . route('member.guards.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->make(true);
    }

    public function export() {
        $rows = GuardDetails::all();

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Address', 'Name', 'ERP NO', 'Center Name', 'CNIC', 'CNIC Expiry', 'Current Post', 'Odoo ID', 'Contract', 'Mobile','Gender','Hourly Rate', 'Created At','Updated At', 'Joining Date'];


        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Employees', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Guards Data');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Guards File');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('guard_details', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });


        })->download('xlsx');
    }

    public function importOdoo()
    {
        set_time_limit(1000);
        GuardDetails::truncate();
        $fields = array('is_guard' => true, 'active' => true, 'current' => true);
        $guards = $this->model->allguards($fields);
        $laravelGuards = count(GuardDetails::all());
        for($i=0; $i<count($guards);$i++)
        {
            $newgaurd = new GuardDetails();
            $newgaurd->joining_date = $guards[$i]['appointmentdate'];
            $newgaurd->address = $guards[$i]['address_home_id'] != false ? $guards[$i]['address_home_id'][1] : "No Address Defined in Odoo";
            $newgaurd->guard_name = $guards[$i]['name'];
            $newgaurd->employee_erp = $guards[$i]['reference'];
            $newgaurd->center_name = $guards[$i]['center_id'] != false ? $guards[$i]['center_id'][1] : "No Center is defined in Odoo";
            $newgaurd->employee_cnic = $guards[$i]['cnic'];
            $newgaurd->cnic_expiry = $guards[$i]['cnic_expiry'];
            $newgaurd->current_post = $guards[$i]['current_post_id'] != false ? $guards[$i]['center_id'][1] : "No Current Post is Defined in Odoo";
            $newgaurd->odoo_id = $guards[$i]['id'];
            $newgaurd->contract = $guards[$i]['guard_contract_id'] != false ? $guards[$i]['guard_contract_id'][1] : "No Contract is Defined in Odoo";
            $newgaurd->mobile = $guards[$i]['mobile_phone'];
            $newgaurd->gender = $guards[$i]['gender'];
            $newgaurd->save();
        }
        return redirect(route('admin.guards.index'));
    }


}

