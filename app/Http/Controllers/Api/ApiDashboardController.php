<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Api;

use App\ProjectTimeLog;
use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\ProjectMember;
use App\Task;
use App\getProjectActivities;
use Illuminate\Support\Facades\DB;
use App\User;
use App\LeaveType;

class ApiDashboardController extends ApiBaseController{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId                 = $request->input('userId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $user                   = User::findOrFail($userId);
        if(!isset($user->employee[0])){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Employee record against this user is not found');
            return response()->json($response, 200);
        }

        $userProjects               = ProjectMember::where('user_id','=', $userId)->get();
        $userCompleteTask           = Task::where('user_id','=', $userId)->where('status','=','completed')->get();
        $userPendingTask            = Task::where('user_id','=', $userId)->where('status','=','incomplete')->get();
        $userTimeLog                = DB::table('users')->select(DB::raw('(select sum(project_time_logs.total_minutes) from `project_time_logs`) as totalHoursLogged'))->first();
        $userProjectsCount          = 0;
        $userCompleteTaskCount      = 0;
        $userPendingTaskCount       = 0;
        $userTimeLogCount           = 0;

        if($userProjects->count() > 0){
            $userProjectsCount      = $userProjects->count();
        }
        if($userCompleteTask->count() > 0){
            $userCompleteTaskCount  = $userCompleteTask->count();
        }
        if($userPendingTask->count() > 0){
            $userPendingTaskCount   = $userPendingTask->count();
        }
        if($userTimeLog->totalHoursLogged > 0){
            $userTimeLogCount       = $userTimeLog->totalHoursLogged;
        }

        $leaveState                             = LeaveType::getAllLeaveStats($userId);

        $data                                   = array();
        $data['count']['userProjects']          = $userProjectsCount;
        $data['count']['userCompleteTaskCount'] = $userCompleteTaskCount;
        $data['count']['userPendingTaskCount']  = $userPendingTaskCount;
        $data['count']['userTimeLogCount']      = $userTimeLogCount;
        $data['count']['LeaveStats']            = $leaveState;

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);
    }

}