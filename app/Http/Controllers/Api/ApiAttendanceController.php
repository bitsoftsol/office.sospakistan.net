<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Api;


use App\User;
use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\getProjectActivities;
use App\Leave;
use App\LeaveType;
use App\AttendanceSetting;
use App\Attendance;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class ApiAttendanceController extends ApiBaseController{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId                 = $request->input('userId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }


        $attendanceSettings = AttendanceSetting::first();
        $openDays = json_decode($attendanceSettings->office_open_days);
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::today()->timezone($this->global->timezone);

        $this->totalWorkingDays = $this->startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $this->endDate);
        $this->daysPresent = Attendance::countDaysPresentByUser($this->startDate, $this->endDate, $userId);
        $this->daysLate = Attendance::countDaysLateByUser($this->startDate, $this->endDate, $userId);
        $this->halfDays = Attendance::countHalfDaysByUser($this->startDate, $this->endDate, $userId);
        $this->todayAttendance = Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon::today()->format('Y-m-d'))
            ->where('user_id', $userId)->first();
        $data = array();
        $data['daysPresent']            = $this->daysPresent;
        $data['daysLate']               = $this->daysLate;
        $data['halfDays']               = $this->halfDays;
        $data['absentDays']             = $this->totalWorkingDays - $this->daysPresent;
        $data['totalWorkingDays']       = $this->totalWorkingDays;
        $data['todayAttendance']        = $this->todayAttendance;

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);
    }

    public function clockIn(Request $request)
    {

        $userId                 = $request->input('userId');
        $workingFrom            = $request->input('workingFrom');

        $todayAttendance = Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon::today()->format('Y-m-d'))
            ->where('user_id', $userId)->first();

        if(!empty($todayAttendance)){
            if(!empty($todayAttendance)){
                $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), "Already clocked in");
                return response()->json($response, 200);
            }
        }

        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $attendanceSettings = AttendanceSetting::first();
        $now = Carbon::now();
        $timestamp = $now->format('Y-m-d').' '.$attendanceSettings->office_start_time;
        $officeStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, $this->global->timezone);
        $officeStartTime = $officeStartTime->setTimezone('UTC');

        $lateTime = $officeStartTime->addMinutes($attendanceSettings->late_mark_duration);

        $attendance = new Attendance();
        $attendance->user_id = $userId;
        $attendance->clock_in_time = $now;
        $attendance->clock_in_ip = request()->ip();
        if(is_null($workingFrom)){
            $attendance->working_from = 'office';
        }
        else{
            $attendance->working_from = $workingFrom;
        }

        if($now->gt($lateTime)){
            $attendance->late = 'yes';
        }

        $attendance->half_day = 'no';
        $attendance->save();
        $data = array();
        $data['attendanceId']       = $attendance->id;

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);
    }

    public function clockOut(Request $request)
    {
        $userId                 = $request->input('userId');
        $attendanceId           = $request->input('attendanceId');

        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        if($attendanceId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }
        $attendance                     = Attendance::findOrFail($attendanceId);
        if($attendance == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Attendance not found');
            return response()->json($response, 200);
        }

        $now                            = Carbon::now();
        $attendance->clock_out_time     = $now;
        $attendance->clock_out_ip       = request()->ip();
        $attendance->save();

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,array());
        return response()->json($response, 200);
    }

    public function currentMonthList(Request $request)
    {
        $userId                 = $request->input('userId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        date_default_timezone_set (date_default_timezone_get());
        $endDate            = strtotime (date('Y-m-d'));
        $startDate1         = date ('Y-m-d', strtotime ('first day of this month', $endDate));
        $endDate1           = date('Y-m-d');
        $startDate          = Carbon::createFromFormat('!Y-m-d', $startDate1)->addDay(-1);
        $endDate            = Carbon::createFromFormat('!Y-m-d', $endDate1); //addDay(1) is hack to include end date

        $attendances = Attendance::userAttendanceByDate($startDate, $endDate, $userId);
        $presentDates = $attendances->pluck('clock_in_date');
        $leavesDates = Leave::where('user_id', $userId)
            ->where('leave_date', '>=', $startDate)
            ->where('leave_date', '<=', $endDate)
            ->where('status', 'approved')
            ->select('leave_date')
            ->get();
        $leaves = [];

        foreach ($leavesDates as $leavesDate) {
            array_push($leaves, $leavesDate->leave_date->format('Y-m-d'));
        }

        $data                       = array();
        $data['attendances']        = $attendances;
        $data['startDate']          = $startDate;
        $data['endDate']            = $endDate;
        $data['presentDates']       = $presentDates;
        $global                     = $this->global;
        $leavesDate                 = $leaves;

        $response = array();

        for($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay()){

            $present = 0;
            $attendanceData = '';
            foreach($attendances as $attendance){
                if($attendance->clock_in_date == $date->toDateString()){
                    $present = 1;
                    $attendanceData = $attendance;
                }
            }

            if($present == 1){

                $temp                   = array();
                $temp['date']           = __('app.'.strtolower( $date->format("F") ))." ".$date->format('d, Y')." ".$date->format('l');
                if(in_array($date->format('Y-m-d'), $leavesDate)){
                    $temp['date']       .= " ".__('app.onLeave');
                }else if($attendanceData->late == 'yes'){
                    $temp['date']       .= " ".__('app.late');
                }
                $temp['status']         = __('modules.attendance.present');
                $temp['clock_in']       = $attendanceData->clock_in_time->timezone($global->timezone)->format('h:i A');
                $temp['clock_out']      = (!is_null($attendanceData->clock_out_time))?$attendanceData->clock_out_time->timezone($global->timezone)->format('h:i A'):'';
                $other                  = array();
                $other[__('modules.attendance.clock_in')."IP"] = $attendanceData->clock_in_ip ;
                $other[__('modules.attendance.clock_out')."IP"] = $attendanceData->clock_out_ip;
                $other[__('modules.attendance.working_from')] = $attendanceData->working_from;
                $temp['others']         = $other;
            }
            else{
                $temp                   = array();

                $temp['date']           = __('app.'.strtolower( $date->format("F") ))." ".$date->format('d, Y')." ".$date->format('l');
                if(in_array($date->format('Y-m-d'), $leavesDate)){
                    $temp['date']       .= " ".__('app.onLeave');
                }
                $temp['status']         = __('modules.attendance.absent');
                $temp['clock_in']       = '-';
                $temp['clock_out']      = '-';
                $temp['others']         = '-';
            }
            $response[]             = $temp;
        }

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$response);
        return response()->json($response, 200);
    }

}