<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Api;


use App\User;
use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\getProjectActivities;
use App\Leave;
use App\LeaveType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApiLeavesController extends ApiBaseController{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId                 = $request->input('userId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $user                   = User::findOrFail($userId);
        if(!isset($user->employee[0])){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Employee record against this user is not found');
            return response()->json($response, 200);
        }

        $data = LeaveType::getAllLeaveStats($userId);

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);
    }

    public function applyLeave(Request $request)
    {
        $userId                 = $request->input('userId');
        $leaveTypeId            = $request->input('leaveTypeId');
        $duration               = $request->input('duration');
        $reason                 = $request->input('reason');
        $status                 = 'pending';
        $leaveDate              = $request->input('leaveDate');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        if($leaveTypeId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Leave type not found');
            return response()->json($response, 200);
        }
        if($leaveTypeId > 0){
            $leaveTypeInfo = LeaveType::where('id','=',$leaveTypeId)->first();
            if($leaveTypeInfo == ''){
                $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Leave type not exist');
                return response()->json($response, 200);
            }

        }
        if($duration == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Duration date should not empty');
            return response()->json($response, 200);
        }
        if($reason == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'reason date should not empty');
            return response()->json($response, 200);
        }
        if($leaveDate == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Leave date should not empty');
            return response()->json($response, 200);
        }


        if($duration == 'multiple'){
            $dates = explode(',', $leaveDate);
            foreach($dates as $date){
                $leave                  = new Leave();
                $leave->user_id         = $userId;
                $leave->leave_type_id   = $leaveTypeId;
                $leave->duration        = $duration;
                $leave->leave_date      = Carbon::parse($date)->format('Y-m-d');
                $leave->reason          = $reason;
                $leave->status          = $status;
                $leave->save();
            }

        }
        else{
            $dates = explode(',', $leaveDate);
            $date = $dates[0];
            $leave                          = new Leave();
            $leave->user_id                 = $userId;
            $leave->leave_type_id           = $leaveTypeId;
            $leave->duration                = $duration;
            //$leave->leave_date              = Carbon::parse($leaveDate)->format('Y-m-d');
            $leave->leave_date              = Carbon::parse($date)->format('Y-m-d');
            $leave->reason                  = $reason;
            $leave->status                  = $status;
            $leave->save();
        }
        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,array());
        return response()->json($response, 200);
    }

    public function getLeaveTypes()
    {
        $leaveTypes                         = LeaveType::all();
        $getLeaveTypes                      = $leaveTypes->toArray();;
        $LeaveTypesCount                    = $leaveTypes->count();
        if($LeaveTypesCount <= 0){
            $response               = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
            return response()->json($response, 200);
        }

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$getLeaveTypes);
        return response()->json($response, 200);
    }

    public function leaveDetail(Request $request)
    {
        $userId                 = $request->input('userId');
        $leaveId                = $request->input('leaveId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $user                   = User::findOrFail($userId);
        if(!isset($user->employee[0])){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Employee record against this user is not found');
            return response()->json($response, 200);
        }

        if($leaveId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $leaveDetail               = Leave::where('id','=',$leaveId)->where('user_id','=',$userId)->first();
        if($leaveDetail == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Leave detail not found');
            return response()->json($response, 200);
        }

        $getAllLeaveStats = LeaveType::getAllLeaveStats($userId);
        $leaveDetail->setAttribute('leaveState', $getAllLeaveStats);

        $data = array('leaveDetail'=>$leaveDetail);
        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);


    }


}