<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Api;


use App\User;
use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\getProjectActivities;
use App\EmployeePayslips;
use App\EmployeePayslipLines;


class ApiPaySlipController extends ApiBaseController{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId                 = $request->input('userId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $userPaySlips                         = EmployeePayslips::where('user_id','=', $userId)->get();
        $userPaySlipsCount                    = $userPaySlips->count();
        if($userPaySlipsCount <= 0){

            $response               = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable, array(), "No payslip found");
            return response()->json($response, 200);
        }
        $data = array('payslipList'=>$userPaySlips);
        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
        return response()->json($response, 200);
    }

    public function detail(Request $request)
    {
        $userId                 = $request->input('userId');
        $payslipId                 = $request->input('payslipId');
        if($userId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }
        if($payslipId == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $userInfo               = User::where('id','=',$userId)->first();
        if($userInfo == ''){
            $response           = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

        $userPaySlip                          = EmployeePayslipLines::where('payslip_id','=', $payslipId)->get();
        $getUserPaySlips                      = $userPaySlip->toArray();
        $userPaySlipsCount                    = $userPaySlip->count();
        if($userPaySlipsCount <= 0){
            $response               = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable, array(), "payslip not found");
            return response()->json($response, 200);
        }

        $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$getUserPaySlips);
        return response()->json($response, 200);
    }



}