<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 2:41 PM
 */

namespace App\Http\Controllers\Api;

use App\Helper\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ApiEmployeeController extends ApiBaseController{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId             = $request->input('userId');
        $employeeUserId     = $request->input('employeeUserId');
        if($userId == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }
        if($employeeUserId == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $users = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('employee_details','employee_details.user_id','=','users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('*', 'users.name As name', 'roles.name As role_name')
            ->where('users.id', $employeeUserId)
            ->first();

        if($users != ''){
            $data = array('employeeData'=>$users);
            $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
            return response()->json($response, 200);
        }

        $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
        return response()->json($response, 200);
    }
}