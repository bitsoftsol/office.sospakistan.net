<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/11/2018
 * Time: 9:56 PM
 */

namespace App\Http\Controllers\Api;


use App\EmailNotificationSetting;
use App\Setting;
use App\Traits\FileSystemSettingTrait;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Helper\ApiResponse;


class ApiBaseController extends Controller

{

    use FileSystemSettingTrait;



    /**

     * @var array

     */

    public $data = [];



    /**

     * @param $name

     * @param $value

     */

    public function __set($name, $value)

    {

        $this->data[$name] = $value;

    }



    /**

     * @param $name

     * @return mixed

     */

    public function __get($name)

    {

        return $this->data[$name];

    }



    /**

     * @param $name

     * @return bool

     */

    public function __isset($name)

    {

        return isset($this->data[ $name ]);

    }



    /**

     * UserBaseController constructor.

     */

    public function __construct()

    {

        // Inject currently logged in user object into every view of user dashboard

        $this->odooModel = '';

        $this->totalGaurds = 0;

        $this->global = Setting::first();

        $this->emailSetting = EmailNotificationSetting::all();


        $this->companyName = $this->global->company_name;



        App::setLocale($this->global->locale);

        Carbon::setLocale($this->global->locale);

        setlocale(LC_TIME,$this->global->locale.'_'.strtoupper($this->global->locale));





    }
}

