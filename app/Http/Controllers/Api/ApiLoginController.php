<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/11/2018
 * Time: 10:10 PM
 */

namespace App\Http\Controllers\Api;

use App\Helper\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class ApiLoginController extends ApiBaseController {

    use AuthenticatesUsers;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userName           = $request->input('email');
        $password           = $request->input('password');
        if($userName == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }
        if($password == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        if($this->guard()->attempt($this->credentials($request))){
            $userInfo       = $this->guard()->user();
            $userId         = $userInfo->id;

            $users = User::with('role')
                ->withoutGlobalScope('active')
                ->join('employee_details as empDet','empDet.user_id','users.id')
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.id', 'users.name', 'users.email',
                    'users.created_at',
                    'roles.name as roleName',
                    'roles.id as roleId',
                    'users.image',
                    'users.status',
                    'empDet.employee_erp',
                    'empDet.first_name',
                    'empDet.last_name',
                    'empDet.slack_username',
                    'empDet.mobile_phone',
                    'users.email',
                    'empDet.work_phone',
                    'empDet.address as home_address',
                    'empDet.work_address')
                ->where('roles.name', '<>', 'client')
                ->where('users.email', '=', $userName)
                ->where('users.id', '=', $userId)
                ->groupBy('users.id')
                ->first();

            if($users != ''){
                $users                  = $users->getAttributes();
                $users['isGuard']       = true;
                $users['imagePath']     = ApiResponse::getEmployeeDirPath($userId);
                $data = array('User'=>$users);
                $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
                return response()->json($response, 200);
            }
            else{
                $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
                return response()->json($response, 200);
            }
        }

        $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
        return response()->json($response, 200);
    }
}