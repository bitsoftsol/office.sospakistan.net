<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/12/2018
 * Time: 2:17 PM
 */

namespace App\Http\Controllers\Api;

use App\Helper\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ApiEmployeeDirectoryController extends ApiBaseController{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId             = $request->input('userId');
        if($userId == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }


        $users = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('employee_details','employee_details.user_id','=','users.id')
            ->join('position_levels','employee_details.position_id','=','position_levels.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'employee_details.id As employeeId',  'users.image' ,'users.name', 'users.email', 'users.mobile','position_levels.po_level_name')
            ->get();

        if($users->count() > 0){
            $data = array('employeeDirectory'=>$users);
            $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
            return response()->json($response, 200);
        }

        $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
        return response()->json($response, 200);
    }
}