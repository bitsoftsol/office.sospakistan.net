<?php
/**
 * Created by PhpStorm.
 * User: Qasim Rafique
 * Date: 12/15/2018
 * Time: 12:43 PM
 */
namespace App\Http\Controllers\Api;
use App\EmployeeDetails;
use App\Helper\ApiResponse;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class ApiProfileController extends ApiBaseController{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $userId             = $request->input('userId');
        if($userId == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }

        $users = User::join('employee_details as empDet','empDet.user_id','=','users.id')
            ->select('empDet.first_name',
                'empDet.last_name',
                'empDet.slack_username',
                'empDet.mobile_phone',
                'users.email',
                'empDet.work_phone',
                'empDet.address as home_address',
                'empDet.work_address'
                )
            ->where('users.id', $userId)
            ->first();

        if($users != ''){
            $data = array('ProfileData'=>$users);
            $response               = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data);
            return response()->json($response, 200);
        }

        $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array());
        return response()->json($response, 200);
    }

    public function update(Request $request)
    {
        $userId             = $request->input('userId');
        $firstName          = $request->input('firstName');
        $lastName           = $request->input('lastName');
        $slackUserName      = $request->input('slackUserName');
        $email              = $request->input('email');
        $mobilePhone        = $request->input('mobilePhone');
        $workPhone          = $request->input('workPhone');
        $homeAddress        = $request->input('homeAddress');
        $workAddress        = $request->input('workAddress');
        if($userId == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array());
            return response()->json($response, 200);
        }
        if($firstName == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'First name Require');
            return response()->json($response, 200);
        }
        if($lastName == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Last name Require');
            return response()->json($response, 200);
        }
        if($slackUserName == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Slack UserName Require');
            return response()->json($response, 200);
        }
        if($email == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Email Require');
            return response()->json($response, 200);
        }
        if($mobilePhone == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Mobile Phone Require');
            return response()->json($response, 200);
        }
        if($workPhone == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Work Phone Require');
            return response()->json($response, 200);
        }
        if($homeAddress == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Home Address Require');
            return response()->json($response, 200);
        }
        if($workAddress == ''){
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorInvalidData,array(), 'Work Address Require');
            return response()->json($response, 200);
        }

        $updateUser         = User::findOrFail($userId);
        $firstName          = $request->input('firstName');
        $lastName           = $request->input('lastName');
        $slackUserName      = $request->input('slackUserName');
        $email              = $request->input('email');
        $workPhone          = $request->input('workPhone');
        $mobilePhone        = $request->input('mobilePhone');
        $homeAddress        = $request->input('homeAddress');
        $workAddress        = $request->input('workAddress');

        if($updateUser->count() > 0){
            $updateUser->email      = $email;
            $updateUser->save();

            $employeeInfo           = EmployeeDetails::where('user_id', '=', $userId)->first();

            if(!empty($employeeInfo)){

                $employeeDetailId               = $employeeInfo->id;
                $updateEmployee                 = EmployeeDetails::findOrFail($employeeDetailId);
                $updateEmployee->first_name     = $firstName;
                $updateEmployee->last_name      = $lastName;
                $updateEmployee->slack_username = $slackUserName;
                $updateEmployee->mobile_phone   = $mobilePhone;
                $updateEmployee->work_phone     = $workPhone;
                $updateEmployee->address        = $homeAddress;
                $updateEmployee->work_address   = $workAddress;
                $updateEmployee->save();

                $users                          = User::join('employee_details as empDet','empDet.user_id','=','users.id')
                                                ->select('empDet.first_name',
                                                    'empDet.last_name',
                                                    'empDet.slack_username',
                                                    'empDet.mobile_phone',
                                                    'users.email',
                                                    'empDet.work_phone',
                                                    'empDet.address as home_address',
                                                    'empDet.work_address'
                                                )
                                                ->where('users.id', $userId)
                                                ->first();

                $data                           = array('updated'=>$users);
                $response                       = ApiResponse::createResponse(ApiResponse::ApiSuccess,$data, 'Data Updated');
                return response()->json($response, 200);
            }
            else{
                $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'Employee not found');
                return response()->json($response, 200);
            }
        }
        else{
            $response       = ApiResponse::createResponse(ApiResponse::ApiErrorDataNotAvailable,array(), 'User not found');
            return response()->json($response, 200);
        }

    }
}